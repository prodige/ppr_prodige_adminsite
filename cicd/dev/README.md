# README
<!-- TOC -->

- [README](#readme)
  - [Overview](#overview)
  - [Start development](#start-development)
    - [Change mode dev, prod](#change-mode-dev-prod)
    - [Run unit test](#run-unit-test)
  - [Stop development](#stop-development)
  - [General Docker commands](#general-docker-commands)
  - [Advanced Docker commands](#advanced-docker-commands)
    - [Clean commands](#clean-commands)
    - [Monitoring commands](#monitoring-commands)

<!-- /TOC -->

## Overview

This file gives commands to start development with Docker.

Require:
 - Docker installed.
 - Docker well configured, cf. **/etc/docker/deamon.json**
 - User in docker group to avoid sudo/su right


## stop pr_prodige docker

```bash
docker stop ppr_prodige_adminsite_web
docker rm ppr_prodige_adminsite_web
```

## Start development

Make these commands in this directory `cd ./cicd/dev`.

Run container
```bash
# Login on Alkante docker registry
docker login docker.alkante.com

# Put UID and GID in env file
echo -e "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > docker-compose.env;

# Build dev image
docker-compose build

# Run containers
docker-compose up
```

In an other terminal, connect as www-data to the container
```bash
# Connect to container composer install as www-data
docker exec --user www-data -w /var/www/html/site -it ppr_prodige_adminsite_web bash
```

In container
```bash
# Install with composer
composer install

#Clear cache
php ./bin/console assets:install web
php ./bin/console cache:clear --env dev
php ./bin/console cache:clear --env prod

# Follow symfony logs
tail -f var/logs/dev.log
tail -f var/logs/prod.log
```

The web site urls are:
 - dev: http://localhost:4200/app_dev.php
 - prod: http://localhost:4200


### Change mode dev, prod

Edit the `./site/.env`

```bash
# mode dev
APP_ENV=dev
# mode prod
APP_ENV=prod
```

### Run unit test

```bash
# Run unit test
php bin/phpunit
```

## Stop development

Make these commands in this directory `cd ./cicd/dev`.

```bash
# Shutdown containers
docker-compose down

# Remove dev image
docker rmi ppr_prodige_adminsite_web
```

## General Docker commands

| Description                    | Commands                                                                                      |
| ------------------------------ | --------------------------------------------------------------------------------------------- |
| Display images                 | `docker images`                                                                               |
| Remove images                  | `docker rmi myimages`                                                                         |
| ------------------------------ | --------------------------------------------------------------------------------------------- |
| Display all running containers | `docker ps`                                                                                   |
| Display all containers         | `docker ps -a`                                                                                |
| Remove container               | `docker rm mycontainer`                                                                       |
| Display info container         | `docker inpect mycontainer`                                                                   |
| Display container output       | `docker logs mycontainer`                                                                     |
| Stop container                 | `docker stop mycontainer`                                                                     |
| Kill container                 | `docker kill mycontainer`                                                                     |
| ------------------------------ | --------------------------------------------------------------------------------------------- |
| Connection as root             | `docker exec --user 0 -w / -it ppr_prodige_adminsite_web bash`                                |
| Connection as www-data         | `docker exec --user www-data -w /var/www/html -it ppr_prodige_adminsite_web bash`             |
| Run composer install           | `docker exec --user www-data -w /var/www/html -it ppr_prodige_adminsite_web composer install` |

## Advanced Docker commands


### Clean commands

```bash
# Stop all containers
docker stop $(docker ps -a -q)

# Remove all containers
docker rm $(docker ps -a -q)

# Delete all images with name or tag as "<none>"
docker rmi `docker images| egrep "<none>" |awk '{print $3}'`
```

### Monitoring commands

```bash
# Watch all containers
watch -n 1 "docker ps -a"

# Watch all images
watch -n 1 "docker images"

# Watch all container resources
docker stats
```
