# Build

The build step is a pipeline described in `Jenkinsfile`.

The steps are run in order with Bash scripts:
- [number]_[script_name].sh

If an error occurs, use the `99_clean.sh` to clean everything and revert rights.

## Test the docker prod

```bash
# Login on Alkante docker registry
docker login docker.alkante.com

# Create dockers
docker-compose -f ./cicd/build/docker-build/docker-compose.yml up
```

[URL](http://localhost:4200)

# Stop dockers

```bash
docker-compose -f ./cicd/build/docker-build/docker-compose.yml down
```
