# Changelog

All notable changes to [ppr_prodige_adminsite](https://gitlab.adullact.net/prodige/ppr_prodige_adminsite) project will be documented in this file.

## [4.4.1](https://gitlab.adullact.net/prodige/ppr_prodige_adminsite/compare/4.4.0_rc22...4.4.1) - 2023-11-23

## [4.4.0_rc22](https://gitlab.adullact.net/prodige/ppr_prodige_adminsite/compare/4.4.0_rc21...4.4.0_rc22) - 2023-02-03

## [4.4.0_rc21](https://gitlab.adullact.net/prodige/ppr_prodige_adminsite/compare/4.4.0_rc20...4.4.0_rc21) - 2023-02-03

## [4.4.0_rc20](https://gitlab.adullact.net/prodige/ppr_prodige_adminsite/compare/4.4.0-rc19...4.4.0_rc20) - 2022-12-08

## [4.4.0-rc19](https://gitlab.adullact.net/prodige/ppr_prodige_adminsite/compare/4.4.0-rc18...4.4.0-rc19) - 2022-12-07

## [4.4.0-rc18](https://gitlab.adullact.net/prodige/ppr_prodige_adminsite/compare/4.4.0-rc17...4.4.0-rc18) - 2022-09-02

### Cicd

- fix Dockerfile

## [4.4.0-rc17](https://gitlab.adullact.net/prodige/ppr_prodige_adminsite/compare/4.4.0-rc16...4.4.0-rc17) - 2022-09-02

### Cicd

- minor fix jenkinfile
