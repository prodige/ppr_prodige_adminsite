# Module PRODIGE administration de site

Module d'administration de plateforme PRODIGE, réservé aux administrateurs, permettant la configuration d'une plateforme et l'accès au suivi d'activité.

### configuration

Modifier le fichier site/app/config/global_parameters.yml

### Installation

[documentation d'insatllation en développement](cicd/dev/README.md).

## Accès à l'application

- [Accès au module](https://adminsite.prodige.internal).
