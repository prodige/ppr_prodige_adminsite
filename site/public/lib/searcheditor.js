Ext.ns('Prodige.Adminsite');

Prodige.Adminsite.SearchEditor = Ext.extend(Ext.form.FormPanel, {

  hasSpecificCriteria : false,
  nbCriteres : 0,
  
  criteria : [],
  ar_config_data : [],
  
  labelSeparator : '&nbsp;',

  initComponent : function() {
    this.criteria = [];
    var me = this;
    //this.option = option;
    //console.log('me.ar_config_data is : ', me.ar_config_data);
    this.tablesStore = new Ext.data.JsonStore({
      fields: [{name:'displayField'}, {name:'valueField'}],
      url : Routing.generate('services_get_param_moteur', {
        table: ( this.hasSpecificCriteria ? 'prodige_download_param' : 'prodige_search_param'),
        service: 'get_tables_lists'
      }),
      autoLoad : true
    });

    this.criteriaNames = new Ext.data.JsonStore({
      fields : ['rowIndex', 'criteriaName'],
      data : []
    });


    if ( this.hasSpecificCriteria ){
      this.criteria.push(new Prodige.Adminsite.SpecificCriteria(this));
    }

    for(var i=0; i<this.nbCriteres; i++) {
      this.criteria.push(new Prodige.Adminsite.Criteria(this, i+1));
    }

    me.items = this.criteria;

    me.on('afterrender', function() {

      me.getForm().items.each(function(fieldForm){
        var original = fieldForm.setValue;
        fieldForm.setValue = fieldForm.setValue.createSequence(function() {
          this.fireEvent('change', this);
          this.setValue = original;
        })
      });

      me.load({
        url : Routing.generate('services_get_param_moteur', {
          table: ( this.hasSpecificCriteria ? 'prodige_download_param' : 'prodige_search_param'),
          service: 'get_config_data'
        }),
        method: 'GET'
      });
    })
    Prodige.Adminsite.SearchEditor.superclass.initComponent.call(this);
  },

  setCriteriaName : function(rowIndex, name) {
    if ( String(name).trim()=="" ) name = null;

    var existing = this.criteriaNames.find('rowIndex', rowIndex);

    if(existing != -1 ) {
      existing = this.criteriaNames.getAt(existing);
      if(name === null) {
        this.criteriaNames.remove(existing);
      } else {
        existing.set('criteriaName', 'n°'+rowIndex+' - '+name);
      }
    } else if(name !== null) {
        this.criteriaNames.loadData([{rowIndex : rowIndex, criteriaName : 'n°'+rowIndex+' - '+name}], true);
    }
    var index = parseInt(rowIndex)+1;
    var next = Ext.getCmp('criteria_'+index++);
    if ( next ) {
      if ( name === null ){
    		next.reset()
      }
      next.setDisabled(name === null);
    }
  },

  getCriteriaByRank : function(rowIndex) {
    return this.criteria[rowIndex];
  }
});

Prodige.Adminsite.SpecificCriteria = Ext.extend(Ext.form.FieldSet, {
  formPanel : null,
  constructor : function(formPanel, config) {
    var me = this;
    this.formPanel = formPanel;

    this.critersStore = new Ext.data.JsonStore({
      fields : ['fieldName', 'valueName'],
      data: [
             {fieldName : 'Aucun'     , valueName: '0'},
             {fieldName : 'critère 1' , valueName: '1'},
             {fieldName : 'critère 2' , valueName: '2'},
             {fieldName : 'critère 3' , valueName: '3'},
             {fieldName : 'critère 4' , valueName: '4'}
            ]
    });

    config = config || {};

    Ext.apply(config, {
      labelWidth : 350,
      labelSeparator : '&nbsp;'
    });
    config.items = [{
      id:'_extraction_attributaire_cb_id',
      name: '_extraction_attributaire_cb_name',
      hiddenName: 'extraction_attributaire_cb_name',
      xtype: 'checkbox',
      fieldLabel:'Extraction des données basée sur une requête attributaire ?'
    }, {
      id : '_extraction_attributaire_select_id',
      name: '_extraction_attributaire_select_name',
      xtype : "combo",
      fieldLabel : "Extraction basée sur le critère",
      hiddenName: 'extraction_attributaire_select_name',
      typeAhead: true,
      triggerAction: 'all',
      lazyRender:true,
      autoLoadOnValue: true,
      autoSelect: true,
      allowBlank: true,
      editable: false,
      store: this.critersStore,
      mode : 'local',
      valueField : 'valueName',
      displayField : 'fieldName'
    }];
    config.border = false;
    config.xtype = 'fieldset';
    Prodige.Adminsite.SpecificCriteria.superclass.constructor.call(this, config);
  }
})

Prodige.Adminsite.Criteria = Ext.extend(Ext.form.FieldSet, {
  labelSeparator : '&nbsp;',
  formPanel : null,
  rowIndex : null,
  constructor : function(formPanel, rowIndex, config) {
    var me = this;
    this.formPanel = formPanel;
    this.rowIndex = rowIndex;
    me.id = 'criteria_'+me.rowIndex;
    me.disabled = me.rowIndex>1;

    this.fieldsStore = new Ext.data.JsonStore({
      fields : ['fieldName'],
      autoLoad : false
    });

    this.relatedCriteria = new Ext.data.JsonStore({
      fields : ['rowIndex', 'criteriaName'],
      autoLoad : false
    });

    var updateRelated = function(criteriaNames) {
      me.relatedCriteria.loadData(
        Ext.pluck(criteriaNames.queryBy(
          function(record){ return record.get('rowIndex') < me.rowIndex; }
        ).items, 'data')
      , false );
    };

    this.formPanel.criteriaNames.on('add', updateRelated);
    this.formPanel.criteriaNames.on('remove', updateRelated);
    this.formPanel.criteriaNames.on('update', updateRelated);

    config = config || {};

    Ext.apply(config, {
    	labelWidth : 120,
      labelSeparator : '&nbsp;',
      defaults : {labelSeparator : '&nbsp;'}
    });
    
    config.layout = 'table';
    config.layoutConfig = {
      // The total column count must be specified here
      columns: 2,
      tableAttrs : {style:{width:'100%'}}
    };

    var suffixName = '[' + this.rowIndex + ']';
    var suffixId = '_' + this.rowIndex;

    config.title = 'Critère '+this.rowIndex;
    config.items = [{
      layout : 'form',
      fieldLabel: "Nom",
      id: 'critere_moteur_nom' + suffixId,
      name: 'critere_moteur_nom' + suffixName,
      xtype: 'textfield',
      listeners : {
        change : function(){
          me.formPanel.setCriteriaName(me.rowIndex, this.getValue())
          Ext.getCmp('_critere_moteur_table' + suffixId).allowBlank     = !(this.getValue());
          Ext.getCmp('_critere_moteur_champ_id' + suffixId).allowBlank  = !(this.getValue());
          Ext.getCmp('_critere_moteur_champ_nom' + suffixId).allowBlank = !(this.getValue());
        }
      }
    }, {
      fieldLabel: "Table",
      id: '_critere_moteur_table' + suffixId,
      name: '_critere_moteur_table' + suffixName,
      hiddenName: 'critere_moteur_table' + suffixName,
      xtype: 'combo',
      typeAhead: true,
      triggerAction: 'all',
      lazyRender:true,
      store : this.formPanel.tablesStore,
      mode: 'local',
      forceSelection : true,
      displayField:'displayField',
      valueField:  'valueField',
      listeners:{
        change: function(combo, newValue, oldValue) {
          me.loadFieldsByLayer(combo.getValue());
          Ext.getCmp('critere_moteur_nom' + suffixId).allowBlank        = !(combo.getValue());
          Ext.getCmp('_critere_moteur_champ_id' + suffixId).allowBlank  = !(combo.getValue());
          Ext.getCmp('_critere_moteur_champ_nom' + suffixId).allowBlank = !(combo.getValue());
        }
      }
    }, {
      fieldLabel: "Champ Identifiant",
      id: '_critere_moteur_champ_id' + suffixId,
      name: '_critere_moteur_champ_id' + suffixName,
      hiddenName: 'critere_moteur_champ_id' + suffixName,
      xtype: 'combo',
      typeAhead: true,
      triggerAction: 'all',
      lazyRender:true,
      store: this.fieldsStore,
      mode: 'local',
      editable: false,
      displayField:'fieldName',
      valueField:  'fieldName'
    }, {
      fieldLabel: "Champ Nom",
      id: '_critere_moteur_champ_nom' + suffixId,
      name: '_critere_moteur_champ_nom' + suffixName,
      hiddenName: 'critere_moteur_champ_nom' + suffixName,
      xtype: 'combo',
      typeAhead: true,
      triggerAction: 'all',
      lazyRender:true,
      store: this.fieldsStore,
      mode: 'local',
      editable: false,
      displayField:'fieldName',
      valueField:  'fieldName'
    }];
    if (rowIndex>1){
    	config.items = config.items.concat([
    		{ xtype:'displayfield', hideLabel:true, value:"<b>Création d'une liaison avec un critère précédent</b>", colspan:2},
        {
          fieldLabel: "Critère lié au critère",
          id: '_critere_moteur_id_join' + suffixId,
          name: '_critere_moteur_id_join'+suffixName,
          hiddenName: 'critere_moteur_id_join' + suffixName,
          xtype: 'combo',
          typeAhead: true,
          triggerAction: 'all',
          lazyRender:true,
          store: this.relatedCriteria, //TODO me.storeRelated
          mode: 'local',
          autoLoadOnValue: true,
          autoSelect: true,
          allowBlank: true,
          editable: false,
          displayField:'criteriaName',
          valueField:  'rowIndex',
          listeners: {
            change: function(combo, rowIndex) {
              Ext.getCmp('_critere_moteur_champ_join'+suffixId).allowBlank = !(combo.getValue());
            }
          }
        }, {
          fieldLabel: "Champ de liaison",
          id: '_critere_moteur_champ_join' + suffixId,
          name: '_critere_moteur_champ_join' + suffixName,
          hiddenName: 'critere_moteur_champ_join' + suffixName,
          xtype: 'combo',
          typeAhead: true,
          triggerAction: 'all',
          lazyRender:true,
          store: this.fieldsStore,
          mode: 'local',
          editable: false,
          displayField:'fieldName',
          valueField:  'fieldName'
        }]
      );
    }

    var temp = [];
    for(var i=0; i<config.items.length; i++) {
      temp.push({
        xtype:'panel', 
        layout:'form', 
        items:[config.items[i]], 
        border : false, 
        colspan:(config.items[i].colspan || 1)
      });
    }
    config.items = temp;
    
    config.buttons = [{
      xtype : 'button',
      text : 'Effacer',
      handler : function(){
        Ext.Msg.confirm(
          'Suppression du critère n°'+me.rowIndex, 
          'Confirmez-vous la suppression du critère n°'+me.rowIndex+' et des critères suivants ?', 
          function(btn){btn=='yes' && me.reset();}
        )
      }
    }];
    config.xtype = 'fieldset';

    Prodige.Adminsite.Criteria.superclass.constructor.call(this, config);
  },

  reset : function(){
  	var me = this;
  	var suffixId = "_"+me.rowIndex;
    var cIndex = me.formPanel.criteriaNames.find('criteria_name', Ext.getCmp('critere_moteur_nom'+suffixId).getValue());
    var cRecord = me.formPanel.criteriaNames.getAt(cIndex);
    Ext.query('[id^=_critere_moteur_id_join]').forEach(function(el){
      var cmpTable = Ext.getCmp(el.id), cmpField = Ext.getCmp(el.id.replace('critere_moteur_id_join', 'critere_moteur_champ_join'));
      if ( cmpTable && cmpField ){
        if ( cmpTable.getValue()==me.rowIndex ){
          cmpTable.setValue(null);
          cmpField.allowBlank = true;
          cmpField.setValue(null);
        }
      }
    })
    me.formPanel.setCriteriaName(me.rowIndex, null)
    me.cascade(function(item){
      if (item instanceof Ext.form.Field && !(item instanceof Ext.form.DisplayField) && !(item instanceof Ext.form.Hidden)){
        item.allowBlank = true;
        item.setValue(null);
      }
    })
  },

  loadFieldsByLayer : function(layerName) {
    if ( !layerName ){
      this.fieldsStore.removeAll();
      return;
    }
    this.fieldsStore.proxy = new Ext.data.HttpProxy({
      url : Routing.generate('services_get_param_moteur', {
        table: layerName,
        service: 'get_layers_fields'
      })
    });

    this.fieldsStore.load({
      append : false
    });
  }

})
