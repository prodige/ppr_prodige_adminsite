Ext.override(Ext.form.ComboBox, {
  getSelection :  function(){
  	var value = this.getValue(),
  	    index = Math.max(this.getStore().findExact(this.valueField, value), this.getStore().findExact(this.displayField, value));
  	if (index!=-1)
      return this.getStore().getAt(index);
    return null;
  }
});

Prodige.Adminsite.WMTSEditor = Ext.extend(Ext.TabPanel, {
  autoTabs : true,
  activeTab : 0,
  deferredRender : false,
  border : false,
  
  cacheGridStore : null,
	
	initComponent : function(){
    this.defaultParamsStore = new Ext.data.JsonStore({
      url : SERVICE_WMTS_EXECUTE_SERVICE+'/getDefaultParams',
    	fields : ['prodige_wmts_settings_constant', 'prodige_wmts_settings_title', 'prodige_wmts_settings_value', 'prodige_wmts_settings_desc', 'prodige_wmts_settings_type'],
      autoLoad : true
    });
		this.cacheGridStore = new Ext.data.JsonStore({
      url : SERVICE_WMTS_EXECUTE_SERVICE+'/getCacheGridList',
      fields : [
        'id', 'title', 'srs', 'extent', 'resolutions', {name:'size', defaultValue:'256 256'}, {name:'units', defaultValue:'m'}, 
      // calculated fields
        {name : 'width', mapping:'size', convert:function(value){return value[0];}, defaultValue:256},
        {name : 'height', mapping:'size', convert:function(value){return value[1];}, defaultValue:256},
        {name : 'minx', mapping:'extent', convert:function(value){return value[0];}},
        {name : 'miny', mapping:'extent', convert:function(value){return value[1];}},
        {name : 'maxx', mapping:'extent', convert:function(value){return value[2];}},
        {name : 'maxy', mapping:'extent', convert:function(value){return value[3];}},
        {name : 'projection', mapping:'srs', convert:function(value){return value.split(':').pop();}}
      ],
      autoLoad : true
    });
		this.items = [];
    this.items.push(this.getLayersList());
    this.items.push(this.getCacheGridList());
    
    Prodige.Adminsite.WMTSEditor.superclass.initComponent.apply(this);
  },
  
  getCacheGridList : function(){
    var me = this;
    var grid;
    grid = new Ext.grid.EditorGridPanel({
      loadMask : true,
      title : 'Définition des grilles',
      autoScroll : true,
      viewConfig : {
        forceFit : true,
        scrollOffset : 20,
        emptyText : 'Aucune définition de grille de tuiles',
        deferEmptyText : false,
        forceFit : true
      },
      frame : true,
      id : "cachegrids",
      height : 300,
      store : me.cacheGridStore,
      columns : [{
        header : "Nom",
        width : 200,
        dataIndex : 'title',
        sortable : true
      }, {
        header : "Projection",
        width : 100,
        dataIndex : 'srs',
        sortable : true
      }, {
        header : "Emprise",
        width : 100,
        dataIndex : 'extent',
        sortable : false,
        renderer : function(value){
          if ( !Ext.isArray(value) ) {
            if ( Ext.isEmpty(value) ) return "";
            value = [value]; 
          }
          var convert = [];
          Ext.each(value, function(item, index){
            convert[index] = parseInt(item);
          })
          return [convert.slice(0,2).join(' '), convert.slice(2,4).join(' ')].join('<br/>');
        }
      }, {
        header : "Echelles",
        width : 150,
        dataIndex : 'resolutions',
        sortable : false,
        renderer : function(value, meta){
          if ( !Ext.isArray(value) ) {
            if ( Ext.isEmpty(value) ) return "";
            value = [value]; 
          }
          var convert = [];
          var qtip = [];
          Ext.each(value, function(item, index){
            convert[index] = Math.round(parseFloat(item) / SCALE_TO_RESOLUTION);
            qtip[index] = '<tr><td style="padding:0px 5px">'+convert[index]+'</td><td style="padding:0px 5px">'+item+'</td></tr>';
          });
          qtip = '<table style="font-size:12px;border:1px solid lightgray;border-collapse:collapse;" border="1" >' +
          		'<tr><th>Echelle</th><th>Résolution</th></tr>' +
          		qtip.join('')+
          		'</table>';
          qtip = qtip.replace(/"/g, "&quot;");
          if ( convert.length>1 ){
            return '<div qtip="'+qtip+'">Du 1/'+Ext.max(convert)+' au 1/'+Ext.min(convert)+'<br/>('+convert.length+' résolutions)</div>';
          }
          if ( convert.length==1 ){
          	return '<div qtip="'+qtip+'">1/'+convert[0]+'</div>';
          }
          return "";
        }
      }],
      
      sm : new Ext.grid.RowSelectionModel({ 
        singleSelect : true, 
        listeners : {
          rowselect : function(){
            grid.getTopToolbar().cascade(function(button){
              if ( button.toggleable ){
                button.setDisabled(false);
              }
            });
          },
          rowdeselect : function(){
            grid.getTopToolbar().cascade(function(button){
              if ( button.toggleable ){
                button.setDisabled(true);
              }
            });
          }
        }
      }),
      listeners : {
        rowdblclick : function(grid, rowIndex){
          var record = grid.getStore().getAt(rowIndex);
          if ( !record ) return;
          me.openCacheGridEditor(record);
        }
      },
      tbar : [{
        text : 'Ajouter',
        handler : function() {
          me.openCacheGridEditor(null);
        }
      }, {
        text : 'Modifier',
        disabled : true,
        toggleable : true,
        handler : function() {
          var sm = grid.getSelectionModel();
          if (sm.hasSelection()) {
            var record = sm.getSelected();
            me.openCacheGridEditor(record);
          }
        }
      }, {
        text : 'Supprimer',
        disabled : true,
        toggleable : true,
        handler : function() {
          var sm = grid.getSelectionModel();
          var record = sm.getSelected();
          if (sm.hasSelection()) {
            Ext.Msg.show({
              title : "Suppression d'une grille",
              buttons : Ext.MessageBox.YESNO,
              msg : 'Confirmez-vous la suppression de la grille "' + record.get('title') + '" ?',
              fn : function(btn) {
                if (btn == 'yes') {
                  Ext.Ajax.request({
                    url : SERVICE_WMTS_EXECUTE_SERVICE+'/deleteCacheGrid',
                    method : 'GET',
                    params : {
                      id : record.get('id')
                    },
                    success : function(response) {
                      response = Ext.decode(response.responseText);
                      if ( response.success )
                        grid.getStore().remove(record);
                      else if ( response.message )
                        Ext.Msg.alert("Erreur : Suppression d'une grille", 'La suppression de la grille "' + record.get('title') + '" a échoué :<br/>'+response.message);
                    },
                    failure : function(response) {
                    	Ext.Msg.alert("Erreur : Suppression d'une grille", 'La suppression de la grille "' + record.get('title') + '" a échoué');
                    }
                  });
                }
              }
            });
          };
        }
      }]
    });
    
    return grid;
	},
	
	getLayersList : function(){
		var me = this;
		var grid;
		grid = new Ext.grid.EditorGridPanel({
      loadMask : true,
      title : 'Couches tuilées',
      autoScroll : true,
      viewConfig : {
        forceFit : true,
        scrollOffset : 20,
        emptyText : 'Aucune couche tuilée',
        deferEmptyText : false,
        forceFit : true
      },
      frame : true,
      id : "layers",
      height : 300,
      store : new Ext.data.JsonStore({
        url : SERVICE_WMTS_EXECUTE_SERVICE+'/getLayersList',
        fields : ['id', 'title', 'grid', 'format', 'status', 'time', 'mode', 'wmts'],
        autoLoad : true
      }),
      columns : [{
        header : "Couche",
        width : 250,
        dataIndex : 'title',
        sortable : true,
        renderer : function(value){
          return '<span title="'+value+'" style="text-overflow:ellipsis">'+value+'</span>';
        }
      }, {
        header : "Grille",
        width : 150,
        dataIndex : 'grid',
        sortable : true,
        renderer : function(value, meta, record){
          var index = me.cacheGridStore.findExact("id", value);
          if ( index!=-1 ){
            value = me.cacheGridStore.getAt(index).get('title');
          }
          meta.title = value;
          meta.qtip = value;
          return '<span title="'+value+'" style="text-overflow:ellipsis">'+value+'</span>';
        }
      }, {
        header : "Génération",
        align : 'center',
        width : 70,
        dataIndex : 'mode',
        sortable : true,
        renderer : function(value, meta, record){
          switch (value){
            case "direct" : return "A la volée";
            case "defer" : return "Planifiée à "+record.get("time");
            default : return value;
          }
        }
      }, {
        header : "Cache",
        align : 'center',
        width : 50,
        dataIndex : 'status',
        sortable : true,
        renderer : function(value, meta, record){
          if ( record.get('mode')=="direct" ) return "";
          switch (value){
            case "todo" : return "A générer";
            case "done" : return "Généré";
            default : return value;
          }
        }
      }],
      
      sm : new Ext.grid.RowSelectionModel({ 
        singleSelect : true, 
        listeners : {
          rowselect : function(){
            grid.getTopToolbar().cascade(function(button){
              if ( button.toggleable ){
                button.setDisabled(false);
              }
            });
          },
          rowdeselect : function(){
            grid.getTopToolbar().cascade(function(button){
              if ( button.toggleable ){
                button.setDisabled(true);
              }
            });
          }
        }
      }),
      listeners : {
        rowdblclick : function(grid, rowIndex){
          var record = grid.getStore().getAt(rowIndex);
          if ( !record ) return;
          me.openLayerWMTSEditor(record);
        }
      },
      tbar : [{
        text : 'Ajouter',
        handler : function() {
          me.openLayerWMTSEditor(null);
        }
      }, {
        text : 'Modifier',
        disabled : true,
        toggleable : true,
        handler : function() {
          var sm = grid.getSelectionModel();
          var sel = sm.getSelected();
          if (sm.hasSelection()) {
            me.openLayerWMTSEditor(sel);
          }
        }
      }, {
        text : 'Supprimer',
        disabled : true,
        toggleable : true,
        handler : function() {
          var sm = grid.getSelectionModel();
          var sel = sm.getSelected();
          if (sm.hasSelection()) {
            Ext.Msg.show({
              title : 'Suppression WMTS',
              buttons : Ext.MessageBox.YESNO,
              msg : 'Supprimer "' + sel.data.title + '" ?',
              fn : function(btn) {
                if (btn == 'yes') {
                  Ext.Ajax.request({
                  	timeout : 30*1000*20,
                    url : SERVICE_WMTS_EXECUTE_SERVICE+'/deleteLayer',
                    method : 'GET',
                    params : {
                      tilesetId : sel.data.id
                    },
                    success : function(response) {
                      grid.getStore().remove(sel);
                    }
                  });
                }
              }
            });
          };
        }
      }]
    });
    
    this.cacheGridStore.on('load', function(){grid.getView().refresh()})
    return grid;
	},
	
	
  openLayerWMTSEditor : function (layer) {
  	var layerId = (layer ? layer.get('id') : -1);
    var me = this;
    var win;
    if ( !this.defaultParamsStore.lastOptions ){
      this.defaultParamsStore.on('load', function(){
        me.onSuccessLayerWMTSEditor(layer);
      });
    } else {
      me.onSuccessLayerWMTSEditor(layer);
    }
  },
  
  onSuccessLayerWMTSEditor : function (layer) {
    var win;

    var layerId = (layer ? layer.get('id') : -1);
    var me = this;
    var tabItems = [];
    var storeTableList = new Ext.data.JsonStore({
    	scriptTag : true,
      url :  SERVICE_WMTS_EXECUTE_SERVICE+'/getCouchesList',
      fields : ['path', 'id', 'name', 'type', 'fmeta_id', 'uuid'],
      storeId : 'path',
      idProperty : 'id',
      autoLoad : true
    });
    var layerCombo = new Ext.form.ComboBox({
      //readOnly : (!!layer),
      //hideTrigger : (!!layer),
      name : "id",
      hiddenName : "layer[id]",
      submitValue : true,
      
      value : (layer ? layer.get('id') : null),
      fieldLabel : "Couches disponibles",
      //typeAhead : false,
      editable : false,
      allowBlank : false,
      forceSelection : true,
      width : 300,
      autoLoad : true,
      triggerAction : 'all',
      mode : 'local',
      store : storeTableList,
      displayField : 'name',
      valueField : 'id',
      listeners : {
        'change' : {
          fn : function(combo, newVal, oldVal) {
            var record = combo.getSelection();
            if ( !record ) return;
            var hiddenLayer;
            
            hiddenLayer = Ext.getCmp('layerPath');
            hiddenLayer && hiddenLayer.setValue(record.get('path'));
            
            hiddenLayer = Ext.getCmp('layerTitle');
            hiddenLayer && hiddenLayer.setValue(record.get('name'));
          },
          scope : this
        },
        'select' : {
          fn : function(field, newVal, oldVal) {
            Ext.getCmp('wmts_status_combo').focus().blur()
          },
          scope : this
        }
      }
    });
    tabItems.push(layerCombo);
    tabItems.push({
      xtype : 'hidden',
      name : 'layer[path]',
      id : 'layerPath'
    });
    tabItems.push({
      xtype : 'hidden',
      name : 'layer[title]',
      id : 'layerTitle'
    });
    
    
    storeTableList.on('load', function(){
    	if ( !layer ) return;
      layerCombo.setValue(layer.get('id'));
      layerCombo.fireEvent('change', layerCombo, layer.get('id'));
    })
    


    tabItems.push({
      xtype : 'compositefield',
      fieldLabel : "Grille de tuilage",
      name : "layer[grid]",
      getValue : function(){
      	return Ext.getCmp('grille').getValue();
      },
      items : [
        new Ext.form.ComboBox({
          id : "grille",
          name : "grid",
          hiddenName : "layer[grid]",
          allowBlank : false,
          forceSelection : true,
          typeAhead : false,
          width : 300,
          triggerAction : 'all',
          mode : 'local',
          store : this.cacheGridStore,
          submitValue : true,
          editable : false,
          value : (layer ? layer.get('grid') : null),
          valueField : 'id',
          displayField : 'title'
        }),
        {
        	xtype:'button',
        	text : 'Détails...',
        	hideLabel : true,
        	handler : function(){
        		var combo = this.previousSibling();
        		if ( !combo ) return;
        		if ( !combo.getValue() ) return;
        		var selection = combo.getSelection();
        		if ( !selection ) return;
        		me.openCacheGridEditor(selection, true);
        	}
        }
      ]
    });
  
    tabItems.push(new Ext.form.ComboBox({
      id : "format",
      name : "format",
      hiddenName : "layer[format]",
      
      allowBlank : false,
      fieldLabel : "Format de l'image",
      typeAhead : false,
      width : 300,
      triggerAction : 'all',
      mode : 'local',
      store : new Ext.data.ArrayStore({
        fields : ['keyFormat', 'name'],
        data : [["PNG", "PNG (image/png)"], ["JPEG", "JPEG (image/jpeg)"]]
      }),
      submitValue : false,
      editable : false,
      value : (layer ? layer.get('format') : 'PNG'),
      valueField : 'keyFormat',
      displayField : 'name'
    }));
    tabItems.push(new Ext.form.ComboBox({
      id : "mode",
      name : "mode",
      hiddenName : "layer[mode]",
      fieldLabel : "Génération",
      typeAhead : false,
      width : 300,
      triggerAction : 'all',
      mode : 'local',
      store : new Ext.data.ArrayStore({
        fields : ['mode', 'name'],
        data : [["direct", "A la volée : Au fur et à mesure de la consultation"], ["defer", "Mise en cache : Génération totale en différé"]]
      }),
      submitValue : true,
      editable : false,
      value : (layer ? layer.get('mode') : "direct"),
      valueField : 'mode',
      displayField : 'name',
      listeners : {
        'select' : {
          fn : function(field, newVal, oldVal) {
            var timeField = Ext.getCmp('generationTime');
            if (newVal.data.mode == "defer") {
              timeField.setDisabled(false);
            } else {
              timeField.setDisabled(true);
            }
          },
          scope : this,
          delay : 100
        }
      }
    }));
    tabItems.push(new Ext.form.TimeField({
      disabled : (layer ? layer.get('mode')=="direct" : true),
      triggerClass : 'x-form-time-trigger',
      fieldLabel : "Heure de la génération",
      name : "time",
      hiddenName : "layer[time]",
      id : "generationTime",
      editable : true,
      format : "H:i",
      value : (layer ? layer.get('time') : null),
      minValue : '00:00',
      maxValue : '23:30'
    }));
    
    var wmts_status;
    wmts_status = new Ext.form.ComboBox({
      hideTrigger : true,
      store : new Ext.data.JsonStore({
        fields : ['status', 'status_label'],
        data : [{
          status : 'todo', status_label : 'A paramétrer obligatoirement'
        }, {
          status : 'done', status_label : 'Représentation existante'
        }]
      }),
      valueField : 'status', displayField : 'status_label',
      validator : function(value){
        var record = wmts_status.getSelection();
        value = record.get(wmts_status.valueField);
      	var msg = 'La représentation de la couche est à faire obligatoirement.'
        var combobox = layerCombo;
        var v = combobox.getValue();
        record = combobox.getSelection();
        
        if ( record && v && !wmts_status.requestChange ){
        	Ext.Ajax.request({
        		url : SERVICE_WMTS_EXECUTE_SERVICE+'/hasMap',
        		params : {
              id : record.get("id"),
              path : record.get("path"),
        			type : record.get("type")
        		},
        		async : false,
        		success : function(response){
        			response = Ext.decode(response.responseText);
        			wmts_status.requestChange = true;
        			wmts_status.setValue(response.status);
        			Ext.getCmp('layer_wmts').setValue(response.wmts);
              Ext.getCmp('layer_msname').setValue(response.msname);
        		}
        	});
        }
        wmts_status.requestChange = false;
        return value=="done" ? true : msg;
      },
      name : "_wmts_status",
      hiddenName : "layer[wmts_status]",
      id : "wmts_status_combo",
      value : (layer && layer.get('wmts') ? "done" : "todo")
    });
    
    
    tabItems.push({
      fieldLabel : "Représentation de la couche",
      preventMark : true,
    	xtype:'compositefield',
      items : [{
        xtype : 'hidden',
        id : 'layer_wmts',
        name : 'layer[wmts]',
        value : (layer ? layer.get('wmts') : '')
      }, {
        xtype : 'hidden',
        id : 'layer_msname',
        name : 'layer[msname]',
        value : (layer ? layer.get('id') : '')
      }, 
      wmts_status, 
      {
      	xtype : 'button',
        text : 'Représentation par défaut',
        handler : function(btn) {
          var combobox = layerCombo;
          var v = combobox.getValue();
          var record = combobox.getSelection();
          if ( !record ) return; 
          
          var mapfile = record.get('id');
          if ( !mapfile ){
            var mapfile = record.get('path');
            if (record.get('type') == 0) {
              mapfile = mapfile.split("/");
              mapfile = mapfile[mapfile.length - 1];
              mapfile = mapfile.split(".")[0];
            }
          } else {
          	mapfile = 'wmts_'+mapfile;
          }
          if ( !mapfile ) return;
          
          var mask = new Ext.LoadMask(win.id, {msg:'Génération du fichier mapfile'});
          var text = btn.text;
          btn.setText('Génération de la carte');
          btn.setDisabled(true);
          Ext.Ajax.request({
          	url : SERVICE_WMTS_EXECUTE_SERVICE+'/openMap',
          	params : {
          		uuid : record.get('uuid'),
          		mapfile : mapfile
          	},
          	success : function(response){
          		btn.setText(text);
          		btn.setDisabled(false);
          		response = Ext.decode(response.responseText);
          		if ( !response.success ){
          			return;
          		}
          		var url = response.edit_url;
//          		Ext.iterate(response.data, function(key, value){
//          			
//          		})
          		if ( !window.open(url) ){
          			Ext.Msg.alert('Fenêtre pop-up bloqué', "Votre navigateur a bloqué l'ouverture de la fenêtre d'édition de la représentation par défaut.<br><a href='"+url+"' target='_blank'>Cliquez sur le lien suivant pour ouvrir la fenêtre</a>").setIcon(Ext.Msg.WARNING);
          		}
          		mask.hide();
          	}
          });
        }
      }]
    });
    
    var tabButtons = [{
    	id : 'validateLayer',
      text : 'Valider',
      disabled : true,
      handler : function() {
        if (Ext.getCmp("formParametrage").form.isValid()) {
          Ext.getCmp("formParametrage").form.submit({
            clientValidation : true,
            success : function(form, action) {
              win.close();
              Ext.getCmp("layers").getStore().load();
            },
            failure : function(form, action) {
              switch (action.failureType) {
                case Ext.form.Action.CLIENT_INVALID :
                  Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                break;
                case Ext.form.Action.CONNECT_FAILURE :
                  Ext.Msg.alert('Failure', 'Ajax communication failed');
                break;
                case Ext.form.Action.SERVER_INVALID :
                default :
                  Ext.Msg.alert('Failure', action.result.msg);
                break;
              }
            }
          });
        } else {
          Ext.MessageBox.alert('Erreur', 'Tous les champs ne sont pas remplis.');
        }
      }
    }, {
      text : 'Fermer',
      handler : function() {
        win.close();
      }
    }];
    storeTableList.on('load', function(){var cmp = Ext.getCmp('validateLayer'); cmp && cmp.setDisabled(false);});
  
    var constantForm = new Ext.FormPanel({
      url : SERVICE_WMTS_EXECUTE_SERVICE+'/updateLayer',
      frame : true,
      border : false,
      id : 'formParametrage',
      autoHeight : true,
      defaults : {forceSelection : true, defaults : {forceSelection : true}},
      labelWidth : 150,
      items : tabItems,
      listeners : {
        afterrender : function(){
          if ( layer ) this.getForm().loadRecord(layer);
          (function(){var cmp = Ext.getCmp('validateLayer'); cmp && cmp.setDisabled(false);}).defer(10000);
        }
      }
    });
    win = new Ext.Window({
      resizable : true,
      layout : 'anchor',
      constrain : true,
      autoScroll : true,
      width : 600,
      modal : false,
      autoDestroy : true,
      shadow : false,
      title : "Ajout d'une couche WMTS",
      buttons : tabButtons,
      items : [constantForm]
    });
    win.show();
  },


  openCacheGridEditor : function (gridRecord, readOnly) {
    var me = this;
    var win;
    if ( !this.defaultParamsStore.lastOptions ){
      this.defaultParamsStore.on('load', function(){
        me.onSuccessCacheGridEditor(gridRecord, readOnly);
      });
    } else {
      me.onSuccessCacheGridEditor(gridRecord, readOnly);
    }
    return;
  },
 

  onSuccessCacheGridEditor : function (gridRecord, readOnly) {
    var me = this;
    
    var tabConstants = Ext.pluck(this.defaultParamsStore.getRange(), 'data');
    var tabItems = [];
    var hiddenGridValue = new Ext.form.Hidden({
      colspan : 2,
      name : 'id'
    });
    tabItems.push(hiddenGridValue);
    tabItems.push({
    	xtype : 'panel',
    	colspan : 2,
    	layout : 'form',
    	items : [{
      	xtype : 'textfield',
      	name : 'title',
      	fieldLabel : 'Nom de la grille'
    	}]
    });
    var temp = [];
    for (var i = 0; i < tabConstants.length; i++) {
    	if ( tabConstants[i]['prodige_wmts_settings_constant']=="PRO_WMTS_SCALE" ){
    		temp[0] = tabConstants[i];
    	}
      else if ( tabConstants[i]['prodige_wmts_settings_constant']=="PRO_WMTS_PROJECTION" ){
        temp[1] = tabConstants[i];
      }
      else if ( tabConstants[i]['prodige_wmts_settings_constant']=="PRO_WMTS_EXTENT" ){
        temp[2] = tabConstants[i];
      }
      else {
      	temp.push(tabConstants[i]);
      }
    }
    tabConstants = temp;
    
    for (var i = 0; i < tabConstants.length; i++) {
      if (tabConstants[i]['prodige_wmts_settings_type'] == "editableGrid") {
      	var title = new Ext.XTemplate('Echelles et résolutions ({0})');
      	var setGridTitle;
        var store = new Ext.data.JsonStore({ 
          fields : ['scale', 'resolution'], 
          idProperty : 'scale',
          listeners : {
          	datachanged : (setGridTitle = function(store){
          		var grid = Ext.getCmp('resolutions_cachegrid');
          		grid && grid.setTitle(title.apply([Ext.util.Format.plural(store.getCount(), ' résolution', ' résolutions')]));
          	}),
            remove : setGridTitle,
            add : setGridTitle,
            update : setGridTitle,
            clear : setGridTitle,
            load : setGridTitle
          }
        });
        var data = [];
        var scalesArray = tabConstants[i]['prodige_wmts_settings_value'].split(";");
        for (var j = 0; j < scalesArray.length; j++) {
          data.push({scale:scalesArray[j], resolution: scalesArray[j] * SCALE_TO_RESOLUTION});
        }
        store.loadData(data);
        store.sort('resolution', 'DESC');
  
        // create the grid
        var scale_editor_wmts = new Ext.form.ComboBox({
          displayField : 'scale', valueField : 'scale',
          typeAhead : false,
          triggerAction : 'all',
          mode : 'local',
          store : new Ext.data.JsonStore({fields : ['scale', 'resolution'], data : data })
        });
        var url_editor_wmts = new Ext.form.TextField();
        var ds_model_wmts = Ext.data.Record.create([{
          name : 'Scale',
          type : 'string'
        }]);
        var grid = new Ext.grid.EditorGridPanel({
        	loadMask : true,
        	rowspan : 3,
        	width : 420,
          frame : true,
          title : title.apply([Ext.util.Format.plural(store.getCount(), ' résolution', ' résolutions')]),
        	isFormField : true,
        	hideLabel : true,
          sm : new Ext.grid.RowSelectionModel({ singleSelect : true }),
          id : 'resolutions_cachegrid',
          name : 'resolutions',
          validate : function(){return this.store.getCount()>0 },
          getName : function(){
          	return this.name;
          },
          getValue : function(){return this.store.collect('resolution');},
          setValue : function(resolutions){
          	var convert = [];
          	Ext.each(resolutions, function(resolution){
          		convert.push({scale:Math.round(parseFloat(resolution)/SCALE_TO_RESOLUTION), resolution:parseFloat(resolution)});
          	})
          	this.store.removeAll();
          	this.store.loadData(convert);
          },
          viewConfig : {scrollOffset : 20},
          height : 265,
          store : store,
          columns : [{
            header : "Échelle",
            width : 195,
            dataIndex : 'scale',
            sortable : false,
            editor : scale_editor_wmts
          }, {
            header : "Résolution",
            width : 195,
            dataIndex : 'resolution',
            sortable : false,
            renderer : function(value){
            	return value+(!Ext.isEmpty(value) ? '<input type="hidden" name="resolutions[]" value="'+value+'">' : '');
            }
          }], 
          tbar : [{
            text : 'Ajouter',
            // icon :'images/table_add.png',
            handler : function() {
              var line = grid.getStore().getCount();
              grid.getStore().insert(line, new ds_model_wmts({
                scale : '',
                resolution : ''
              }));
              grid.startEditing(line, 0);
            }
          }, {
            text : 'Supprimer',
            //icon :'images/table_delete.png',
            handler : function() {
              var el = grid.getSelectionModel().getSelected();
              grid.getStore().remove(el);
            }
          }],
          listeners : {
            bodyresize : function(e) {
              var g = this;
              var view = g.getView();
              var c = g.getGridEl();
              var csize = c.getSize(true);
              
              var hdHeight = view.mainHd.getHeight();
              var vh = csize.height - (hdHeight);
              
              view.scroller.setHeight(vh);
            },
            afteredit : function(e) {
              if (e.field == "scale") {
                var index = store.findExact('scale', String(e.value));
                if ( index!=e.row ){
                  var row = parseInt(e.row);
                  grid.getStore().removeAt(row);
                  return;
                }
                var resolution = parseFloat(e.value) * SCALE_TO_RESOLUTION;
                var row = parseInt(e.row);
                grid.getStore().removeAt(row);
                grid.getStore().insert(row, new ds_model_wmts({
                  scale : e.value,
                  resolution : resolution || ''
                }));
                grid.getStore().sort('resolution', 'DESC');
              }
            }
          }
        });
        tabItems.push(grid);
      } else if (tabConstants[i]['prodige_wmts_settings_type'] == "4TextFiled") {
      	var recordFields = ['minx', 'miny', 'maxx', 'maxy'];
        var tabLabel = tabConstants[i]['prodige_wmts_settings_desc'].split(";");
        var tabValue = tabConstants[i]['prodige_wmts_settings_value'].split(";");
        var fieldset = {
          xtype : 'fieldset',
          width : 'auto',
          id : tabConstants[i]['prodige_wmts_settings_constant'],
          title : tabConstants[i]['prodige_wmts_settings_title'],
          // collapsed: true,
          defaultType : 'textfield',
          items : []
        };
        tabLabel.forEach(function(alabel, index){
          fieldset.items.push({
            fieldLabel : alabel,
            value : tabValue[index],
            name : recordFields[index],
            id : tabConstants[i]['prodige_wmts_settings_constant'] + alabel
          });
        })
        tabItems.push(fieldset);
      
      } else if (tabConstants[i]['prodige_wmts_settings_type'] == "selectfield") {
        var storeDate = new Ext.data.JsonStore({
          url : SERVICE_GET_PROJECTIONS,
          storeId : 'myStore',
          autoDestroy : true,
          idProperty : 'epsg',
          fields : ['epsg', 'title', 'proj4', 'display'],
          autoLoad : true
        });
        var combo = new Ext.form.ComboBox({
          id : tabConstants[i]['prodige_wmts_settings_desc'],
          name : 'projection_combo',
          fieldLabel : tabConstants[i]['prodige_wmts_settings_desc'],
          typeAhead : false,
          triggerAction : 'all',
          mode : 'local',
          margins : '0 0 0 10',
          store : storeDate,
          hiddenName : 'projection',
          valueField : 'epsg',
          value:tabConstants[i]['prodige_wmts_settings_value'],
          displayField : 'display'
        });
        tabItems.push({
          xtype : 'panel',
          layout : 'form',
          items : [combo]
        });
        var projValue = tabConstants[i]['prodige_wmts_settings_value'];
        storeDate.on('load', function(store) {
          if ( !gridRecord ) combo.setValue(projValue);
          else combo.setValue(gridRecord.get(combo.hiddenName));
        })
      }
    }
    
    tabItems.push({
    	xtype : 'fieldset',
    	title : 'Dimensions des tuiles',
      width : 'auto',
    	items : [{
    		xtype : 'numberfield',
    		name : 'width',
    		allowDecimals : false,
    		allowNegative : false,
    		fieldLabel : 'Largeur',
    		value : 256
    	},{
        xtype : 'numberfield',
        name : 'height',
        allowDecimals : false,
        allowNegative : false,
        fieldLabel : 'Hauteur',
        value : 256
      }]
    });
    var tabButtons = [{
      text : 'Valider',
      handler : function() {
      	var form = Ext.getCmp("formCacheGrid").getForm();
      	if ( !form ) return;
  
        if ( form.isValid() ) {
          form.submit({
            clientValidation : true,
            success : function(form, action) {
            	var response = Ext.decode(action.response.responseText);
            	var recordData = response.record;
            	var gridParent = Ext.getCmp('cachegrids');
            	if ( recordData && gridParent ){
            		var store = gridParent.getStore();
            		var recordType = store.recordType;
            		var id = recordData.id;
            		var index = store.findExact('id', id);
            		var record;
            		if ( index==-1 ){
            			record = new recordType(recordData, id);
            			store.add(record);
            		} else {
            			record =  store.getAt(index);
            			if ( record ){
            				Ext.iterate(recordData, function(key, value){
            					record.set(key, value);
            				});
            				record.commit();
            			}
            		}
            	}
              win.close();
              Ext.Msg.alert('Paramétrage', "Le paramétrage est pris en compte");
            },
            failure : function(form, action) {
              switch (action.failureType) {
                case Ext.form.Action.CLIENT_INVALID :
                  Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                break;
                case Ext.form.Action.CONNECT_FAILURE :
                  Ext.Msg.alert('Failure', 'Ajax communication failed');
                break;
                case Ext.form.Action.SERVER_INVALID :
                default :
                  Ext.Msg.alert('Failure', action.result.msg);
                break;
              }
            }
          });
        } else {
          Ext.MessageBox.alert('Erreurs de saisie', 'Tous les champs ne sont pas remplis.');
        }
      }
    }, {
    	text : 'Annuler',
    	handler : function(){
    		win.close();
    	}
    }];
    
    var setReadOnly = function(array){
    	array.forEach(function(item){
        item.hideTrigger = readOnly;
    		item.readOnly = readOnly;
    		if ( Ext.isArray(item.items) ){
    			setReadOnly(item.items);
    		}
        if ( readOnly ){
          if ( item.colModel ){
          	item.colModel.isCellEditable = function(){return false};
          }
        	if ( item.toolbars ){
        		item.toolbars.forEach(function(toolbar){toolbar.hide()});
          }
          delete item.topToolbar;
        	if ( item.tbar ){
        		if ( item.tbar instanceof Ext.Element ) item.tbar.hide();
        		else delete item.tbar;
        	}
        }
    	})
    }
    setReadOnly(tabItems);
  
    var constantForm = new Ext.FormPanel({
      url : SERVICE_WMTS_EXECUTE_SERVICE+'/updateCacheGrid',
      frame : true,
      id : 'formCacheGrid',
      layout : {
      	type : 'table',
      	columns : 2,
      	tableAttrs : {width : '100%', cellspacing:2}
      },
      autoHeight : true,
      labelWidth : 100,
      labelSeparator : ' ',
      defaults : { labelSeparator : ' ', margin : 5, allowBlank : false, labelWidth : 90, defaults : {width:'100%', allowBlank : false}},
      items : tabItems,
      buttons : (readOnly ? null : tabButtons),
      listeners : {
      	afterrender : function(){
      		this.getForm().add(Ext.getCmp('resolutions_cachegrid'));
      		if ( gridRecord ) this.getForm().loadRecord(gridRecord);
      	}
      }
    });
    
  
    var win = new Ext.Window({
      resizable : true,
      layout : 'anchor',
      autoScroll : true,
      //height : 500,
      width : 800,
      modal : false,
      autoDestroy : true,
      shadow : false,
      title : "Définition de grilles de tuilage",
      items : [constantForm]
    });
    win.show();
  
  }


});//end class
function getSubarray(array, indexArray,isResolution){
	var result = [];
	var j = 0;
	if(!indexArray){
		for(var i=0;i<array.length;i++){
			result[j] = [];
			result[j].push(array[i]*SCALE_TO_RESOLUTION);
			result[j].push(array[i]);
			j++;
		}
	}else{
		for(var i=0;i<array.length;i++){
			if(isResolution){
				var resolution = (array[i]*SCALE_TO_RESOLUTION).toString();
				if(indexArray.indexOf(resolution) >= 0){
					result[j] = [];
					result[j]['resolution']=array[i]*SCALE_TO_RESOLUTION;
					result[j]['scale']=array[i];
					j++;
				}
			}else{
				var resolution = (array[i]*SCALE_TO_RESOLUTION).toString();
				if(indexArray.indexOf(array[i]) >= 0){
					result[j] = [];
					result[j]['resolution']=array[i]*SCALE_TO_RESOLUTION;
					result[j]['scale']=array[i];
					j++;
				}
			}				
		}
	}	
	return result;
}

