<?php
/** 
 * @brief Constantes sp�cifiants les types de r�cup�ration :
 *        Get, Post, Get puis Post, Post puis Get 
 */
define("REQ_GET", 1);
define("REQ_POST", 2);
define("REQ_GET_POST", 3);
define("REQ_POST_GET", 4);

/**
 * @brief fonction de test par d�faut pour la function request
 *
 * @param  strValue  Valeur � tester
 * @return Retourne toujours true
 */
function DefaultTest($strValue) { return true; }

/**
 * @brief fonction de r�cup�ration de variables post�es par un checkbox
 *
 * @param  strVar              Nom de la variable
 * @param  reqMethod           Doit prendre l'une des valeurs constantes reqXXXX
 * @return La valeur du param�tre r�cup�r�
 */
function RequestCheckbox($strVar, $reqMethod)
{
  $strRes = Request($strVar, $reqMethod, "");
  if( $strRes == "" ) return "0";
  elseif( $strRes == "on" ) return "1";
  return $strRes;
}

/**
 * @brief fonction de r�cup�ration de variables post�es
 *
 * @param  strVar              Nom de la variable
 * @param  reqMethod           Doit prendre l'une des valeurs constantes reqXXXX
 * @param  strDefault          Valeur par d�faut si n'existe pas
 * @param  strFunctionTestType Nom de la fonction pour tester le type de la valeur
 * @return La valeur du param�tre r�cup�r�
 */
function Request($strVar, $reqMethod, $strDefault, $strFunctionTestType="DefaultTest")
{
  if( $strVar == "" ) return $strDefault;
  
  $bTest = false;
  $strVal = $strDefault;
  switch( $reqMethod ) {
  case REQ_GET :
    if( isset($_GET[$strVar]) )
      eval("\$bTest = $strFunctionTestType(\$_GET[\$strVar]);");
    $strVal = (($bTest==true && $_GET[$strVar]!="") ? $_GET[$strVar] : $strDefault);
    break;
  case REQ_POST :
    if( isset($_POST[$strVar]) )
      eval("\$bTest = $strFunctionTestType(\$_POST[\$strVar]);");
    $strVal = (($bTest==true && $_POST[$strVar]!="") ? $_POST[$strVar] : $strDefault);
    break;
  case REQ_GET_POST :
    if( isset($_GET[$strVar]) )
      eval("\$bTest = $strFunctionTestType(\$_GET[\$strVar]);");
    $strVal = (($bTest==true && $_GET[$strVar]!="") ? $_GET[$strVar] : $strDefault);
    if( $strVal==$strDefault ) {
      $bTest = false;
      if( isset($_POST[$strVar]) )
        eval("\$bTest = $strFunctionTestType(\$_POST[\$strVar]);");
      $strVal = (($bTest==true && $_POST[$strVar]!="") ? $_POST[$strVar] : $strDefault);
    }
    break;
  case REQ_POST_GET :
    if( isset($_POST[$strVar]) )
      eval("\$bTest = $strFunctionTestType(\$_POST[\$strVar]);");
    $strVal = (($bTest==true && $_POST[$strVar]!="") ? $_POST[$strVar] : $strDefault);
    if( $strVal==$strDefault ) {
      $bTest = false;
      if( isset($_GET[$strVar]) )
        eval("\$bTest = $strFunctionTestType(\$_GET[\$strVar]);");
      $strVal = (($bTest==true && $_GET[$strVar]!="") ? $_GET[$strVar] : $strDefault);
    }
    break;
  }
  
  //emp�che l'inclusion de tag javascript, iframe
  if(defined("ALK_MODE_SECURE") && ALK_MODE_SECURE &&  is_string($strVal) ){
    $strVal = ereg_replace( "<[sS][cC][rR][iI][pP][tT][^>]*>.*</[sS][cC][rR][iI][pP][tT][^>]*>", "", stripslashes($strVal) );
    $strVal = ereg_replace( "<[sS][cC][rR][iI][pP][tT]/[^>]*>", "", stripslashes($strVal) );
    $strVal = ereg_replace( "<[iI][fF][rR][aA][mM][eE][^>]*>.*</[iI][fF][rR][aA][mM][eE][^>]*>", "", stripslashes($strVal) );
    $strVal = ereg_replace( "<[iI][fF][rR][aA][mM][eE][^>]*/>", "", stripslashes($strVal) );
  }

  return $strVal;
}

/**
 * @brief Encode puis retourne une chaine de caract�res
 *        repr�sentant un param�tre dans une URL http
 * 
 * @param strParam  Chaine de caract�res � encoder
 * @return Retourne un string
 */
function EncodeRequest($strParam)
{
  return Encode($strParam);
}

/**
 * @brief R�cup�re le param�tre http selon la m�thode $reqMethod
 *        D�code le param encod�e par EncodeRequest() 
 *        V�rifie le format en fonction de $strDefault et $strFunctionTestType
 *        Puis retourne la valeur du param�tre
 * 
 * @param strParam  Chaine de caract�res � encoder
 * @return Retourne un string
 */
function RequestDecode($strVar, $reqMethod, $strDefault, $strFunctionTestType="DefaultTest")
{  
  // r�cup�re le param�tre encod�
  $strParam = Request($strVar, $reqMethod, "");
 
  // supprime des caract�res non encod� par l'�diteur
  $strParam = str_replace("<p>", "", $strParam );
  $strParam = str_replace("</p>", "", $strParam );

  //d�codage
  $strDecode = Decode($strParam);
   
  // v�rifie le format du param d�cod�
  eval("\$bTest = $strFunctionTestType(\$strDecode);");
  $strDecode = (($bTest==true && $strDecode!="") ? $strDecode : $strDefault);

  return $strDecode;
}

/**
 * @brief Decode de l'hexa en chaine de caract�re puis retourne une chaine de caract�res
 * 
 * @param strParam  Chaine de caract�res � decoder
 * @return Retourne un string
 */
function Decode($strParam)
{
  //decode le param�tre de h�xa � texte
  $strDecode = "";
  for($i=0; $i<strlen($strParam); $i+=2 ) {
    $strCodeHex = substr($strParam, $i, 2);
    $strDecode .= chr(hexdec($strCodeHex));
  }
  return $strDecode;
}

/**
 * @brief Encode en hexa puis retourne une chaine de caract�res
 * 
 * @param strParam  Chaine de caract�res � encoder
 * @return Retourne un string
 */
function Encode($strParam)
{
  //encode le param�tre en h�xa
  $strEncode = "";
  for($i=0; $i<strlen($strParam); $i++) {
    $strEncode .= dechex(ord($strParam[$i]));
  }
  return $strEncode;
}

?>