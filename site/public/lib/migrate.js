Ext.Ajax.timeout = 6000000;
Ext.onReady(function() {
var win;

//Formulaire compte
var carmen_form = new Ext.FormPanel({
        url: 'services/migrate.php?',
        frame: true,
        title: 'Migration des comptes de version 1.5 vers 1.6 ',
        autoHeight : true,
        timeout: 6000000,
        items: [{
            xtype: 'textfield',
            fieldLabel: 'répertoire à migrer',
            width : 400,
            allowBlank : false,
            name: 'compteName'
         }, {
           id :'lbError',
           name :'lbError',
           xtype :'label',
           text :'',
           width :160
         } 
         ],
         buttons: [{
              text: 'Lancer l\'automate',
              handler: function(){
                carmen_form.getForm().submit({
                  waitTitle :'Migration en cours...',
                  waitMsg :'Merci de patienter...',
                  timeout: 6000000,
                  success: function(f,a){
                    document.getElementById("lbError").innerHTML = (a.result.msg);
                  },
                  failure: function(f,a){
                    Ext.Msg.alert('Warning', "compte invalide ou manquant");
                  }
                });
              }
         }]
         
    });


//Tableau des onglets
var PanelCarmen = new Ext.TabPanel({
  region: 'center',
  xtype: 'tabpanel',
  activeTab: 0,
  items: [carmen_form]
});




var viewport = new Ext.Viewport({
  layout: 'border',
  renderTo: Ext.getBody(),
  items: [{
    region: 'north',
    xtype: 'panel',
    html: ''
  },{
    region: 'west',
    xtype: 'panel',
    split: true,
    width: 200,
    html: ''
  },PanelCarmen,{
    region: 'east',
    xtype: 'panel',
    split: true,
    width: 200,
    html: ''
  },{
    region: 'south',
    xtype: 'panel',
    html: ''
  }]
});




});







