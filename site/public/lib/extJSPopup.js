
var tabExtPopup = new Array();

var popupWindowGroup = new Ext.WindowGroup();

/**
 * Ouverture de popups
 */
function loadExtPopup(url, cible, title, width, height){
  var popup_position = tabExtPopup.length ;
  
  if (tabExtPopup[popup_position]){
    tabExtPopup[popup_position].close();
  }
  
  var url = url+"&target="+cible+"&popup_pos="+popup_position;
  tabExtPopup[popup_position] = new Ext.ux.ManagedIFrame.Window({
      title           : title,
      width           : Math.min(Ext.getBody().getViewSize().width-10,eval(width)),
      height          : Math.min(Ext.getBody().getViewSize().height-10,eval(height)),
      constrainHeader : true,
      maximizable     : !Ext.isIE7,
      collapsible     : true,
      closable        : true,
      shadow          : Ext.isIE,
      animCollapse    : false,
      autoScroll      : true,
      manager         : popupWindowGroup,
      hideMode        : 'nosize',
      defaultSrc      : url,
      loadMask        : {msg:'Chargement...'},
      close           : function(){closeExtPopup(popup_position);},//'close'
      closeAction     : 'close'
  });
  
  // reposition the popup on loaded
  tabExtPopup[popup_position].on("documentloaded", function(){
    console.log(this);
    this.setPosition(Math.max(this.getPosition()[0], 0), Math.max(this.getPosition()[1], 0));
  });
  
  tabExtPopup[popup_position].show();
  if (tabExtPopup[popup_position].getFrameWindow()){
     if (window.frames[cible]){
        //ext js does'nt destroy the window.frames[cible] when destroying tabExtPopup[popup_position]
        //so if this iframe already exists we remplace it with the new one
        window.frames[cible]=tabExtPopup[popup_position].getFrameWindow();  
     }
     tabExtPopup[popup_position].getFrameWindow().name = cible; 
  }
  
}

/**
 * Fermeture de la popup
 */
function closeExtPopup(popup_position){
   
  if (tabExtPopup[popup_position]!=null){
    var iframe = document.getElementById("IframePopup_Contenu_"+popup_position);
    //tabExtPopup[popup_position].removeAll();
    tabExtPopup[popup_position].destroy();
    //iframe.parentNode.removeChild(iframe);
    
  }
}

/**
 * Ajustement de la taille de la popup
 */
function resizeExtPopup(popup_position, width, height){
  if (tabExtPopup[popup_position]){
     //+20 en largeur pour prendre en compte l'éventuelle barre de scroll
     //+40 en hauteur pour prendre en compte la place réservée au titre de la popup
    tabExtPopup[popup_position].setSize(width+20,height+40);
    tabExtPopup[popup_position].doLayout();   
  }
}

/**
 * Recentrage de la popup
 */
function moveExtPopup(popup_position){
  if (tabExtPopup[popup_position]){
    tabExtPopup[popup_position].center();
  }
}