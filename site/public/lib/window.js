Ext.Msg.minWidth = 500;
var _SERVICE_GET_PARAMS = (typeof(SERVICE_GET_PARAMS) != "undefined" ? encodeURIComponent(SERVICE_GET_PARAMS) : null);
var PARAMETRAGE_WMTS          = 3;
var PARAMETRAGE_PROJECTIONS   = 4;
var PARAMETRAGE_TOOLS         = 5;
var PARAMETRAGE_WMXS          = 6;
var PARAMETRAGE_SEARCHENGINE  = 7;
var PARAMETRAGE_CATALOGUE     = 8;
var PARAMETRAGE_CARTO         = 9;
var PARAMETRAGE_TERRITORIES   = 10;
var PARAMETRAGE_EXTRANET      = 11;
var PARAMETRAGE_MODELES_CARTO = 12;
var PARAMETRAGE_AUTOMATE      = 13;
var PARAMETRAGE_FLUXATOM      = 14;
var PARAMETRAGE_SERVERS       = 15;
var PARAMETRAGE_QUEUE_DOWNLOAD= 16;

function displayWindow(path, option) {
	if ( Ext.getCmp('parametrage') ) {
	  Ext.Msg.alert("Paramètres en cours d'édition", "Une fenêtre de paramétrage est déjà ouverte.<br/>Veuillez terminer l'édition en cours.");
	  return;
	}

  var win;

  if ( [PARAMETRAGE_CATALOGUE, PARAMETRAGE_CARTO, PARAMETRAGE_AUTOMATE, PARAMETRAGE_FLUXATOM, PARAMETRAGE_SERVERS].indexOf(option)!=-1) {
  	var url = '';
  	switch (option){
  		case PARAMETRAGE_CATALOGUE : url = SERVICE_GET_PARAMETRAGE_CATALOGUE; break;
  		case PARAMETRAGE_CARTO     : url = SERVICE_GET_PARAMETRAGE_CARTO;     break;
  		case PARAMETRAGE_AUTOMATE  : url = SERVICE_GET_PARAMETRAGE_AUTOMATE;  break;
  		case PARAMETRAGE_FLUXATOM  : url = SERVICE_GET_PARAMETRAGE_FLUXATOM;  break;
  		case PARAMETRAGE_SERVERS   : url = SERVICE_GET_PARAMETRAGE_SERVERS;   break;
  	}
    Ext.Ajax.request({
      url : url,
      method : 'GET',
      scriptTag : true,
      params : {
        service : 'getConstant'
      },
      success : function(response) {
        var tabConstants = setResponseObject(response).responseObject
            || eval('(' + response.responseText + ')');
        var tabItems = new Array();
        for (i = 0; i < tabConstants.length; i++) {
          if (tabConstants[i]['prodige_settings_type'] == "timefield") {
            tabItems[i] = new Ext.form.TimeField({
              triggerClass : 'x-form-time-trigger',
              fieldLabel : tabConstants[i]['prodige_settings_desc'],
              name : tabConstants[i]['prodige_settings_constant'],
              editable : false,
              format : "H:i",
              minValue : (option == PARAMETRAGE_AUTOMATE ? '23:30' : '00:00'),
              maxValue : (option == PARAMETRAGE_AUTOMATE ? '23:30' : '23:45'),
              value : tabConstants[i]['prodige_settings_value']
            });
          } else if (tabConstants[i]['prodige_settings_type'] == "checkboxfield") {

            tabItems[i] = new Ext.form.Checkbox({
              fieldLabel : tabConstants[i]['prodige_settings_desc'],
              name       : tabConstants[i]['prodige_settings_constant'],
              value      : tabConstants[i]['prodige_settings_value'],
              checked    : (tabConstants[i]['prodige_settings_value'] == "on"),
              id         : tabConstants[i]['prodige_settings_constant'],
              allowBlank : false
            });

          } else if (tabConstants[i]['prodige_settings_type'] == "multiselectfield") {
            if (option == PARAMETRAGE_CARTO) { //cas parametre carto
              var currentTabConstantsMulti = tabConstants[i];
              var storeDate = new Ext.data.JsonStore({
                url : SERVICE_GET_PROJECTIONS,
                storeId : 'myStore',
                autoDestroy : true,
                idProperty : 'epsg',
                fields : ['epsg', 'nom', 'proj4', 'display'],
                autoLoad : {
                  callback : function() {
                    storeCombo.setValue( currentTabConstantsMulti['prodige_settings_value'].split(" ; ") );
                  }
                }
              });
              var storeCombo = new Ext.ux.form.SuperBoxSelect({
                allowBlank : false,
                msgTarget : 'under',
                allowAddNewData : true,
                addNewDataOnBlur : true,
                resizable : true,
                anchor : '98%',
                hideLabel : false,
                minChars : 0,
                queryParams : "q",
                //width : 400,
                hideTrigger : false,
                id : tabConstants[i]['prodige_settings_constant'],
                name : tabConstants[i]['prodige_settings_constant'] + "[]",
                mode : 'local',
                store : storeDate,
                valueField : "display",
                displayField : "display",
                valueDelimiter : ";",
                fieldLabel : tabConstants[i]['prodige_settings_desc']
              });
              tabItems[i] = storeCombo;
            } else { //autre cas
              if (option == PARAMETRAGE_FLUXATOM) {// FLUX ATOM        			
                if (tabConstants[i]['prodige_settings_constant'] == "PRO_FLUXATOM_PROJECTION") {
                  var currentTabConstantsProj = tabConstants[i];
                  var storeDate = new Ext.data.JsonStore({
                    url : SERVICE_GET_PROJECTIONS,
                    storeId : 'myStore',
                    autoDestroy : true,
                    idProperty : 'epsg',
                    fields : ['epsg', 'nom', 'proj4', 'display'],
                    autoLoad : {
                      callback : function() {
                        storeComboProj.setValue(currentTabConstantsProj['prodige_settings_value'].split(" ; "));
                      }
                    }
                  });
                  var storeComboProj = new Ext.ux.form.SuperBoxSelect({
                    allowBlank : false,
                    msgTarget : 'under',
                    allowAddNewData : true,
                    addNewDataOnBlur : true,
                    resizable : true,
                    anchor : '98%',
                    hideLabel : false,
                    minChars : 0,
                    queryParams : "q",
                    //width : 400,
                    hideTrigger : false,
                    id : tabConstants[i]['prodige_settings_constant'],
                    name : tabConstants[i]['prodige_settings_constant'] + "[]",
                    mode : 'local',
                    store : storeDate,
                    valueField : "display",
                    displayField : "display",
                    valueDelimiter : ";",
                    fieldLabel : tabConstants[i]['prodige_settings_desc']
                  });
                  tabItems[i] = storeComboProj;

                } else {// TODO cas des formats. 

                  var currentTabConstantsFormat = tabConstants[i];
                  var storeDate = new Ext.data.JsonStore({
                    url : SERVICE_GET_FORMATS,
                    storeId : 'myStore',
                    autoDestroy : true,
                    idProperty : 'format',
                    fields : ['format', 'nom', 'display'],
                    autoLoad : {
                      callback : function() {
                        storeComboFormat.setValue(currentTabConstantsFormat['prodige_settings_value'].split(" ; "));
                      }
                    }
                  });
                  var storeComboFormat = new Ext.ux.form.SuperBoxSelect({
                    allowBlank : false,
                    msgTarget : 'under',
                    allowAddNewData : true,
                    addNewDataOnBlur : true,
                    resizable : true,
                    anchor : '98%',
                    hideLabel : false,
                    minChars : 0,
                    queryParams : "q",
                    //width : 400,
                    hideTrigger : false,
                    id : tabConstants[i]['prodige_settings_constant'],
                    name : tabConstants[i]['prodige_settings_constant'] + "[]",
                    mode : 'local',
                    store : storeDate,
                    valueField : "display",
                    displayField : "display",
                    valueDelimiter : ";",
                    fieldLabel : tabConstants[i]['prodige_settings_desc']
                  });
                  tabItems[i] = storeComboFormat;
                }

              } else {
                var storeDate = new Ext.data.ArrayStore({
                  fields : ['key', 'name'],
                  data : tabConstants[i]['prodige_settings_param']
                });
                tabItems[i] = new Ext.ux.form.SuperBoxSelect({
                  hideLabel : false,
                  minChars : 0,
                  queryParam : "q",
                  //width : 400,
                  hideTrigger : false,
                  id : tabConstants[i]['prodige_settings_constant'],
                  name : tabConstants[i]['prodige_settings_constant'] + "[]",
                  mode : 'local',
                  store : storeDate,
                  valueField : "key",
                  displayField : "name",
                  value : getSubarray(tabConstants[i]['prodige_settings_param'], tabConstants[i]['prodige_settings_value'].split(";")),
                  valueDelimiter : ";",
                  fieldLabel : tabConstants[i]['prodige_settings_desc']
                });
              }
            }

          } else if (tabConstants[i]['prodige_settings_type'] == "selectfield") {
            if (option == PARAMETRAGE_FLUXATOM) {
              if (tabConstants[i]['prodige_settings_constant'] == "PRO_FLUXATOM_COUCHE_TERRITOIRE") {
                // SERVICE_GET_TABLELIST_CARTO : Service supprimé
                var storeTableList = new Ext.data.JsonStore({
                  url : SERVICE_GET_TABLELIST_CARTO,
                  fields : ['name'],
                  autoLoad : true
                });

                tabItems[i] = new Ext.form.ComboBox({
                  //allowBlank: false,
                  id : tabConstants[i]['prodige_settings_constant'],
                  name : tabConstants[i]['prodige_settings_constant'],
                  fieldLabel : tabConstants[i]['prodige_settings_desc'],
                  typeAhead : false,
                  emptyText : 'Sélectionnez une valeur',
                  //width : 400,
                  triggerAction : 'all',
                  mode : 'local',
                  store : storeTableList, // car le service SERVICE_GET_TABLELIST_CARTO est supprimé
                  //store: {},
                  submitValue : true,
                  value : tabConstants[i]['prodige_settings_value'],
                  displayField : 'name',
                  valueField : 'name',
                  listeners : {
                    'select' : {
                      fn : function(b, evt) {
                        this.majListFields();
                      },
                      scope : this
                    }
                  }//,
                });
              } else {

                if (tabConstants[i]['prodige_settings_constant'] == "PRO_FLUXATOM_FIELD_ID_COUCHE_TERRITOIRE"
                 || tabConstants[i]['prodige_settings_constant'] == "PRO_FLUXATOM_FIELD_LABEL"
                ){
                  tabItems[i] = new Ext.form.ComboBox({
                    id : tabConstants[i]['prodige_settings_constant'],
                    name : tabConstants[i]['prodige_settings_constant'],
                    fieldLabel : tabConstants[i]['prodige_settings_desc'],
                    typeAhead : false,
                    //width : 400,
                    //allowBlank: false,
                    emptyText : 'Sélectionnez une valeur',
                    triggerAction : 'all',
                    mode : 'local',
                    //store :storeTableListField,
                    submitValue : true,
                    value : tabConstants[i]['prodige_settings_value'],
                    displayField : 'name',
                    valueField : 'name',
                    queryMode : 'local',
                    listConfig : {
                      loadingText : 'Chargement',
                      getInnerTpl : function() {
                        return '<tpl for="."><div ext:qtip="{valeurTitle}" class="x-combo-list-item">{valeurTitle}</div></tpl>';
                      },
                      minWidth : 150
                    },
                    store : new Ext.data.ArrayStore({
                      fields : ['name', 'name'],
                      data : []
                    })
                  });

                } else {

                  var storeDate = new Ext.data.JsonStore({
                    url : SERVICE_GET_PROJECTIONS,
                    storeId : 'myStore',
                    autoDestroy : true,
                    idProperty : 'epsg',
                    fields : ['epsg', 'nom', 'proj4', 'display'],
                    autoLoad : true
                  });
                  tabItems[i] = new Ext.form.ComboBox({
                    id : tabConstants[i]['prodige_settings_constant'],
                    name : tabConstants[i]['prodige_settings_constant'],
                    fieldLabel : tabConstants[i]['prodige_settings_desc'],
                    typeAhead : false,
                    //width : 400,
                    triggerAction : 'all',
                    mode : 'local',
                    store : storeDate,
                    submitValue : true,
                    value : tabConstants[i]['prodige_settings_value'],
                    displayField : 'display'
                  });

                }
              }

            } else {

              var storeDate = new Ext.data.JsonStore({
                url : SERVICE_GET_PROJECTIONS,
                storeId : 'myStore',
                autoDestroy : true,
                idProperty : 'epsg',
                fields : ['epsg', 'nom', 'proj4', 'display'],
                autoLoad : true
              });
              tabItems[i] = new Ext.form.ComboBox({
                id : tabConstants[i]['prodige_settings_constant'],
                name : tabConstants[i]['prodige_settings_constant'],
                fieldLabel : tabConstants[i]['prodige_settings_desc'],
                typeAhead : false,
                //width : 400,
                triggerAction : 'all',
                mode : 'local',
                store : storeDate,
                submitValue : true,
                value : tabConstants[i]['prodige_settings_value'],
                displayField : 'display'
              });
            }
          } else {
            tabItems[i] = {
              xtype : 'textfield',
              //width : 400,
              allowBlank : true,
              fieldLabel : tabConstants[i]['prodige_settings_desc'],
              value      : tabConstants[i]['prodige_settings_value'],
              name       : tabConstants[i]['prodige_settings_constant'],
              id         : tabConstants[i]['prodige_settings_constant']
            };
          }
        }
        var tabButtons = [{
          text : 'Valider',
          handler : function() {
            if (Ext.getCmp("formParametrage").form.isValid()) {
              Ext.getCmp("formParametrage").form.submit({
                clientValidation : true,
                success : function(form, action) {
                  if (option == PARAMETRAGE_SERVERS) {
                    Ext.Msg.alert('Paramétrage', 'Les fichiers wms.map et wfs.map ont été mis à jour<br><br>Il faut maintenant configurer le serveur apache. Pour cela,<br>Veuillez avertir votre administrateur réseau des changements<br>effectués et lui fournir la procédure à suivre.');
                  } else {
                    Ext.Msg.alert('Paramétrage', "Le paramétrage est pris en compte");
                  }
                },
                failure : function(form, action) {
                  switch (action.failureType) {
                    case Ext.form.Action.CLIENT_INVALID :
                      Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE :
                      Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID :
                      Ext.Msg.alert('Failure', action.result.msg);
                  }
                }

              });
            } else {
              Ext.MessageBox.alert('Erreur', 'Tous les champs ne sont pas remplis.');
            }
          }
        }, {
          text : 'Réinitialiser',
          handler : function() {
          	var url = '';
            switch (option){
              case PARAMETRAGE_CATALOGUE : url = SERVICE_GET_PARAMETRAGE_CATALOGUE; break;
              case PARAMETRAGE_CARTO     : url = SERVICE_GET_PARAMETRAGE_CARTO;     break;
              case PARAMETRAGE_AUTOMATE  : url = SERVICE_GET_PARAMETRAGE_AUTOMATE;  break;
              case PARAMETRAGE_FLUXATOM  : url = SERVICE_GET_PARAMETRAGE_FLUXATOM;  break;
              case PARAMETRAGE_SERVERS   : url = SERVICE_GET_PARAMETRAGE_SERVERS;   break;
            }
            Ext.Ajax.request({
              url : url,
              method : 'GET',
              params : {
                service : 'initParametrage'
              },
              success : function(response) {
                Ext.MessageBox.alert('Réinitialisation', 'Les paramètres ont été réinitialisés');
                //Ext.getCmp("parametrage").close(); //TODO to decomment
              }
            });
          }
        }];

        if (option == PARAMETRAGE_AUTOMATE) {
          // configuration de l'automate
          tabButtons[2] = new Ext.Button({
            tooltip : 'Tester la connexion au serveur FTP',
            tooltipType : 'title',
            text : 'Tester la connexion FTP',
            xtype : 'button',
            listeners : {
              'click' : {
                fn : function(b, evt) {
                  if (confirm('Veuillez vous assurer d\'avoir valider les paramètres au préalable.\nContinuer le test de connexion ?')) {
                    Ext.Ajax.request({
                      url : SERVICE_CONFIG_AUTOMATE,
                      method : 'GET',
                      params : {
                        action : 'testConnection'
                      },
                      success : function(response) {
                        if (setResponseObject(response).responseObject.success) {
                          Ext.MessageBox.alert('Succès', setResponseObject(response).responseObject.status);
                        } else {
                          Ext.MessageBox.alert('Échec', setResponseObject(response).responseObject.status);
                        }
                      }
                    });
                  }
                },
                scope : this
              }
            }
          });
          tabButtons[3] = new Ext.Button({
            tooltip : 'Génération d\'un fichier de paramétrage de l\'automate à destination d\'un service',
            tooltipType : 'title',
            text : 'Générer un fichier de paramétrage chiffré',
            xtype : 'button',
            listeners : {
              'click' : {
                fn : function(b, evt) {
                  if (confirm('Veuillez vous assurer d\'avoir valider les paramètres au préalable.\nContinuer la génération ?')) {
                    var service = prompt('Nom du service : ', '');
                    if (service) {
                      // Ext.Ajax.Request();
                      window.location.href = SERVICE_CONFIG_AUTOMATE + "?action=generateConfig&service=" + escape(service);
                    } else {
                      alert('Un nom de service est requis. Génération abandonnée.');
                    }
                  }
                },
                scope : this
              }
            }
          });
        }
        var url = '';
        switch (option){
          case PARAMETRAGE_CATALOGUE : url = SERVICE_GET_PARAMETRAGE_CATALOGUE; break;
          case PARAMETRAGE_CARTO     : url = SERVICE_GET_PARAMETRAGE_CARTO;     break;
          case PARAMETRAGE_AUTOMATE  : url = SERVICE_GET_PARAMETRAGE_AUTOMATE;  break;
          case PARAMETRAGE_FLUXATOM  : url = SERVICE_GET_PARAMETRAGE_FLUXATOM;  break;
          case PARAMETRAGE_SERVERS   : url = SERVICE_GET_PARAMETRAGE_SERVERS;   break;
        }
        var constantForm = new Ext.FormPanel({
          padding : 5,
          autoScroll : true,
          frame : true,
        	method : 'GET',
          url : url,
          baseParams : {service:"updateConstant"},
          id : 'formParametrage',
          scrollable : 'y',
          labelWidth : 200,
          items : tabItems,
          defaults : {anchor : '98%', width : '100%'}
        });

        var title = "Paramétrage";
        switch (option){
          case PARAMETRAGE_CATALOGUE : title += " du module catalogue";    break;
          case PARAMETRAGE_CARTO     : title += " du module cartographie"; break;
          case PARAMETRAGE_AUTOMATE  : title += " de l'automate";          break;
          case PARAMETRAGE_FLUXATOM  : title += " du flux ATOM";           break;
          case PARAMETRAGE_SERVERS   : title += " des serveurs WxS";       break;
        }
        var win = new Ext.Window({
          resizable : true,
          layout : 'fit',
          constrain : true,
          height : Math.min(Ext.getBody().getViewSize().height - 10, (option == PARAMETRAGE_CATALOGUE ? 630 : 550)),
          width  : Math.min(Ext.getBody().getViewSize().width - 10,  (option == PARAMETRAGE_CATALOGUE ? 683 : 1050)),
          modal : false,
          scrollable : 'y',
          autoDestroy : true,
          shadow : false,
          id : 'parametrage',
          title : title,
          items : [constantForm],
          buttons : tabButtons,
          closeAction : 'destroy'
        });
        win.show();
      },
      failure : function(response) {
        Ext.MessageBox.alert('Failed', response);
      }
    });
    return;
  }
//Gestion de la file de téléchargement
  if(option==PARAMETRAGE_QUEUE_DOWNLOAD){
	  var dataArray=[];
	  var tabItems = new Array();
	  var tabButtons =  new Array();
	  var champ_date=18;
	  var champ_table=4;
	  var champ_id_metadata=5;
	  var champ_type=6;
	  Ext.Ajax.request({
	      url: SERVICE_GESTION_QUEUE,
	      method: 'GET',
	      params: { 
	          service: 'listQueue'
	      },
	      success: function(response) {
	        var jsonArray=response.responseObject["listQueue"];
	    	  for (var i = 0; i < jsonArray.length; i++) {
	    	      var data=jsonArray[i].split('|');
	    	      for(var j=0;j<data.length;j++){   	 
	    	    	  if(j==champ_date){
	    	    		  var tab_str=data[j].split(' ');
	    	    		  if(tab_str.length>=2){
	    	    			  var tab_time=tab_str[1].split(':');
	    	    			  if(tab_time.length>=3){
	    	    				  var date_tmp=new Date(tab_str[0].substring(0,4),parseInt(tab_str[0].substring(4,6),10)-1,tab_str[0].substring(6,8),tab_time[0],tab_time[1],tab_time[2]);
	    	    				  data[j]=date_tmp.toLocaleString();
	    	    			  }
	    	    		  }
	    	    	  }else if(j==champ_table||j==champ_id_metadata){
	    	    		  data[j]=data[j].replace(/%/g,',');
	    	    	  }else if(j==champ_type){
	    	    		 var valeur_type=["Fichier vecteur","Couche postgis","table postgis","couche raster","fichier majic","WFS"];
	    	    		 var index=parseInt(data[j]);
	    	    		 if(index>0 && index<valeur_type.length){
	    	    			 data[j]=valeur_type[index];
	    	    		 }
	    	    	  }
	    	      }
	    	      dataArray.push(data);
	    	  }
	    	  var champ=response.responseObject["colone"];
	    	  var index_champ=response.responseObject["index"];
	    	  
	    	  var columns=[]
	    	
	    	  for (var i=0; i< index_champ.length; i++){  	 
	    			  object_tmp={
			                  header :champ[index_champ[i]],
			                  dataIndex :champ[index_champ[i]],
	    					  sortable :true,
	    					  editor: new Ext.form.TextField({
		                      allowBlank: false
	    					  })
	    			  }
	    			  columns.push(object_tmp);
	    		  	  
	    	  }
	          var store = new Ext.data.ArrayStore({
	              data: dataArray,
	              fields: champ

	            });
	            var grid = new Ext.grid.GridPanel(
	                 {
	                      sm :new Ext.grid.RowSelectionModel( {
	                        singleSelect :true
	                      }),
	                      frame :true,
	                      title :' ',
	                      autoHeight :true,
	                      store :store,
	                      columns : columns,
	                      tbar : [
	                              {
	                                text :'Supprimer',
	                                icon :'images/table_delete.png',
	                                handler : function() {
	                                    var sm = grid.getSelectionModel();
	                                    var sel = sm.getSelected();
	                                    if (sm.hasSelection()) {
	                                      Ext.Msg.show( {
	                                            title :'Supprimer fichier sélectonné',
	                                            buttons : Ext.MessageBox.YESNO,
	                                            msg :'Supprimer le fichier ?' + sel.data.ref,
	                                            fn : function(btn) {
	                                              if (btn == 'yes') {
	                                                var conn = new Ext.data.Connection();
	                                                conn.request( {
	                                                      url : SERVICE_GESTION_QUEUE+"?service=delfile",
	                                                      params : {
	                                                    	filename :sel.data.ref
	                                                      },
	                                                      success : function() {
	                                                        grid.getStore().remove(sel);
	                                                      },
	                                                      failure : function() {
	                                                        Ext.Msg.alert('Error',
	                                                            'Unable to delete file');
	                                                      }
	                                                    });
	                                              }
	                                            }
	                                          });
	                                    };
	                                  }
	                                  
	                                },{
		                                text :'Tout Supprimer',
		                                icon :'images/table_delete.png',
		                                handler : function() {
		                                      Ext.Msg.show( {
		                                            title :'Supprimer les fichiers',
		                                            buttons : Ext.MessageBox.YESNO,
		                                            msg :'Supprimer tous les fichiers ?',
		                                            fn : function(btn) {
		                                              if (btn == 'yes') {
		                                                var conn = new Ext.data.Connection();
		                                                conn.request( {
		                                                      url : SERVICE_GESTION_QUEUE+"?service=delallfile",
		                                                      success : function() {
		                                                        grid.getStore().removeAll();
		                                                      },
		                                                      failure : function() {
		                                                        Ext.Msg.alert('Error',
		                                                            'Unable to delete file');
		                                                      }
		                                                   
		                                              })
		                                            }
		                                          }
		                                      })
		                                }
		                             }
	                              ]
	                });

	              
	            var win = new Ext.Window({          
	                  resizable   : true,
	                  layout:'anchor',
	                  modal:false,
	                  autoDestroy :true,
	                  shadow : false,
	                  id : ' ',
	                  constrain: true,
	                  autoScroll: true,
	                  height: Math.min(Ext.getBody().getViewSize().height-10,340),
	                  width: Math.min(Ext.getBody().getViewSize().width-10,1300),
	                  title       : " ",
	                  items : [grid]
	                });
	                win.show();
		  },
		  failure: function(response){
			  
				Ext.MessageBox.alert('Failed', "".response); 
          }
	  });

   return ;

  }
  //acces extranet externe
  if (option == PARAMETRAGE_EXTRANET) {

    // SERVICE_GET_PARAMETRAGE_EXTERNAL_ACCESS : Service supprimé
    /*var store = new Ext.data.JsonStore({
      // store configs
      autoDestroy: true,
      url: SERVICE_GET_PARAMETRAGE_EXTERNAL_ACCESS+"?service=getData",
      storeId: 'myStore',
      // reader configs
      idProperty: 'pk_prodige_external_access',
      fields: ['pk_prodige_external_access', 'prodige_extaccess_referer', 'usr_id']
    });

    // SERVICE_GET_PARAMETRAGE_EXTERNAL_ACCESS : Service supprimé
    var storeCombo = new Ext.data.JsonStore({
      // store configs
      autoDestroy: true,
      url: SERVICE_GET_PARAMETRAGE_EXTERNAL_ACCESS+"?service=getUsers",
      storeId: 'myStore',
      // reader configs
      idProperty: 'pk_utilisateur',
      fields: ['pk_utilisateur', 'usr_id']
    });*/
    var ds_model = Ext.data.Record.create(['pk_prodige_external_access', {
      name : 'pk_prodige_external_access',
      type : 'string'
    }, {
      name : 'prodige_extaccess_referer',
      type : 'string'
    }, {
      name : 'usr_id',
      type : 'string'
    }]);

    var grid = new Ext.grid.EditorGridPanel({
      autoScroll : true,
      sm : new Ext.grid.RowSelectionModel({ singleSelect : true }),
      frame : true,
      title : 'Définition',
      autoHeight : true,
      viewConfig : { 
        forceFit : true,
        scrollOffset : 20
      },
      //store :store, // car le service SERVICE_GET_PARAMETRAGE_EXTERNAL_ACCESS est supprimé
      store : {},
      columns : [{
        header : "Identifiant",
        dataIndex : 'pk_prodige_external_access',
        sortable : true
      }, {
        header : "Referer",
        dataIndex : 'prodige_extaccess_referer',
        sortable : true,
        editor : new Ext.form.TextField({ allowBlank : false })
      }, {
        header : "Utilisateur Associé",
        dataIndex : 'usr_id',
        sortable : true,
        width : 300,
        editor : new Ext.form.ComboBox({
          typeAhead : true,
          tpl : '<tpl for="."><div id="" ext:qtip="{usr_id}" class="x-combo-list-item">{usr_id}</div></tpl>',
          triggerAction : 'all',
          //store :storeCombo, // car le service SERVICE_GET_PARAMETRAGE_EXTERNAL_ACCESS est supprimé
          store : {},
          displayField : 'usr_id',
          valueField : 'pk_utilisateur',
          // transform the data already specified in html
          lazyRender : true,
          listClass : 'x-combo-list-small'
        })
      }],
      tbar : [{
        text : 'Supprimer',
        icon : 'images/table_delete.png',
        handler : function() {
          var sm = grid.getSelectionModel();
          var sel = sm.getSelected();
          if (sm.hasSelection()) {
            Ext.Msg.show({
              title : 'Supprimer l\'accès',
              buttons : Ext.MessageBox.YESNO,
              msg : 'Supprimer l\'accès ?',
              fn : function(btn) {
                if (btn == 'yes') {
                  Ext.Ajax.request({
                    url : SERVICE_GET_PARAMETRAGE_EXTERNAL_ACCESS + "?service=delData",
                    params : {
                      id : sel.data.pk_prodige_external_access
                    },
                    success : function(resp, opt) {
                      grid.getStore().remove(sel);
                    },
                    failure : function(resp, opt) {
                      Ext.Msg.alert('Error', 'Unable to delete movie');
                    }
                  });
                }
              }
            });
          };
        }
      }, {
        text : 'Ajouter',
        icon : 'images/table_add.png',
        handler : function() {
          Ext.Ajax.request({
            url : SERVICE_GET_PARAMETRAGE_EXTERNAL_ACCESS + "?service=addData",
            params : {},
            success : function(resp, opt) {
              grid.getStore().insert(0, new ds_model());
              grid.startEditing(0, 0);
              grid.getStore().getAt(0).set("pk_prodige_external_access",
                  resp.responseObject.pk_prodige_external_access);
            },
            failure : function(resp, opt) {
              e.record.reject();
            }
          });
        }
      }],
      listeners : {
        afteredit : function(e) {
          Ext.Ajax.request({
            url : SERVICE_GET_PARAMETRAGE_EXTERNAL_ACCESS + "?service=updateData",
            params : {
              id : e.record.data.pk_prodige_external_access,
              field : (e.field == "usr_id"
                  ? "prodige_extaccess_fk_utilisateur"
                  : e.field),
              value : e.value
            },
            success : function(resp, opt) {
              e.record.commit();
            },
            failure : function(resp, opt) {
              e.record.reject();
            }
          });
        }
      }
    });

    var win = new Ext.Window({
      resizable : true,
      layout : 'anchor',
      modal : false,
      autoDestroy : true,
      shadow : false,
      id : 'parametrage',
      constrain : true,
      height : Math.min(Ext.getBody().getViewSize().height - 10, 340),
      width : Math.min(Ext.getBody().getViewSize().width - 10, 700),
      title : "Paramétrage des accès depuis des extranet externes",
      items : [grid]
    });
    win.show();
    store.load();
    return;
  }
  //Modèles de cartes
  if (option == PARAMETRAGE_MODELES_CARTO) {

    var store = new Ext.data.JsonStore({
      // store configs
      autoDestroy : true,
      url : SERVICE_GET_MODELES_CARTO + "?service=getData",
      storeId : 'myStore',
      // reader configs
      idProperty : 'pk_modele_carto',
      fields : ['pk_modele_carto', 'modele_carto_intitule', 'modele_carto_type', 'modele_uuid']
    });

    var arrayData = [{type:"1", nom:"VISUALISATION"}, {type:"2", nom:"EXTRACTION"}];

    var memoryProxy = new Ext.data.MemoryProxy(arrayData);

    var storeCombo2 = new Ext.data.JsonStore({
      fields : ['type', 'nom'],
      idProperty : 'type',
      storeId : 'myStore2',
      proxy : memoryProxy,
      data : arrayData
    });
    storeCombo2.load();

    var ds_model = Ext.data.Record.create(['pk_prodige_external_access', {
      name : 'pk_prodige_external_access',
      type : 'string'
    }, {
      name : 'prodige_extaccess_referer',
      type : 'string'
    }, {
      name : 'usr_id',
      type : 'string'
    }, {
      name : 'type',
      type : 'string'
    }]);

    var nom_editor = new Ext.form.TextField();
    var grid = new Ext.grid.EditorGridPanel({
      autoScroll : true,
      sm : new Ext.grid.RowSelectionModel({ singleSelect : true }),
      frame : true,
      title : 'Définition',
      autoHeight : true,
      store : store,
      ddGroup : 'mygridDD',
      enableDragDrop : true,
      viewConfig : {
        forceFit : true,
        scrollOffset : 20,
        sm : new Ext.grid.RowSelectionModel({ singleSelect : true }),
        plugins : { ptype : 'gridviewdragdrop' }
      },
      columns : [{
        width : 200,
        header : "Identifiant",
        dataIndex : 'pk_modele_carto',
        sortable : false
      }, {
        header : "Nom de la carte",
        width : 300,
        dataIndex : 'modele_carto_intitule',
        editor : nom_editor,
        sortable : false
      }, {
        dataIndex : 'modele_carto_type',
        header : "Type de modèle",
        renderer : function(value){
        	var record;
        	if ( record = storeCombo2.getById(value) ){
        		return record.get('nom');
        	}
        	return value;
        },
        sortable : true,
        editor : {
        	xtype : 'combo',
          typeAhead : true,
          tpl : '<tpl for="."><div id="" ext:qtip="{type}" class="x-combo-list-item">{nom}</div></tpl>',
          triggerAction : 'all',
          store : storeCombo2,
          displayField : 'nom',
          valueField : 'type',
          //value:tabConstants[i]['prodige_settings_value'],
          // transform the data already specified in html
          lazyRender : true,
          listClass : 'x-combo-list-small'
        }
      }],
      tbar : [{
        text : 'Editer',
        icon : 'images/table_update.png',
        handler : function() {
          var sm = grid.getSelectionModel();
          var sel = sm.getSelected();
          if (sm.hasSelection()) {
            window.open(SERVICE_OPEN_MAP + "/" + sel.data.modele_uuid);
          }
        }
      }, {
        text : 'Supprimer',
        icon : 'images/table_delete.png',
        handler : function() {
          var sm = grid.getSelectionModel();
          var sel = sm.getSelected();
          if (sm.hasSelection()) {
            Ext.Msg.show({
              title : 'Supprimer le modèle',
              buttons : Ext.MessageBox.YESNO,
              msg : 'Supprimer le modèle ?',
              fn : function(btn) {
                if (btn == 'yes') {
                  Ext.MessageBox.wait("Suppression en cours...", "Modèles");
                  Ext.Ajax.request({
                    url : SERVICE_GET_MODELES_CARTO + "?service=delData",
                    params : {
                      id : sel.data.pk_modele_carto
                    },
                    success : function(resp, opt) {
                      grid.getStore().remove(sel);
                      Ext.MessageBox.hide();
                    },
                    failure : function(resp, opt) {
                      Ext.MessageBox.hide();
                      Ext.Msg.alert('Error', 'Unable to delete model');

                    }
                  });
                }
              }
            });
          };
        }
      }, {
        text : 'Ajouter',
        icon : 'images/table_add.png',
        handler : function() {
          Ext.MessageBox.wait("Création en cours...", "Modèles");
          Ext.Ajax.request({
            url : SERVICE_GET_MODELES_CARTO + "?service=addData",
            params : {},
            success : function(resp, opt) {
              var resp = setResponseObject(resp);
              grid.getStore().insert(0, new ds_model());
              grid.startEditing(0, 0);
              grid.getStore().getAt(0).set("pk_modele_carto", resp.responseObject.pk_modele_carto);
              grid.getStore().getAt(0).set("modele_carto_intitule", resp.responseObject.modele_carto_intitule);
              grid.getStore().getAt(0).set("modele_carto_type", resp.responseObject.modele_carto_type);
              grid.getStore().getAt(0).set("modele_uuid", resp.responseObject.modele_uuid);
              Ext.MessageBox.hide();
            },
            failure : function(resp, opt) {
              e.record.reject();
              Ext.MessageBox.hide();
            }
          });
        }
      }],
      listeners : {
        afteredit : function(e) {
          Ext.Ajax.request({
            url : SERVICE_GET_MODELES_CARTO + "?service=updateData",
            params : {
              id : grid.getSelectionModel().getSelected().data.pk_modele_carto,
              field : e.field,
              value : e.value
            },
            success : function(resp, opt) {
              e.record.commit();
            },
            failure : function(resp, opt) {
              e.record.reject();
            }
          });
        },
        render : {
          scope : this,
          fn : function(grid) {
            var ddrow = new Ext.dd.DropTarget(grid.container, {
              ddGroup : 'mygridDD',
              copy : false,
              notifyDrop : function(dd, e, data) {
                var ds = grid.store;
                var sm = grid.getSelectionModel()
                var rows = sm.getSelections();
                var tabModelType = new Array();
                //TODO utiliser la valeur du champ
                tabModelType["VISUALISATION"] = 1;
                tabModelType["EXTRACTION"] = 2;

                if (dd.getDragData(e)) {
                  var cindex = dd.getDragData(e).rowIndex;
                  if (typeof(cindex) != "undefined") {
                    for (i = 0; i < rows.length; i++) {
                      ds.remove(ds.getById(rows[i].id));
                    }
                    ds.insert(cindex, data.selections);
                    sm.clearSelections();
                    //Sauvegarde automatique de l'ordonnancement
                    var new_ordering = grid.getStore().getRange();
                    var new_ordering_string = "foo=foo";

                    for (var i = 0; i < new_ordering.length; i++) {
                      new_ordering_string += "#%#"
                          + new_ordering[i].data.pk_modele_carto
                          + "#%#"
                          + new_ordering[i].data.modele_carto_intitule
                          + "#%#"
                          + (typeof(tabModelType[new_ordering[i].data.modele_carto_type]) != "undefined"
                              ? tabModelType[new_ordering[i].data.modele_carto_type]
                              : new_ordering[i].data.modele_carto_type);
                    }
                    Ext.Ajax.request({
                      url : SERVICE_GET_MODELES_CARTO
                          + "?service=updateOrder",
                      params : {
                        nbrows : new_ordering.length,
                        elems : new_ordering_string
                      },
                      success : function(resp, opt) {
                      },
                      failure : function(resp, opt) {
                        Ext.Msg.alert('Error', 'Unable to reorder');
                      }
                    });
                  }
                }
              }
            })
            store.load();
          }
        }
      }
    });

    var win = new Ext.Window({
      resizable : true,
      layout : 'anchor',
      modal : false,
      autoDestroy : true,
      shadow : false,
      id : 'modele',
      constrain : true,
      height : Math.min(Ext.getBody().getViewSize().height - 10, 250),
      width : Math.min(Ext.getBody().getViewSize().width - 10, 780),
      title : "Modèles de cartes",
      items : [grid]
    });
    win.show();
    store.load();
    return;
  }

  //moteur de recherche  
  if (option == PARAMETRAGE_SEARCHENGINE || option == PARAMETRAGE_TERRITORIES) {
    // 7 Moteur de recherche
    // 10 Territoire d'extraction
    /*var bodySize = Ext.getBody().getSize();
    loadExtPopup(SERVICE_GET_PARAM_MOTEUR+"?"+(option==10 ? "table=prodige_download_param" : ""), "moteur", (option==10 ? "Territoires d'extraction" : "Moteur de recherche"), Math.min(800, bodySize.width) -100, bodySize.height-50);*/

    var search_editor;
    var win = new Ext.Window({
      closeAction : 'destroy',
      modal : false,
      autoDestroy : true,
      shadow : false,
      constrain : true,
      autoScroll : true,
      height : Math.max(Ext.getBody().getViewSize().height - 40, 250),
      width : Math.min(Ext.getBody().getViewSize().width - 10, 780),
      title : (option == PARAMETRAGE_SEARCHENGINE ? "Moteur de recherche" : "Territoires d'extraction"),
      items : [(search_editor = new Prodige.Adminsite.SearchEditor({
        nbCriteres : (option == PARAMETRAGE_SEARCHENGINE ? 8 : (option == PARAMETRAGE_TERRITORIES ? 6 : 0)),
        hasSpecificCriteria : (option == PARAMETRAGE_TERRITORIES)
      }))],
      buttons : [{
        text : 'Valider',
        handler : function() {
          var form = search_editor.getForm();
          if (form.isValid()) {
            form.submit({
              url : Routing.generate('services_get_param_moteur', {
                table : (option == PARAMETRAGE_SEARCHENGINE
                    ? 'prodige_search_param'
                    : (option == PARAMETRAGE_TERRITORIES ? 'prodige_download_param' : '')),
                service : 'submit_panel_form'
              }),
              method : 'POST',
              success : function(form, action) {
                var response = Ext.decode(action.response.responseText);
                if (response) {
                  Ext.Msg.alert('Moteur de recherche', 'configuration enregistrée');
                } else {
                  Ext.Msg.alert('Moteur de recherche', 'problème dans l\'enregistrement de la configuration');
                }
              },
              failure : function(error) {
                console.log('failure : error is : ', error);
              }
            });
          }
        }
      }],
      listeners : {
        close : function() {
          search_editor.destroy();
          this.destroy();
        }
      }
    });
    win.show();
    return;
  }
  if (option == PARAMETRAGE_TOOLS) {
    var store = new Ext.data.Store({
      url : Routing.generate('admin_getdataflow', {
        iMode : option,
        directory : path,
        serviceUrl : _SERVICE_GET_PARAMS
      }),
      reader : new Ext.data.XmlReader({
        record : 'TOOL',
        id : 'NOM',
        totalRecords : '@total'
      }, [{
        name : 'NOM',
        type : 'string',
        mapping : '@NOM'
      }, {
        name : 'ACTIF',
        type : 'bool',
        mapping : '@ACTIF'
      }])
    });
    var nom_editor = new Ext.form.TextField();
    var active_editor = new Ext.form.Checkbox();

    // the check column is created using a custom plugin
    var checkColumn = new Ext.grid.CheckColumn({
      header : 'ACTIF',
      dataIndex : 'ACTIF',
      editor : active_editor,
      width : 200
    });

    var grid = new Ext.grid.EditorGridPanel({
      sm : new Ext.grid.RowSelectionModel({ singleSelect : true }),
      frame : true,
      title : 'Choix des outils',
      autoHeight : true,
      width : 800,
      store : store,
      plugins : [checkColumn],
      columns : [{
        header : "NOM",
        width : 600,
        dataIndex : 'NOM',
        sortable : false
      }, 
        checkColumn
      ],
      listeners : {
        afteredit : function(e) {
          Ext.Ajax.request({
            url : Routing.generate('admin_updateService', {
              path : encodeURIComponent(path),
              option : option,
              serviceUrl : _SERVICE_GET_PARAMS
            }),
            params : {
              action : 'update',
              id : e.row,
              field : e.field,
              value : e.value
            },
            success : function(resp, opt) {
              e.record.commit();
            },
            failure : function(resp, opt) {
              e.record.reject();
            }
          });
        }
      }
    });

  } else {
    var readerWmxsx = [{
      name : 'NOM',
      type : 'string',
      mapping : '@NOM'
    }, {
      name : 'URL',
      type : 'string',
      mapping : '@URL'
    }, {
      name : 'WMSC_WMS_ONLINERESOURCE',
      type : 'string',
      mapping : '@WMSC_WMS_ONLINERESOURCE'
    }];
    var readerWxs = [{
      name : 'NOM',
      type : 'string',
      mapping : '@NOM'
    }, {
      name : 'URL',
      type : 'string',
      mapping : '@URL'
    }];
    var readerProj = [{
      name : 'NOM',
      type : 'string',
      mapping : '@NOM'
    }, {
      name : 'EPSG',
      type : 'string',
      mapping : '@EPSG'
    }];
    
    // create the grid
    var nom_editor = new Ext.form.TextField();
    var url_editor = new Ext.form.TextField();
    var wms_editor = new Ext.form.TextField();
    
    var store;
    
    var reader
      , columns
      , title='Liste des serveurs'
      , tbar = [{
          text : 'Ajouter',
          icon : 'images/table_add.png',
          handler : function() {
            var line = grid.getStore().data.length;
            grid.getStore().insert(line, new ds_model({
              title : 'Ajouter un serveur',
              director : '',
              genre : 0,
              tagline : ''
            }));
            grid.startEditing(line, 0);
            Ext.Ajax.request({
              url : Routing.generate('admin_updateService', {
                path : encodeURIComponent(path),
                option : option,
                serviceUrl : _SERVICE_GET_PARAMS
              }),
              params : { action : 'add' },
              success : function(resp, opt) {
              },
              failure : function(resp, opt) {
                Ext.Msg.alert('Error', 'Unable to add data');
              }
            });
          }
        }];
    switch (option){
    	case PARAMETRAGE_PROJECTIONS :
    	  title = 'Liste des projections';
    	  reader = new Ext.data.XmlReader({
          record : 'PROJECTION',
          id : 'NOM',
          totalRecords : '@total'
        }, readerProj);
        
        columns = [{
          header : "NOM",
          width : 150,
          dataIndex : 'NOM',
          sortable : true,
          editor : nom_editor
        }, {
          header : "EPSG",
          dataIndex : 'EPSG',
          sortable : true,
          editor : url_editor
        }];
        tbar = [{
          text : 'Ajouter',
          icon : 'images/table_add.png',
          handler : function(button) {
            var search_editor;
              var win;
              win = Ext.getCmp('ProjectionEditor');
              if ( win ) win.destroy();
              win = new Ext.Window({
                id : 'ProjectionEditor',
            	y : button.getPosition()[1]-50,
            	width : 600,
              closeAction : 'close',
              modal : false,
              autoDestroy : true,
              shadow : false,
              autoScroll : true,
              title : "Ajout d'une projection",
              items : [(search_editor = new Prodige.Adminsite.ProjectionEditor())],
              listeners : {
                destroy : function() {
                  search_editor && search_editor.destroy();
                  delete search_editor;
                },
                close : function() {
                  search_editor && search_editor.destroy();
                  delete search_editor;
                  this.destroy();
                  store.reload();
                }
              }
            });
            win.show();
            return;
          }
        }, {
          text : 'Modifier la projection',
          disabled : true,
          id : 'modifier_'+option,
          icon : 'images/table_update.png',
          handler : function(button) {
            var sm = grid.getSelectionModel();
            var sel = sm.getSelected();
            var index = grid.getStore();
            index = index.indexOf(sel);
            
            if (sm.hasSelection()) {
              var search_editor;
              var win;
              win = Ext.getCmp('ProjectionEditor');
              if ( win ) win.destroy();
              win = new Ext.Window({
              	id : 'ProjectionEditor',
                y : button.getPosition()[1]-50,
                closeAction : 'close',
                modal : false,
                autoDestroy : true,
                shadow : false,
                autoScroll : true,
                title : "Modifier la projection",
                items : [(search_editor = new Prodige.Adminsite.ProjectionEditor( {
                  mode : 'ModifieProjection',
                  idRow : index,
                  EPSG : sel.get('EPSG'),
                  NOM : sel.get('NOM')
                }))],
                listeners : {
                  destroy : function() {
                    search_editor && search_editor.destroy();
                    delete search_editor;
                  },
                  close : function() {
                    search_editor && search_editor.destroy();
                    delete search_editor;
                    this.destroy();
                    
                    store.on('load', function(){
                      var index = store.find('EPSG', sel.get('EPSG'));
                      if ( index!=-1 ) {
                      	sm.selectRow(index);
                      }
                    }, store, {single:true});
                    store.reload();
                  }
                }
              });
              win.show();
              return;
            }
          }
        }];
      break;
      
      case PARAMETRAGE_WMXS :
      //case PARAMETRAGE_WMTS :
        reader = new Ext.data.XmlReader({
          record : 'SERVEUR',
          id : 'NOM',
          totalRecords : '@total'
        }, readerWmxsx);
        
        columns = [{
          header : "NOM",
          width : 200,
          dataIndex : 'NOM',
          sortable : true,
          editor : nom_editor
        }, {
          header : "URL",
          width : 300,
          dataIndex : 'URL',
          sortable : true,
          editor : url_editor
        }, {
          header : "WMS Associé",
          width : 300,
          dataIndex : 'WMSC_WMS_ONLINERESOURCE',
          sortable : true,
          editor : wms_editor
        }];
      break;
      
      default : 
        reader = new Ext.data.XmlReader({
          record : 'SERVEUR',
          id : 'NOM',
          totalRecords : '@total'
        }, readerWxs);
        
        columns = [{
          header : "NOM",
          width : 200,
          dataIndex : 'NOM',
          sortable : true,
          editor : nom_editor
        }, {
          header : "URL",
          width : 600,
          dataIndex : 'URL',
          sortable : true,
          editor : url_editor
        }]
      break;
    }
    store = new Ext.data.Store({
      url : Routing.generate('admin_getdataflow', {
        iMode : option,
        directory : path,
        serviceUrl : _SERVICE_GET_PARAMS
      }),
      reader : reader
    });


    var ds_model = Ext.data.Record.create(['id', {
      name : 'NOM',
      type : 'string'
    }, {
      name : 'URL',
      type : 'string'
    }]);

    var grid = new Ext.grid.EditorGridPanel({
      autoScroll : true,
      sm : new Ext.grid.RowSelectionModel({  
        singleSelect : true, 
        listeners : {
          rowselect : function(sm, rowIndex, record){
          	var btns = ['supprimer_'+option, 'modifier_'+option];
          	btns.forEach(function(id){
          		var btn = Ext.getCmp(id);
          		if ( !btn ) return;
          		btn.setDisabled(false);
          	});
          },
          rowdeselect : function(sm, rowIndex, record){
            var btns = ['supprimer_'+option, 'modifier_'+option];
            btns.forEach(function(id){
              var btn = Ext.getCmp(id);
              if ( !btn ) return;
              btn.setDisabled(true);
            });
          } 
        } 
      }),
      frame : true,
      id : 'editorGridPanel_list',
      title : title,
      height : 300,
      store : store,
      ddGroup : 'mygridDD',
      enableDragDrop : true,
      viewConfig : {
      	forceFit : true,
      	scrollOffset : 20,
        plugins : {
          ptype : 'gridviewdragdrop'
        }
      },
      columns : columns,
      tbar : tbar.concat([{
      	id : 'supprimer_'+option,
      	disabled : true,
        text : 'Supprimer',
        icon : 'images/table_delete.png',
        handler : function() {
          var sm = grid.getSelectionModel();
          var sel = sm.getSelected();
          if (sm.hasSelection()) {
            Ext.MessageBox.minWidth = 600;
            Ext.Msg.show({
              title : 'Confirmation avant suppression',
              buttons : Ext.MessageBox.YESNO,
              msg : 'Supprimer ' + sel.data.NOM + '?',
              fn : function(btn) {
                if (btn == 'yes') {
                	var params;
                	switch (option){
                    case PARAMETRAGE_PROJECTIONS :
                      params = {
                        action : 'delete',
                        nom : sel.data.NOM,
                        epsg : sel.data.EPSG
                      };
                    break;
                    case PARAMETRAGE_WMXS :
                    //case PARAMETRAGE_WMTS :
                      params = {
                        action : 'delete',
                        nom : sel.data.NOM,
                        url : sel.data.URL,
                        wms_online : sel.data.WMSC_WMS_ONLINERESOURCE
                      };
                    break;
                    default :
                      params = {
                        action : 'delete',
                        nom : sel.data.NOM,
                        url : sel.data.URL
                      };
                    break;
                	}
                  Ext.Ajax.request({
                    url : Routing.generate('admin_updateService', {
                      path : encodeURIComponent(path),
                      option : option,
                      serviceUrl : _SERVICE_GET_PARAMS
                    }),
                    params : params,
                    success : function(resp, opt) {
                      grid.getStore().remove(sel);
                    },
                    failure : function(resp, opt) {
                      Ext.Msg.alert('Error', 'Unable to delete data');
                    }
                  });
                }
              }
            });
          };
        }
      }]) ,
      listeners : {
        afteredit : function(e) {
          Ext.Ajax.request({
            url : Routing.generate('admin_updateService', {
              path : encodeURIComponent(path),
              option : option,
              serviceUrl : _SERVICE_GET_PARAMS
            }),
            params : {
              action : 'update',
              id : e.row,
              field : e.field,
              value : e.value
            },
            success : function(resp, opt) {
              e.record.commit();
            },
            failure : function(resp, opt) {
              e.record.reject();
            }
          });
        },
        render : {
          scope : this,
          fn : function(grid) {
            var ddrow = new Ext.dd.DropTarget(grid.container, {
              ddGroup : 'mygridDD',
              copy : false,
              notifyDrop : function(dd, e, data) {
                var ds = grid.store;
                var sm = grid.getSelectionModel()
                var rows = sm.getSelections();
                if (dd.getDragData(e)) {
                  var cindex = dd.getDragData(e).rowIndex;
                  if (typeof(cindex) != "undefined") {
                    for (i = 0; i < rows.length; i++) {
                      ds.remove(ds.getById(rows[i].id));
                    }
                    ds.insert(cindex, data.selections);
                    sm.clearSelections();
                    //Sauvegarde automatique de l'ordonnancement
                    var new_ordering = grid.getStore().getRange();
                    var new_ordering_string = "foo=foo";
                    for (var i = 0; i < new_ordering.length; i++) {
                      new_ordering_string += "|NOM"
                          + i
                          + "="
                          + new_ordering[i].data.NOM
                          + (typeof(new_ordering[i].data.EPSG) != "undefined"
                              ? "|EPSG" + i + "=" + new_ordering[i].data.EPSG
                              : "|URL" + i + "=" + new_ordering[i].data.URL);
                    }
                    Ext.Ajax.request({
                      url : Routing.generate('admin_updateService', {
                        path : encodeURIComponent(path),
                        option : option,
                        serviceUrl : _SERVICE_GET_PARAMS
                      }),
                      params : {
                        action : 'rewrite_ordered_xml',
                        nbrows : new_ordering.length,
                        elems : new_ordering_string
                      },
                      success : function(resp, opt) {
                      },
                      failure : function(resp, opt) {
                        Ext.Msg.alert('Error', 'Unable to reorder');
                      }
                    });
                  }
                }
              }
            })
            store.load();
          }
        }
      }

    });
  }
  if (option == PARAMETRAGE_WMTS) {
  	var parametrage_wmts = new Prodige.Adminsite.WMTSEditor();
  }//end PARAMETRAGE_WMTS
  else {
    store.load();
  }

  // create the window on the first click and reuse on subsequent clicks
  if (!win) {
    win = new Ext.Window({
      constrain : true,
      height : Math.min(Ext.getBody().getViewSize().height - 10, 400),
      width : Math.min(Ext.getBody().getViewSize().width - 10, 840),
      closeAction : 'hide',
      plain : true,
      items : (option == PARAMETRAGE_WMTS
      ? parametrage_wmts
      : new Ext.TabPanel({
          autoTabs : true,
          activeTab : 0,
          deferredRender : false,
          border : false,
          items : [grid]
        })
      ),

      buttons : [{
        text : 'Fermer',
        handler : function() {
          win.hide();
        }
      }]
    });
  }
  win.show();

}

function getSubarray(array, indexArray) {
  var result = new Array();
  var j = 0;
  for (var i = 0; i < array.length; i++) {
    if (indexArray.indexOf(array[i][0]) >= 0) {
      result[j] = new Array();
      result[j]['key'] = array[i][0];
      result[j]['name'] = array[i][1];
      j++;
    }
  }
  return result;
}

function majListFields() {

  var ctrlTableName = Ext.getCmp("PRO_FLUXATOM_COUCHE_TERRITOIRE");
  var ctrlFieldId = Ext.getCmp("PRO_FLUXATOM_FIELD_ID_COUCHE_TERRITOIRE");
  var ctrlFieldLabel = Ext.getCmp("PRO_FLUXATOM_FIELD_LABEL");
  var queryParams = {
    tableName : ctrlTableName.getValue()
  }
  var Tab = new Array();
  Ext.Ajax.request({
    method : 'GET',
    url : SERVICE_GET_FIELD_LIST_CARTO,
    success : function(response) {
      response = Ext.decode(response.responseText);

      ctrlFieldId.emptyText = 'Sélectionnez';
      ctrlFieldLabel.emptyText = 'Sélectionnez';
      ctrlFieldId.clearValue();
      ctrlFieldLabel.clearValue();
      for (id in response) {
        Tab.push([response[id], response[id]]);
      }

      ctrlFieldId.getStore().removeAll();
      ctrlFieldId.getStore().loadData(Tab);
      ctrlFieldId.applyEmptyText();
      ctrlFieldLabel.getStore().removeAll();
      ctrlFieldLabel.getStore().loadData(Tab);
      ctrlFieldLabel.applyEmptyText();

    },
    failure : function(response) {

      Ext.Msg.alert("Ajouter un service", "Aucun résultat ne correspond à votre recherche.");

    },
    params : queryParams
  })
}
