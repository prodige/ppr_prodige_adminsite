var scriptName  = './remote_add_user_carmen.sh'; 


Ext.onReady(function() {
  Ext.QuickTips.init();
  var win;

  // auto fill of login/directory/banner field
  var fillField = function(field, newVal, oldVal) {
    var login_f = Ext.getCmp('cmn-field-login');
    var banner_f = Ext.getCmp('cmn-field-banner');
    login_f.setValue('Admin_' + newVal);
    banner_f.setValue(newVal);
  };

  var scriptInfo = function(f, a) {
    if (!win) {
      var login_f = Ext.getCmp('cmn-field-login');
      var dir_f = Ext.getCmp('cmn-field-directory');
      var account_f = Ext.getCmp('cmn-field-accountName');
      var login = login_f.getValue();
      var dir = dir_f.getValue();
      var account = account_f.getValue();

      win = new Ext.Window({
            width : Math.min(Ext.getBody().getViewSize().width - 10, 400),
            constrain : true,
            autoHeight : true,
            closeAction : 'hide',
            plain : true,
            items : new Ext.TabPanel({
              autoTabs : true,
              activeTab : 0,
              deferredRender : false,
              border : false,
              html : '<h1>Etape 1 <br>Création du compte <b>' + account
                  + '</b></h1><br><p>Lancement du script ' + scriptName
                  + '......</p>' + '<div><b>Login :</b> ' + login
                  + '</div>' + '<div><b>Répertoire :</b> ' + dir
                  + '</div><br><br>'
            }),
            buttons : [{
              text : 'Terminé',
              id : 'cmn-btn-finish',
              type : 'submit',
              disabled : true,
              handler : function() {
                win.close();
                win = null;
                carmen_form.getForm().reset();
                carmen_form.getForm().render();
                gridTotal.getStore().reload();
                gridTotal.render();
                gridTotal.show();
              }
            }]
          });
    }

    win.show();

    carmen_form.getForm().submit({
      success : function(f, a) {
        var btnFinish = Ext.getCmp('cmn-btn-finish');

        // if (btnFinish==null)
        // alert("btnFinish est a null");
        // else
        if (btnFinish != null)
          btnFinish.enable();
      },
      failure : function(f, a) {
        Ext.Msg.alert('Erreur', a.result.errormsg);
        win.close();
      }
    });

  };

  // Formulaire compte
  var carmen_form = new Ext.FormPanel({
    url : 'services/getservices.php?iMode=0',
    frame : true,
    title : 'Création des utilisateurs',
    autoHeight : true,
    labelWidth : 120,
    items : [{
      xtype : 'textfield',
      fieldLabel : 'Nom du compte *',
      width : 400,
      allowBlank : false,
      name : 'compteName',
      id : 'cmn-field-accountName'// ,
        // vtype : 'alphanum'
      }, {
      xtype : 'textfield',
      fieldLabel : 'Répertoire *',
      width : 400,
      allowBlank : false,
      name : 'repert',
      id : 'cmn-field-directory',
      vtype : 'alphanum',
      listeners : {
        'change' : {
          fn : fillField,
          scope : this,
          delay : 100
        }
      }
    }, {
      xtype : 'textfield',
      fieldLabel : 'Login adhérent ',
      width : 400,
      allowBlank : true,
      name : 'login',
      id : 'cmn-field-login',
      disabled : false,
      fieldClass : 'x-item-disabled',
      readOnly : true
        // ,vtype : 'alphanum'
      }, {
      xtype : 'textfield',
      fieldLabel : 'Bandeau ',
      width : 400,
      allowBlank : true,
      name : 'bandeau',
      id : 'cmn-field-banner',
      disabled : false,
      fieldClass : 'x-item-disabled',
      readOnly : true
        // ,vtype : 'alphanum'
      }, {
      xtype : 'textfield',
      fieldLabel : 'Mot de passe * ',
      inputType : 'password',
      allowBlank : false,
      name : 'pwd',
      id : 'cmn-field-pwd'
    }, {
      xtype : 'combo',
      tpl : '<tpl for="."><div id="" ext:qtip="{label}" class="x-combo-list-item">{label}</div></tpl>',
      id : 'comboDNS',
      name : 'dns',
      fieldLabel : 'Nom du domaine',
      editable : false,
      loadingText : 'Chargement',
      emptyText : '',
      typeAhead : true,
      triggerAction : 'all',
      minChars : 1,
      queryDelay : 250,
      mode : 'local',
      store : new Ext.data.SimpleStore({
            fields : ['qstring1', 'label'],
            data : tabDNSStore
          }),
      displayField : 'label',
      valueField : 'qstring1',
      width : 200
    }

    ],
    buttons : [{
          text : 'Créer',
          handler : scriptInfo
        }]
  });
// SERVICE_GET_TABLELIST_CARTO : Service supprimé


// displayWindow('', 8);

  // hismail
  // create the Data Store
  var storeTotal = new Ext.data.Store({
    // proxy: new Ext.data.ScriptTagProxy({url:
    // 'services/getdataflow.php?iMode=0'+(typeof(SERVICE_GET_PARAMS)!="undefined"
    // ? '&serviceUrl='+SERVICE_GET_PARAMS : '')}),
    proxy : new Ext.data.HttpProxy({
      url : Routing.generate('admin_getdataflow', {
        iMode : 0,
        serviceUrl : (typeof(SERVICE_GET_PARAMS) != "undefined"
            ? encodeURIComponent(SERVICE_GET_PARAMS)
            : null)
      })
    }),
    reader : new Ext.data.XmlReader({
      record : 'service',
      id : 'idx',
      totalRecords : '@total'
    }, [{
      name : 'name',
      type : 'string',
      mapping : '@name'
    }, {
      name : 'path',
      type : 'string',
      mapping : '@path'
    }, {
      name : 'login',
      type : 'string',
      mapping : '@login'
    }, {
      name : 'banner',
      type : 'string',
      mapping : '@banner'
    }, {
      name : 'dns',
      type : 'string',
      mapping : '@dns'
    }])
  });

  // create the grid
  var tabColumns = new Array();
  tabColumns.push({
    header : "Nom du Compte",
    width : 200,
    dataIndex : 'name',
    align : 'left'
  });
  if (typeof(MODE_PRODIGE) == "undefined")
    tabColumns.push({
      header : "Répertoire",
      width : 200,
      dataIndex : 'path'
    }, {
      header : "Login",
      width : 200,
      dataIndex : 'login'
    }, {
      header : "Bandeau",
      width : 200,
      dataIndex : 'banner'
    }, {
      header : "DNS",
      width : 200,
      dataIndex : 'dns'
    }, {
      header : "OUTILS ADMIN",
      renderer : function(v, params, record) {
        return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',5);" >';
      }
    });
  else {
    tabColumns.push({
      header : "PARAMETRAGE CATALOGUE",
      renderer : function(v, params, record) {
        return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',8);" >';
      }
    }, {
      header : "PARAMETRAGE CARTO",
      renderer : function(v, params, record) {
        return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',9);" >';
      }
    }, {
      header : "PARAMETRAGE AUTOMATE",
      renderer : function(v, params, record) {
        return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',13);" >';
      }
    },
    // Suite à la suppression du service :
    // SERVICE_GET_PARAMETRAGE_EXTERNAL_ACCESS
    // {header: "ACCES EXTERNES", width: 100, dataIndex: 'path',renderer:
    // function(v,params,record){return '<img src="/images/extjs_update.png"
    // onclick="displayWindow(\''+v+'\',11);" >';}, sortable: true},
    {
      header : "MOTEUR DE RECHERCHE",
      renderer : function(v, params, record) {
        return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',7);" >';
      }
    }, {
      header : "TERRITOIRE D'EXTRACTION",
      renderer : function(v, params, record) {
        return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',10);" >';
      }
    }, {
      header : "MODELES DE CARTES",
      renderer : function(v, params, record) {
        return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',12);" >';
      }
    }, {
      header : "FLUX ATOM",
      renderer : function(v, params, record) {
        return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',14);" >';
      }
    });
  }
  tabColumns.push(

      // {header: "OUTILS CONSULT", width: 100, dataIndex: 'path',renderer:
      // function(v,params,record){return '<img src="/images/extjs_update.png"
      // onclick="displayWindow(\''+v+'\',13);" >';}, sortable: true},
      //{header: "WFS", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',1);" >';}, sortable: true},
      //{header: "WMS", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',2);" >';} , sortable: true},
      //{header: "WMS-C", width: 100,  dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',6);" >';} , sortable: true},
      {
    header : "WMTS",
    renderer : function(v, params, record) {
      return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',3);" >';
    }
  }, {
    header : "PARAMETRAGE PROJECTION",
    renderer : function(v, params, record) {
      return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',4);" >';
    }
  }, {
    header : "SERVEURS WxS",
    renderer : function(v, params, record) {
      return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',15);" >';
    }
  },
    {
      header : "TELECHARGEMENT DIFFERE",
      renderer : function(v, params, record) {
        return '<img src="/images/extjs_update.png" onclick="displayWindow(\'' + v + '\',16);" >';
    }
  });
  
  storeTotal.load();
  var gridTotal = new Ext.grid.GridPanel({
    frame : true,
    title : 'Liste des serveurs',
    height : 200,
    width : 'auto',
    store : storeTotal,
    viewConfig : {
      forceFit : true
    },
    colModel : new Ext.grid.ColumnModel({
      columns : tabColumns,
      defaults : {
        dataIndex : 'path',
        width : 100,
        sortable : true,
        menuDisabled : true,
        align : 'center'
      }
    })
  });

  var tabPanels = new Array();
  tabPanels.push(gridTotal);
  if (typeof(MODE_PRODIGE) == "undefined")
    tabPanels.push(carmen_form);
  else {
    tabPanels.push(suiviActivite());
    // tabPanels.push(suiviSGBD);
    // tabPanels.push(suiviSGBDCarto);
  }

  // Tableau des onglets
  var viewport = new Ext.Viewport({
    layout : 'border',
    renderTo : Ext.getBody(),
    items : [{
      region : 'center',
      xtype : 'tabpanel',
      activeTab : 0,
      items : tabPanels
    }]
  });
});


var tabExtPopup = new Array();
var popupWindowGroup = new Ext.WindowGroup();
Ext.namespace('Ext.ux.plugins');

/**
 * Ouverture de popups
 */
function loadExtPopup(url, cible, title, width, height){
  
  if ( !width || width==0 ) {
    width = 400;
  }
  if ( !height || height==0 ) {
    height = 300;
  }
  var popup_position = tabExtPopup.length ;
  var err1 = "La fenêtre est déjà ouverte !";
  
  
  try{
    for (var i=0; i<top.frames.length; i++){
      if (top.frames[i].name==cible){
        throw err1;
      }
    }
    var url = url+"&target="+cible+"&popup_pos="+popup_position;
    if (tabExtPopup[popup_position]){
      tabExtPopup[popup_position].close();
    }
    /** extjs 3 * */
    tabExtPopup[popup_position] = new Ext.ux.ManagedIFrame.Window({
      title         : title,
      width         : Math.min(Ext.getBody().getViewSize().width-10,eval(width)),
      height        : Math.min(Ext.getBody().getViewSize().height-10,eval(height)),
      maximizable   : true,
      collapsible   : true,
      id            : cible,
      constrain     : true,
      closable      : true,
      shadow        : Ext.isIE,
      animCollapse  : false,
      autoScroll    : true,
      manager       : popupWindowGroup,
      hideMode      : 'nosize',
      defaultSrc    : url,
      loadMask      :  {msg:'Chargement...'},
      close         : function(){closeExtPopup(popup_position);},// 'close'
      closeAction   : 'close'
    });

    tabExtPopup[popup_position].show();

    if (tabExtPopup[popup_position].getFrameWindow()){
     if (window.frames[cible]){
        // ext js does'nt destroy the window.frames[cible] when destroying
        // tabExtPopup[popup_position]
        // so if this iframe already exists we remplace it with the new one
        window.frames[cible]=tabExtPopup[popup_position].getFrameWindow();  
     }
     tabExtPopup[popup_position].getFrameWindow().name = cible; 
     
    }
    
  }
  catch(e){
    if (e == err1){
      alert(err1);
    }
    else{
      throw e;
    }
  }
}
/**
 * Fermeture de la popup
 */
function closeExtPopup(popup_position){
  if (tabExtPopup[popup_position]!=null){
    tabExtPopup[popup_position].destroy();
    tabExtPopup[popup_position]=null;    
  }
}

function showWindowView(service, title, viewName, bCanDelete, additionnalRequest){
  
  var fields = new Array();
  var tabColumns = new Array();
  
  Ext.Ajax.request({
    url: service,
    method: 'GET',
    params: { 
      service: 'getFields',
      view : viewName,
      additionnalRequest :additionnalRequest
    },
    success: function(response) {
      fields = setResponseObject(response).responseObject;
      if(!is_array(fields)){
        Ext.MessageBox.alert('Exécution', fields); 
        return;
      }
      var store = new Ext.data.JsonStore({
        url: service+'?service=getData&view='+viewName+(typeof(additionnalRequest)!="undefined" ?'&additionnalRequest='+ additionnalRequest : ''),      
        fields: fields,
        listeners: {
          load: function(){Ext.fly("window_addWFS").unmask()}
        }
      });     
      if(bCanDelete){
        var itemDeleter = new Extensive.grid.ItemDeleter();
        tabColumns.push(itemDeleter);
      }
      var bodySize = Ext.getBody().getSize();
      var columnWidth = bodySize.width/fields.length-35;
      for(var i=0;i<fields.length;i++){
        tabColumns.push({id: "fields_"+i, header: fields[i], width: columnWidth, sortable: true, dataIndex: fields[i]});
      }
      
      // create the Grid
      var grid = new Ext.grid.GridPanel({
          store: store,
          columns: tabColumns,
          stripeRows: true,
          //autoWidth : true,
          width: '100%',      
          height:      bodySize.height-50,       
          view: new Ext.ux.grid.BufferView({
              // render rows as they come into viewable area.
              scrollDelay: false,
              //cleanDelay: 200,
              emptyText : 'Aucune donnée trouvée',
              deferEmptyText : false,
              forceFit: true,
              showPreview: true, // custom property
              enableRowBody: true // required to create a second, full-width row to show expanded Record data

          }),
          
          deleteAction : deleteFromView,
          viewName : viewName,
          selModel: (bCanDelete ? itemDeleter : new Ext.grid.RowSelectionModel)
          
      });
      
      var win = new Ext.Window({   
      	layout : 'fit',
        id         :"window_addWFS",
        resizable  : true,
        title      : title,
        width      : Math.min(Ext.getBody().getViewSize().width-10, 1020),
        height     : 'auto',
        constrain  : true,
        autoScroll : true,
        items      : [grid]
      });
      
      win.show();
      grid.render('db-grid');
      Ext.fly("window_addWFS").mask("Récupération des données en cours...", 'x-mask-loading');
      
      // load data
      store.load();
    },
    failure: function(response){
      Ext.fly("window_addWFS").unmask();
      Ext.MessageBox.alert('Failed', response); 
    }
  }
  );
}

function showWindowViewDiffPC(service, title, bCanDelete){
	  
	  var fields = new Array();
	  var tabColumns = new Array();
	  fields.push('Table');
      var store = new Ext.data.JsonStore({
        url: service+'?service=getDataProdiCatalo&column=Table',     
        fields: fields,
        listeners: {
          load: function(){Ext.fly("window_addWFS").unmask()}
        }
      });     
      if(bCanDelete){
        var itemDeleter = new Extensive.grid.ItemDeleter();
        tabColumns.push(itemDeleter);
      }
      var bodySize = Ext.getBody().getSize();
      var columnWidth = bodySize.width/fields.length-35;
      for(var i=0;i<fields.length;i++){
	        tabColumns.push({id: "fields_"+i, header: fields[i], width: columnWidth, sortable: true, dataIndex: fields[i]});
      }
      
      // create the Grid
      var grid = new Ext.grid.GridPanel({
          store: store,
          columns: tabColumns,
          stripeRows: true,          
          view: new Ext.ux.grid.BufferView({
              // render rows as they come into viewable area.
              scrollDelay: false,
              //cleanDelay: 200,
              
              emptyText : 'Aucune donnée trouvée',
              deferEmptyText : false,
              forceFit: true,
              showPreview: true, // custom property
              enableRowBody: true // required to create a second, full-width row to show expanded Record data

          }),
          
          deleteAction : deleteFromView,
          viewName : 'getDataProdiCatalo',
          height:      bodySize.height-50,
          selModel: (bCanDelete ? itemDeleter : new Ext.grid.RowSelectionModel)
          
      });
      
      var win = new Ext.Window({          
        id         :"window_addWFS",
        resizable  : true,
        title      : title,
        width      : Math.min(Ext.getBody().getViewSize().width-10, 1020),
        height     : 'auto',
        constrain  : true,
        autoScroll : true,
        items      : [grid]
      });
      
      win.show();
      grid.render('db-grid');
      Ext.fly("window_addWFS").mask("Récupération des données en cours...", 'x-mask-loading');
      
      // load data
      store.load();
}




function is_array(input){
  return typeof(input)=='object'&&(input instanceof Array);
}

function deleteFromView(record, viewName){
  var isAll = false;
  if(record=="all"){
    idValue = "all";
    isAll = true;
  }else{
    switch(viewName){
      case "incoherence_couche_fiche_metadonnees":
        idValue = record.data["pk_couche_donnees"];
      break;
      case "incoherence_carte_fiche_metadonnees":
        idValue = record.data["pk_carte_projet"];
      break;
      case "incoherence_fiche_metadonnees_metadata" :
        idValue = record.data["pk_fiche_metadonnees"];
      break;
      case "incoherence_metadata_fiche_metadonnees" :
        idValue = record.data["id"];
      break;  
      case "getDataProdiCatalo" :
          idValue = record.data["Table"];
        break;  
      case "incoherence_couche_sous_domaines" :
      	idValue = record.data["pk_couche_donnees"];
      break;  
      case "incoherence_carte_sous_domaines" :
      	idValue = record.data["pk_carte_projet"];
      	break;
      case "incoherence_fiche_metadonnees_dom_ssdom" :
       	idValue = record.data["id"];
      break;  
    }
  }
  Ext.Ajax.request({
    url: SERVICE_GET_VIEWS,
    method: 'GET',
    params: { 
      service : "deleteData",
      viewName : viewName,
      isAll : isAll,
      idValue : idValue
    },
    success: function(response) {
      Ext.MessageBox.alert('Suppression d\'incohérence', 'Suppression effectuée'+(viewName == "incoherence_metadata_fiche_metadonnees" ? 
                           '.Une réindexation de Géosource est nécessaire' : '') ); 
    },
    failure: function(response){
      //  console.log(response); 
      Ext.MessageBox.alert('Failed', response.statusText); 
    }
  });
  
}

/**
 * export au format excel le résultat de la requete
 * @return
 */
function exportExcel(strSql, service){
  try {
    Ext.destroy(Ext.get('downloadIframe'));
  }
  catch(e) {}
  console.log(strSql);
  //Ext.fly("window_addWFS").mask("Génération du fichier en cours...", 'x-mask-loading');
  Ext.DomHelper.append(document.body, {
    tag: 'iframe',
    id:'downloadIframe',
    frameBorder: 0,
   // onload : 'Ext.fly("window_addWFS").unmask()',
    width: 0,
    height: 0,
    css: 'display:none;visibility:hidden;height:0px;',
    src: service+'?service=exportData&additionnalRequest='+escape(strSql)
  });
}


function suiviActivite() {
	var buttonText = "Consulter";
  var suiviActiviteItem = [];
  suiviActiviteItem.push({
    xtype : 'label',
    fieldLabel : '<b>Suivi des incohérences</b>',
    allowBlank : false,
    name : 'incoherences'
      // vtype : 'alphanum'
    });
  suiviActiviteItem.push({
    xtype : 'button',
    fieldLabel : 'Cartes sans fiches métadonnées',
    text : buttonText,
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Cartes sans fiches métadonnées <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from incoherence_carte_fiche_metadonnees", "'
              + SERVICE_GET_DATA + '");\'/>',
          'incoherence_carte_fiche_metadonnees', true);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Couches sans fiches métadonnées',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Couches sans fiches métadonnées <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from incoherence_couche_fiche_metadonnees", "'
              + SERVICE_GET_DATA + '");\'/>',
          'incoherence_couche_fiche_metadonnees', true);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Fiches de métadonnées (catalogue) sans métadonnées Géosource',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Fiches de métadonnées (catalogue) sans métadonnées Géosource <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from incoherence_fiche_metadonnees_metadata", "'
              + SERVICE_GET_DATA + '");\'/>',
          'incoherence_fiche_metadonnees_metadata', true);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Couches non rattachées à un sous-domaine',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Couches non rattachées à un sous-domaine <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from incoherence_couche_sous_domaines", "'
              + SERVICE_GET_DATA + '");\'/>',
          'incoherence_couche_sous_domaines', true);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Cartes non rattachées à un sous-domaine',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Cartes non rattachées à un sous-domaine <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from incoherence_carte_sous_domaines", "'
              + SERVICE_GET_DATA + '");\'/>',
          'incoherence_carte_sous_domaines', true);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Données présentes en base PRODIGE mais absentes du catalogue',
    handler : function() {
      showWindowViewDiffPC(
          SERVICE_GET_VIEWS,
          'Données présentes en base PRODIGE mais absentes du catalogue <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("getDataProdiCatalo", "'
              + SERVICE_GET_DATA + '");\'/>', true);
    },
    width : 80
  });

  suiviActiviteItem.push({
    xtype : 'panel', height : 15, html : ''
  });
  suiviActiviteItem.push({
    xtype : 'label',
    fieldLabel : '<b>Statistiques</b>',
    allowBlank : false,
    name : 'statistiques'
      // vtype : 'alphanum'
    });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Liste des couches par producteur, date',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Liste des couches par producteur, date <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from statistique_liste_metadonnees_prod_date_couche", "'
              + SERVICE_GET_DATA + '");\'/>',
          'statistique_liste_metadonnees_prod_date_couche', false);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Nombre de couches publiées par domaine',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Nombre de couches publiées par domaine <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from statistique_nombre_couches_publiees_domaine", "'
              + SERVICE_GET_DATA + '");\'/>',
          'statistique_nombre_couches_publiees_domaine', false);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Nombre de couches publiées par sous-domaine',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Nombre de couches publiées par sous-domaine <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from statistique_nombre_couches_publiees_sousdomaine", "'
              + SERVICE_GET_DATA + '");\'/>',
          'statistique_nombre_couches_publiees_sousdomaine', false);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Liste des  couches publiées',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Liste des couches publiées <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from statistique_liste_couche_publiees", "'
              + SERVICE_GET_DATA + '");\'/>',
          'statistique_liste_couche_publiees', false);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Liste des couches publiées depuis une date',
    handler : function() {
      var date = new Ext.form.DateField({
            id : 'date',
            fieldLabel : 'Date',
            format : 'd/m/Y',
            allowBlank : false
          });
      var win = new Ext.Window({
        resizable : true,
        title : "Choix de la date",
        width : Math.min(Ext.getBody().getViewSize().width - 10, 150),
        height : 'auto',
        constrain : true,
        items : [date],
        buttons : [{
          text : 'Valider',
          id : 'cmn-btn-finish',
          type : 'submit',
          handler : function() {
            showWindowView(
                SERVICE_GET_VIEWS,
                'Liste des couches publiées <img src="/images/excel.gif" class="x-btn" onclick="javascript:exportExcel(\'select * from statistique_liste_couche_publiees where to_date(changedate, \\\'YYYY-MM-DD\\\')  > to_date(\\\''
                    + Ext.getCmp('date').value
                    + '\\\', \\\'DD/MM/YYYY\\\') \', \''
                    + SERVICE_GET_DATA
                    + '\');"/>', 'statistique_liste_couche_publiees', false,
                "to_date(changedate, 'YYYY-MM-DD')  > to_date('"
                    + Ext.getCmp("date").value + "', 'DD/MM/YYYY') ");
          }
        }]
      });
      win.show();

    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Nombre de cartes publiées par domaine',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Nombre de cartes publiées par domaine <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from statistique_nombre_cartes_publiees_domaine", "'
              + SERVICE_GET_DATA + '");\'/>',
          'statistique_nombre_cartes_publiees_domaine', false);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Nombre de cartes publiées par sous-domaine',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Nombre de cartes publiées par sous-domaine <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from statistique_nombre_cartes_publiees_sousdomaine", "'
              + SERVICE_GET_DATA + '");\'/>',
          'statistique_nombre_cartes_publiees_sousdomaine', false);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Liste des  cartes publiées',
    handler : function() {
      showWindowView(
          SERVICE_GET_VIEWS,
          'Liste des cartes publiées <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from statistique_liste_carte_publiees", "'
              + SERVICE_GET_DATA + '");\'/>',
          'statistique_liste_carte_publiees', false);
    },
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Liste des cartes publiées depuis une date',
    handler : function() {
      var date = new Ext.form.DateField({
        id : 'date',
        fieldLabel : 'Date',
        format : 'd/m/Y',
        allowBlank : false
      });

      var win = new Ext.Window({
        resizable : true,
        title : "Choix de la date",
        width : Math.min(Ext.getBody().getViewSize().width - 10, 150),
        height : 'auto',
        constrain : true,
        items : [date],
        buttons : [{
          text : 'Valider',
          id : 'cmn-btn-finish',
          type : 'submit',
          handler : function() {
            showWindowView(
                SERVICE_GET_VIEWS,
                'Liste des cartes publiées <img src="/images/excel.gif" class="x-btn" onclick="javascript:exportExcel(\'select * from statistique_liste_carte_publiees where to_date(changedate, \\\'YYYY-MM-DD\\\')  > to_date(\\\''
                    + Ext.getCmp('date').value
                    + '\\\', \\\'DD/MM/YYYY\\\') \', \''
                    + SERVICE_GET_DATA
                    + '\');"/>', 'statistique_liste_carte_publiees', false,
                "to_date(changedate, 'YYYY-MM-DD')  > to_date('"
                    + Ext.getCmp("date").value + "', 'DD/MM/YYYY') ");
          }
        }]
      });
      win.show();
    },
    width : 80
  });
  suiviActiviteItem.push({
  	xtype : 'panel', height : 15, html : ''
  });
  suiviActiviteItem.push({
    xtype : 'button',
    labelWidth : 150,
    fieldLabel : '<b>Exports</b>',
    allowBlank : false,
    name : 'exports',
    text : '<b>Purger</b>',
    handler : function() {
      Ext.MessageBox.confirm('Confirmer',
      'Confirmer la suppression des fichiers de logs ?', function(btn) {
        if (btn == 'yes') {
          Ext.Ajax.request({
            url : SERVICE_PURGE_EXPORTS,
            method : 'GET',
            success : function(response) {
              Ext.MessageBox.alert('Purge des exports', 'les fichiers de logs ont été effacés');
            },
            failure : function(response) {
              Ext.MessageBox.alert('Failed', response);
            }
          });
        }
      });
    },
    width : 80
  });
  
  var downloadHandler = function(category){
    try {
      Ext.destroy(Ext.get('downloadIframe'));
    } catch (e) {
    }
    Ext.DomHelper.append(document.body, {
      tag : 'iframe',
      id : 'downloadIframe',
      frameBorder : 0,
      width : 0,
      height : 0,
      css : 'display:none;visibility:hidden;height:0px;',
      src : Routing.generate('services_get_log', {
        'categorie' : category
      }),
    });
  };
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Connexions',
    handler : downloadHandler.createCallback('Connexions'),
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Téléchargements',
    handler : downloadHandler.createCallback('Telechargements'),
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Consultation de métadonnées',
    handler : downloadHandler.createCallback('ConsultationMetadonnees'),
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Consultation de cartes',
    handler : downloadHandler.createCallback('ConsultationCarte'),
    width : 80
  });

  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Nombre de visiteurs',
    handler : downloadHandler.createCallback('Sessions'),
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Consultation WFS',
    handler : downloadHandler.createCallback('WFS'),
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Consultation WMS',
    handler : downloadHandler.createCallback('WMS'),
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Consultation WMSC',
    handler : downloadHandler.createCallback('WMSC'),
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Consultation WMTS',
    handler : downloadHandler.createCallback('WMTS'),
    width : 80
  });
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Logs API',
    handler : downloadHandler.createCallback('apiupdate'),
    width : 80
  });
  
  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Logs Publipostage',
    handler : downloadHandler.createCallback('mailing'),
    width : 80
  });

  suiviActiviteItem.push({
    xtype : 'button',
    text : buttonText,
    fieldLabel : 'Logs CGU',
    handler : downloadHandler.createCallback('cgu'),
    width : 80
  });

  // Partie délpacé par hismail
  // Avant : Dans le "success" de la requete Ajax appelant le service SERVICE_GET_REQUEST supprimé
  var suiviActivite = new Ext.FormPanel({
    url : 'services/getservices.php?iMode=0',
    frame : true,
    title : 'Suivi d\'activité',
    //autoHeight : true,
    labelWidth : 400,
    labelSeparator : '&nbsp;',
    autoScroll : true,
    items : suiviActiviteItem
  });
  return suiviActivite;
}

function _fonctionnalites_supprimees() {
  return null;

/*
  // suiviSGBD : formPanel supprimé
  var suiviSGBD = new Ext.FormPanel({
    url : '',
    frame : true,
    title : 'Suivi de la base de données Catalogue',
    autoHeight : true,
    labelWidth : 300,
    items : [{
      xtype : 'label',
      fieldLabel : '<b>Consultation de l\'arborescence de la base de données</b>',
      allowBlank : false,
      name : 'arbo_sgbd'
        // vtype : 'alphanum'
    }, {
      // column layout with 2 columns
      xtype : 'compositefield',
      fieldLabel : 'Liste des tables',
      // defaults for columns
      items : [{
        // left column
        // defaults for fields
        xtype : 'combo',
        tpl : '<tpl for="."><div id="" ext:qtip="{name}" class="x-combo-list-item">{name}</div></tpl>',
        id : 'comboTables',
        name : 'tables',
        editable : false,
        loadingText : 'Chargement',
        emptyText : '',
        typeAhead : true,
        triggerAction : 'all',
        minChars : 1,
        queryDelay : 250,
        store : storeTableList,
        displayField : 'name',
        valueField : 'name',
        width : 200,
        mode : 'local'
      }, {
        xtype : 'button',
        text : 'Structure',
        cls : 'buttoncls',
        handler : function() {
          // SERVICE_GET_STRUCTRE : Service supprimé
          if (Ext.getCmp("comboTables").getValue() != "") {
            showWindowView(SERVICE_GET_STRUCTRE, 'Structure de la table', Ext
                    .getCmp("comboTables").value, false);
          } else {
            Ext.MessageBox.alert('Suppression',
                "Veuillez sélectionner au préalable une table");
          }
        },
        width : 80
      }, {
        xtype : 'button',
        text : 'Contenu',
        cls : 'buttoncls',
        handler : function() {
          if (Ext.getCmp("comboTables").getValue() != "") {
            showWindowView(SERVICE_GET_DATA, 'Données de la table', Ext
                    .getCmp("comboTables").value, false, "select * from \""
                    + Ext.getCmp("comboTables").value + "\"");
          } else {
            Ext.MessageBox.alert('Suppression',
                "Veuillez sélectionner au préalable une table");
          }
        },
        width : 80
      }

      ]

    }, {
      xtype : 'compositefield',
      fieldLabel : 'Liste des vues',

      items : [{
        xtype : 'combo',
        tpl : '<tpl for="."><div id="" ext:qtip="{name}" class="x-combo-list-item">{name}</div></tpl>',
        id : 'comboViews',
        name : 'vues',
        editable : false,
        loadingText : 'Chargement',
        emptyText : '',
        typeAhead : true,
        triggerAction : 'all',
        minChars : 1,
        queryDelay : 250,
        width : 200,
        store : storeViewList,
        displayField : 'name',
        valueField : 'name',
        mode : 'local'
      }, {
        xtype : 'button',
        text : 'Structure',
        cls : 'buttoncls',
        handler : function() {
          // SERVICE_GET_STRUCTRE : Service supprimé
          if (Ext.getCmp("comboViews").getValue() != "") {
            showWindowView(SERVICE_GET_STRUCTRE, 'Structure de la vue', Ext
                    .getCmp("comboViews").value, false);
          } else {
            Ext.MessageBox.alert('Suppression',
                "Veuillez sélectionner au préalable une vue");
          }
        },
        width : 80
      }, {
        // right column
        // defaults for fields
        xtype : 'button',
        text : 'Contenu',
        cls : 'buttoncls',
        handler : function() {
          if (Ext.getCmp("comboViews").getValue() != "") {
            showWindowView(SERVICE_GET_DATA, 'Données de la vue', Ext
                    .getCmp("comboViews").value, false, "select * from \""
                    + Ext.getCmp("comboViews").value + "\"");
          } else {
            Ext.MessageBox.alert('Suppression',
                "Veuillez sélectionner au préalable une vue");
          }
        },
        width : 80
      }]
    }, {
      xtype : 'label',
      fieldLabel : '<b>Exécution de requêtes</b>',
      allowBlank : false,
      name : 'request_exec'
        // vtype : 'alphanum'
      }, {
      // column layout with 2 columns
      layout : 'column'
      // defaults for columns
      ,
      defaults : {
        columnWidth : 0.5,
        layout : 'form',
        border : false,
        xtype : 'panel'
      },
      items : [{
        // left column
        // defaults for fields
        defaults : {
          anchor : '100%'
        },
        labelWidth : 300,
        items : [{
          xtype : 'combo',
          tpl : '<tpl for="."><div id="" ext:qtip="{name}" class="x-combo-list-item">{name}</div></tpl>',
          id : 'comboRequest',
          name : 'comboRequest',
          fieldLabel : 'Requêtes sauvegardées',
          editable : false,
          loadingText : 'Chargement',
          emptyText : '',
          typeAhead : true,
          triggerAction : 'all',
          minChars : 1,
          queryDelay : 250,
          store : storeRequestCatalogue,
          displayField : 'name',
          valueField : 'request',
          width : 200,
          mode : 'local',
          listeners : {
            select : function() {
              var tabParam = Ext.getCmp("comboRequest").getValue().split("|");
              Ext.getCmp("request").setValue(tabParam[0]);
            }
          }
        }, {
          xtype : 'textarea',
          fieldLabel : 'Requête',
          name : 'request',
          height : '90px',
          id : 'request'
        }]

      }, {
        // right column
        // defaults for fields
        defaults : {
          anchor : '20%'
        },
        items : [{
          xtype : 'button',
          cls : 'buttoncls',
          text : 'Supprimer',
          handler : function() {
            if (Ext.getCmp("comboRequest").getValue() != "") {
              Ext.MessageBox.confirm('Confirmer',
                  'Confirmer la suppression de la requête ?', function(btn) {
                    if (btn == 'yes') {
                      var tabParam = Ext.getCmp("comboRequest").getValue()
                          .split("|");
                      // SERVICE_GET_REQUEST : Service supprimé
                      Ext.Ajax.request({
                        url : SERVICE_GET_REQUEST,
                        method : 'GET',
                        params : {
                          service : 'delData',
                          request_id : tabParam[1]
                        },
                        success : function(response) {
                          var responseObject = setResponseObject(response).responseObject;
                          Ext.getCmp("request").setValue("");
                          Ext.getCmp("comboRequest").setValue("");
                          storeRequestCatalogue.reload();
                          Ext.MessageBox.alert('Exécution', responseObject);

                        },
                        failure : function(response) {
                          Ext.MessageBox.alert('Exécution', response);
                        }
                      });
                    }
                  });
            } else {
              Ext.MessageBox.alert('Suppression',
                  "Veuillez sélectionner au préalable une requête");
            }

          },
          width : 80
        }, {
          xtype : 'button',
          cls : 'buttoncls',
          text : 'Exécuter',
          handler : function() {
            var strSql = Ext.getCmp("request").getValue();
            if (strSql.toLowerCase().substr(0, 6) == "update"
                || strSql.toLowerCase().substr(0, 7) == "(update") {
              Ext.MessageBox
                  .confirm(
                      'Confirmer',
                      'Voulez-vous vraiment modifier cet ou ces enregistrement(s)?',
                      function(btn) {
                        if (btn == 'yes') {
                          showWindowView(SERVICE_GET_DATA,
                              'Résultat de la requête', strSql, false, strSql);
                        }
                      });
            } else if (strSql.toLowerCase().substr(0, 6) == "delete"
                || strSql.toLowerCase().substr(0, 7) == "(delete") {

              Ext.MessageBox
                  .confirm(
                      'Confirmer',
                      'Voulez-vous vraiment supprimer cet ou ces enregistrement(s)?',
                      function(btn) {
                        if (btn == 'yes') {
                          showWindowView(SERVICE_GET_DATA,
                              'Résultat de la requête', strSql, false, strSql);
                        }
                      });
            } else {
              showWindowView(
                  SERVICE_GET_DATA,
                  'Résultat de la requête <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("'
                      + strSql + '", "' + SERVICE_GET_DATA + '");\'/>', strSql,
                  false, strSql);
            }

          },
          width : 80
        }, {
          xtype : 'button',
          cls : 'buttoncls',
          text : 'Sauvegarder',
          handler : function() {
            Ext.Msg.prompt('Sauvegarde',
                'Entrez une description de la requête', function(btn, text) {
                  if (btn == 'ok') {
                    var request_description = text;
                    var strSql = Ext.getCmp("request").getValue();
                    // SERVICE_GET_REQUEST : Service supprimé
                    Ext.Ajax.request({
                      url : SERVICE_GET_REQUEST,
                      method : 'GET',
                      params : {
                        service : 'saveData',
                        request_sql : strSql,
                        request_description : request_description
                      },
                      success : function(response) {
                        var responseObject = setResponseObject(response).responseObject;
                        storeRequestCatalogue.reload();
                        Ext.MessageBox.alert('Exécution', responseObject);
                      },
                      failure : function(response) {
                        Ext.MessageBox.alert('Exécution', response);
                      }
                    });
                  }
                });
          },
          width : 80
        }, {
          xtype : 'button',
          cls : 'buttoncls',
          text : 'Sauvegarder en suivi d\'activité',
          handler : function() {
            Ext.Msg.prompt('Sauvegarder en suivi d\'activité',
                'Saisissez le nom de la requête personnalisée', function(btn,
                    text) {
                  if (btn == 'ok') {
                    var request_description = text;
                    var strSql = Ext.getCmp("request").getValue();
                    // SERVICE_GET_REQUEST : Service supprimé
                    Ext.Ajax.request({
                      url : SERVICE_GET_REQUEST,
                      method : 'GET',
                      params : {
                        service : 'createSuivi',
                        request_sql : strSql,
                        request_description : request_description
                      },
                      success : function(response) {
                        var responseObject = setResponseObject(response).responseObject;
                        storeRequestCatalogue.reload();
                        Ext.MessageBox.alert('Exécution', responseObject);
                      },
                      failure : function(response) {
                        Ext.MessageBox.alert('Exécution', response);
                      }
                    });
                  }
                });
          },
          width : 80
        }, {
          xtype : 'button',
          text : 'Effacer',
          cls : 'buttoncls',
          handler : function() {
            Ext.getCmp("request").setValue("");
          },
          width : 80
        }]
      }]
    }]
  });
}
var storeTableListCarto = new Ext.data.JsonStore({
    url: SERVICE_GET_TABLELIST_CARTO,      
    fields: ['name'],
    autoLoad: true
});     

//SERVICE_GET_TABLELIST_CARTO : Service supprimé
var storeViewListCarto = new Ext.data.JsonStore({
    url: SERVICE_GET_TABLELIST_CARTO+"?bView=1",      
    fields: ['name'],
    autoLoad: true
});
// end : hismail


//Requêtes personnalisées
// SERVICE_GET_REQUEST : Service supprimé
Ext.Ajax.request({
    url: SERVICE_GET_REQUEST,
    method: 'GET',
    params: { 
      service: 'getSuivi'
    },
    success: function(response) {
      var responseObject = setResponseObject(response).responseObject;
      suiviActiviteItem.push({
          xtype: 'label',
          fieldLabel: '<b>Requêtes personnalisées</b>',
          allowBlank : false,
          name: 'statistiques'
          // vtype : 'alphanum'
        });
      for(var i=0;i<responseObject.length;i++){
        suiviActiviteItem.push({
            xtype: 'button',
           text: 'consulter',
           fieldLabel: responseObject[i].name,
           viewname :  responseObject[i].viewname,
           handler: function(){
              showWindowView(SERVICE_GET_VIEWS, this.fieldLabel+' <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("select * from '+this.viewname+'", "'+SERVICE_GET_DATA+'");\'/>',this.viewname,false);
           },
          width : 80
        });
      }

      var suiviActivite = new Ext.FormPanel({
        url: 'services/getservices.php?iMode=0',
        frame: true,
        title: 'Suivi d\'activité',
        //autoHeight : true,
        labelWidth : 500,
        autoScroll:true,
        items: suiviActiviteItem
          });    
      

      // SERVICE_GET_TABLELIST: Service  supprimé
      var storeTableList = new Ext.data.JsonStore({
        url: SERVICE_GET_TABLELIST,      
        fields: ['name'],
        autoLoad: true
      });
      // SERVICE_GET_TABLELIST_CARTO : Service supprimé
      var storeTableListCarto = new Ext.data.JsonStore({
          url: SERVICE_GET_TABLELIST_CARTO,      
          fields: ['name'],
          autoLoad: true
      });     

      var storeViewList = new Ext.data.JsonStore({
        url: SERVICE_GET_TABLELIST+"?bView=1",      
        fields: ['name'],
        autoLoad: true
      });
      // SERVICE_GET_TABLELIST_CARTO : Service supprimé
      var storeViewListCarto = new Ext.data.JsonStore({
          url: SERVICE_GET_TABLELIST_CARTO+"?bView=1",      
          fields: ['name'],
          autoLoad: true
      });
      // SERVICE_GET_REQUEST : Service supprimé
      var storeRequestCatalogue = new Ext.data.JsonStore({
        url: SERVICE_GET_REQUEST+"?service=getData",      
        fields: ['id', 'name', 'request'],
        autoLoad: true
      });
      // SERVICE_GET_REQUEST_CARTO : Service supprimé
      var storeRequestCarto = new Ext.data.JsonStore({
          url: SERVICE_GET_REQUEST_CARTO+"?service=getData",      
          fields: ['id', 'name', 'request'],
          autoLoad: true
      });

      // suiviSGBDCarto : FormPanel supprimé
      var suiviSGBDCarto = new Ext.FormPanel({
        url: '',
        frame: true,
        title: 'Suivi de la base de données Cartographique',
        autoHeight : true,
        labelWidth : 300,
        items: [{
            xtype: 'label',
            fieldLabel: '<b>Consultation de l\'arborescence de la base de données</b>',
            allowBlank : false,
            name: 'arbo_sgbd'
            // vtype : 'alphanum'
         }, {
            // column layout with 2 columns
            xtype:'compositefield',
            fieldLabel: 'Liste des tables',
            // defaults for columns
            items:[
                {
                  xtype: 'combo',
                  tpl : '<tpl for="."><div id="" ext:qtip="{name}" class="x-combo-list-item">{name}</div></tpl>',
                  id: 'comboTablesCarto',
                  name: 'tables',
                  editable: false,
                  loadingText: 'Chargement',
                  emptyText: '',
                  typeAhead : true,
                  triggerAction: 'all',
                  minChars: 1,
                  queryDelay: 250,     
                  store : storeTableListCarto,
                  displayField: 'name',
                  valueField: 'name',
                  width : 200,
                  mode: 'local'
                }, 
                {
                  xtype: 'button',
                    text: 'Structure',
                    cls : 'buttoncls',
                    handler: function(){
                      // SERVICE_GET_STRUCTRE_CARTO : Service supprimé
                      if(Ext.getCmp("comboTablesCarto").getValue()!=""){
                        showWindowView(SERVICE_GET_STRUCTRE_CARTO, 'Structure de la table', Ext.getCmp("comboTablesCarto").value, false);
                      }else{
                        Ext.MessageBox.alert('Suppression', "Veuillez sélectionner au préalable une table");
                      }
                    },
                    width : 80
                } 
                ,
                {
                    xtype: 'button',
                    text: 'Contenu',
                    cls : 'buttoncls',
                    handler: function(){
                      // SERVICE_GET_DATA_CARTO : service supprimé
                      if(Ext.getCmp("comboTablesCarto").getValue()!=""){  
                        showWindowView(SERVICE_GET_DATA_CARTO, 'Données de la table', Ext.getCmp("comboTablesCarto").value, false, "select * from \""+Ext.getCmp("comboTablesCarto").value+"\"");
                      }else{
                        Ext.MessageBox.alert('Suppression', "Veuillez sélectionner au préalable une table");
                      }
                    },
                    width : 80
                }
              ]
              
            },{
            // right column
            // defaults for fields
          xtype:'compositefield',
          fieldLabel: 'Liste des vues',
            items:[ {
                xtype: 'combo',
                tpl : '<tpl for="."><div id="" ext:qtip="{name}" class="x-combo-list-item">{name}</div></tpl>',
                id: 'comboViewsCarto',
                name: 'vues',
                editable: false,
                loadingText: 'Chargement',
                emptyText: '',
                typeAhead : true,
                triggerAction: 'all',
                minChars: 1,
                queryDelay: 250,  
                width : 200,
                store : storeViewListCarto,
                displayField: 'name',
                valueField: 'name',
                mode: 'local'
              },{
               xtype: 'button',
               text: 'Structure',
               cls : 'buttoncls',
               handler: function(){
                 // SERVICE_GET_STRUCTRE_CARTO : Service supprimé
                 if(Ext.getCmp("comboViewsCarto").getValue()!=""){
                   showWindowView(SERVICE_GET_STRUCTRE_CARTO, 'Structure de la vue', Ext.getCmp("comboViewsCarto").value, false);
                 }else{
                   Ext.MessageBox.alert('Suppression', "Veuillez sélectionner au préalable une vue");
                 }
               },
               width : 80
            },
            {
                xtype: 'button',
                text: 'Contenu',
                cls : 'buttoncls',
                handler: function(){
                 // SERVICE_GET_DATA_CARTO : service supprimé
                 if(Ext.getCmp("comboViewsCarto").getValue()!=""){
                   showWindowView(SERVICE_GET_DATA_CARTO, 'Données de la vue', Ext.getCmp("comboViewsCarto").value, false, "select * from \""+Ext.getCmp("comboViewsCarto").value+"\"");
                 }else{
                   Ext.MessageBox.alert('Suppression', "Veuillez sélectionner au préalable une vue");
                 }
                },
                width : 80
             }
            ]
            
            },
            {
              xtype: 'label',
              fieldLabel: '<b>Exécution de requêtes</b>',
              allowBlank : false,
              name: 'request_exec'
              // vtype : 'alphanum'
           },
           {
             // column layout with 2 columns
             layout:'column'
             // defaults for columns
             ,defaults:{
                columnWidth:0.5
               ,layout:'form'
               ,border:false
               ,xtype:'panel'
             }
             ,items:[{
               // left column
               // defaults for fields
               defaults:{anchor:'100%'}
               ,items:[
                 {
                   xtype: 'combo',
                   tpl : '<tpl for="."><div id="" ext:qtip="{name}" class="x-combo-list-item">{name}</div></tpl>',
                   id: 'comboRequestCarto',
                   name: 'comboRequestCarto',
                   fieldLabel: 'Requêtes sauvegardées',
                   editable: false,
                   loadingText: 'Chargement',
                   emptyText: '',
                   typeAhead : true,
                   triggerAction: 'all',
                   minChars: 1,
                   queryDelay: 250,     
                   store : storeRequestCarto,
                   displayField: 'name',
                   valueField: 'request',
                   width : 200,
                   mode: 'local',
                   listeners: {
                     select: function(){
                     var tabParam = Ext.getCmp("comboRequestCarto").getValue().split("|");
                       Ext.getCmp("requestCarto").setValue(tabParam[0]);
                     }
                   }
                 } 
                 ,
                 {
                   xtype:'textarea',
                   fieldLabel:'Requête',
                   name:'requestCarto',
                   id :'requestCarto'
                 }
               ]
               
             },{
             // right column
             // defaults for fields
             defaults:{anchor:'20%'}
             ,items:[{
               xtype: 'button',
               cls : 'buttoncls',
               text: 'Supprimer',
               handler: function(){
                  if(Ext.getCmp("comboRequestCarto").getValue()!=""){
                    Ext.MessageBox.confirm('Confirmer','Confirmer la suppression de la requête ?', 
                    function(btn){ 
                        if (btn == 'yes'){
                          var tabParam = Ext.getCmp("comboRequestCarto").getValue().split("|");
                          // SERVICE_GET_REQUEST_CARTO : Service supprimé
                          Ext.Ajax.request({
                            url: SERVICE_GET_REQUEST_CARTO,
                            method: 'GET',
                            params: { 
                              service: 'delData',
                              request_id : tabParam[1]
                            },
                            success: function(response) {
                              var responseObject = setResponseObject(response).responseObject;
                              Ext.getCmp("request").setValue("");
                              Ext.getCmp("comboRequestCarto").setValue("");
                              storeRequestCarto.reload();
                              Ext.MessageBox.alert('Exécution', responseObject); 
                              
                            },
                            failure : function(response){
                              Ext.MessageBox.alert('Exécution', response); 
                            }
                          });
                        }
                      }
                    );
                  }else{
                    Ext.MessageBox.alert('Suppression', "Veuillez sélectionner au préalable une requête"); 
                  }
                  
               },
               width : 80
             }, {
                xtype: 'button',
                cls : 'buttoncls',
                text: 'Exécuter',
                handler: function(){
                 var strSql = Ext.getCmp("requestCarto").getValue();
                 if (strSql.toLowerCase().substr(0, 6) == "update"  || strSql.toLowerCase().substr(0, 7) == "(update"){
                   Ext.MessageBox.confirm('Confirmer','Voulez-vous vraiment modifier cet ou ces enregistrement(s)?', 
                       function(btn){ 
                         // SERVICE_GET_DATA_CARTO : service supprimé
                         if (btn == 'yes'){     
                           showWindowView(SERVICE_GET_DATA_CARTO, 'Résultat de la requête', strSql, false, strSql);
                         }
                       }
                   );
                 }
                 else if (strSql.toLowerCase().substr(0, 6) == "delete"  || strSql.toLowerCase().substr(0, 7) == "(delete"){

                   Ext.MessageBox.confirm('Confirmer','Voulez-vous vraiment supprimer cet ou ces enregistrement(s)?', 
                       function(btn){ 
                         // SERVICE_GET_DATA_CARTO : service supprimé
                         if (btn == 'yes'){
                           showWindowView(SERVICE_GET_DATA_CARTO, 'Résultat de la requête', strSql, false, strSql);
                         }
                       }
                   );
                 }   else{
                 // SERVICE_GET_DATA_CARTO : service supprimé
                 showWindowView(SERVICE_GET_DATA_CARTO, 'Résultat de la requête <img src=\'/images/excel.gif\' class=\'x-btn\'onclick=\'javascript:exportExcel("'+strSql+'", "'+SERVICE_GET_DATA_CARTO+'");\'/>', strSql, false, strSql);
                 }
                
                },
                width : 80
             }, {
               xtype: 'button',
               cls : 'buttoncls',
               text: 'Sauvegarder',
               handler: function(){
                 Ext.Msg.prompt('Sauvegarde', 'Entrez une description de la requête', function(btn, text){
                   if (btn == 'ok'){
                     var request_description = text;
                     var strSql = Ext.getCmp("requestCarto").getValue();  
                     // SERVICE_GET_REQUEST_CARTO : Service supprimé
                     Ext.Ajax.request({
                       url: SERVICE_GET_REQUEST_CARTO,
                       method: 'GET',
                       params: { 
                         service: 'saveData',
                         request_sql : strSql,
                         request_description : request_description
                       },
                       success: function(response) {
                         var responseObject = setResponseObject(response).responseObject;
                         storeRequestCarto.reload();
                         Ext.MessageBox.alert('Exécution', responseObject); 
                       },
                       failure : function(response){
                         Ext.MessageBox.alert('Exécution', response); 
                       }
                     });
                   }
                 });  
               },
               width : 80
            }, {
              xtype: 'button',
              text: 'Effacer',
              cls : 'buttoncls',
              handler: function(){
                Ext.getCmp("requestCarto").setValue("");
              },
              width : 80
           }
             ]
             }
           ]
           }

           ]
       });

      // start
      // create the Data Store
      var storeTotal = new Ext.data.Store({
          //proxy: new Ext.data.ScriptTagProxy({url: 'services/getdataflow.php?iMode=0'+(typeof(SERVICE_GET_PARAMS)!="undefined" ? '&serviceUrl='+SERVICE_GET_PARAMS : '')}),
          proxy: new Ext.data.HttpProxy({url: 
            Routing.generate('admin_getdataflow', {
              iMode : 0,
              serviceUrl : (typeof (SERVICE_GET_PARAMS) != "undefined" ? encodeURIComponent(SERVICE_GET_PARAMS) : null)
            })
          }),
          reader: new Ext.data.XmlReader({
                 record: 'service',
                 id: 'idx',
                 totalRecords: '@total'
             }, [{name:'name', type:'string', mapping:'@name'}, {name:'path', type:'string', mapping:'@path'},
                 {name:'login', type:'string', mapping:'@login'}, {name:'banner', type:'string', mapping:'@banner'},
                 {name:'dns', type:'string', mapping:'@dns'}  
             ])
      });

      // create the grid
      var tabColumns = new Array();
      tabColumns.push(
          {header: "Nom du Compte", width: 200, dataIndex: 'name', sortable: true}
          );
      if(typeof(MODE_PRODIGE)=="undefined" )
        tabColumns.push({header: "Répertoire", width: 200, dataIndex: 'path', sortable: true},
        {header: "Login", width: 200, dataIndex: 'login', sortable: true},
        {header: "Bandeau", width: 200, dataIndex: 'banner', sortable: true},
        {header: "DNS", width: 200, dataIndex: 'dns', sortable: true},
        {header: "OUTILS ADMIN", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',5);" >';}, sortable: true});
      else{
        tabColumns.push(
            {header: "PARAMETRAGE CATALOGUE", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',8);" >';}, sortable: true},
            {header: "PARAMETRAGE CARTO", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',9);" >';}, sortable: true},
            {header: "PARAMETRAGE AUTOMATE", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',13);" >';}, sortable: true},
            {header: "ACCES EXTERNES", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',11);" >';}, sortable: true},
            {header: "MOTEUR DE RECHERCHE", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',7);" >';}, sortable: true},
            {header: "TERRITOIRE D'EXTRACTION", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',10);" >';}, sortable: true},
            {header: "MODELES DE CARTES", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',12);" >';}, sortable: true},
            {header: "FLUX ATOM", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',14);" >';}, sortable: true}
        );        
      }
      tabColumns.push(

      //{header: "OUTILS CONSULT", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',13);" >';}, sortable: true},
      {header: "WFS", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',1);" >';}, sortable: true},
      {header: "WMS", width: 100, dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',2);" >';} , sortable: true},
      {header: "WMS-C", width: 100,  dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',6);" >';} , sortable: true},
      {header: "WMTS", width: 100,  dataIndex: 'path',renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',3);" >';} , sortable: true},
      {header: "PARAMETRAGE PROJECTION", width: 100, dataIndex: 'path', renderer: function(v,params,record){return '<img src="/images/extjs_update.png" onclick="displayWindow(\''+v+'\',4);" >';}, sortable: true}
      );

      storeTotal.load(); 
      var gridTotal = new Ext.grid.GridPanel({
        frame:true,
        title: 'Liste des serveurs',
        height: 200,
        width: 'auto',
        store: storeTotal,
        columns: tabColumns
      });
         
      var tabPanels = new Array();
      tabPanels.push(gridTotal);
      if(typeof(MODE_PRODIGE)=="undefined" )
        tabPanels.push(carmen_form);
      else{
        tabPanels.push(suiviActivite);
        //tabPanels.push(suiviSGBD);
        //tabPanels.push(suiviSGBDCarto);
      }

      // Tableau des onglets
      var PanelCarmen = new Ext.TabPanel({
        region: 'center',
        xtype: 'tabpanel',
        activeTab: 0,
        items: tabPanels
      });
      var viewport = new Ext.Viewport({
        layout: 'border',
        renderTo: Ext.getBody(),
        items: [PanelCarmen]
      });
      // end
    },
    failure : function(response){
      Ext.MessageBox.alert('Exécution', response); 
    }
});
*/

}