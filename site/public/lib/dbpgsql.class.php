<?php
/**
 * @author Alkante
 * 
 * @abstract classe de connexion à une base de données PostgreSQL / PostGIS
 *           gère la connexion, déconnexion et les requêtes de sélection et de mise à jour
 */

class DbPgSql
{
  /** connection */
  protected $conn;

  /** Adresse IP du serveur de base de donnees */
  protected $strHost;
    
  /** Identifiant de l'utilisateur */
  protected $strLogin;

  /** Nom de la base de donnees */
  protected $strDb;

  /** Mot de passe */
  protected $strPwd;

  /** Alias */
  protected $strAlias;

  /** numéro de port */
  protected $strPort;
    
  /** encodage utilisé */
  protected $strDbEncoding;

  /** 
   * Schema de la base de données
   */
  protected $strSchema;
  
  /**
   *  @abstract constructeur de la classe db : initialisation des attributs de l'objet db
   *
   * @param strLogin Identifiant de l'utilisateur
   * @param strHost  Adresse IP du serveur de base de donnees
   * @param strPwd   Mot de passe
   * @param strDb    Nom de la base de donnees
   * @param strPort  Numéro de port
   */  
  public function __construct($strLogin, $strHost, $strPwd, $strDb, $strPort)
  {
    $this->strLogin = $strLogin;
    $this->strHost = $strHost;
    $this->strPwd = $strPwd;
    $this->strDb = $strDb;
    $this->strSchema = "alkanet, alkarto, public";
    $this->strAlias = "";
    $this->strPort = $strPort;
    $this->strDbEncoding = 'UTF8';
    
    $this->connect();
  }
  
  /**
   *  @abstract établit la connection avec la base de données
   */
  public function connect()
  {
    $strConn= "host=".$this->strHost;
    if( $this->strPort != "" ) 
      $strConn .= " port=".$this->strPort; 
    $strConn .= " dbname=".$this->strDb.
      " user=".$this->strLogin.
      " password=".$this->strPwd;

    $this->conn = @pg_connect($strConn); 
    if( !$this->conn )
      trigger_error("Impossible de se connecter à la base PgSQL (connect )");

    $this->strDbEncoding = @pg_client_encoding($this->conn);
    
  }
  
  /**
   *  @abstract déconnection avec la base de données
   */
  public function disconnect()
  {
  }
  
  /**
   * @abstract exécute une requête sql
   * @param strSql  requête sql
   * @return Array() contenant le résultat de la requête 
   */
  public function execute($strSql)
  {
    $tabRes = array();
    
    $strSql = "set search_path to ".$this->strSchema."; ".$strSql;
    $res = pg_query($this->conn, $strSql);
    
    if( $res ) {
      $iCount = pg_num_rows($res);
      if( $iCount > 0 ) {
        for($i=0; $i<$iCount; $i++) {
          $tab = pg_fetch_array($res);
          $tabRes[] = $tab;
        }
      }
    } else {
      trigger_error("Erreur d'exécution de la requête : (".$strSql.")");
    }
    
    return $tabRes;
  }
  
  /**
   * @abstract modifie le nom du schema utilisé pour effectuer les requêtes
   * @param strSchema Nom du schéma, correspond au préfixe placé dans le nom de la table
   */
  public function setSchema($strSchema) 
  {
    $this->strSchema = $strSchema; 
  }
}

?>
