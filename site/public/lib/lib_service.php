<?php

/**
 * @brief put in session service information
 *        if session is lost, i.e. the service array is no more known, 
 *        then reload main page or raise an error if trigger_error is set to true
 * @param trigger_error In case of session is lost, if set to true, 
 *                      raise an error rather reloading page
 * @return true if the service has correctly been loaded or is already loaded
 *         false if the service idx is unknown
 */
function loadService($trigger_error=false){
  // looking for a service idx passed from URL	
  if (isset($_GET["service_idx"])) {
		session_unset();
		$service_idx = $_GET["service_idx"];
	} 
	else {
    // service already set ?
    if (isset($_SESSION["service"])){
      return true;
    }
    else { //reloading page
      if ($trigger_error)
        trigger_error(
          CARMEN_ERROR_SESSION_EXPIRED . 
          "|Votre Session a expiré. La page est 
          rechargée dans l'environement initial." . 
          "|CARMEN_ERROR");
      else {
        echo "<html><head>
              <!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
			  <meta http-equiv='X-UA-Compatible' content='IE=9'/>
			  <!-- ExtJS include libraries -->
              <link rel=\"stylesheet\" type=\"text/css\" href=\"./IHM/JavaScript/extjs/resources/css/ext-all.css\" />
              <link rel=\"stylesheet\" type=\"text/css\" href=\"./IHM/extjsOverload/olive/css/xtheme-olive.css\" />
              <script type=\"text/JavaScript\" src=\"./IHM/JavaScript/extjs/adapter/ext/ext-base.js\"></script>
              <script type=\"text/JavaScript\" src=\"./IHM/JavaScript/extjs/ext-all.js\"></script>
        		  <script language='javascript'>
      				  Ext.onReady(function() {
      				    top.Ext.Msg.alert('Erreur',
                    'Votre session a expiré. La page est rechargée dans l’environnement initial',
                    function()
									  {
									    top.window.location.reload();
									  });
                });
      				</script>
      				</html>";
        die();
      }
    }
	}
	
	$dom = new DOMDocument();
	$dom->load(CARMEN_URL_PATH_DATA."SYSTEM/services.xml");
	//$dom->load('services.xml');
	$tabServices = $dom->getElementsByTagName('service');
	$services= array();
	foreach($tabServices as $index => $tabService){
		$idx = $tabService->getAttribute('idx');
		$service['idx'] = $idx;
		$service['name'] = $tabService->getAttribute('name');
		$service['path'] = $tabService->getAttribute('path');
		$service['login'] = $tabService->getAttribute('login');
		$service['password'] = $tabService->getAttribute('password');
		$service['dns'] = $tabService->getAttribute('dns');
		$service['banner'] =$tabService->getAttribute('banner');
		//TODO remplacer dans les fichiers appelant services.php
		$service['Nom'] = $tabService->getAttribute('name');
		$service['Répertoire'] = $tabService->getAttribute('path');
		$service['Image'] ="";
		$service['Bandeau'] =$tabService->getAttribute('banner');
		$service['Administrateur'] = $tabService->getAttribute('login');
		$service['Pass'] = $tabService->getAttribute('password');
		$services[$idx] = $service;
		//if($tabService->getAttribute('idx') == intval($service_idx) - 1)
    if($tabService->getAttribute('idx') == intval($service_idx))
		  $_SESSION["service"]=$service;
	}
	$_SESSION["services"]=$services;
	return (isset($_SESSION["service"]));
}

/**
 * @brief enregistre en session les paramètres du map
 * @param map string : nom du mapfile
 * @param directory string : chemin complet jusqu'au mapfile
 * @return
 */
function loadMap($map = "", $directory=""){
	if($map != ""){
	  if(file_exists($directory.$map)){
	    $_SESSION["map"] = $map;
			return true; 
		}else{
			return false;
		}
	}
	if(isset($_SESSION["map"])){
		return true;
	}
	return false;
}
?>