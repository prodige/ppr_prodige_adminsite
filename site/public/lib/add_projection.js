Ext.ns('Prodige.Adminsite');
Ext.form.Field.prototype.setReadOnly = function(readOnly){
    if(this.rendered){
        this.el.dom.readOnly = readOnly;
    }
    this.readOnly = readOnly;
};
Ext.form.ComboBox.prototype.setReadOnly = Ext.form.ComboBox.prototype.setHideTrigger;

Prodige.Adminsite.ProjectionRecord = Ext.data.Record.create([
  'nom', 
  'proj4', 
  {name:'epsg_proj', type:'int', mapping:'auth_srid'}, 
  {name:'srid', type:'int'}, 
  'auth_name', 
  'auth_srid', 
  'srtext', 
  {
    name : 'auth_name_display',
    convert : function(value, record){
      return record.auth_name+':'+record.auth_srid+' - '+(record.nom || "Nom inconnu")
    }
  }
]);

Prodige.Adminsite.ProjectionEditor = Ext.extend(Ext.form.FormPanel, {
  
  openerStore : null,
  recordType : null,
  existingProjections : null,
  
  EPSG : null,
  NOM : null,
  idRow: -1,
  mode: 'AddProjection',

  initComponent : function() {
    var me = this;

    var fname;

    me.labelSeparator = " ";
    me.defaults = {
      labelSeparator : " ",
      labelWidth : 100,
      labelAlign : "right"
    };
    
    me.items = [];
    if ( me.idRow==-1 ){
      me.existingProjections = new Ext.data.Store({
        id: 'ExistingProjections_store',
        reader : new Ext.data.JsonReader({idProperty:'auth_srid'}, Prodige.Adminsite.ProjectionRecord),
        sortInfo : {field:'auth_srid', direction : 'ASC'},
        url : Routing.generate('admin_get_proxy_spatial_ref_sys', {
          mode: "epsg_intersect"
        }),
        autoLoad: true,
        listeners : {
          load : function(store, records){
            var combo = Ext.getCmp('proj_dispo');
            if (!combo) return;
            combo.getEl().removeClass('x-form-empty-field');
            combo.emptyText = "Sélectionnez une projection pour l'ajouter au site";
            combo.setValue(null);
          }
        }
      });
      me.items.push({
        xtype : 'fieldset',
        defaults : {
          labelSeparator : " ",
          labelWidth : 350,
          labelAlign : "right"
        },
        labelWidth : 150,
        title : "Projections disponibles",
        items : [{
          width : 400,
          xtype : "combo",
          fieldLabel : "Projections disponibles",
          listEmptyText : 'Aucune projection',
          mode : 'local',
          loadingText : 'Chargement en cours...',
          valueField : 'epsg_proj',
          displayField : 'auth_name_display'/*'nom'*/,
          name : (fname = "proj_dispo"),
          id   : fname,
          triggerAction : 'all',
          listWidth : 500,
          
          doQuery : function(q, forceAll){
            q = Ext.isEmpty(q) ? '' : q;
            var qe = {
                query: q,
                forceAll: forceAll,
                combo: this,
                cancel:false
            };
            if(this.fireEvent('beforequery', qe)===false || qe.cancel){
                return false;
            }
            q = qe.query;
            forceAll = qe.forceAll;
            if(forceAll === true || (q.length >= this.minChars)){
              if(this.lastQuery !== q){
                this.lastQuery = q;
                if(this.mode == 'local'){
                  this.selectedIndex = -1;
                  if(forceAll){
                    this.store.clearFilter();
                  }else{
                    this.store.filter(this.displayField, q, true);
                  }
                  this.onLoad();
                }else{
                  this.store.baseParams[this.queryParam] = q;
                  this.store.load({
                    params: this.getParams(q)
                  });
                  this.expand();
                }
              }else{
                this.selectedIndex = -1;
                this.onLoad();
              }
            }
          },
          forceSelection : true,
          emptyText : 'Chargement en cours...',
          store : me.existingProjections,
          listeners : {
            select : function(combo){
              var epsg_proj = combo.getValue();
              var record;
              if(epsg_proj) {
                record = me.existingProjections.find('srid'/*'epsg_proj'*/, epsg_proj);
                if(record == -1) 
                  return;
                record = me.existingProjections.getAt(record);
                if(!record)
                  return;
                record.data.epsg_proj = record.data.srid;
                
                me.record = record;
                me.getForm().loadRecord(record);
                //champs en lecture seule si on sélectionne une projection existante
                //me.setReadOnly(true);
              } else {
                me.reset();
              }
            }
          }
        }]
      });
    }
    
    //FORM FIELDS
    me.items = me.items.concat([{
      xtype : 'fieldset',
      title : 'Propriétés de la projection',
      defaults : {
        style : {width : "350px"}
      },
      items : [{
        xtype : 'compositefield',
        name : 'epsg_proj',
        setValue : function(value){this.items.items[0].setValue(value)},
        getValue : function(){this.items.items[0].getValue()},
        fieldLabel : "EPSG",
        style : {width : "430px"},
        items : [{
          readOnly : (me.idRow!=-1),
          style : {width : "350px"},
          allowBlank : false,
          xtype : "textfield",
          name  : (fname = 'epsg_proj'),
          id    : fname
        }, {
          xtype : 'button',
          text : 'Pré-remplir',
          handler : function(){me.preremplir_epsg();} 
        }]
      }, {
        fieldLabel : "Nom",
        xtype : "textfield",
        name  : (fname = 'nom'),
        id    : fname
      }, {
        fieldLabel : "proj4",
        xtype : "textarea",
        name  : (fname = 'proj4'),
        id    : fname
      }, {
        fieldLabel : "srid",
        xtype : "textfield",
        name  : (fname = 'srid'),
        id    : fname
      }, {
        fieldLabel : "auth_name",
        xtype : "textfield",
        name  : (fname = 'auth_name'),
        id    : fname
      }, {
        fieldLabel : "auth_srid",
        xtype : "textfield",
        name  : (fname = 'auth_srid'),
        id    : fname
      }, {
        fieldLabel : "srtext",
        xtype : "textarea",
        name  : (fname = 'srtext'),
        id    : fname
      }]
    }]);

    me.buttons = [{
      text : (me.mode == 'ModifieProjection' ? 'Modifier la définition' : "Ajouter à la liste des projections du site"),
      handler : function(){me.save_projection();}
    }, {
      text : "Effacer le formulaire",
      handler : function(){me.reset();}
    }, {
      text : "Fermer",
      handler : function(){me.findParentByType("window").close();}
    }];

    Prodige.Adminsite.ProjectionEditor.superclass.initComponent.call(this);
    
    
    this.on('afterlayout', function(){
      var record;
      if ( me.EPSG ){
        record = new Prodige.Adminsite.ProjectionRecord({epsg_proj : me.EPSG, nom : me.NOM});
      } else{
        record = new Prodige.Adminsite.ProjectionRecord({});
      }
      me.record = record;
      me.getForm().loadRecord(record);
      if ( me.EPSG ) me.preremplir_epsg();
    })
  },

  reset : function(){
    this.getForm().reset();
    this.setReadOnly(false);
  },

  setReadOnly : function(readonly){
    this.getForm().items.each(function(field) {
      if(field.xtype != 'combo' && field.xtype != 'hidden') {
        field.setReadOnly && field.setReadOnly(readonly);
      }
    });
  },


  /**
   * fonction permettant de creer une nouvelle projection a partir des informations saisies dans le formulaire
   * alimente postgis/proj/proj4js et met a jour la liste deroulante des projections disponibles
   */
  save_projection : function(){
    var me = this;
    var form = me.getForm();
    var s_action = (this.idRow ? "Ajout" : "Modification");
    var s_event = (this.idRow ? "créée" : "mise à jour");
    Ext.MessageBox.minWidth = 600;
    if(!form.isValid()) {
      Ext.MessageBox.alert(s_action+" d'une projection", "Veuillez saisir les informations de la projection.");
      return false;
    }
    Ext.Ajax.request({
      timeout : 600*1000,
      url: Routing.generate('admin_get_proxy_spatial_ref_sys'),
      method: 'POST',
      jsonData : {
        'mode'    : "save_epsg",
        'epsg': Ext.getCmp('epsg_proj').getValue(),
        'nom': Ext.getCmp('nom').getValue(),
        'proj4': Ext.getCmp('proj4').getValue(),
        'srid': Ext.getCmp('srid').getValue(),
        'auth_name': Ext.getCmp('auth_name').getValue(),
        'auth_srid': Ext.getCmp('auth_srid').getValue(),
        'srtext': Ext.getCmp('srtext').getValue()
      },
      success: function(response){
        try {
          response = Ext.decode(response.responseText);
          var success = response.success;
          var message = "<li>"+response.message.join('</li><li>')+"</li>";
          if(success){
            Ext.MessageBox.alert(s_action+" d'une projection", 'La projection a bien été '+s_event+'.<br/><br/>Détails d\'exécution : '+message+'');
            return true;
          }
          else {
            Ext.MessageBox.alert(s_action+" d'une projection - Erreur", 'La projection n\'a pu être enregistrée.<br/><br/> Erreurs : '+message+'.');
            return false;
          }
        } catch(error){
        }
      },
      failure: function(){
        Ext.MessageBox.alert(s_action+" d'une projection - Erreur", 'La nouvelle projection n\'a pu être enregistrée.<br/><br/> Erreur : Problème dans la requête AJAX.');
        return false;
      }
    });
  },

  preremplir_epsg : function(){
    var me = this;
    var record = me.record;
    var epsg_code_cmp = Ext.getCmp('epsg_proj');
    if ( !epsg_code_cmp || !epsg_code_cmp.isValid() ) return;
    if ( !record ) return;

    var select = Ext.getCmp("proj_dispo");
    select && select.reset();

    var mask = new Ext.LoadMask(this.getEl(), {removeMask : true, msg : "Interrogation de la base spatialreference.org..."});

    mask.show();

    
    var epsg_code = epsg_code_cmp.getValue();
    //On recupere valeur proj4 de la projection a preremplir
    //http://spatialreference.org/ref/epsg/EPSG_PROJ/proj4/
    var proj4 = "";
    var postgis = "";
    Ext.Ajax.request({
      url: Routing.generate('admin_get_proxy_spatial_ref_sys'),
      method : 'GET',
      params: {
        'mode'    : "preremplir",
        'epsg_proj' : epsg_code
      },
      success: function(response){
        try {
          response = Ext.decode(response.responseText);
          Ext.iterate(response, function(key, value){
            if ( key=="nom" && record.get(key) ) return;
            record.set(key, value, {commit:true,silent:true});
          });
          
          me.getForm().loadRecord(record);
        } catch(error){
          Ext.MessageBox.alert('Projection '+epsg_code, 'L\'int&eacute;rrogation de la base spatialreference.org a échoué.')
        }
        mask.hide();
      },
      failure: function(){
        mask.hide();
        Ext.MessageBox.alert('Projection '+epsg_code, 'L\'int&eacute;rrogation de la base spatialreference.org n\'a renvoy&eacute aucun r&eacute;sultat.')
      }
    });
  }
})
