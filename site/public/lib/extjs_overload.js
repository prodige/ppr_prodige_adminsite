/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */

//
// This is the main layout definition.
//

Ext.lib.Ajax.isCrossDomain = function(u) {
  //PRODIGE 4.0 : non crossdomain car les services sont rapatriés dans adminsite 
  // !! A VERIFIER + A CONFIRMER + A ETUDIER (toute situation)
  //return true;

  var match = /(?:(\w*:)\/\/)?([\w\.\-\_]*(?::\d*)?)/.exec(u);
  if (!match[1])
    return false; // No protocol, not cross-domain
  return (match[1] != location.protocol) || (match[2] != location.host);
};

Ext.override(
  Ext.data.Connection,
  {

    request : function(o) {
      if (this.fireEvent("beforerequest", this, o) !== false) {
        var p = o.params;

        if (typeof p == "function") {
          p = p.call(o.scope || window, o);
        }
        if (typeof p == "object") {
          p = Ext.urlEncode(p);
        }
        if (this.extraParams) {
          var extras = Ext.urlEncode(this.extraParams);
          p = p ? (p + '&' + extras) : extras;
        }

        var url = o.url || this.url;
        if (typeof url == 'function') {
          url = url.call(o.scope || window, o);
        }

        if (o.form) {
          var form = Ext.getDom(o.form);
          url = url || form.action;

          var enctype = form.getAttribute("enctype");
          if (o.isUpload
              || (enctype && enctype.toLowerCase() == 'multipart/form-data')) {
            return this.doFormUpload(o, p, url);
          }
          var f = Ext.lib.Ajax.serializeForm(form);
          p = p ? (p + '&' + f) : f;
        }

        var hs = o.headers;
        if (this.defaultHeaders) {
          hs = Ext.apply(hs || {}, this.defaultHeaders);
          if (!o.headers) {
            o.headers = hs;
          }
        }

        var cb = {
          success :this.handleResponse,
          failure :this.handleFailure,
          scope :this,
          argument : {
            options :o
          },
          timeout :this.timeout
        };

        var method = o.method || this.method || (p ? "POST" : "GET");

        if (method == 'GET'
            && (this.disableCaching && o.disableCaching !== false)
            || o.disableCaching === true) {
          url += (url.indexOf('?') != -1 ? '&' : '?') + '_dc='
              + (new Date().getTime());
        }

        if (typeof o.autoAbort == 'boolean') { // options
          // gets top
          // priority
          if (o.autoAbort) {
            this.abort();
          }
        } else if (this.autoAbort !== false) {
          this.abort();
        }
        if ((method == 'GET' && p) || o.xmlData || o.jsonData) {
          url += (url.indexOf('?') != -1 ? '&' : '?') + p;
          p = '';
        }
        if (o.scriptTag || this.scriptTag
            || Ext.lib.Ajax.isCrossDomain(url)) {
          this.transId = this.scriptRequest(method, url, cb, p, o);
        } else {
          this.transId = Ext.lib.Ajax.request(method, url, cb, p, o);
        }
        return this.transId;
      } else {
        Ext.callback(o.callback, o.scope, [ o, null, null ]);
        return null;
      }
    },

    scriptRequest : function(method, url, cb, data, options) {
      var transId = ++Ext.data.ScriptTagProxy.TRANS_ID;
      var trans = {
        id :transId,
        cb :options.callbackName || "stcCallback" + transId,
        scriptId :"stcScript" + transId,
        options :options
      };

      url += (url.indexOf("?") != -1 ? "&" : "?")
          + data
          + String.format("&{0}={1}", options.callbackParam
              || this.callbackParam || 'callback', trans.cb);

      var conn = this;
      window[trans.cb] = function(o) {
        conn.handleScriptResponse(o, trans);
      };

      // Set up the timeout handler
      trans.timeoutId = this.handleScriptFailure.defer(cb.timeout, this,
          [ trans ]);

      var script = document.createElement("script");
      script.setAttribute("src", url);
      script.setAttribute("type", "text/javascript");
      script.setAttribute("id", trans.scriptId);
      document.getElementsByTagName("head")[0].appendChild(script);

      return trans;
    },

    handleScriptResponse : function(o, trans) {
      this.transId = false;
      this.destroyScriptTrans(trans, true);
      var options = trans.options;

      // Attempt to parse a string parameter as XML.
      var doc;
      if (typeof o == 'string') {
        if (window.ActiveXObject) {
          doc = new ActiveXObject("Microsoft.XMLDOM");
          doc.async = "false";
          doc.loadXML(o);
        } else {
          doc = new DOMParser().parseFromString(o, "text/xml");
        }
      }

      // Create the bogus XHR
      response = {
        responseObject :o,
        responseText :(typeof o == "object") ? Ext.util.JSON.encode(o)
            : String(o),
        responseXML :doc,
        argument :options.argument
      }
      
      this.fireEvent("requestcomplete", this, response, options);
      Ext.callback(options.success, options.scope, [ response, options ]);
      Ext.callback(options.callback, options.scope, [ options, true,
          response ]);
    },

    handleScriptFailure : function(trans) {
      this.transId = false;
      this.destroyScriptTrans(trans, false);
      var options = trans.options;
      response = {
        argument :options.argument,
        status :500,
        statusText :'Server failed to respond',
        responseText :''
      };
      this.fireEvent("requestexception", this, response, options, {
        status :-1,
        statusText :'communication failure'
      });
      Ext.callback(options.failure, options.scope, [ response, options ]);
      Ext.callback(options.callback, options.scope, [ options, false,
          response ]);
    },

    // private
    destroyScriptTrans : function(trans, isLoaded) {
      document.getElementsByTagName("head")[0].removeChild(document
          .getElementById(trans.scriptId));
      clearTimeout(trans.timeoutId);
      if (isLoaded) {
        window[trans.cb] = undefined;
        try {
          delete window[trans.cb];
        } catch (e) {
        }
      } else {
        // if hasn't been loaded, wait for load to remove it
        // to prevent script error
        window[trans.cb] = function() {
          window[trans.cb] = undefined;
          try {
            delete window[trans.cb];
          } catch (e) {
          }
        };
      }},
          // private
    handleResponse : function(response){
        this.transId = false;
        var options = response.argument.options;
        response.argument = options ? options.argument : null;
        this.fireEvent("requestcomplete", this, response, options);
        if(options.success){
            options.success.call(options.scope, response, options);
        }
        if(options.callback){
            options.callback.call(options.scope, options, true, response);
        }
    },

    // private
    handleFailure : function(response, e){
        this.transId = false;
        var options = response.argument.options;
        response.argument = options ? options.argument : null;
        this.fireEvent("requestexception", this, response, options, e);
        if(options.failure){
            options.failure.call(options.scope, response, options);
        }
        if(options.callback){
            options.callback.call(options.scope, options, false, response);
        }
    }
    
});

///////////////////////////////////////////////////////////////////////////////////////////////////
//plugin : Ext.grid.CheckColumn

Ext.grid.CheckColumn = function(config) {
Ext.apply(this, config);
if (!this.id) {
 this.id = Ext.id();
}
this.renderer = this.renderer.createDelegate(this);
};

Ext.grid.CheckColumn.prototype = {
init : function(grid) {
 this.grid = grid;
 this.grid.on('render', function() {
   var view = this.grid.getView();
   view.mainBody.on('mousedown', this.onMouseDown, this);
 }, this);
},
// changed to fire afteredit event
onMouseDown : function(e, t) {
 if (t.className && t.className.indexOf('x-grid3-cc-' + this.id) != -1) {
   e.stopEvent();
   var index = this.grid.getView().findRowIndex(t);
   var record = this.grid.store.getAt(index);
   record.set(this.dataIndex, !record.data[this.dataIndex]);

   // fire afterEdit with all required params
   this.grid.fireEvent('afterEdit', {
     grid :this.grid,
     record :record,
     field :this.dataIndex,
     value :record.data[this.dataIndex],
     originalValue :!record.data[this.dataIndex],
     row :index,
     column :this.grid.getColumnModel().getIndexById(this.id)
   });
 }
},

renderer : function(v, p, record) {
 p.css += ' x-grid3-check-col-td';
 return '<div class="x-grid3-check-col' + (v ? '-on' : '') + ' x-grid3-cc-'
     + this.id + '">&#160;</div>';
}
};
/////////////////////////////////////////////////////////////////////////////////////////////////////

Ext.ns('Extensive.grid');
Extensive.grid.ItemDeleter = Ext.extend(
  Ext.grid.RowSelectionModel,
  {
    width :30,
    sortable :false,
    dataIndex :0, // this is needed, otherwise there will be an error
    stripeRows: true,
    id :'deleter',
    header : '<div class="extensive-remove" style="width: 15px; height: 16px;"></div>',
    initEvents : function() {
      Extensive.grid.ItemDeleter.superclass.initEvents.call(this);
      this.grid.on('cellclick',
          function(grid, rowIndex, columnIndex, e) {
            if (columnIndex == grid.getColumnModel().getIndexById('deleter')) {
              var record = grid.getStore().getAt(rowIndex);
              grid.deleteAction(record, grid.viewName);
              grid.getStore().remove(record);
              grid.getView().refresh();
            }
          });
      this.grid.on('headerclick',
          function(grid, columnIndex, e) {
            if (columnIndex == grid.getColumnModel().getIndexById('deleter')) {
              grid.deleteAction('all', grid.viewName);
              grid.getStore().removeAll()
              grid.getView().refresh();
            }
          });
    },
  
    renderer : function(v, p, record, rowIndex) {
      return '<div class="extensive-remove" style="width: 15px; height: 16px;"></div>';
    }
  });

function setResponseObject(response) {
  if(response.responseObject)
    return response;
  response.responseObject = eval('('+response.responseText+')');
  return response;
}