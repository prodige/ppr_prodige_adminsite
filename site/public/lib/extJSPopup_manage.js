
/**
 * Ouverture de la popup extjs 
 */
function  ExtJSPopup_Ouvrir(url, cible, title, width, height)
{  
  loadExtPopup(url, cible, title, width, height);
}

/**
 * Fermeture de la popup extjs
 */
function  ExtJSPopup_Fermer(popup_position)
{
  var iframe_admin = window;
  iframe_admin.closeExtPopup(popup_position);
  //window.close();
}

//charlotte 22 mai 2009
/**
 * Ajustement de la taille de la popup ExtJS (div) et de l'iframe contenue dans cette popup IframePopup_Contenu
 */
function  ExtJSPopup_resizeExtJS(popup_position)
{
  
  var   data=null;
  var   hauteur = 0;
  var   largeur = 0;
  var   dx, dy;
  var iframe_admin=null;
  
  try
  {
    
    //IframePopup_Contenu
    iframe_admin = window;
    iframe_popup_window = iframe_admin.tabExtPopup[popup_position].getFrameWindow();

    //impossible d'obtenir certaines propriétés sur le iframe_popup_byid (objet iframe) donc
    //on passe par le iframe_popup_byname (objet window)
    
    if (iframe_popup_window)
    { 

      largeur = iframe_popup_window.document.documentElement.scrollWidth +15;
      hauteur = iframe_popup_window.document.documentElement.scrollHeight;
      
      largeur_admin = iframe_admin.document.documentElement.scrollWidth +15;
      hauteur_admin = iframe_admin.document.documentElement.scrollHeight;
     
      if (hauteur > (hauteur_admin * 0.9))
        document.body.scroll = "yes";
      else{
        document.body.scroll = "no";
      } 
 
      if (largeur  > (largeur_admin * 0.95)){
        document.body.scroll = "yes";
      }
      else{
        document.body.scroll = "no";
      }

      //charlotte : deuxieme retaille enlevée
      largeur = Math.min(largeur, largeur_admin * 0.95);
      hauteur = Math.min(hauteur, hauteur_admin * 0.9);

      if (document.body.scroll=="yes"){
        hauteur=eval(hauteur+20);
      }

      //resize extjs popup          
      iframe_admin.resizeExtPopup(popup_position, largeur, hauteur);
      iframe_admin.moveExtPopup(popup_position);
      
      
    }
  }
  catch(e)
  {
    
  }
  
}