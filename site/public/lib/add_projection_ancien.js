var projections_disponibles = null;

/**
 * remplit la liste deroulante des projections disponibles
 * cette liste etant l'intersection du fichier /usr/share/proj/epsg et de la table spatial_ref_sys
 * @return 
 */
function load_proj_dispo_select(proj_selected){
	var win_loading = new Ext.Window({
		width: 300,
		height: 100,
		modal: true,
		closeAction: 'hide',
		resizable: false,
		maximizable: false,
		title: 'Chargement',
		html: '<p>Liste des projections disponibles en cours de chargement.</p>'
	});
	win_loading.show(this);
	
	var epsg_dispo_file 	= "";
	var epsg_dispo_table	= "";
	//On recupere les projections en base dans la table spatial_ref_sys
	Ext.Ajax.timeout = 90000;
	Ext.Ajax.request({
		url: "proxy_spatial_ref_sys.php?",
		params: {
			'mode' : "epsg_intersect"
		},
		success: function(response){
			projections_disponibles = eval('('+response.responseText+')');
			//On remplit la liste déroulante
			var list_to_populate = document.getElementById('proj_dispo');
			var nbOptions = 1;
			var option = document.createElement("option");
			option.value = "-1";
			option.text = "Saisir une projection disponible.";
			try{
				list_to_populate.add(option, liste_to_populate.options[null]);
			}catch(e){
				list_to_populate.add(option, null);
			}
			for(var projection in projections_disponibles){
				var option = document.createElement("option");
				var c_proj = projections_disponibles[projection];
				option.value = c_proj.srid;
				option.title = c_proj.nom+" ("+c_proj.auth_name+" "+c_proj.auth_srid+")";
				option.text  = c_proj.nom+" ("+c_proj.auth_name+" "+c_proj.auth_srid+")";
				if(c_proj.srid == proj_selected)
					option.setAttribute("selected", "selected");
				try{
					list_to_populate.add(option, list_to_populate.options[null]);
				}catch(e){
					list_to_populate.add(option,null);
				}
				nbOptions++;
			}
			if(nbOptions == 1){
				Ext.MessageBox.alert('Erreur', 'Aucune projection disponible.');
			}	

			transform_projections_list();
			win_loading.hide(this);
		},
		failure: function(){
			win_loading.hide(this);
			Ext.MessageBox.alert('Erreur', 'La consultation de la base de donn&eacute;es a renvoy&eacute; une erreur.');
		}
	});
}


/**
 * remplissage des champs au click d'une projection dans la liste déroulante
 * @return void
 */
function populate_by_epsg(srid){
	if(srid != -1)
	  var projection_array = projections_disponibles[srid];
	if(projection_array && (typeof projection_array !== 'undefined')){
	  document.getElementById('epsg_proj').value 		= projection_array.srid;
	  document.getElementById('nom_proj').value			= projection_array.nom;
	  document.getElementById('proj4_proj').value 		= projection_array.proj4text;
	  document.getElementById('srid_proj').value 		= projection_array.srid
	  document.getElementById('auth_name_proj').value 	= projection_array.auth_name;
	  document.getElementById('auth_srid_proj').value 	= projection_array.auth_srid;
	  document.getElementById('srtext_proj').value 		= projection_array.srtext;
	  setReadOnly(true);
	}else{
	  document.getElementById('epsg_proj').value 	  = "";
	  document.getElementById('nom_proj').value		  = "";
	  document.getElementById('proj4_proj').value 	  = "";
	  document.getElementById('srid_proj').value 	  = "";
	  document.getElementById('auth_name_proj').value = "";
	  document.getElementById('auth_srid_proj').value = "";
	  document.getElementById('srtext_proj').value 	  = "";
	  setReadOnly(false);
	}	
}


/**
 * ajout d'une projection
 * @return void
 */
function preremplir_epsg(){
	var select = document.getElementById("proj_dispo");
	select.selectedIndex = 0;
	var win_loading = new Ext.Window({
		width: 300,
		height: 100,
		modal: true,
		closeAction: 'hide',
		resizable: false,
		maximizable: false,
		title: 'Chargement',
		html: '<p>Interrogation de la base spatialreference.org.</p>'
	});
	win_loading.show(this);
	
	var epsg_code = document.getElementById('epsg_proj').value;
	//On recupere valeur proj4 de la projection a preremplir
	//http://spatialreference.org/ref/epsg/EPSG_PROJ/proj4/
	var proj4 = "";
	var postgis = "";
	Ext.Ajax.request({
		url: "proxy_spatial_ref_sys.php?",
		params: {
			'mode'		: "preremplir",
			'epsg_proj' : epsg_code,
		},
		success: function(response){
			var result_array = eval('('+response.responseText+')');
			if((result_array.proj4 == '') || (result_array.postgis=='')){
				Ext.MessageBox.alert('Erreur', 'L\'int&eacute;rrogation de la base spatialreference.org n\'a renvoy&eacute aucun r&eacute;sultat pour la projection '+epsg_code+'.')
			}
			//On vide le champ nom s'il etait present
			document.getElementById('nom_proj').value = "Undefined ("+result_array.auth_name+" "+result_array.auth_srid+")";
			//On remplit le champ proj4
			document.getElementById('proj4_proj').value = result_array.proj4;
			//On remplit les champs postgis(srid,atuh_name,auth_srid,srtext)
			document.getElementById('srid_proj').value = result_array.srid;
			document.getElementById('auth_name_proj').value = result_array.auth_name;
			document.getElementById('auth_srid_proj').value = result_array.auth_srid;
			document.getElementById('srtext_proj').value = result_array.srtext;
			win_loading.hide(this);
		},
		failure: function(){
			win_loading.hide(this);
			Ext.MessageBox.alert('Projection '+epsg_code, 'L\'int&eacute;rrogation de la base spatialreference.org n\'a renvoy&eacute aucun r&eacute;sultat.')
		}
	});
}

/**
 * liste deroulante pour les projections en base
 */
function transform_projections_list() {
	Ext.QuickTips.init();
	Ext.override( Ext.form.ComboBox, {
	  anyMatch: false,
      doQuery : function(q, forceAll){
	    if(q === undefined || q === null){
	      q = '';
	    }
	    var qe = {
	      query: q,
		  forceAll: forceAll,
		  combo: this,
		  cancel:false
	    };
	    if(this.fireEvent('beforequery', qe)===false || qe.cancel){
		  return false;
	    }
	    q = qe.query;
	    forceAll = qe.forceAll;
	    if(forceAll === true || (q.length >= this.minChars)){
		  if(this.lastQuery !== q){
		    this.lastQuery = q;
		    if(this.mode == 'local'){
		      this.selectedIndex = -1;
		      if(forceAll){
		        this.store.clearFilter();
	 	      }else{
		        this.store.filter(this.displayField, q, this.anyMatch);
		      }
		      this.onLoad();
		    }else{
		      this.store.baseParams[this.queryParam] = q;
		      this.store.load({
		        params: this.getParams(q)
		      });
		      this.expand();
		    }
          }else{
		    this.selectedIndex = -1;
		    this.onLoad();
	      }
	    }
      }
	});
       
  var transformed = new Ext.form.ComboBox({
	  tpl: '<tpl for="."><div ext:qtip="{text}" id="" class="x-combo-list-item">{text}</div></tpl>',
      allowBlank: false, 
      mode: 'local',
      displayField: 'text',
      valueField: 'value',
	  typeAhead: false,
      anyMatch: true,
      triggerAction: 'all',
      transform: 'proj_dispo',
      width: 345,
      forceSelection: true,
      listeners: {
        select: function(combo,record,index){
          var comboBox_value = transformed.getValue();
          populate_by_epsg(comboBox_value);
        }
      }
  });
}

/**
 * fonction permettant de creer une nouvelle projection a partir des informations saisies dans le formulaire
 * alimente postgis/proj/proj4js et met a jour la liste deroulante des projections disponibles
 */
function create_new_projection(){
  //1. On recupere les informations du formulaire
  var epsg		= document.getElementById('epsg_proj').value;
  var nom		= document.getElementById('nom_proj').value;
  var proj4		= document.getElementById('proj4_proj').value;
  var srid		= document.getElementById('srid_proj').value;
  var auth_name	= document.getElementById('auth_name_proj').value;
  var auth_srid	= document.getElementById('auth_srid_proj').value;
  var srtext	= document.getElementById('srtext_proj').value;
  //2. On insere les informations dans la base Postgis PRODIGE public.spatial_ref_sys et dans le ficier /usr/share/proj/epsg
  Ext.Ajax.timeout = 90000;
  Ext.Ajax.request({
	url: "proxy_spatial_ref_sys.php?",
	params: {
		'mode' 		: "insert_epsg",
		'epsg' 		: epsg,
		'nom'  		: nom,
		'proj4'		: proj4,
		'srid'		: srid,
		'auth_name'	: auth_name,
		'auth_srid'	: auth_srid,
		'srtext'	: srtext
	},
	success: function(response){
		result = response.responseText;
		if(result == "1")
		  Ext.MessageBox.alert('Nouvelle projection', 'La nouvelle projection a bien été créée.');
		else
		  Ext.MessageBox.alert('Erreur', 'La nouvelle projection n\'a pu être enregistrée.\n Erreur : '+result+'.');
	},
	failure: function(){
		Ext.MessageBox.alert('Erreur', 'La nouvelle projection n\'a pu être enregistrée.\n Erreur : Problème dans la requête AJAX.');
	}
  });
  //3. On ajoute cette option a la liste deroulante
  var list_to_populate = document.getElementById('proj_dispo');
  var option = document.createElement("option");
  option.value 		= srid;
  option.text  		= nom + " (" + auth_name + " " + auth_srid + ")";
  try{
    list_to_populate.add(option, liste_to_populate.options[null]);
  }catch(e){
	list_to_populate.add(option, null);
  }
  //4. On l'ajoute a notre tableau des projections disponibles
  projections_disponibles[srid] = {
    'srid': srid, 
    'nom': nom,
    'proj4text': proj4,
    'auth_name': auth_name,
    'auth_srid': auth_srid,
    'srtext': srtext
  }
}

/**
 * Ajoute ou supprime l'attribute readOnly aux champs du formulaire en fonction de la valeur de la variable toSet
 * @param toSet
 */
function setReadOnly(toSet){
  if(toSet){
    document.getElementById('epsg_proj').setAttribute('readonly', 'readonly');
    document.getElementById('nom_proj').setAttribute('readonly', 'readonly');
    document.getElementById('proj4_proj').setAttribute('readonly', 'readonly');
    document.getElementById('srid_proj').setAttribute('readonly', 'readonly');
    document.getElementById('auth_name_proj').setAttribute('readonly', 'readonly');
    document.getElementById('auth_srid_proj').setAttribute('readonly', 'readonly');
    document.getElementById('srtext_proj').setAttribute('readonly', 'readonly');  
  }else{
    document.getElementById('epsg_proj').removeAttribute('readonly');
    document.getElementById('nom_proj').removeAttribute('readonly');
    document.getElementById('proj4_proj').removeAttribute('readonly');
    document.getElementById('srid_proj').removeAttribute('readonly');
    document.getElementById('auth_name_proj').removeAttribute('readonly');
    document.getElementById('auth_srid_proj').removeAttribute('readonly');
    document.getElementById('srtext_proj').removeAttribute('readonly');
  }
}