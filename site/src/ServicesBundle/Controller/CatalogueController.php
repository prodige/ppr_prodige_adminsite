<?php

namespace ProdigeAdminSite\ServicesBundle\Controller;

use Doctrine\DBAL\Connection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Prodige\ProdigeBundle\Controller\BaseController;

use Prodige\ProdigeBundle\Common\AlkRequest;
use Symfony\Component\Validator\Constraints\Count;
use ProdigeAdminSite\ServicesBundle\Common\ServicesCatalogue;
use ProdigeAdminSite\ServicesBundle\Common\excel\Workbook;

class CatalogueController extends BaseController {

    /**
     * Get Doctrine connection
     * @param string $name
     * @param string $schema
     * @return Connection
     */
    protected function getConnection($name, $schema="public") {
        $conn = $this->getDoctrine()->getConnection($name);
        $conn->exec('set search_path to '.$schema);
        // Pour que la connection soit utilisé dans le namespace ServicesCatalogue
        ServicesCatalogue::setConnection($conn);
        return $conn;
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/catalogue/get_views", name="catalogue_get_views", options={"expose"=true}, methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function getViewsAction(Request $request) {

        /*
        $AdminPath = "Administration/";
        require_once($AdminPath."DAO/DAO/DAO.php");
        */

        //$dao = new DAO();
        $callback = $request->get("callback","");
        $view = $request->get("view","");
        $service = $request->get("service");

        $response = "";
        $conn = $this->getConnection('catalogue', 'catalogue');

        try {
            if ( method_exists($this, $service) ){
                $response = $this->$service($request, $conn, $callback, $view);
            } else {
                throw new \Exception(__METHOD__." : service inconnu = {$service}");
            }
        }catch(\Exception $exception){
            $response = $callback."(".json_encode(array("success"=>false, "msg"=>$exception->getMessage())).")";
        }

        $data = array();
        //Récupération des données d'une vue en json;
        /*if($service =="getData"){}else if ($service =="getFields"){}else if ($service =="deleteData"){}
        else if($service =="getDataProdiCatalo"){}*/
        
        return new Response($response);
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/catalogue/get_parametrages", name="services_get_param_catalogue", options={"expose"=true})
     * #Method({"GET", "POST"})
     */
    public function getParametragesAction(Request $request) { // testé hismail
        /**
         * @author Alkante
         * Service de lecture et mise à jour des paramétrages
         */
        //permet de ne pas prendre en compte le paramétrage SGBD

        //require_once('include/geonetworkInterface.php');
        //require_once('parametrage.php');
        //$service = AlkRequest::_GET("service");
        $service = $request->get("service");

        $conn = $this->getConnection('catalogue', 'catalogue');

        if($service == "initParametrage")
            $includeParametrageSgbd = false; // TODO : ne sera pas utilisable après ...

            //$AdminPath = "Administration/";
            //require_once($AdminPath."DAO/DAO/DAO.php");


            //$dao = new DAO();
            //$callback =AlkRequest::_GET("callback");
            $callback = $request->get("callback");
            //Déja pris
            //$service = AlkRequest::_GET("service");
            $data = array();
            //Récupération des données d'une vue en json;
            if($service =="getConstant"){ // testé hismail
                $query = "SELECT * from prodige_settings order by pk_prodige_settings_id";
                //$rs = $dao->Execute($query);
                $rs = $conn->fetchAllAssociative($query);
                $i =0;
                //while ($row = pg_fetch_array($rs))
                foreach($rs as $row)
                {
                    foreach($row as $key => $value){
                        if(!is_numeric($key)){
                            if($key == 'prodige_settings_param' && isset($value) && !is_null($value) && $value!=""){
                                $param = preg_split("/;/", $value);
                                
                                //$queryParam = "SELECT ".$param[0]." FROM ".$param[1];
                                $queryParam = $conn->createQueryBuilder();
                                $queryParam->select($param[0])
                                            ->from($param[1]);
                                
                                //$rsParam = $dao->Execute($queryParam);
                                $rsParam = $queryParam->execute()->fetchAllAssociative(\PDO::FETCH_ASSOC);
                                $j = 0;
                                $listParam = array();
                                //while ($rowParam = pg_fetch_array($rsParam))
                                foreach ($rsParam as $rowParam)
                                {
                                	$listParam[] = array_values($rowParam);
                                }
                                $data[$i][($key)] = $listParam;
                        }else{
                            $data[$i][($key)]= ($value);
                        }
                      }
                    }
                    $i++;
                }
                // geonetworkInterface ne sera pas utilisé
                /*
                $query = "SELECT  accs_id,  accs_adresse, accs_adresse_admin, accs_adresse_tele from acces_serveur limit 1";
                $rs = $dao->BuildResultSet($query);
                for ($rs->First(); !$rs->EOF(); $rs->Next()){
                    $data[$i]["prodige_settings_value"]= ($rs->Read(0));
                    $data[$i]["prodige_settings_desc"] = ("nom du serveur cartographique");
                    $data[$i]["prodige_settings_constant"] = "accs_id";
                    $i++;
                    $data[$i]["prodige_settings_value"]= $rs->Read(1);
                    $data[$i]["prodige_settings_desc"] = ("Url du serveur de consultation de cartes");
                    $data[$i]["prodige_settings_constant"] = "accs_adresse";
                    $i++;
                    $data[$i]["prodige_settings_value"]= $rs->Read(2);
                    $data[$i]["prodige_settings_desc"] = ("Url du serveur d'administration de cartes");
                    $data[$i]["prodige_settings_constant"] = "accs_adresse_admin";
                    $i++;
                    $data[$i]["prodige_settings_value"]= $rs->Read(3);
                    $data[$i]["prodige_settings_desc"] = ("Url du serveur de téléchargement de données");
                    $data[$i]["prodige_settings_constant"] = "accs_adresse_tele";
                }*/
                //GetFieldName
                //echo $callback."(".json_encode($data).")";
                $response = $callback."(".json_encode($data).")";
            }else if ($service =="updateConstant"){ // testé hismail
                // geonetworkInterface ne sera pas utilisé
                //cas particulier des mots de passe Géosource
               /* $geonetwork = new Geonetwork();
                if ($geonetwork->serverConnect("administrateur") ) {
                    $postParam = array();
                    $postParam["password"] = $PRO_GEONETWORK_ADMINISTRATEUR_PWD;
                    $postParam["newPassword"] = $_REQUEST["PRO_GEONETWORK_ADMINISTRATEUR_PWD"];
                    $postParam["newPassword2"] = $_REQUEST["PRO_GEONETWORK_ADMINISTRATEUR_PWD"];
                    $geonetwork->post($PRO_GEONETWORK_DIRECTORY."/srv/fre/user.pwupdate",$postParam);
                }
                //force la reconnexion à se faire
                $geonetwork->_isConnected = false;
                if ($geonetwork->serverConnect("editeur") ) {
                    $postParam = array();
                    $postParam["password"] =$PRO_GEONETWORK_EDITEUR_PWD;
                    $postParam["newPassword"] =$_REQUEST["PRO_GEONETWORK_EDITEUR_PWD"];
                    $postParam["newPassword2"] =$_REQUEST["PRO_GEONETWORK_EDITEUR_PWD"];
                    $geonetwork->post($PRO_GEONETWORK_DIRECTORY."/srv/fre/user.pwupdate",$postParam);
                }
                //force la reconnexion à se faire
                $geonetwork->_isConnected = false;
                if ($geonetwork->serverConnect("carte_perso") ) {
                    $postParam = array();
                    $postParam["password"] =$PRO_GEONETWORK_CARTE_PERSO_PWD;
                    $postParam["newPassword"] =$_REQUEST["PRO_GEONETWORK_CARTE_PERSO_PWD"];
                    $postParam["newPassword2"] =$_REQUEST["PRO_GEONETWORK_CARTE_PERSO_PWD"];
                    $geonetwork->post($PRO_GEONETWORK_DIRECTORY."/srv/fre/user.pwupdate",$postParam);
                }*/
                $query = " SELECT prodige_settings_constant from prodige_settings";
                //$rs = $dao->BuildResultSet($query);
                $rs = $conn->fetchAllAssociative($query);
                //for ($rs->First(); !$rs->EOF(); $rs->Next()){
                foreach($rs as $row){
                    //$var_name = $rs->Read(0);
                    $var_name = $row["prodige_settings_constant"];
                    //if (isset($_REQUEST[$var_name])){
                    if($request->get($var_name)) {
                        //si on obtient un tableau, pour liste des catalogues
                        //if(gettype($_REQUEST[$var_name])=="array"){
                        if(is_array($request->get($var_name))){
                            //$_REQUEST[$var_name]=implode(";", $_REQUEST[$var_name]);
                            $value = $request->get($var_name);
                            if ( empty($value) ) continue;
                            $request->query->set($var_name, implode(";", $value));
                            $request->request->set($var_name, implode(";", $value));
                        }
                        //$query = " update prodige_settings set prodige_settings_value = '".($_REQUEST[$var_name])."' where prodige_settings_constant = '".$var_name."'";
                        $query = $conn->createQueryBuilder();
                        $query->update("prodige_settings")
                              ->set("prodige_settings_value", ":prodige_settings_val")
                              ->where("prodige_settings_constant= :prodige_settings_const")
                              ->setParameters(array("prodige_settings_val" => $request->get($var_name), "prodige_settings_const" => $var_name));

                    } else {//checkbox case
                        //$query = " update prodige_settings set prodige_settings_value = 'off' where prodige_settings_constant = '".$var_name."'";
                        $query = $conn->createQueryBuilder();
                        $query->update("prodige_settings")
                        ->set("prodige_settings_value", "'off'")
                        ->where("prodige_settings_constant= :prodige_settings_const")
                        ->setParameter('prodige_settings_const', $var_name);
                    }
                    //$rs2 = $dao->Execute($query);
                    //$rs2 = $conn->executeUpdate($query, array($var_name));
                    $rs2 = $query->execute();

                }
                //maj acces_serveur
                /*
                $query = " update acces_serveur set accs_id = '".($_REQUEST["accs_id"])."',
             accs_adresse ='".($_REQUEST["accs_adresse"])."',
             accs_adresse_tele ='".($_REQUEST["accs_adresse_tele"])."',
             accs_adresse_admin ='".($_REQUEST["accs_adresse_admin"])."'";
                $rs = $dao->Execute($query);
        */
                //if(!isset($_REQUEST["PRO_MODE_PROPOSITION_ACTIF"])){//cas particulier du mode proposition actif
                if(!$request->get("PRO_MODE_PROPOSITION_ACTIF", false)){//cas particulier du mode proposition actif
                    //réinitialisation des métadonnées au statut en cours
                    $query = " update fiche_metadonnees set statut = 1 where statut = 2;";
                    //$rs = $dao->Execute($query);
                    $rs = $conn->executeStatement($query);
                }



                //$dao->Execute("COMMIT");
                //echo $callback."({success:true})";
                $response = $callback."({success:true})";
            }elseif($service == "initParametrage"){ // testé hismail
                $query = "SELECT prodige_settings_constant from prodige_settings";
                //$rs = $dao->BuildResultSet($query);
                $rs = $conn->fetchAllAssociative($query);

                $adminsite = $this->container->getParameter("adminsite");
                $prodige_settings_catalogue = (isset($adminsite["prodige_settings_catalogue"]) ? $adminsite["prodige_settings_catalogue"] : array());
                //for ($rs->First(); !$rs->EOF(); $rs->Next()){
                foreach($rs as $row) {
                    //$var_name = $rs->Read(0);
                    $var_name = $row["prodige_settings_constant"];
                    if(!array_key_exists($var_name, $prodige_settings_catalogue))
                        continue;
                    //$paramValue = $$var_name;
                    $paramValue = $prodige_settings_catalogue[$var_name];
                    //$query = " update prodige_settings set prodige_settings_value = '".$paramValue."' where prodige_settings_constant = '".$rs->Read(0)."'";
                    $query = $conn->createQueryBuilder();
                    $query->update("prodige_settings")
                    ->set("prodige_settings_value", ":prodige_settings_value")
                    ->where("prodige_settings_constant= :prodige_settings_constant")
                    ->setParameters(array(
                        "prodige_settings_value" => $paramValue,
                        "prodige_settings_constant" => $var_name
                    ));
                    //$dao->Execute($query);
                    $query->execute();
                }
                /*
                $query = " update acces_serveur set
            accs_id = '".$accs_id."',
            accs_adresse = '".$accs_adresse."',
            accs_adresse_admin = '".$accs_adresse_admin."',
            accs_adresse_tele = '".$accs_adresse_tele."'";
                $dao->Execute($query);
                $dao->Execute("COMMIT");*/

                //echo $callback."({success:true})";
                $response = $callback."({success:true})";
            }elseif($service == "getConstantByName"){
                //$constantName = AlkRequest::_GET("constantName");
                $constantName = $request->get("constantName");
                $constvalue = "";
                //$query = "SELECT prodige_settings_value from prodige_settings where prodige_settings_constant='".$constantName."';";
                $query = "SELECT prodige_settings_value from prodige_settings where prodige_settings_constant=?;";
                //$rs = $dao->BuildResultSet($query);
                $rs = $conn->fetchAllAssociative($query, array($constantName));
                //$i =0;
                //if($rs->GetNbRows()==1){
                if(count($rs)) {
                    //$rs->First();
                    //$constvalue = $rs->Read(0);
                    $constvalue = $rs[0]["prodige_settings_value"];
                }
                //echo json_encode($constvalue);
                $response = $callback."(".json_encode($constvalue).")";
            }
            return new Response($response);
    }

/************************** Fonctions pas actions de controlleurs ************************************************************/

    // Les différentes fonctionnalités possibles passés en paramètre "service" du service "catalogue_get_views" sont divisées en fonctions
    // Vu la présence de 2 switchs (service et viewname) et appel des services entre modules ...
    /**
     * Service : getData
     * @param Request $request
     * @param  $conn
     * @param  $callback
     * @param  $view
     * @return $response
     */
    public function getData(Request $request, $conn, $callback, $view) {
        $data = array();
        $query = $conn->createQueryBuilder();
        $query->select("*")
        ->from($view);
        if($view=="incoherence_metadata_fiche_metadonnees") {
            $additionnalRequest = "id not in (".PRO_WMS_METADATA_ID.",".PRO_WFS_METADATA_ID.",".PRO_DOWNLOAD_METADATA_ID.")";
        }else {
            $additionnalRequest = ($request->get("additionnalRequest", false)) ? stripslashes($request->get("additionnalRequest")) : "";
        }
        if($additionnalRequest!=""){
            $query->where($additionnalRequest);
        }
        $rs = $query->execute()->fetchAll();
        $i =0;

        foreach ($rs as $row) {
            foreach($row as $key => $value){
                if(!is_numeric($key))
                    $data[$i][($key)]= $value;
            }
            $i++;
        }

        $response = $callback."(".json_encode($data).")";
        return $response;
    }

    /**
     * Service : getFields
     * @param Request $request
     * @param  $conn
     * @param  $callback
     * @param  $view
     * @return $response
     */
    public function getFields(Request $request, $conn, $callback, $view) {

       /* if($view == "incoherence_fiche_metadonnees_dom_ssdom"){
            $data = array(
                "id",
                "titre",
                "date de création de la fiche",
                "identifiant géosource",
                "statut"
            );
        } else {*/
            //TODO
            // Avant : a.attname, ---> Après : a.attname as a_attname,
            $query = "SELECT a.attnum, a.attname as a_attname".
                " FROM pg_class c, pg_attribute a, pg_type t " .
                //" WHERE c.relname = '".strtolower($view)."'".
                " WHERE c.relname = ?".
                " and a.attnum > 0".
                " and a.attname <> 'the_geom'".
                " and a.attrelid = c.oid".
                " and a.atttypid = t.oid".
                " ORDER BY attnum";
            //$rs = $dao->BuildResultSet($query);
            $rs = $conn->fetchAll($query, array(strtolower($view)));
            $lastType = '';
            $lastSDom = '';
            //for ($rs->First(); !$rs->EOF(); $rs->Next()){
            foreach ($rs as $row) {
                //$data[]=($rs->read(1));
                $data[]=($row['a_attname']);
            }
        //
        //echo $callback."(".json_encode($data).")";
        $response = $callback."(".json_encode($data).")";
        return $response;
    }

    /**
     * Service : deleteData
     * @param Request $request
     * @param  $conn
     * @param  $callback
     * @param  $view
     * @return $response
     * @throws Exception
     */
    public function deleteData(Request $request, $conn, $callback, $view) {

        //$idValue = $_GET["idValue"];
        $idValue = $request->get("idValue");
        //$viewName = $_GET["viewName"];
        $viewName = $request->get("viewName");

        $success = true;
        $query = array(); 

        switch($viewName) {
            case "incoherence_couche_fiche_metadonnees":
                $query[0] = "delete from couche_donnees ".
                    ($idValue == "all" ?
                    "where pk_couche_donnees in (select pk_couche_donnees from incoherence_couche_fiche_metadonnees)" :
                    //"where pk_couche_donnees = ".$idValue);
                    "where pk_couche_donnees = ?");
            break;
            case "incoherence_carte_fiche_metadonnees":
                $query[0] = "delete from carte_projet ".
                    ($idValue == "all" ?
                    "where pk_carte_projet in (select pk_carte_projet from incoherence_carte_fiche_metadonnees)" :
                    //"where pk_carte_projet = ".$idValue);
                    "where pk_carte_projet = ?");
            break;
            case "incoherence_fiche_metadonnees_metadata" :
                $query[0] = "delete from carte_projet where ".
                    ($idValue == "all" ?
                    "cartp_fk_fiche_metadonnees	in (select pk_fiche_metadonnees from incoherence_fiche_metadonnees_metadata); " :
                    //"cartp_fk_fiche_metadonnees = ".$idValue.";").
                    "cartp_fk_fiche_metadonnees = ?;");
                
                  $query[1] =  "delete from fiche_metadonnees ".
                    ($idValue == "all" ?
                    "where pk_fiche_metadonnees in (select pk_fiche_metadonnees from incoherence_fiche_metadonnees_metadata)" :
                    //"where pk_fiche_metadonnees = ".$idValue);
                    "where pk_fiche_metadonnees = ?");
            break;
            case "incoherence_metadata_fiche_metadonnees" :
                $query[0]  = "delete from public.validation ".
                    ($idValue == "all" ?
                    //"where metadataid in (select id from incoherence_metadata_fiche_metadonnees) and metadataid not in (".PRO_WMS_METADATA_ID.",".PRO_WFS_METADATA_ID.",".PRO_DOWNLOAD_METADATA_ID.")" :
                    "where metadataid in (select id from incoherence_metadata_fiche_metadonnees) and metadataid not in (?, ?, ?)" :
                    //"where metadataid = ".$idValue).";".
                    "where metadataid = ?").";"; 
                
                  $query[1]  =   "delete from public.metadatacateg  ".
                    ($idValue == "all" ?
                    //"where metadataid in (select id from incoherence_metadata_fiche_metadonnees) and metadataid not in (".PRO_WMS_METADATA_ID.",".PRO_WFS_METADATA_ID.",".PRO_DOWNLOAD_METADATA_ID.")" :
                    "where metadataid in (select id from incoherence_metadata_fiche_metadonnees) and metadataid not in (?, ?, ?)" :
                    //"where metadataid = ".$idValue).";".
                    "where metadataid = ?").";";
                   $query[2]  =  "delete from public.operationallowed  ".
                    ($idValue == "all" ?
                    //"where metadataid in (select id from incoherence_metadata_fiche_metadonnees) and metadataid not in (".PRO_WMS_METADATA_ID.",".PRO_WFS_METADATA_ID.",".PRO_DOWNLOAD_METADATA_ID.")" :
                    "where metadataid in (select id from incoherence_metadata_fiche_metadonnees) and metadataid not in (?, ?, ?)" :
                    //"where metadataid = ".$idValue).";".
                    "where metadataid = ?").";"; 
                    $query[3]  = "delete from public.metadata  ".
                    ($idValue == "all" ?
                    //"where id in (select id from incoherence_metadata_fiche_metadonnees) and id not in (".PRO_WMS_METADATA_ID.",".PRO_WFS_METADATA_ID.",".PRO_DOWNLOAD_METADATA_ID.")" :
                    "where metadataid in (select id from incoherence_metadata_fiche_metadonnees) and metadataid not in (?, ?, ?)" :
                    //"where id = ".$idValue);
                    "where id = ?");
            break;
            case "incoherence_couche_sous_domaines":
                $query[0]   = "delete from fiche_metadonnees where ".
                    ($idValue == "all" ? " fmeta_fk_couche_donnees in (select pk_couche_donnees from incoherence_couche_sous_domaines)" :
                    //"fmeta_fk_couche_donnees = ".$idValue).
                    "fmeta_fk_couche_donnees = ?").
                    ";"; 
                
                    $query[1] = "delete from couche_donnees ".
                    ($idValue == "all" ?
                    "where pk_couche_donnees in (select pk_couche_donnees from incoherence_couche_sous_domaines)" :
                    //"where pk_couche_donnees = ".$idValue);
                    "where pk_couche_donnees = ?");
            break;
            case "incoherence_carte_sous_domaines":
                $query[0] = "delete from carte_projet ".
                    ($idValue == "all" ?
                    "where pk_carte_projet in (select pk_carte_projet from incoherence_carte_sous_domaines)" :
                    //"where pk_carte_projet = ".$idValue);
                    "where pk_carte_projet = ?");
            break;
            case "getDataProdiCatalo" :
                // Un grand changement par rapport à la version précédente
                // Pas d'appels de services entre les différents modules (prodigeadminsite, prodige admincarto et prodigecatalogue)
                // Avant
                // Ce service appel
                // $urlServGetTableList = "http://".$accs_adresse_admin."/PRRA/Services/deleteTable.php?token=".$param
                // Qui appel à son tour (revient à la service d'appel principal)
                // $urlServGetTableList = "http://".$PRO_SITE_URL."/PRRA/administration_get_views.php?service=getDataProdiCatalo&column=Table&_dc=1433843039901";

                //$isAll = $_GET["isAll"];
                $isAll = @eval("return ".$request->get("isAll", "false").";") ?: false;
                //$data = array();
                //if(isset($tabGet["isAll"]) && $tabGet["isAll"] =="true"){
                //$urlServGetTableList = "http://".$PRO_SITE_URL."/PRRA/administration_get_views.php?service=getDataProdiCatalo&column=Table&_dc=1433843039901";
                //$res = @file_get_contents($urlServGetTableList);error_log($res);
                //$res = substr(substr($res,1), 0, -1);
                //$res = json_decode($res);
                //foreach ($res as $tableName){
                    //$data[]= $tableName->Table;
                //}
                $tableNames = array();
                if($isAll){
                   $request->query->set("column", "table");
                   $res = $this->getDataProdiCatalo($request, $conn);
                   $res = substr(substr($res,1), 0, -1);
                   $res = json_decode($res, true);
                   foreach ($res as $tableName){
                       $tableNames[]= $tableName["table"];
                   }
                } else {
                    //$data[] = $tabGet["tableName"];
                    $tableNames[] = $idValue;
                }
                
                if(!empty($tableNames)) {
                    $connCarto = $this->getConnection('prodige', 'public');
                    ServicesCatalogue::setConnection($connCarto);
                    $success = ServicesCatalogue::deleteTable($tableNames);
                }
            break;
        } // Fin switch
        
        
        if(!(empty($query))) {
            try {
                $viewNames = array(
                    "incoherence_couche_fiche_metadonnees",
                    "incoherence_carte_fiche_metadonnees",
                    "incoherence_fiche_metadonnees_metadata",
                    "incoherence_carte_sous_domaines", 
                    "incoherence_couche_sous_domaines"
                );
                $conn->beginTransaction();
                for($i = 0 ; $i <= count($query); $i++) {
                    if(isset($query[$i])) {
                        if(in_array($viewName, $viewNames)) {
                            if($idValue=="all"){
                                $conn->executeQuery($query[$i]);
                            }else{
                                $conn->executeQuery($query[$i], array($idValue));
                            }
                        } else {
                            $conn->executeQuery($query[$i], array(PRO_WMS_METADATA_ID, PRO_WFS_METADATA_ID, PRO_DOWNLOAD_METADATA_ID, $idValue));
                        }
                        $conn->executeQuery("COMMIT");     
                        //$conn->commit();
                    }
                }
            } catch (\Exception $exception){
                
                $conn->rollBack();
                $success = false;
                throw $exception;
            }
        }
        
        //echo $callback."({success:true})";
        $response = $callback."({success:".($success ? "true" : "false")."})";
        return $response;
    }

    /**
     * Service : getDataProdiCatalo
     * @param Request $request
     * @param  $conn
     * @param  $callback
     * @param  $view
     * @return $response
     */
    public function getDataProdiCatalo(Request $request, $conn=null, $callback="", $view=null) {

        //$column = (isset($_GET["column"]) ? stripslashes($_GET["column"]) : "");
        $column = stripslashes($request->get("column", ""));
        // TODO ???

        //$urlServGetTableList = "http://".$accs_adresse_admin."/PRRA/Services/getTableList.php?";
        //$res = @file_get_contents($urlServGetTableList);
        //$res = explode(',',trim($res,'[]'));
        $data = array();
        
        $connCarto = $this->getConnection('prodige', 'public');
        ServicesCatalogue::setConnection($connCarto);
        
        $tableNames = ServicesCatalogue::getTableList();
        //foreach ($res as $tableName){
        foreach ($tableNames as $tableName){
            //$tableName = explode(':',trim($tableName,'{}'));
            //$query = "SELECT count(*) as nb FROM couche_donnees WHERE couche_donnees.couchd_emplacement_stockage like ".$tableName[1]." AND (couchd_type_stockage=1 OR couchd_type_stockage=-3);";
            $query = "SELECT 1 FROM couche_donnees WHERE couche_donnees.couchd_emplacement_stockage like ? AND (couchd_type_stockage=1 OR couchd_type_stockage=-3);";
            //$rs = $dao->BuildResultSet($query);
            $rs = $conn->executeQuery($query, array($tableName));
            //$rs->First();
            //if($rs->Read(0)==0){
            if($rs->rowCount()==0){
                $data[][$column]= $tableName;
            }
        }
        /*$urlServGetTableList = "http://".$accs_adresse_admin."/PRRA/Services/getTableList.php?bView=1";
         $res = @file_get_contents($urlServGetTableList);
         $res = explode(',',trim($res,'[]'));
         foreach ($res as $tableName){
         $tableName = explode(':',trim($tableName,'{}'));
         $query = "SELECT count(*)
         FROM couche_donnees
         WHERE couche_donnees.couchd_emplacement_stockage like ".$tableName[1]." AND (couchd_type_stockage=-4);";
         $rs = $dao->BuildResultSet($query);
         $rs->First();
         if($rs->Read(0)==0){
         $data[][$column]= trim($tableName[1],"'");
         }
         }*/
        //echo $callback."(".json_encode($data).")";
        $response = $callback."(".json_encode($data).")";
        return $response;
    }
    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/catalogue/get_couches_list", name="services_get_couches_list", options={"expose"=true})
     * #Method({"GET", "POST"})
     */
    public function getCouchesListAction(Request $request) {

        //$AdminPath = "../Administration/";
        //require_once("../parametrage.php");
        //require_once($AdminPath."DAO/DAO/DAO.php");

        //$callback = ( isset($_REQUEST["callback"]) ? $_REQUEST["callback"] : "" );
        $callback = $request->get("callback", "");
        //$dao = new DAO();
        $connCatalogue = $this->getConnection('catalogue', 'catalogue');
        //$callback = "stcCallback1010"; pour tester
        if($callback!=""){

            $data = array();
            //Récupération des données d'une vue en json;
            $query = "select couchd_nom, couchd_emplacement_stockage, couchd_type_stockage from catalogue.couche_donnees where ".
                "couchd_type_stockage=1 OR couchd_type_stockage=0 order by couchd_nom";
            //$rs_tables=$dao->BuildResultSet($query);
            $rs_tables = $connCatalogue->fetchAllAssociative($query);
            //$tablesCarto = file_get_contents("http://".$accs_adresse_admin."/PRRA/Services/getTableList.php");

            /*$tablesCarto = preg_split("/,/",trim($tablesCarto,"[]"));
            $dataCarto = array();
            foreach ($tablesCarto as $table){
                $table = preg_split("/:/",trim($table,'{}'));
                $dataCarto[]=trim($table[1],"'");
            }*/

            $connCarto = $this->getConnection('prodige', 'public');
            ServicesCatalogue::setConnection($connCarto);
            $dataCarto = ServicesCatalogue::getTableList();
            ServicesCatalogue::setConnection($connCatalogue);

            //for ($rs_tables->First(); !$rs_tables->EOF(); $rs_tables->Next())
            foreach($rs_tables as $row)
            {
                //if(in_array($rs_tables->Read(1), $dataCarto) && $rs_tables->Read(2) == 1)
                if(in_array($row["couchd_emplacement_stockage"], $dataCarto) && $row["couchd_type_stockage"] == 1) {
                    //$data[] = array('path'=>$rs_tables->Read(1),'name'=>$rs_tables->Read(0),'type'=>$rs_tables->Read(2));
                    $data[] = array('path'=>$row["couchd_emplacement_stockage"],'name'=>$row["couchd_nom"],'type'=>$row["couchd_type_stockage"]);
                }
                //else if($rs_tables->Read(2) == 0)
                else if($row["couchd_type_stockage"] == 0) {
                    //$data[] = array('path'=>$rs_tables->Read(1),'name'=>$rs_tables->Read(0),'type'=>$rs_tables->Read(2));
                    $data[] = array('path'=>$row["couchd_emplacement_stockage"],'name'=>$row["couchd_nom"],'type'=>$row["couchd_type_stockage"]);
                }
            }
            //echo $callback."(".json_encode($data).")";
            $response = $callback."(".json_encode($data).")";
        } else{ // version automate non modifiable
            $data = array();
            //Récupération des données d'une vue en json;
            $query = "select couchd_nom, couchd_emplacement_stockage from catalogue.couche_donnees where ".
                "couchd_type_stockage=1 order by couchd_nom";
            //$rs_tables=$dao->BuildResultSet($query);
            $rs_tables = $connCatalogue->fetchAllAssociative($query);
            $str = "[";
            //for ($rs_tables->First(); !$rs_tables->EOF(); $rs_tables->Next())
            foreach($rs_tables as $row)
            {
                //$data[] = array($rs_tables->Read(1),$rs_tables->Read(0));
                $data[] = array($row["couchd_emplacement_stockage"],$row["couchd_nom"]);
            }
            //echo json_encode($data);
            $response = json_encode($data);
        }
        return new Response($response ?: "FIN");
    }
    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/catalogue/get_data", name="catalogue_get_data", options={"expose"=true})
     * #Method({"GET", "POST"})
     */
    public function getDataAction(Request $request) {

        //$AdminPath = "../Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");


        //$dao = new DAO();

        //$dao->setSearchPath("public, ".PRO_SCHEMA_NAME);
        $adminsite = $this->container->getParameter("adminsite");
        $conn = $this->getConnection('catalogue', "public, ".$adminsite["PRO_SCHEMA_NAME"]);
        //$callback = (isset($_GET["callback"]) ? $_GET["callback"] :"");
        $callback = $request->get("callback", "");
        //$view = (isset($_GET["view"]) ? $_GET["view"] : "");
        $view = $request->get("view", "");
        //$service = (isset($_GET["service"]) ? $_GET["service"] : "");
        $service = $request->get("service", "");
        //$strSql = (isset($_GET["additionnalRequest"]) ? StripSlashes($_GET["additionnalRequest"]) : "");
        $strSql = $request->get("additionnalRequest", "");

        $data = array();
        //Récupération des données d'une vue en json;
        if($service =="getData"){
            if( strtolower(substr($strSql, 0, 6)) == "select"  ||
                strtolower(substr($strSql, 0, 7)) == "(select" ||
                strtolower(substr($strSql, 0, 4)) == "show"    )
            {
                //$rs = $dao->ExecuteGetResult($strSql);
                $rs = $conn->fetchAllAssociative($strSql);
                $i = 0;
                //while ($row = pg_fetch_array($rs))
                foreach($rs as $row)
                {
                    foreach($row as $key => $value){
                        if(!is_numeric($key))
                            $data[$i][($key)]= $value;
                    }
                    $i++;
                }
                //GetFieldName
                //echo $callback."(".json_encode($data).")";
                $response = $callback."(".json_encode($data).")";
            }
            //UPDATE and DELETE
            else if( strtolower(substr($strSql, 0, 6)) == "update"  ||
                strtolower(substr($strSql, 0, 7)) == "(update" ||
                strtolower(substr($strSql, 0, 6)) == "delete"  ||
                strtolower(substr($strSql, 0, 7)) == "(delete" )
            {
                //$rs = $dao->ExecuteGetResult($strSql);
                try {
                    $cmdtuples = $conn->executeStatement($strSql);
                    if($cmdtuples > 0) {
                        //$cmdtuples = pg_affected_rows($rs);
                        //$sqlStmt = "COMMIT";
                        //$dao->Execute($sqlStmt);
                        //echo $callback."(".$cmdtuples . " ligne(s) affectée(s).)";
                        $response = $callback."(".$cmdtuples . " ligne(s) affectée(s).)";
                    }
                } catch(\Exception $e) {
                    //echo $callback."(\"".("Il y a eu une erreur à l'exécution de la requête")."\")";
                    $response = $callback."(\"".("Il y a eu une erreur à l'exécution de la requête")."\")";
                }
            }
            else {
                //echo $callback."(\"".("Seules les requêtes de type SELECT, UPDATE ou DELETE sont acceptées.")."\")";
                $response = $callback."(\"".("Seules les requêtes de type SELECT, UPDATE ou DELETE sont acceptées.")."\")";
            }
        }else if ($service =="getFields"){
            if( strtolower(substr($strSql, 0, 6)) == "select"  ||
                strtolower(substr($strSql, 0, 7)) == "(select" ||
                strtolower(substr($strSql, 0, 4)) == "show"    )
            {
                //$rs = $dao->ExecuteGetResult($strSql);
                $rs = $conn->fetchAllAssociative($strSql);
                if($rs){
                    $lastType = '';
                    $lastSDom = '';
                    //$i = 0;
                    //$row = pg_fetch_array($rs);
                    //if($row){
                    foreach($row as $key => $value){
                        if(!is_numeric($key))
                            $data[]= ($key);
                    }
                    //}
                    //$i++;
                    //echo $callback."(".json_encode($data).")";
                    $response = $callback."(".json_encode($data).")";
                }else{
                    //echo $callback."(\"".("Il y a eu une erreur à l'exécution de la requête")."\")";
                    $response = $callback."(\"".("Il y a eu une erreur à l'exécution de la requête")."\")";
                }
            }//exécution directe
            else if( strtolower(substr($strSql, 0, 6)) == "update"  ||
                strtolower(substr($strSql, 0, 7)) == "(update" ||
                strtolower(substr($strSql, 0, 6)) == "delete"  ||
                strtolower(substr($strSql, 0, 7)) == "(delete" )
            {
                //$rs = $dao->ExecuteGetResult($strSql);
                try {
                    $cmdtuples = $conn->executeStatement($strSql);
                    if($cmdtuples > 0) {
                        //$cmdtuples = pg_affected_rows($rs);
                        //$sqlStmt = "COMMIT";
                        //$dao->Execute($sqlStmt);
                        //echo $callback."(\"".($cmdtuples . " ligne(s) affectée(s)")."\")";
                        $response = $callback."(\"".($cmdtuples . " ligne(s) affectée(s)")."\")";
                    }
                } catch(\Exception $e) {
                    //echo $callback."(\"".("Il y a eu une erreur à l'exécution de la requête")."\")";
                    $response = $callback."(\"".("Il y a eu une erreur à l'exécution de la requête")."\")";
                }
            }else{
                //echo $callback."(\"".("Seules les requêtes de type SELECT, UPDATE ou DELETE sont acceptées")."\")";
                $response = $callback."(\"".("Seules les requêtes de type SELECT, UPDATE ou DELETE sont acceptées")."\")";
            }
        }elseif($service =="exportData"){
            $tabValues =array();
            $tabFields = array();
                        //select * from incoherence_carte_fiche_metadonnees
            if($strSql == "select * from incoherence_fiche_metadonnees_dom_ssdom"){
                $data = array();
                $query = "SELECT pk_fiche_metadonnees as id,
                            array_to_string(array_agg(ssdom_nom), ',') as ss_dom,
                            array_to_string(array_agg(dom_nom), ',') as dom,
                            xpath('//gmd:keyword[count(../gmd:type /gmd:MD_KeywordTypeCode[@codeListValue=\"theme\"])=1]/gco:CharacterString/text()',
                            ('<?xml version=\"1.0\" encoding=\"utf-8\"?>' || data)::xml,ARRAY[ARRAY['gmd','http://www.isotc211.org/2005/gmd'],
                            ARRAY['gco','http://www.isotc211.org/2005/gco']]) as list_domaines ,
                            to_char(fm.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS \"date de création de la fiche\",
                            fm.fmeta_id AS \"identifiant géosource\",
                            fm.statut AS fm_statut,
                            xpath('//gmd:citation//gmd:title/gco:CharacterString/text()',('<?xml version=\"1.0\" encoding=\"utf-8\"?>' || data)::xml, ".
                            		"ARRAY[ARRAY['gmd','http://www.isotc211.org/2005/gmd'],ARRAY['gco','http://www.isotc211.org/2005/gco']])::text as xpathtitle
                            FROM fiche_metadonnees fm, carte_projet cp, ssdom_visualise_carte svc, domaine d, sous_domaine sd, public.metadata meta
                            WHERE
                            cp.cartp_fk_fiche_metadonnees = fm.pk_fiche_metadonnees AND
                            cp.pk_carte_projet = svc.ssdcart_fk_carte_projet AND
                            svc.ssdcart_fk_sous_domaine = sd.pk_sous_domaine AND
                            sd.ssdom_fk_domaine=d.pk_domaine AND
                            fm.fmeta_id::integer = meta.id
                            group by pk_fiche_metadonnees, meta.data, \"date de création de la fiche\",\"identifiant géosource\",fm.statut ;";
                //$rs = $dao->BuildResultSet($query);
                $rs = $conn->fetchAllAssociative($query);
                $iRow=0;
                //for ($rs->First(); !$rs->EOF(); $rs->Next()){
                foreach($rs as $row) {
                    $isValid = true;
                    //$sous_domaine = explode(",",$rs->read(1));
                    $sous_domaine = explode(",", $row["ss_dom"]);
                    //$domaine = explode(",",$rs->read(2));
                    $domaine = explode(",",$row["dom"]);
                    $dom_sdom = array_merge($domaine,$sous_domaine);
                    //$listDomaine = str_replace("\"", "", trim($rs->read(3)));
                    $listDomaine = str_replace("\"", "", trim($row['list_domaines']));
                    foreach ($dom_sdom as $domSdom){
                        if($domSdom !=""){
                            if(strpos($listDomaine, $domSdom )===false){
                                $isValid = false;
                                $tabValues[$iRow] =array(
                                    //$rs->read(0),
                                    $row["id"],
                                    //trim($rs->read(7),"{\"}"),
                                    trim($row["xpathtitle"],"{\"}"),
                                    //$rs->read(4),
                                    $row["date de création de la fiche"],
                                    //$rs->read(5),
                                    $row["identifiant géosource"],
                                    //$rs->read(6)
                                    $row["fm_statut"]
                                );
                                $iRow++;
                                break;
                            }
                        }
                    }

                }
                $query = "SELECT pk_fiche_metadonnees AS id, array_to_string(array_agg(ssdom_nom), ',') as ss_dom,array_to_string(array_agg(dom_nom), ',') as dom,
                            xpath('//gmd:keyword[count(../gmd:type /gmd:MD_KeywordTypeCode[@codeListValue=\"theme\"])=1]/gco:CharacterString/text()',
                            ('<?xml version=\"1.0\" encoding=\"utf-8\"?>' || data)::xml,ARRAY[ARRAY['gmd','http://www.isotc211.org/2005/gmd'],
                            ARRAY['gco','http://www.isotc211.org/2005/gco']]) as list_domaines ,to_char(fm.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS \"date de création de la fiche\",
                            fm.fmeta_id AS \"identifiant géosource\", fm.statut AS fm_statut,
                            xpath('//gmd:citation//gmd:title/gco:CharacterString/text()',('<?xml version=\"1.0\" encoding=\"utf-8\"?>' || data)::xml, ".
                            		"ARRAY[ARRAY['gmd','http://www.isotc211.org/2005/gmd'],ARRAY['gco','http://www.isotc211.org/2005/gco']])::text as xpathtitle
                            FROM fiche_metadonnees fm, couche_donnees cd, ssdom_dispose_couche sdc, domaine d, sous_domaine sd, public.metadata meta
                            WHERE
                            fm.fmeta_fk_couche_donnees = cd.pk_couche_donnees AND
                            sdc.ssdcouch_fk_couche_donnees = cd.pk_couche_donnees AND
                            sdc.ssdcouch_fk_sous_domaine = sd.pk_sous_domaine AND
                            sd.ssdom_fk_domaine=d.pk_domaine AND
                            fm.fmeta_id::integer = meta.id
                            group by pk_fiche_metadonnees, meta.data, \"date de création de la fiche\",\"identifiant géosource\",fm.statut ;";
                //$rs = $dao->BuildResultSet($query);
                $rs = $conn->fetchAllAssociative($query);
                //for ($rs->First(); !$rs->EOF(); $rs->Next()){
                foreach($rs as $row) {
                    $isValid = true;
                    //$sous_domaine = explode(",",$rs->read(1));
                    $sous_domaine = explode(",",$row["ss_dom"]);
                    //$domaine = explode(",",$rs->read(2));
                    $domaine = explode(",",$row["dom"]);
                    $dom_sdom = array_merge($domaine,$sous_domaine);
                    //$listDomaine = str_replace("\"", "", trim($rs->read(3)));
                    $listDomaine = str_replace("\"", "", trim($row['list_domaines']));
                    foreach ($dom_sdom as $domSdom){
                        if($domSdom !=""){
                            if(strpos($listDomaine, $domSdom )===false){
                                $isValid = false;
                                $tabValues[$iRow]=array(
                                    //$rs->read(0),
                                    $row["id"],
                                    //trim($rs->read(7),"{\"}"),
                                    $row["xpathtitle"],
                                    //$rs->read(4),
                                    $row["date de création de la fiche"],
                                    //$rs->read(5),
                                    $row["identifiant géosource"],
                                    //$rs->read(6)
                                    $row["fm_statut"]
                                );
                                $iRow++;
                                break;
                            }
                        }
                    }

                }
                $tabFields = array(
                    "id",
                    "titre",
                    "date de création de la fiche",
                    "identifiant géosource",
                    "statut"
                );
            }elseif ($strSql == "getDataProdiCatalo") {
                //$urlServGetTableList = "http://".$accs_adresse_admin."/PRRA/Services/getTableList.php?";
                //$res = @file_get_contents($urlServGetTableList);
                $connCarto = $this->getConnection('prodige', 'public');
                ServicesCatalogue::setConnection($connCarto);
                $tableNames = ServicesCatalogue::getTableList();
                //$res = explode(',',trim($res,'[]'));
                $iRow = 0;
                //foreach ($res as $tableName){
                foreach ($tableNames as $tableName){
                   
                    //$query = "SELECT count(*) FROM couche_donnees WHERE couche_donnees.couchd_emplacement_stockage like ".$tableName[1]." AND (couchd_type_stockage=1 OR couchd_type_stockage=-3);";
                    $query = "SELECT count(*) AS count FROM couche_donnees WHERE couche_donnees.couchd_emplacement_stockage like ? AND (couchd_type_stockage=1 OR couchd_type_stockage=-3);";
                    //$rs = $dao->BuildResultSet($query);
                    $rs = $conn->fetchAllAssociative($query, array($tableName));
                    //$rs->First();
                    //if($rs->Read(0)==0){
                    if($rs[0]["count"] == 0) {
                        $tabValues[$iRow][0]= $tableName;
                        $iRow++;
                    }
                }
                $tabFields = array("Table");
            }else{
                //$rs_request=$dao->BuildResultSet($strSql);
                $rs_request = $conn->fetchAll($strSql);
                //$rs_request = $conn->
                $iRow=0;
                //for ($rs_request->First(); !$rs_request->EOF(); $rs_request->Next())
                foreach($rs_request as $row)
                {
                    $columnNames = array_keys($row);
                    //for($iCol=0;$iCol<$rs_request->GetNumFields();$iCol++){
                    for($iCol=0 ; $iCol < count($row) ; $iCol++) {
                        //TODO Vincent
                        $tabValues[$iRow][$iCol] = $row[$columnNames[$iCol]]; // $rs_request->Read($iCol);
                        if ($iRow==0)
                        {
                            $tabFields[$iCol]= $columnNames[$iCol]; // $rs_request->GetFieldName($iCol);
                        }
                    }
                    $iRow++;
                }
            }

            //echo getExportExcel($tabValues, $tabFields);
            $this->getExportExcel($tabValues, $tabFields);
        }
        return new Response($response?:"FIN");

    }

    /**
     * Export the result in Excel format
     * @param tabValues : table to export
     */
    public function getExportExcel($tabValues,$tabFields) {
        include_once(__DIR__.'/../Common/excel/Worksheet.php');
        include_once(__DIR__.'/../Common/excel/Workbook.php');
        //a mettre dans temp
        $strFileExport = "export.xls";
        // Creating a workbook
        header("Content-type: application/vnd.ms-excel;");
        header("Content-Disposition: attachment; filename=$strFileExport" );
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        $workbook = new Workbook("-");
        //$workbook = new Workbook();

        // Creating the first worksheet
        $worksheet1 =& $workbook->add_worksheet('Export');

        $iCol = 0;
        $iRow = 0;
        $iHeightRow = 20;
        $iWidthCol = 15;
    
        // boucle permettant d'afficher les titres'
        foreach ($tabFields as $key => $value){
            $worksheet1->set_row($iRow, $iHeightRow);
            $worksheet1->set_column($iRow, $iCol, $iWidthCol);
            $worksheet1->write_string($iRow, $iCol, utf8_decode($value) , 0);
            $iCol++;
        }
        $iCol = 0;
        $iRow++;
        $cpt = 0;
    
        foreach($tabValues as $iRowValue=>$tabRow){
            foreach($tabRow as $iColValue=>$value){
                $worksheet1->set_row($iRowValue+$iRow, $iHeightRow);
                $worksheet1->write_string($iRowValue+$iRow, $iColValue, utf8_decode(trim($value)), 0);
            }
        }
        $workbook->close();
    
        // Destruction de l'export precedent
        if( file_exists($strFileExport) && is_file($strFileExport) ) {
            unlink($strFileExport);
        }
        
        //Redirection du fichier vers l'utiisateur
        if (file_exists($strFileExport)) {
            $tabLignes = file($strFileExport) ;
            for($i=0; $i<count($tabLignes); $i++) {
                echo $tabLignes[$i];
            }
        }
        exit;
    }
    
    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/catalogue/purge_logs", name="services_purge_logs", options={"expose"=true})
     */
    public function servicesPurgeLog(Request $request)
    {
        return $this->container->get('prodige.logger')->purgeLogs() ? new Response() : new Response('Erreur lors de la suppression des logs', 500);
    }
    
    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/catalogue/get_log/{categorie}", name="services_get_log", options={"expose"=true})
     */
    public function servicesGetLog(Request $request, $categorie)
    {

        $logPath = $this->container->getParameter('PRODIGE_PATH_LOGS');
        $logFile = $logPath . '/' . $categorie . '.csv';
        if(!file_exists($logFile) ) {
            @file_put_contents($logFile, "");
        }
        $response = new \Symfony\Component\HttpFoundation\BinaryFileResponse($logFile);
        $response->trustXSendfileTypeHeader();
        $response->headers->set('Expires', '0');
        $response->headers->set('Cache-Control', 'must-revalidate');
        $response->headers->set('Pragma', 'public');
        $response->setContentDisposition(
            \Symfony\Component\HttpFoundation\ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            basename($logFile),
            basename($logFile)
        );
        return $response;
    }
}
