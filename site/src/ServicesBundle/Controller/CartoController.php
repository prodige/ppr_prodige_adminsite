<?php

namespace ProdigeAdminSite\ServicesBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Prodige\ProdigeBundle\Controller\BaseController;

use Prodige\ProdigeBundle\Common\AlkRequest;
use Prodige\ProdigeBundle\Common\UpdateArbo;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Process\Process;

class CartoController extends BaseController {

    /**
     * @param  $conn
     * @param  $newSearchePath
     * @return \Doctrine\DBAL\Connection
     */
    protected function changeSearchPath($conn, $newSearchePath) {
        $conn->exec('set search_path to '.$newSearchePath);
        return $conn;
    }

    /**
     * Get next sequence value
     * @param unknown $conn
     * @param unknown $seqName
     */
    protected function getSequenceNextValue($conn, $seqName) {
        return $conn->fetchColumn("select nextval('".$seqName."')");
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/getparams", name="carto_get_params", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getParamsAction(Request $request) { // testé hismail
        $toXml = $request->get("xml", false);
        $file  = $request->get("file", false);

        //mode écriture
        //if(isset($_REQUEST["xml"]) && isset($_REQUEST["file"])){
        if($toXml && $file) {
            $strXml = stripslashes($_REQUEST["xml"]);
            $dom = new \DomDocument();
            $dom->loadXML($strXml);
            $dom->save($file);
            exit();
        }

        //mode lecture
        //if (isset($_REQUEST["file"])) {
        if($file) {
            if (file_exists($file)) {
                $tailleFichier = filesize($file);
                return new Response(file_get_contents($file), Response::HTTP_OK, array(
                    "Content-Type"=>"text/xml; charset=ISO-8859-1",
                    "Content-Length"=>$tailleFichier,
                ));
            }
        }
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/get_server_config", name="services_get_server_config", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getServerConfigAction(Request $request) {

      $callback = $request->get("callback", "");
      $service = $request->get("service", "");
      $data = array();

      if($service =="getConstant" || $service == "initParametrage"){
        $oMap = ms_newMapObj($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wms.map");
        $wms_server_title = $oMap->getMetaData("wms_title");
        $wms_server_abstract = $oMap->getMetaData("wms_abstract");
        $wms_server_onlineresource =  $oMap->getMetaData("wms_onlineresource");
        $wms_server_id = 1;
        /*WFS*/
        $oMap = ms_newMapObj($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wfs.map");
        $wfs_server_title = $oMap->getMetaData("wfs_title");
        $wfs_server_abstract = $oMap->getMetaData("wfs_abstract");
        $wfs_server_onlineresource = $oMap->getMetaData("wfs_onlineresource");
        $wfs_server_id = 2;
        $data[] = array("pk_prodige_settings_id" => 0, "prodige_settings_constant" => 'wms_server_title', "prodige_settings_title" => '', "prodige_settings_value" => $wms_server_title, "prodige_settings_desc" => "Nom du serveur WMS"," 	prodige_settings_type" => "textfield", " 	prodige_settings_param" => '');
        $data[] = array("pk_prodige_settings_id" => 0, "prodige_settings_constant" => 'wms_server_abstract', "prodige_settings_title" => '', "prodige_settings_value" => $wms_server_abstract, "prodige_settings_desc" => "Résumé WMS"," 	prodige_settings_type" => "textfield", " 	prodige_settings_param" => '');
        $data[] = array("pk_prodige_settings_id" => 0, "prodige_settings_constant" => 'wms_server_onlineresource', "wms_server_onlineresource" => '', "prodige_settings_value" => $wms_server_onlineresource, "prodige_settings_desc" => "Url d'accès WMS"," 	prodige_settings_type" => "textfield", " 	prodige_settings_param" => '');

        $data[] = array("pk_prodige_settings_id" => 0, "prodige_settings_constant" => 'wfs_server_title', "prodige_settings_title" => '', "prodige_settings_value" => $wfs_server_title, "prodige_settings_desc" => "Nom du serveur WFS"," 	prodige_settings_type" => "textfield", " 	prodige_settings_param" => '');
        $data[] = array("pk_prodige_settings_id" => 0, "prodige_settings_constant" => 'wfs_server_abstract', "prodige_settings_title" => '', "prodige_settings_value" => $wfs_server_abstract, "prodige_settings_desc" => "Résumé WFS"," 	prodige_settings_type" => "textfield", " 	prodige_settings_param" => '');
        $data[] = array("pk_prodige_settings_id" => 0, "prodige_settings_constant" => 'wfs_server_onlineresource', "wms_server_onlineresource" => '', "prodige_settings_value" => $wfs_server_onlineresource, "prodige_settings_desc" => "Url d'accès WFS"," 	prodige_settings_type" => "textfield", " 	prodige_settings_param" => '');

        $response = $callback."(".json_encode($data).")";
      }else if ($service =="updateConstant"){
        /*WFS parameters*/
        $wfs_server_id = isset($_POST["wfs_server_id"]) ? $_POST["wfs_server_id"]: "";
        $wfs_server_title = isset($_POST["wfs_server_title"]) ? ($_POST["wfs_server_title"]) : "";
        $wfs_server_abstract = isset($_POST["wfs_server_abstract"]) ? ($_POST["wfs_server_abstract"]) : "";
        $wfs_server_onlineresource = isset($_POST["wfs_server_onlineresource"]) ? $_POST["wfs_server_onlineresource"] : "";
        /*update the mapfiles*/
        /*WMS*/
        $oMap = ms_newMapObj($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wms.map");
        $oMap->setMetaData("wms_title",$request->get("wms_server_title"));
        $oMap->setMetaData("wms_abstract",$request->get("wms_server_abstract"));
        $oMap->setMetaData("wms_onlineresource", $request->get("wms_server_onlineresource"));

        $oMap->save($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wms.map");
        /*WFS*/
        $oMap = ms_newMapObj($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wfs.map");
        $oMap->setMetaData("wfs_title",$request->get("wfs_server_title"));
        $oMap->setMetaData("wfs_abstract",$request->get("wfs_server_abstract"));
        $oMap->setMetaData("wfs_onlineresource", $request->get("wfs_server_onlineresource"));
        $oMap->save($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wfs.map");
        $response = $callback."({success:true})";
      }
      return new Response($response ?: "FIN");

    }


    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/get_parametrages", name="services_get_param_carto", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getParametragesAction(Request $request) { // testé hismail

        /**
         * @author Alkante
         * Service de lecture et mise à jour des paramétrages
         */
        //permet de ne pas prendre en compte le paramétrage SGBD
        $includeParametrageSgbd = false;

        $callback = $request->get("callback", "");
        $service = $request->get("service", "");
        $data = array();
        $conn = $this->getProdigeConnection($this->container->getParameter("adminsite")["PRO_SCHEMA_PARAMETRAGE_NAME"]);
        //var_dump($conn->getParams()); die();
        if($service =="getConstant"){
            $query = " SELECT * from prodige_settings order by pk_prodige_settings_id ";
            $rs = $conn->fetchAllAssociative($query);
            $i =0;
            foreach($rs as $row)
            {
                foreach($row as $key => $value){
                    if(!is_numeric($key)){
                        //$data[$i][($key)]= ($value);
                        $data[$i][$key]= $value;
                    }
                }
                $i++;
            }
            $response = $callback."(".json_encode($data).")";
        }else if ($service =="updateConstant"){ // testé hismail
            $query = " SELECT prodige_settings_constant from prodige_settings";
            $rs = $conn->fetchAllAssociative($query);
            foreach($rs as $row) {
                $var_name = $row["prodige_settings_constant"];
                if($var_name == "PRO_PROJ_WFS" && ($request->get($var_name, "") != "")) {
                    $epsg = preg_split("/ - /",$request->get($var_name));
                    $epsg = trim($epsg[0],"EPSG:");
                    $oMap = ms_newMapObj($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wfs.map");
                    $oMap->setProjection("epsg:".$epsg);
                    $oMap->setMetadata("wfs_srs", "EPSG:".$epsg);
                    $oMap->save($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wfs.map");
                }
                if($var_name == "PRO_PROJ_DEFAULT_WMS" && ($request->get($var_name, "") != "")){
                    $epsg = preg_split("/ - /",$request->get($var_name));
                    $epsg = trim($epsg[0],"EPSG:");
                    $oMap = ms_newMapObj($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wms.map");
                    $oMap->setProjection("epsg:".$epsg);
                    $oMap->save($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wms.map");
                }
                if($var_name == "PRO_PROJ_WMS" ){
                    $original = $request->get($var_name, array());
                    $request->query->set($var_name, implode(" ; ", $original));
                    $request->request->set($var_name, implode(" ; ", $original));

                    $projections = $original;
                    $projections = array_map("urldecode", $projections);
                    $projections = array_map(function($projection) {
                        $projection = current(explode(" - ", $projection));
                        $projection = trim($projection, "EPSG:");
                        return "EPSG:".$projection;
                    }, $projections);
                    $oMap = ms_newMapObj($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wms.map");
                    $oMap->setMetadata("wms_srs", implode(" ", $projections));
                    $oMap->save($this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/wms.map");

                }
                $request_value = $request->get($var_name, false);
                if( $request_value && !empty($request_value) ) {
                    $query = $conn->createQueryBuilder();
                    $query->update("prodige_settings")
                    ->set("prodige_settings_value", ":prodige_settings_val")
                    ->where("prodige_settings_constant= :prodige_settings_const")
                    ->setParameters(array("prodige_settings_val" => $request_value, "prodige_settings_const" => $var_name));
                }
                else {
                    $query = $conn->createQueryBuilder();
                    $query->update("prodige_settings")
                    ->set("prodige_settings_value", "'off'")
                    ->where("prodige_settings_constant= :prodige_settings_const")
                    ->setParameter('prodige_settings_const', $var_name);
                }
               $rs2 = $query->execute();
            }
            $response = $callback."({success:true})";
        }elseif($service == "initParametrage"){ // testé hismail
            $query = " SELECT prodige_settings_constant from prodige_settings";
            $rs = $conn->fetchAllAssociative($query);

            $adminsite = $this->container->getParameter("adminsite");
            $prodige_settings_carto = (isset($adminsite["prodige_settings_carto"]) ? $adminsite["prodige_settings_carto"] : array());

            foreach($rs as $row) {
                $var_name = $row["prodige_settings_constant"];
                if(!array_key_exists($var_name, $prodige_settings_carto))
                    continue;
                $paramValue = $prodige_settings_carto[$var_name];
                $query = $conn->createQueryBuilder();
                $query->update("prodige_settings")
                ->set("prodige_settings_value", ":prodige_settings_val")
                ->where("prodige_settings_constant= :prodige_settings_const")
                ->setParameters(array("prodige_settings_val" => $paramValue, "prodige_settings_const" => $row["prodige_settings_constant"]));
                $rs = $query->execute();
            }
            $response = $callback."({success:true})";
        }elseif($service == "getConstantByName"){
            $constantName = $request->get("constantName");
            $constvalue = "";
            $query = "SELECT prodige_settings_value from prodige_settings where prodige_settings_constant=?;";
            $rs = $conn->fetchAllAssociative($query, array($constantName));
            if(count($rs) == 1) {
                $constvalue = $rs[0]["prodige_settings_value"];
            }
            $response = json_encode($constvalue);
        }
        return new Response($response ?: "FIN");
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/get_projections", name="services_get_projections", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getProjectionsAction(Request $request) { // testé hismail
        //require_once("path.php");
        //$file = $CheminDonnees."/cartes/Publication/Projections_Administration.xml";
        $file = $this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/Projections_Administration.xml";
        //$callback =isset($_GET["callback"]) ? $_GET["callback"] : null;
        $callback = $request->get('callback', null);
        //$epsgRequest =isset($_GET["epsg"]) ? $_GET["epsg"] : null;
        $epsgRequest = $request->get('epsg', null);
        if (file_exists($file)){
            $result = array();
            $xml = simplexml_load_file($file);
            for($i=0;$i<count($xml->PROJECTION);$i++){
                //if(isset($epsgRequest) && $epsgRequest==(string)$xml->PROJECTION[$i]['EPSG']){
                if(($epsgRequest != null) && $epsgRequest==(string)$xml->PROJECTION[$i]['EPSG']){
                    echo json_encode(array(
                        "epsg"=>(string)$xml->PROJECTION[$i]['EPSG'],
                        "nom"=>(string)$xml->PROJECTION[$i]['NOM'],
                        "proj4"	=> (string)$xml->PROJECTION[$i]['PROJ4'],
                        "display"=>"EPSG:".(string)$xml->PROJECTION[$i]['EPSG']." - ".(string)$xml->PROJECTION[$i]['NOM']
                    ));
                    exit(0);
                }
                $result[] = array(
                    "epsg"=>(string)$xml->PROJECTION[$i]['EPSG'],
                    "nom"=>(string)$xml->PROJECTION[$i]['NOM'],
                    "proj4"	=> (string)$xml->PROJECTION[$i]['PROJ4'],
                    "display"=>"EPSG:".(string)$xml->PROJECTION[$i]['EPSG']." - ".(string)$xml->PROJECTION[$i]['NOM']
                );
            }

            //if(isset($epsgRequest))
            if($epsgRequest != null)
                echo "";
                else if($callback!=null)
                    //echo $callback."(".json_encode($result).")";
                    $response = $callback."(".json_encode($result).")";
                    else
                        //echo json_encode($result);
                        //$response = json_encode($result);
                        $response = $callback."(".json_encode($result).")";
        }
        return new Response($response ?: "FIN");
    }


    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/get_modeles", name="services_get_modeles_carto", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getModelesAction(Request $request){


        $callback = $request->get("callback");
        $service = $request->get("service");

        $data = array();
        switch($service){
          case "getData":

              $modelesTypes = array("1" => "VISUALISATION", "2" => "EXTRACTION");

              $databaseModels = array();
              $conn_prodige = $this->getProdigeConnection('carmen');
              $tabInfoModel = $conn_prodige->fetchAllAssociative("SELECT map_wmsmetadata_uuid, map_file from map where map_model =true and published_id is null");
              foreach($tabInfoModel as $modele) {
                  $databaseModels[$modele["map_file"].".map"] = $modele["map_wmsmetadata_uuid"];
              }

              $ALL_MAPS = array();
              if(file_exists(PRO_MAPFILE_PATH."/Modeles_Administration.xml")){
                  $xmlDoc = simplexml_load_file(PRO_MAPFILE_PATH."/Modeles_Administration.xml");
                  foreach ($xmlDoc->MODELE as $modele) {
                      if(array_key_exists((string)$modele["ID"], $databaseModels)){
                          array_push($ALL_MAPS, array("pk_modele_carto" => (string)$modele["ID"],
                                                      "modele_carto_intitule" => (string)$modele["NAME"],
                                                      "modele_uuid" => $databaseModels[(string)$modele["ID"]],
                                                      "modele_carto_type" => $modelesTypes[(string)$modele["TYPE"]]));
                      }
                  }
              }
              $response = $callback."(".json_encode($ALL_MAPS).")";
              break;
          case "delData":
              $id = $request->get("id");
              if(file_exists(PRO_MAPFILE_PATH."/Modeles_Administration.xml")){
                  $xmlDoc = simplexml_load_file(PRO_MAPFILE_PATH."/Modeles_Administration.xml");
                  foreach ($xmlDoc->MODELE as $modele) {
                      if($modele["ID"] == $id){
                          $dom=dom_import_simplexml($modele);
                          $dom->parentNode->removeChild($dom);
                      }
                  }
                  $xmlDoc->asXml(PRO_MAPFILE_PATH."/Modeles_Administration.xml");
                  //Service delete map
                  $conn_prodige = $this->getProdigeConnection('carmen');
                  $tabInfoModel = $conn_prodige->fetchAllAssociative("SELECT map_wmsmetadata_uuid from map where map_file =:map_file and published_id is null",
                                                          array("map_file"=> basename($id)));
                  foreach($tabInfoModel as $modele) {
                      $uuid = $modele["map_wmsmetadata_uuid"];
                      $carmenDeleteMapAction = $this->container->getParameter('PRODIGE_URL_CATALOGUE')."/geosource/mapDelete/".$uuid;
                      $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenDeleteMapAction);
                      $headers = array();
                      $jsonResp = $this->curl($carmenDeleteMapAction, 'GET', array('ticket'=>$ticket), array(), array(), $headers);
                  }
                  $response = $callback."({'success':true})";
              }else{
                  $response = $callback."({'success':false})";
              }
              break;
          case "addData":
              if(file_exists(PRO_MAPFILE_PATH."/Modele.map")){
                  $id = $this->getNextModel();
                  //$oMap =  @ms_newMapObj(PRO_MAPFILE_PATH."/Modele.map");
                  $conn_prodige = $this->getProdigeConnection('carmen');
                  $tabInfoModel = $conn_prodige->fetchAllAssociative("SELECT map_id from map where map_file =:map_file and published_id is null", array("map_file"=> "Modele"));
                  $map_id ="";
                  foreach($tabInfoModel as $modele) {
                      $map_id = $modele["map_id"];
                  }
                  if($map_id==""){
                      throw new \Exception("Modele map is not present in database");
                  }
                  //1 edit model map
                  $carmenEditMapAction = $this->container->getParameter('PRODIGE_URL_ADMINCARTO')."/api/map/edit/".$map_id."/resume";
                  $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenEditMapAction);
                  $headers = array();
                  $jsonResp = $this->curl($carmenEditMapAction, 'GET', array('ticket'=>$ticket), array(), array(), $headers);

                  $jsonObj = json_decode($jsonResp);
                  if($jsonObj && $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId){
                      $map_id =  $jsonObj->map->mapId;
                      //2 change map parameters
                      $uuid = uniqid();
                      $conn_prodige->executeStatement("update map set published_id =:map_id, map_wmsmetadata_uuid=:uuid where map_id=:map_id",
                          array("map_id" => $map_id,
                              "uuid"   => $uuid
                          ));
                      //3 save / publish map
                      $carmenSaveMapAction = $this->container->getParameter('PRODIGE_URL_ADMINCARTO')."/api/map/publish/".$map_id."/".$map_id;
                      $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenSaveMapAction);
                      $jsonResp = $this->curl($carmenSaveMapAction, 'POST', array('ticket'=>$ticket),  array("mapModel"=>"true", "mapFile" => basename($id, ".map")) , array(), $headers);

                      $jsonObj = json_decode($jsonResp);
                      if($jsonObj && $jsonObj->success && $jsonObj->map){
                          if(file_exists(PRO_MAPFILE_PATH."/Modeles_Administration.xml")){
                              $xmlDoc = simplexml_load_file(PRO_MAPFILE_PATH."/Modeles_Administration.xml");
                              $modele = $xmlDoc->addChild('MODELE');
                              $modele['NAME'] = "Modele";
                              $modele['ID'] = $id;
                              $modele['TYPE'] = '1';
                              $xmlDoc->asXml(PRO_MAPFILE_PATH."/Modeles_Administration.xml");
                          }
                          $response = $callback."({'pk_modele_carto':'".$id."', 'modele_carto_intitule': 'Modele', 'modele_carto_type': '1', 'modele_uuid':'".$uuid."'})";

                      }
                  }else{
                      $response = $callback."({'success':false})";
                  }

              }else{
                  $response = $callback."({'success':false})";
              }
              break;
          case "updateOrder":
              if(file_exists(PRO_MAPFILE_PATH."/Modeles_Administration.xml")){
                  unlink(PRO_MAPFILE_PATH."/Modeles_Administration.xml");
              }
              $xmlData = "<?xml version= \"1.0\" encoding=\"UTF-8\"?>
    		<MODELES></MODELES>";
              $xmlFile = fopen(PRO_MAPFILE_PATH."/Modeles_Administration.xml", "a+");
              fwrite($xmlFile, $xmlData);
              fclose($xmlFile);


              $new_order=preg_split("/#%#/", $request->get("elems"));
              if(file_exists(PRO_MAPFILE_PATH."/Modeles_Administration.xml")){
                  $xmlDoc = simplexml_load_file(PRO_MAPFILE_PATH."/Modeles_Administration.xml");
                  for($i=1; $i<count($new_order); $i= $i+3){
                      $modele = $xmlDoc->addChild('MODELE');
                      $modele['ID'] = $new_order[$i];
                      $modele['NAME'] = $new_order[$i+1];
                      $modele['TYPE'] = $new_order[$i+2];
                  }
                  $xmlDoc->asXml(PRO_MAPFILE_PATH."/Modeles_Administration.xml");
                  $response = $callback."({'success':true})";
              }else{
                  $response = $callback."({'success':false})";
              }
              break;
          case "updateData":
              if(file_exists(PRO_MAPFILE_PATH."/Modeles_Administration.xml")){
                  $id = $request->get("id");
                  $value = $request->get("value");
                  $fieldType="";
                  if($request->get("field", false)){
                      $fieldType = $request->get("field");
                  }
                  $xmlDoc = simplexml_load_file(PRO_MAPFILE_PATH."/Modeles_Administration.xml");
                  foreach ($xmlDoc->MODELE as $modele) {
                      if($modele["ID"] == $id){
                          if($fieldType=="modele_carto_type"){
                              $modele["TYPE"] = $value;
                              break;
                          }else{
                              $modele["NAME"] = $value;
                              break;
                          }
                      }
                  }
                  $xmlDoc->asXml(PRO_MAPFILE_PATH."/Modeles_Administration.xml");
                  $response = $callback."({'success':true})";
              }else{
                  $response = $callback."({'success':false})";
              }
              break;
        }
        return new Response($response);
    }

    /**
     * Retourne l'identifiant du prochain modèle
     * @return unknown_type
     */
    protected function getNextModel(){
        $tabId = array();
        $MyDirectory = opendir(PRO_MAPFILE_PATH) or die('Erreur');
        while($Entry = readdir($MyDirectory)) {
            if(!is_dir(PRO_MAPFILE_PATH.'/'.$Entry)) {
                if($this->GetExtensionName($Entry)=="map" && substr($Entry, 0, 13)=="modele_MODEL_"){
                    $id = substr($Entry, 13, -4);
                    array_push($tabId,$id);
                }
            }
        }
        $id = (!empty($tabId) ?  max($tabId) : 0);
        return  "MODEL_".($id+1).".map";
    }


    /* GetExtensionName - Renvoie l'extension d'un fichier
     . $File (char): Nom du fichier
    . $Dot  (bool): avec le point true/false
    */

    protected function GetExtensionName($File, $Dot=false)
    {
        if ($Dot == true) {
            $Ext = strtolower(substr($File, strrpos($File, '.')));
        } else{
            $Ext = strtolower(substr($File, strrpos($File, '.') + 1));
        }
        return $Ext;
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/get_parametrages_automate", name="services_get_param_automate", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getParametragesAutomateAction(Request $request) { //testé hismail
        /**
         * @author Alkante
         * Service de lecture et mise à jour des paramétrages
         */
        //permet de ne pas prendre en compte le paramétrage SGBD
        $includeParametrageSgbd = false;

        //$AdminPath = "Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        //require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");

        $conn = $this->getProdigeConnection($this->container->getParameter("adminsite")["PRO_SCHEMA_PARAMETRAGE_NAME"]);
        try {
            //ConnectionFactory::BeginTransaction();
            $conn->beginTransaction();
            //$dao = new DAO();
            //$callback =$_GET["callback"];
            $callback = $request->get("callback");
            //$service = $_GET["service"];
            $service = $request->get("service");
            $data = array();
            //$dao->setSearchPath(PRO_SCHEMA_PARAMETRAGE_NAME);

            // 3.2.8 - créer le répertoire ftptampon s'il n'existe pas
            //$ftptampon = $CheminDonnees . "ftptampon/";
            $ftptampon =  $this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]. "ftptampon/";
            if( !is_dir($ftptampon) ) @mkdir($ftptampon);

            if($service =="getConstant"){ //testé hismail
                $query = " SELECT * from prodige_settings_automate order by pk_prodige_settings_automate_id ";
                //$rs = $dao->Execute($query);
                $rs = $conn->fetchAllAssociative($query);
                $i =0;
                //while ($row = pg_fetch_array($rs))
                foreach($rs as $row)
                {
                    foreach($row as $key => $value){
                        if(!is_numeric($key))
                            //$data[$i][($key)]= ($value);
                            $data[$i][$key]= $value;
                    }
                    $i++;
                }
                //GetFieldName
                //echo $callback."(".json_encode($data).")";
                $response = $callback."(".json_encode($data).")";
            }else if ($service =="updateConstant"){ //testé hismail
                $query = " SELECT prodige_settings_constant from prodige_settings_automate";
                //$rs = $dao->BuildResultSet($query);
                $rs = $conn->fetchAllAssociative($query);
                //for ($rs->First(); !$rs->EOF(); $rs->Next()){
                foreach($rs as $row) {
                    //$var_name = $rs->Read(0);
                    $var_name = $row["prodige_settings_constant"];
                    //if (isset($_REQUEST[$var_name]))
                    if(($request->get($var_name, false)) || (empty($request->get($var_name)))) { // empty($request->get($var_name)) car la chaine de caractères vide "" donne "false"
                        //$query = " update prodige_settings_automate set prodige_settings_value = '".($_REQUEST[$var_name])."' where prodige_settings_constant = '".$var_name."'";
                        $query = $conn->createQueryBuilder();
                        $query->update("prodige_settings_automate")
                        ->set("prodige_settings_value", ":prodige_settings_val")
                        ->where("prodige_settings_constant= :prodige_settings_const")
                        ->setParameters(array("prodige_settings_val" => $request->get($var_name), "prodige_settings_const" => $var_name));
                    }
                        else {//checkbox case
                            //$query = " update prodige_settings_automate set prodige_settings_value = 'off' where prodige_settings_constant = '".$var_name."'";
                            $query = $conn->createQueryBuilder();
                            $query->update("prodige_settings_automate")
                            ->set("prodige_settings_value", "'off'")
                            ->where("prodige_settings_constant= :prodige_settings_const")
                            ->setParameter('prodige_settings_const', $var_name);
                        }
                        //$rs2 = $dao->Execute($query);
                        $rs2 = $query->execute();
                }

                //$dao->Execute("COMMIT");
                $conn->commit();
                //echo $callback."({success:true})";
                $response = $callback."({success:true})";
            }elseif($service == "initParametrage"){ //testé hismail
                $query = " SELECT prodige_settings_constant from prodige_settings_automate";
                //$rs = $dao->BuildResultSet($query);
                $rs = $conn->fetchAllAssociative($query);

                $adminsite = $this->container->getParameter("adminsite");
                $prodige_settings_automate = (isset($adminsite["prodige_settings_automate"]) ? $adminsite["prodige_settings_automate"] : array());

                //for ($rs->First(); !$rs->EOF(); $rs->Next()){
                foreach($rs as $row) {
                    //$var_name = $rs->Read(0);
                    $var_name = $row["prodige_settings_constant"];
                    if(!array_key_exists($var_name, $prodige_settings_automate))
                        continue;
                    $paramValue = $prodige_settings_automate[$var_name];
                    //query = " update prodige_settings_automate set prodige_settings_value = '".$paramValue."' where prodige_settings_constant = '".$rs->Read(0)."'";
                    $query = $conn->createQueryBuilder();
                    $query->update("prodige_settings_automate")
                    ->set("prodige_settings_value", ":prodige_settings_value")
                    ->where("prodige_settings_constant= :prodige_settings_constant")
                    ->setParameters(array(
                        "prodige_settings_value" => $paramValue,
                        "prodige_settings_constant" => $var_name
                    ));
                    //$dao->Execute($query);

                    $query->execute();
                }

                //$dao->Execute("COMMIT");
                $conn->commit();
                //echo $callback."({success:true})";
                $response = $callback."({success:true})";
            }
        } catch (\Exception $exception){
            $conn->rollBack();
        }
        //ConnectionFactory::CloseConnection();
        $conn->close();
        return new Response($response ?: "FIN");
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/get_parametrages_fluxatom", name="services_get_param_flux_atom", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getParametragesFluxatomAction(Request $request) {
        /**
         * @author Alkante
         * Service de lecture et mise à jour des paramétrages
         */
        //permet de ne pas prendre en compte le paramétrage SGBD
        $includeParametrageSgbd = false;

        $conn = $this->getProdigeConnection($this->container->getParameter("adminsite")["PRO_SCHEMA_PARAMETRAGE_NAME"]);
        $conn->beginTransaction();
        $callback = $request->get("callback", "");
        $service = $request->get("service");
        $data = array();

        // Extrait du début du service "updateArbo.php" du module "prodigecatalogue"
        // Ce service est devenue une méthode statique "updateArbo" dans la classe "UpdateArbo" placé dans le bundle "prodige" (commun à tous les modules du prodige)
        // Ces paramètres ($metadata_id, $uuid, $uuidparent, $PRO_ROOT_FILE_ATOM) seront utilisés dans l'appel de la méthode statique updateArbo de la classe UpdateArbo
        $metadata_id = -1;

        if(($request->get("metadata_id", false)) && (!empty($request->get("nom")))) {
            $metadata_id = $request->get("metadata_id");
        }
        $uuid = -1;

        if(($request->get("uuid", false)) && (!empty($request->get("uuid")))) {
            $uuid = $request->get("uuid");
        }
        $uuidparent = -1;
        if(($request->get("uuidparent", false)) && (!empty($request->get("uuidparent")))) {
            $uuidparent = $request->get("uuidparent");
        }
        // Fin Extrait

        $adminsite = $this->container->getParameter("adminsite");
        $prodige_settings_carto = (isset($adminsite["prodige_settings_carto"]) ? $adminsite["prodige_settings_carto"] : array());

        $PRO_ROOT_FILE_ATOM = (isset($prodige_settings_carto["PRO_ROOT_FILE_ATOM"]) ? $prodige_settings_carto["PRO_ROOT_FILE_ATOM"] : "");

        if($service =="getConstant"){ //testé hismail
            $query = " SELECT * from prodige_settings_fluxatom order by pk_prodige_settings_fluxatom_id ";
            //$rs = $dao->Execute($query);
            $rs = $conn->fetchAllAssociative($query);
            $i =0;
            //while ($row = pg_fetch_array($rs))
            foreach($rs as $row)
            {
                foreach($row as $key => $value){
                    if(!is_numeric($key))
                        //$data[$i][($key)]= ($value);
                        $data[$i][$key]= $value;
                }
                $i++;
            }
            //GetFieldName
            $response = $callback."(".json_encode($data).")";
        }
        else if ($service =="updateConstant"){
            $TabCorresp=array(); // correspondance entre ctet flux atom et champs de la table prodige_download_param
            $TabCorresp["PRO_FLUXATOM_COUCHE_TERRITOIRE"] = "critere_moteur_table";
            $TabCorresp["PRO_FLUXATOM_FIELD_ID_COUCHE_TERRITOIRE"] = "critere_moteur_champ_id";
            $TabCorresp["PRO_FLUXATOM_FIELD_LABEL"] = "critere_moteur_champ_nom";

            $query = " SELECT prodige_settings_constant from prodige_settings_fluxatom";
            $rs = $conn->fetchAllAssociative($query);
            foreach($rs as $row) {
                $var_name = $row["prodige_settings_constant"];
                $var_value = $request->get($var_name, false);

                if($var_value && !is_array($var_value)){
                    if(isset($TabCorresp[$var_name]) && $TabCorresp[$var_name]!=""){
                        //$queryTableDownload = "update prodige_download_param set ".$TabCorresp[$var_name]." = '".($_REQUEST[$var_name])."' where b_flux_atom=1";
                        $queryTableDownload = $conn->createQueryBuilder();
                        $queryTableDownload->update("prodige_download_param")
                                            ->set($TabCorresp[$var_name], ":TabCorresp_var_name")
                                            ->where("b_flux_atom= 1")
                                            ->setParameter('TabCorresp_var_name', $var_value);
                    }else{
                        $queryTableDownload = "";
                    }
                }
                else if ($var_value && is_array($var_value) && !empty($var_value)){// cas des multiselect
                    $var_value = implode(" ; ", $var_value);
                }
                else {//checkbox case
                	$var_value = "off";
                }
                $query = $conn->createQueryBuilder();
                $query->update("prodige_settings_fluxatom")
                ->set("prodige_settings_value", ":prodige_settings_value")
                ->where("prodige_settings_constant= :prodige_settings_constant")
                ->setParameters(array(
                    "prodige_settings_value" => $var_value,
                    "prodige_settings_constant" => $var_name
                ));
                //var_dump($query->getSQL());
                $rs2 = $query->execute();

                if(isset($queryTableDownload) && $queryTableDownload!=""){
                    $rs3 = $queryTableDownload->execute();
                }
            }
            // si modification des constantes : on supprime l'arbo de stockage de fichier zip
            // suppression de l'arborescence de téléchargement correspondant au flux ATOM
            /**répertoire d'entrée des fichiers d'export zip atomdata**/
            $res = UpdateArbo::updateArbo($this->getCatalogueConnection('catalogue'), $metadata_id, $uuid, $uuidparent);
            $conn->commit();

            $response = $callback."({success:true})";
        }elseif($service == "initParametrage"){
            $query = " SELECT prodige_settings_constant from prodige_settings_fluxatom";

            $rs = $conn->fetchAllAssociative($query);
            foreach($rs as $row) {
                $var_name = $row["prodige_settings_constant"];
                if($var_name=="PRO_FLUXATOM_FIELD_ID_COUCHE_TERRITOIRE" || $var_name=="PRO_FLUXATOM_COUCHE_TERRITOIRE" || $var_name=="PRO_FLUXATOM_FIELD_LABEL"){
                    $paramValue= "Sélectionnez";
                }else{
                    $paramValue = "";
                }
                //$query = " update prodige_settings_fluxatom set prodige_settings_value = '".$paramValue."' where prodige_settings_constant = '".$rs->Read(0)."'";
                $query = $conn->createQueryBuilder();
                $query->update("prodige_settings_fluxatom")
                ->set("prodige_settings_value", ":prodige_settings_value")
                ->where("prodige_settings_constant= :prodige_settings_constant")
                ->setParameters(array(
                    "prodige_settings_value" => $paramValue,
                    "prodige_settings_constant" => $var_name
                ));
                $query->execute();
            }
            //file_get_contents($catalogue_url."/PRRA/Services/updateArbo.php?metadata_id=-1");
            $res = UpdateArbo::updateArbo($this->getCatalogueConnection('catalogue'), $metadata_id, $uuid, $uuidparent);

            $response = $callback."({success:true})";
        }elseif($service == "getSettingsFluxAtom"){// appelé pour récupérer les déclinaison possibles de découpage entre le format, les élement du territoire et les projections possibles

            $conn = $this->changeSearchPath($conn, 'parametrage, public');
            // projections
            $strProjections = "";
            $tabProjections = array();
            $strSql = "SELECT prodige_settings_value FROM prodige_settings_fluxatom where prodige_settings_constant='PRO_FLUXATOM_PROJECTION'";
            $rs3 = $conn->fetchAllAssociative($strSql);
            if (count($rs3) > 0 ) {
                $strProjections = $rs3[0]["prodige_settings_value"];
            }
            $tabProjections = explode(" ; ", $strProjections);
            $data["projection"]=$tabProjections;

            // formats
            $strFormats ="";
            $tabFormats = array();
            $tabFormatsBrut = array();
            $strSql = "SELECT prodige_settings_value FROM prodige_settings_fluxatom where prodige_settings_constant='PRO_FLUXATOM_FORMAT'";
            $rs4 = $conn->fetchAllAssociative($strSql);
            if(count($rs4) > 0) {
                $strFormats = $rs4[0]["prodige_settings_value"];
            }
            $tabFormatsBrut = explode(" ; ", $strFormats);
            // traitement pour ne récupere que les code des dformats ex . shp
            foreach($tabFormatsBrut as $k => $strFormat){
                if($strFormat!=""){
                    $tabForm = explode(" - ", $strFormat);
                    $tabFormats[] = $valFormat = substr($tabForm[0], 7); // ex. val = shp
                    $nomFormat = $tabForm[1];
                }
            }
            $data["formats"]= $tabFormats;
            // territoire
            // table contenant les element geog // provient des couches listées dans  prodige_download_param
            $table = "";
            $data["territoire"]["TABLE"]="";
            $strSql = "SELECT prodige_settings_value FROM prodige_settings_fluxatom where prodige_settings_constant='PRO_FLUXATOM_COUCHE_TERRITOIRE'";
            //$rs5     = $dao->BuildResultSet($strSql);
            $rs5 = $conn->fetchAllAssociative($strSql);
            if(count($rs5) > 0) {
                $table = $rs5[0]["prodige_settings_value"];
                //$data["territoire"]["TABLE"]=$table;
            }
            // traitement pour récupérer les parametre du territoire à partir de la table prodige_download_param
            $strSql = "SELECT critere_moteur_table, critere_moteur_champ_id, critere_moteur_champ_nom, pk_critere_moteur  FROM prodige_download_param where b_flux_atom=1";
            $rs6 = $conn->fetchAllAssociative($strSql);
            if (count($rs6) > 0) {
                $table = $rs6[0]["critere_moteur_table"];
                $data["territoire"]["TABLE"]=$table;
                $champsId = $rs6[0]["critere_moteur_champ_id"];
                $data["territoire"]["CHAMP_ID"]= $champsId;
                $champslabel = $rs6[0]["critere_moteur_champ_nom"];
                $data["territoire"]["CHAMP_LABEL"]=$champslabel;
                $data["territoire"]["ID_TYPE_DOWNLOAD"]= $rs6[0]["pk_critere_moteur"]; // id pk dans la table prodige_download_param, util pour l'utisiation de prodigetelecarto
            }
            if($data["territoire"]["TABLE"]!="" && $data["territoire"]["TABLE"]!= ("Sélectionnez une valeur") && $data["territoire"]["TABLE"]!= ("Sélectionnez")
                && $data["territoire"]["CHAMP_ID"]!="" && $data["territoire"]["CHAMP_ID"]!= ("Sélectionnez une valeur") && $data["territoire"]["CHAMP_ID"]!= ("Sélectionnez")
                && $data["territoire"]["CHAMP_LABEL"]!="" && $data["territoire"]["CHAMP_LABEL"]!=("Sélectionnez une valeur") && $data["territoire"]["CHAMP_LABEL"]!=("Sélectionnez")){
                    // recuperation des informations sur la couche permettant de définir les élements du territoire dans la table prodige_download_param
                    // list des id des elements de territoire
                    $strList = "";
                    //$strSql = "SELECT distinct ".$data["territoire"]["CHAMP_ID"].", ".$data["territoire"]["CHAMP_LABEL"]." FROM public.".$data["territoire"]["TABLE"];
                    $strSql = $conn->createQueryBuilder();
                    $strSql->select($data["territoire"]["CHAMP_ID"].", ".$data["territoire"]["CHAMP_LABEL"])
                    ->from("public.".$data["territoire"]["TABLE"]);
                    $rs8 = $conn->fetchAllAssociative($strSql);
                    $j=0;
                    foreach($rs8 as $row) {
                        $data["territoire"]["LISTID"][$j]= $row[$data["territoire"]["CHAMP_ID"]];
                        $data["territoire"]["LISTNOM"][$j]= $row[$data["territoire"]["CHAMP_LABEL"]];
                        $j++;
                    }
            }else{
                $data["territoire"]["TABLE"]=NULL;
            }

            $response = json_encode($data);

        }elseif($service == "getListProdigeDownloadParam"){// pour alimenter les listes permettant de définir le territoire du flux atom dans adminsite
            // todo prendre en compte le label et l'id des champs permettant d'identifier les éléments du territoire

            //$callback =(isset($_GET["callback"])?$_GET["callback"]:NULL); //déjà pris au début ...
            //$bview = (isset($_GET["bView"]) ? $_GET["bView"] : false);
            $bview = $request->get("bView");
            $data = array();

            //Récupération des données  en json;
            $query = "select critere_moteur_nom from prodige_download_param ;";
            $rs_tables = $conn->fetchAllAssociative($query);
            $strData ="[";
            foreach($rs_tables as $row)
            {
                $strData .= "{'name':'".$row["critere_moteur_nom"]."'},";
            }
            $strData = substr($strData, 0, -1). "]";
            if(isset($callback))
                //echo $callback."(".$strData.")";
                $response = $callback."(".$strData.")";
            else //echo $strData;
                $response = $strData;
        }
        return new Response($response ?: "FIN");
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/services_get_tablelist_carto", name="services_get_tablelist_carto", options={"expose"=true}, methods={"GET","POST"})
     */
    public function getTableListCartoAction(Request $request) { //testé hismail
        //$AdminPath = "../Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        //require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");

        //$dao = new DAO();
        //$callback =(isset($_GET["callback"])?$_GET["callback"]:NULL);
        $callback = $request->get("callback", NULL);
        //$bview = (isset($_GET["bView"]) ? $_GET["bView"] : false);
        $bview = $request->get("bView", false);

        $conn = $this->getProdigeConnection('public');

        $data = array();
        if(!$bview){
            //$tableSkippedClause = " and not (tablename in ('spatial_ref_sys','geometry_columns')) ";
            $tableSkippedClause = " not (tablename in ('spatial_ref_sys','geometry_columns')) ";
        }
        else {
            //$tableSkippedClause = " and not (viewname in ('geography_columns')) ";
            $tableSkippedClause = " not (viewname in ('geography_columns')) ";
        }

        //Récupération des données d'une vue en json;
        /*$query = "select ".($bview ? "viewname" : "tablename")." from ".($bview ? "pg_views" : "pg_tables").
        " where schemaname='public' ".$tableSkippedClause."order by ".($bview ? "viewname" : "tablename").";";*/
        $query = $conn->createQueryBuilder();
        $query->select(($bview ? "viewname" : "tablename"))
        ->from(($bview ? "pg_views" : "pg_tables"))
        ->where("schemaname='public'")
        ->andWhere($tableSkippedClause)
        ->orderBy(($bview ? "viewname" : "tablename"));

        //$rs_tables=$dao->BuildResultSet($query)
        $rs_tables = $query->execute()->fetchAllAssociative();

        $strData = array();

        //for ($rs_tables->First(); !$rs_tables->EOF(); $rs_tables->Next())
        foreach($rs_tables as $row)
        {
            //$strData .= "{'name':'".$rs_tables->read(0)."'},";
            $strData[] = array("name"=> $row[($bview ? "viewname" : "tablename")]);
        }
        $strData = json_encode($strData);
        if(isset($callback)){
            //echo $callback."(".$strData.")";
            $response = $callback."(".$strData.")";
        }
        else {//echo $strData;
            $response = $strData;
        }
       return new Response($response ?: "FIN");
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/get_formats", name="services_get_formats", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getFormatsAction(Request $request) { // testé hismail
        //require_once("path.php");
        $file = $this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/cartes/Publication/Formats_Administration.xml";
        $callback = $request->get("callback", null);
        $formatRequest = $request->get("format", null);
        if (file_exists($file)){
            $result = array();
            $xml = simplexml_load_file($file);
            for($i=0;$i<count($xml->FORMAT);$i++){
                if(isset($formatRequest) && $formatRequest==(string)$xml->FORMAT[$i]['CODE']){
                    $result =  array(
                        "format"=>(string)$xml->FORMAT[$i]['CODE'],
                        "nom"=>(string)$xml->FORMAT[$i]['NOM'],
                        "proj4"	=> (string)$xml->FORMAT[$i]['PROJ4'],
                        "display"=>"FORMAT:".(string)$xml->FORMAT[$i]['CODE']." - ".(string)$xml->FORMAT[$i]['NOM']
                    );
                    break;
                }
                $result[] = array(
                    "format"=>(string)$xml->FORMAT[$i]['CODE'],
                    "nom"=>(string)$xml->FORMAT[$i]['NOM'],
                    "proj4"	=> (string)$xml->PROJECTION[$i]['PROJ4'],
                    "display"=>"FORMAT:".(string)$xml->FORMAT[$i]['CODE']." - ".(string)$xml->FORMAT[$i]['NOM']
                );
            }

            if(isset($formatRequest))
                $response = json_encode($result);
            else if($callback!=null)
                $response = $callback."(".json_encode($result).")";
            else
                $response = json_encode($result);
        }
        return new Response($response ?: "FIN");
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/get_config_automate", name="services_get_config_automate", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getConfigAutomateAction(Request $request) {
        /**
         * @author Alkante
         * - generateConfig : Service de génération d'un fichier de configuration chiffré
         * pour l'automate de mise à jour des couches par les services
         * - testConnection : Service de test de la connexion au serveur FTP
         */
        //permet de ne pas prendre en compte le paramétrage SGBD
        //$includeParametrageSgbd = false;

        //$AdminPath = "Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        //require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");
        //require_once("path.php");

        $conn = $this->getProdigeConnection($this->container->getParameter("adminsite")["PRO_SCHEMA_PARAMETRAGE_NAME"]);

        try {
            //ConnectionFactory::BeginTransaction();
            $conn->beginTransaction();
            //$dao = new DAO();
            $data = array();
            //$dao->setSearchPath(PRO_SCHEMA_PARAMETRAGE_NAME);

            //$callback = $_GET["callback"];
            $callback = $request->get("callback");
            //$action   = isset($_GET["action"]) ? $_GET["action"] : "";
            $action   = $request->get("action", "");
            //$service  = isset($_GET["service"]) ? $_GET["service"] : "";
            $service  = $request->get("service");
            $service  = preg_replace("/[^a-zA-Z0-9]+/", "_", $service);

            $values = array(
                "PRO_AUTOMATE_FTP_HOST" => "",
                "PRO_AUTOMATE_FTP_PORT" => "21",
                "PRO_AUTOMATE_FTP_USER" => "",
                "PRO_AUTOMATE_FTP_PASSWORD" => "",
                "PRO_AUTOMATE_FTP_PROXY_HOST" => "",
                "PRO_AUTOMATE_FTP_PROXY_PORT" => "",
                "PRO_AUTOMATE_FTP_PROXY_USER" => "",
                "PRO_AUTOMATE_FTP_PROXY_PASSWORD" => "",
            );

            $query = " SELECT prodige_settings_constant,prodige_settings_value from prodige_settings_automate order by pk_prodige_settings_automate_id ";
            //$rs = $dao->Execute($query);
            $rs = $conn->fetchAllAssociative($query);
            //var_dump(die($rs));
            //while ($row = pg_fetch_array($rs))
            foreach($rs as $row)
            {
                if( array_key_exists(($row["prodige_settings_constant"]), $values) ) {
                    //$values[($row[0])] = ($row[1]);
                    $values[($row["prodige_settings_constant"])] = ($row["prodige_settings_value"]);
                }
            }
            $host = preg_split("/:/", $values["PRO_AUTOMATE_FTP_HOST"], 2, PREG_SPLIT_NO_EMPTY);
            $values["PRO_AUTOMATE_FTP_HOST"] = (is_array($host) && count($host)>0) ? $host[0] : $values["PRO_AUTOMATE_FTP_HOST"];
            if( $values["PRO_AUTOMATE_FTP_HOST"]=="" || $values["PRO_AUTOMATE_FTP_HOST"]=="localhost" ) $values["PRO_AUTOMATE_FTP_HOST"] = $_SERVER["SERVER_NAME"];
            $values["PRO_AUTOMATE_FTP_PORT"] = (is_array($host) && count($host)>1) ? $host[1] : $values["PRO_AUTOMATE_FTP_PORT"];

            $host = preg_split("/:/", $values["PRO_AUTOMATE_FTP_PROXY_HOST"], 2, PREG_SPLIT_NO_EMPTY);
            $values["PRO_AUTOMATE_FTP_PROXY_HOST"] = (is_array($host) && count($host)>0) ? $host[0] : $values["PRO_AUTOMATE_FTP_PROXY_HOST"];
            $values["PRO_AUTOMATE_FTP_PROXY_PORT"] = (is_array($host) && count($host)>1) ? $host[1] : $values["PRO_AUTOMATE_FTP_PROXY_PORT"];

            if($action=="generateConfig") {

                // configuration de l'environnement pour GPG
                putenv("GNUPGHOME=/home/prodige/.gnupg"); // TODO externalize parameter

                // ajouter la clé privée dans le trousseau gpg (si elle n'est pas déjà présente)
                $privatekey = "/home/prodige/automate/prodige.sub.key";
                $publickey  = "/home/prodige/automate/prodige.pub.key";

                $process_args = array("gpg","--batch", "--allow-secret-key-import", "--import", $privatekey );
                $process = new Process($process_args);
                $process -> run ();
                // version avant passage par Symfony Process
                    //shell_exec("gpg --batch --allow-secret-key-import --import $privatekey");

                $casloginUrl  = "https://" . $this->container->getParameter('cas_host') . ':' . $this->container->getParameter('cas_port') . $this->container->getParameter('cas_context');
                $casloginUrl .= "/login?service=" . $this->container->getParameter('PRODIGE_URL_CATALOGUE') . '/geosource/get_config';

                $catalogueUrl = $this->container->getParameter('PRODIGE_URL_ADMINCARTO') . '/prodige/importdata/gettablelist';

                // générer le fichier de paramétrage
                $config = "";
                $config .= "ftp_host=" . $values["PRO_AUTOMATE_FTP_HOST"];
                $config .= PHP_EOL . "ftp_port=" . $values["PRO_AUTOMATE_FTP_PORT"];
                $config .= PHP_EOL . "ftp_user=" . $values["PRO_AUTOMATE_FTP_USER"];
                $config .= PHP_EOL . "ftp_password=" . $values["PRO_AUTOMATE_FTP_PASSWORD"];
                $config .= PHP_EOL . "ftp_proxy_host=" . $values["PRO_AUTOMATE_FTP_PROXY_HOST"];
                $config .= PHP_EOL . "ftp_proxy_port=" . $values["PRO_AUTOMATE_FTP_PROXY_PORT"];
                $config .= PHP_EOL . "ftp_proxy_user=" . $values["PRO_AUTOMATE_FTP_PROXY_USER"];
                $config .= PHP_EOL . "ftp_proxy_password=" . $values["PRO_AUTOMATE_FTP_PROXY_PASSWORD"];
                $config .= PHP_EOL . "url=" . $this->container->getParameter('PRODIGE_URL_CATALOGUE');
                $config .= PHP_EOL . "caslogin=" . $casloginUrl;
                $config .= PHP_EOL . "catalogue=" . $catalogueUrl;
                $config .= PHP_EOL . "service=" . $service;

                $clearfile   = "/home/prodige/automate/config_".$service.".txt";
                $encodedfile = "/home/prodige/automate/config_".$service.".gpg";
                file_put_contents($clearfile, $config);
                // encoder (signer) le fichier avec la clé privée
                $process_args = array("gpg","--batch", "--no-tty", "--yes", "--passphrase=prodige", "--output",$encodedfile, "--sign", $clearfile );
                $process = new Process($process_args);
                $process -> run ();
                // version avant passage par Symfony Process
                    //shell_exec("gpg --batch --no-tty --yes --passphrase=prodige --output $encodedfile --sign $clearfile");
                @unlink($clearfile);

                $zip = new \ZipArchive();
                $filename = "/home/prodige/automate/config_automate_".$service.".zip";
                @unlink($filename);

                if ($zip->open($filename, \ZipArchive::CREATE)!==true) {
                    exit("Impossible d'ouvrir $filename\n");
                }
                $zip->addFile($encodedfile, basename($encodedfile));
                $zip->addFile($publickey, basename($publickey));
                $zip->close();

                // supprimer les fichiers temporaires
                @unlink($encodedfile);

                // envoyer les headers pour le téléchargement
                if(ini_get('zlib.output_compression')) ini_set('zlib.output_compression', 'Off');

                $response = new \Symfony\Component\HttpFoundation\BinaryFileResponse($filename, 200, array(
                    'Pragma'  => 'public',
                    'Expires' => '0',
                    'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                    'Cache-Control' => 'private', // required for certain browsers
                    'Content-type'  => 'application/zip',
                    'Content-Disposition' => 'attachment; filename="' . basename($filename) . '"',
                    'Content-Transfer-Encoding' => 'binary',
                    'Content-length'=> filesize($filename),
                ));
                $response->deleteFileAfterSend(true);

                return $response;

            } else if ($action == "testConnection") { //testé hismail

                $status = "Connexion au serveur FTP établie avec succès !";
                $success = "true";

                // essayer de se connecter à l'hôte
                $ftp = @ftp_connect($values["PRO_AUTOMATE_FTP_HOST"], $values["PRO_AUTOMATE_FTP_PORT"]);
                if( $ftp!==false ) {
                    // ok, essayer de s'authentifier
                    if( !@ftp_login($ftp, $values["PRO_AUTOMATE_FTP_USER"], $values["PRO_AUTOMATE_FTP_PASSWORD"]) ) {
                        $status = "Impossible de s'identifier sur le serveur FTP (".$values["PRO_AUTOMATE_FTP_USER"].":".$values["PRO_AUTOMATE_FTP_PASSWORD"].")";
                        $success = "false";
                    }
                    @ftp_close($ftp);
                } else {
                    $status = "Impossible d'établir une connexion avec le serveur FTP (".$values["PRO_AUTOMATE_FTP_HOST"].":".$values["PRO_AUTOMATE_FTP_PORT"].")";
                    $success = "false";
                }

                //echo $callback."({success:".$success.", status:'".addslashes($status)."'})";
                $response = $callback."({success:".$success.", status:'".addslashes($status)."'})";
                //exit();
            }
        }catch (\Exception $exception){
            $this->get('logger')->error($exception->getMessage());
            $conn->rollBack();
        }

        return new Response($response ?: "FIN");
    }
    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/carto/get_field_list_carto", name="services_get_field_list_carto", options={"expose"=true}, methods={"GET","POST"})
     */
    public function getFieldListCartoAction(Request $request) {

        //$AdminPath = "../Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        //require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");

        //$dao = new DAO();
        $conn = $this->getProdigeConnection('public');
        //$callback =(isset($_GET["callback"])?$_GET["callback"]:NULL);
        $callback = $request->query->get("callback", NULL);
        //$bSkipId = (isset($_GET["bSkipId"]) ? $_GET["bSkipId"] : false);
        $bSkipId = $request->query->get("bSkipId", false);
        //$bSkipGeom = (isset($_GET["bSkipGeom"]) ? $_GET["bSkipGeom"] : false);
        $bSkipGeom = $request->query->get("bSkipGeom", false);
        //$tableName = (isset($_GET["tableName"]) ? $_GET["tableName"] : false);
        $tableName = $request->query->get("tableName", false);

        $data = array();

        $whereClause = "";
        //$whereClause .= $bSkipId ? " and column_name <> 'gid'" : "";
        $whereClause .= $bSkipId ? " column_name <> 'gid'" : "";
        //$whereClause .= $bSkipGeom ? " and column_name <> 'the_geom'" : "";
        $whereClause .= $bSkipGeom ? " column_name <> 'the_geom'" : "";
        //Récupération des données d'une vue en json;
        $query = $conn->createQueryBuilder();
        //$query = "select column_name, data_type from information_schema.columns where table_name='".$tableName."' ".$whereClause;
        $query->select("column_name, data_type")
        ->from("information_schema.columns")
        ->where("table_name=:table_name")
        ->andWhere($whereClause)
        ->setParameter('table_name', $tableName);
        //$rs_tables=$dao->BuildResultSet($query);
        $rs_tables = $query->execute()->fetchAllAssociative();

        //for ($rs_tables->First(); !$rs_tables->EOF(); $rs_tables->Next())
        foreach($rs_tables as $row)
        {
            //$data[$rs_tables->read(0)] = $rs_tables->read(0);
            $data[$row["column_name"]] = $row["column_name"];
        }

        if(isset($callback)) {
            //echo $callback."(".json_encode($data).")";
            $response = $callback."(".json_encode($data).")";
        } else {
            //echo json_encode($data);
            $response = json_encode($data);
       }
       return new Response($response ?: "FIN");
    }


   /**
    * @Security("is_granted('ROLE_USER')")
    * @Route("/carto/get_param_moteur/{table}/{service}", name="services_get_param_moteur", options={"expose"=true}, methods={"GET","POST"})
    */
   public function getParamMoteurAction(Request $request, $table, $service) {

       $conn = $this->getProdigeConnection('public');

       $response = "";

       try {
           if(method_exists($this, $service)) {
               $response = $this->$service($request, $conn, $table);
           } else {
               throw new \Exception(__METHOD__." : service inconnu = {$service}");
           }
       }catch(\Exception $exception){
           throw $exception;
           $response = array("success"=>false, "msg"=>$exception->getMessage());
       }

       return new JsonResponse($response);

   }

   /**
    * @see self::getParamMoteurAction
    *
    * @param Request $request
    * @param \Doctrine\DBAL\Connection $conn
    * @param string $table
    */
   public function get_config_data(Request $request, \Doctrine\DBAL\Connection $conn, $table) {

       //$nbCritere = (($table == "prodige_search_param") ? 9 : 5); //8 critères si moteur de recherche cartographique, 4 si territoires d'extraction RQ. flux atom : critere_moteur_order =-1 et donc pas pris en compte
       $nbCritere = 9;

       $conn->executeStatement('set search_path to '. $this->container->getParameter("adminsite")["PRO_SCHEMA_PARAMETRAGE_NAME"]);
       //$conn = $this->changeSearchPath($conn, $this->container->getParameter("adminsite")["PRO_SCHEMA_PARAMETRAGE_NAME"]);

       //var_dump($rs);
       //die();
       $ar_allData = array();

       $strSqlTestExist = $conn->createQueryBuilder();
       $strSqlTestExist->select("*")->from($table)->where(" critere_moteur_nom<>'' and critere_moteur_nom is not null ");

       $rs = $strSqlTestExist->execute()->fetchAllAssociative();
       $data = array();
       foreach($rs as $row) {
           $critere_moteur_order = ($row['critere_moteur_order']);

           $data['critere_moteur_nom['.$critere_moteur_order.']']        = ($row['critere_moteur_nom'] ?: null);
           $data['critere_moteur_table['.$critere_moteur_order.']']      = ($row['critere_moteur_table'] ?: null);
           $data['critere_moteur_champ_id['.$critere_moteur_order.']']   = ($row['critere_moteur_champ_id'] ?: null);
           $data['critere_moteur_champ_nom['.$critere_moteur_order.']']  = ($row['critere_moteur_champ_nom'] ?: null);
           $data['critere_moteur_id_join['.$critere_moteur_order.']']    = ($row['critere_moteur_id_join'] ?: null);
           $data['critere_moteur_champ_join['.$critere_moteur_order.']'] = ($row['critere_moteur_champ_join'] ?: null);
           $data['extraction_attributaire_select_name'] = ($row['critere_moteur_extractionattributaire'] ?: null);
           $data['_extraction_attributaire_cb_name'] = ($row['critere_moteur_extractionattributaire']!=0 ? "on": "off");

       }
       return array("success"=>true, "data"=>$data);
   }

   /**
    * @see self::getParamMoteurAction
    *
    * @param Request $request
    * @param \Doctrine\DBAL\Connection $conn
    * @param string $table
    */
   public function get_tables_lists(Request $request, \Doctrine\DBAL\Connection $conn, $table) {

        $strSqlTables = "select tablename
                         FROM pg_tables
                         where schemaname='public'
                         and tablename not like 'c_annot%'
                         and tablename not in(?, 'contexte', 'geometry_columns', 'spatial_ref_sys')
                         order by tablename";
        //TODO test query en dure
        //$strSqlTables = "select tablename FROM pg_tables where schemaname='public' and tablename not like 'c_annot%' and tablename not in('prodige_search_param', 'contexte', 'geometry_columns', 'spatial_ref_sys') order by tablename";
        $rsTables = $conn->fetchAllAssociative($strSqlTables, array($table));
        $data = array();
        foreach($rsTables as $row) {
            $data[] = array(
                'displayField' => $row["tablename"],
                'valueField'   => $row["tablename"]
            );
        }
        return $data;
   }
   /**
    * @see self::getParamMoteurAction
    *
    * @param Request $request
    * @param \Doctrine\DBAL\Connection $conn
    * @param string $table
    */
   public function get_layers_fields(Request $request, \Doctrine\DBAL\Connection $conn, $table) {

       $data = array();
       $query = "SELECT a.attnum, a.attname ".
           " FROM pg_class c, pg_attribute a, pg_type t " .
           " WHERE c.relname = ? ".
           " and a.attnum > 0".
           " and a.attname <> 'the_geom'".
           " and a.attrelid = c.oid".
           " and a.atttypid = t.oid".
           " ORDER BY attnum";
       $rs = $conn->fetchAllAssociative($query, array($table));
       foreach($rs as $row) {
           $data[]["fieldName"] = ($row["attname"]);
       }
       return $data;
   }
   /**
    * @see self::getParamMoteurAction
    *
    * @param Request $request
    * @param \Doctrine\DBAL\Connection $conn
    * @param string $table
    */
   public function submit_panel_form(Request $request, \Doctrine\DBAL\Connection $conn, $table) {

       $conn->executeStatement('set search_path to '. $this->container->getParameter("adminsite")["PRO_SCHEMA_PARAMETRAGE_NAME"]);

       try {
           $conn->beginTransaction();


           $strSqlDelete = $conn->createQueryBuilder();
           $strSqlDelete->delete($table);
           $strSqlDelete->execute();

           $strSqlInsert = $conn->createQueryBuilder();
           $strSqlInsert->insert($table)
           ->setValue('pk_critere_moteur', ':pk_critere_moteur')
           ->setValue('critere_moteur_nom', ':critere_moteur_nom')
           ->setValue('critere_moteur_order', ':critere_moteur_order')
           ->setValue('critere_moteur_table', ':critere_moteur_table')
           ->setValue('critere_moteur_champ_id', ':critere_moteur_champ_id')
           ->setValue('critere_moteur_champ_nom', ':critere_moteur_champ_nom')
           ->setValue('critere_moteur_extractionattributaire', ':critere_moteur_extractionattributaire')
           ->setValue('critere_moteur_id_join',     ':critere_moteur_id_join')
           ->setValue('critere_moteur_champ_join', ':critere_moteur_champ_join');

           $critere_moteur_nom        = $request->request->get("critere_moteur_nom", array());
           $critere_moteur_table      = $request->request->get("critere_moteur_table", array());
           $critere_moteur_champ_id   = $request->request->get("critere_moteur_champ_id", array());
           $critere_moteur_champ_nom  = $request->request->get("critere_moteur_champ_nom", array());
           $critere_moteur_id_join    = $request->request->get("critere_moteur_id_join", array());
           $critere_moteur_champ_join = $request->request->get("critere_moteur_champ_join", array());

           $extraction_attributaire_critere = "0";

           $_extraction_attributaire_cb_name  = $request->request->get("_extraction_attributaire_cb_name", "off");
           $critere_moteur_id_join[1] = null;
           $critere_moteur_champ_join[1] = "";

           //change global webservice locator.map then frontcarto loading context service overrides localisationConfig for contextes that does not have personal localisationConfig
           $oMap = ms_newMapObj(PRO_MAPFILE_PATH."/locator.map");
           $oMap->web->set("imageurl", $this->container->getParameter("PRODIGE_URL_FRONTCARTO")."/mapimage/");
           $oMap->setMetadata("wfs_enable_request", "*");
           $oMap->setMetadata("wfs_getfeature_formatlist",	"geojson,csv,ogrgml");

           //while loop since numlayers change and oLayer->index too
           while ($oMap->numlayers!==0) {
               $oLayer = $oMap->getLayer(0);
               $index = $oLayer->index;
               $oMap->removeLayer($index);
           }

           foreach ($critere_moteur_nom as $critere_order=>$nom_critere) {
               if(empty($nom_critere))
                   continue;

               $table_critere = ($critere_moteur_table[$critere_order]);
               $feature_id    = ($critere_moteur_champ_id[$critere_order]);
               $feature_item  = ($critere_moteur_champ_nom[$critere_order]);
               if($_extraction_attributaire_cb_name =="on" && $critere_order == $request->request->get("extraction_attributaire_select_name", 0)){
                  $extraction_attributaire_critere = $request->request->get("extraction_attributaire_select_name", 0);
               }

               $critere_lie   = ($critere_moteur_id_join[$critere_order]);
               $feature_join  = ($critere_moteur_champ_join[$critere_order]);
               if( is_null($critere_lie) || $critere_lie==="") {
                   $critere_lie = null;
                   $feature_join = "";
               }

               $pk_critere_id = $this->getSequenceNextValue($conn, "seq_".$table);

               $strSqlInsert
               ->setParameter("pk_critere_moteur", $pk_critere_id)
               ->setParameter("critere_moteur_nom", $nom_critere)
               ->setParameter("critere_moteur_order", $critere_order)
               ->setParameter("critere_moteur_table", $table_critere)
               ->setParameter("critere_moteur_champ_id", $feature_id)
               ->setParameter("critere_moteur_champ_nom", $feature_item)
               ->setParameter("critere_moteur_extractionattributaire", $extraction_attributaire_critere)
               ->setParameter("critere_moteur_id_join", $critere_lie)
               ->setParameter("critere_moteur_champ_join", $feature_join);
               $rs = $strSqlInsert->execute();

               $layerFields = array($feature_id, $feature_item);
               if($feature_join!=''){
                   $layerFields[] = $feature_join;
               }
               //create layer
               $msLocatorLayer = ms_newLayerObj($oMap);
               $msLocatorLayer->set("name", "layer".$pk_critere_id."_locator");
               $msLocatorLayer->set("type", MS_LAYER_POLYGON);
               $msLocatorLayer->setProjection("EPSG:".PRO_IMPORT_EPSG);

               $msLocatorLayer->set("status", MS_ON);
               $msLocatorLayer->set("connection", "user=".$this->container->getParameter('prodige_user')." password=".$this->container->getParameter('prodige_password')." dbname=".$this->container->getParameter('prodige_name')." host=".$this->container->getParameter('prodige_host')." port=".$this->container->getParameter('prodige_port'));
               $msLocatorLayer->setConnectionType(MS_POSTGIS);

               $msLocatorLayer->set("data", "the_geom from (select ST_Envelope(the_geom) as the_geom, gid, ".
                                    implode(", ", $layerFields).
                                    " from " .$table_critere." order by ".$feature_item.") as foo using unique gid using srid=".PRO_IMPORT_EPSG);
               $msLocatorLayer->setMetaData("ows_include_items", "all");
               $msLocatorLayer->setMetaData("gml_include_items", "all");
               $msLocatorLayer->setMetaData("gml_types", "auto");

           }

           $oMap->save(PRO_MAPFILE_PATH."/locator.map");

           $conn->commit();

           $success = true;

       }
       catch(\Exception $exception) {
           $conn->rollBack();
           $success = false;
           throw $exception;
       }
       return json_encode(array("success"=>$success));
   }
}