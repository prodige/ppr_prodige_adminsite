<?php

namespace ProdigeAdminSite\ServicesBundle\Controller;

use Prodige\ProdigeBundle\Services\ConfigReader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Prodige\ProdigeBundle\Controller\BaseController;

use Symfony\Component\HttpFoundation\JsonResponse;
use ProdigeAdminSite\ServicesBundle\Common\ServicesCatalogue;
use Symfony\Component\Process\Process;
/**
 * @Route("/wmts")
 */
class WMTSController extends BaseController {
    
    
    /**
     * @Route("/generate", name="adminsite_wmts_generate", options={"expose"=true})
     */
    public function generateWMTSTask(){
        $mapcacheExec = $this->container->getParameter("CMD_MAPCACHE");
        $mapcacheDirectory = $this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/mapcache";
        $tilesDirectory = $mapcacheDirectory."/tiles";
        @mkdir($tilesDirectory, 0770, true);
        
        $mapcacheXmlFile = $mapcacheDirectory."/mapcache.xml";
        if  (!file_exists($mapcacheXmlFile) ) return;
        $xml = simplexml_load_file($mapcacheXmlFile);
        
        $minutes = date('i');
        $minutes = (floor($minutes/15)%4)*15;
        $time = date('H:').$minutes;
        $xpath = '//mapcache/tileset[./metadata/generation/@mode="defer" and ./metadata/generation/@status="todo" and ./metadata/generation/@time="'.$time.'"]';
        $tilesets = $xml->xpath($xpath);
        foreach ($tilesets as $tileset){
            $name = (string)$tileset->attributes()->name;
            $errors = array(); 
            $hasError = null;            
            $process_args = array($mapcacheExec, "-c", $mapcacheXmlFile,"-t",  $name,"-o", "\"2012/12/01 20:45\"", "-M", "8,8" , "2>&1"); 
            $process = new Process($process_args); 
            $process->run(); 
            $errors = explode (" ", $process->getOutput()); 
            $hasError = $process->getExitCode(); 
            
            //$cmd = $mapcacheExec." -c ".$mapcacheXmlFile." -t ".$name." -o \"2012/12/01 20:45\" -M 8,8 2>&1";
            // exec($cmd, $errors, $hasError);
            
            if ( !$hasError ){
                $tileset->metadata[0]->generation[0]->attributes()->status = "done";
            }
        }
        
        $xml->asXML($mapcacheXmlFile);
    
        return new JsonResponse(array('success'=>true));
    }
    /**
     * @Route("/migrate", name="adminsite_wmts_migrate", options={"expose"=true})
     */
    public function migrateAction() 
    {
    	
        $conn = $this->getProdigeConnection($this->container->getParameter("adminsite")["PRO_SCHEMA_PARAMETRAGE_NAME"]);
        
        $mapcacheDirectory = $this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/mapcache";
        $tilesDirectory = $mapcacheDirectory."/tiles";
        @mkdir($tilesDirectory, 0770, true);
        $mapcacheXmlFile = $mapcacheDirectory."/mapcache.xml";
        $this->initializeMapcache($mapcacheXmlFile, $tilesDirectory);
        
        $xml = simplexml_load_file($mapcacheXmlFile);
        $xml->addChild("auto_reload", "true");
        $baseDirs = $xml->xpath('//mapcache/cache[@name="disk"]');
        foreach ($baseDirs as $baseDir)$baseDir->base[0] = $tilesDirectory;
        
        $query = " SELECT * from prodige_wmts_layers";
        $rs = $conn->fetchAllAssociative($query);
        foreach($rs as $row){
        	$prodige_wmts_settings_layer = $row["prodige_wmts_settings_layer"];
        	$prodige_wmts_settings_name = $row["prodige_wmts_settings_name"];
        	
          $layers = $xml->xpath('//mapcache/tileset[@name="'.$prodige_wmts_settings_layer.'"]');
          foreach($layers as $layer){
          	if ( $layer->metadata ) continue;
          	$metadata = $layer->addChild("metadata");
          	$metadata->addChild("title", $prodige_wmts_settings_name);
          	$generation = $metadata->addChild("generation");
          	$generation->addAttribute("status", ($row["prodige_wmts_settings_status"]==1 ? "todo" : "done"));
          	$generation->addAttribute("time", preg_replace("!:00$!", "", $row["prodige_wmts_settings_time"]));
          	$generation->addAttribute("mode", ($row["prodige_wmts_settings_time"] ? "defer" : "direct"));
          }
        }
        $xml->asXML($mapcacheXmlFile);
        return new Response($xml->asXML());
    }
    
    protected function initializeMapcache($mapcacheXmlFile, $tilesDirectory)
    {
        if (!file_exists($mapcacheXmlFile)){
            file_put_contents($mapcacheXmlFile, 
<<<MAPCACHE
<?xml version="1.0" encoding="UTF-8"?>
	<mapcache>
	   <cache name="disk" type="disk">
	      <base>{$tilesDirectory}</base>
	      <symlink_blank/>
	   </cache>
	   <default_format>PNG</default_format>
	   <service type="wms" enabled="true">
	   </service>
	   <service type="wmts" enabled="true"/>
	   <service type="tms" enabled="false"/>
	   <service type="kml" enabled="false"/>
	   <service type="gmaps" enabled="false"/>
	   <service type="ve" enabled="false"/>
	   <service type="demo" enabled="false"/>
	   <errors>report</errors>
	   <lock_dir>/tmp</lock_dir>
	</mapcache>
MAPCACHE
            );
        }
    }
    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/execute/{service}", name="adminsite_wmts_execute_service", options={"expose"=true}, methods={"GET","POST"})
     */
    public function executeServiceAction(Request $request, $service) {

        $conn = $this->getProdigeConnection($this->container->getParameter("adminsite")["PRO_SCHEMA_PARAMETRAGE_NAME"]);

        $callback = $request->get("callback");
        
        $mapcacheDirectory = $this->container->getParameter("adminsite")["CARMEN_URL_PATH_DATA"]."/mapcache";
        $this->tilesDirectory = $mapcacheDirectory."/tiles";
        @mkdir($this->tilesDirectory, 0770, true);
        $this->mapcacheXmlFile = $mapcacheDirectory."/mapcache.xml";
    
        $this->initializeMapcache($this->mapcacheXmlFile, $this->tilesDirectory);
        
        if ( method_exists($this, $service) ){
            $response = $this->$service($request, $conn);
            if ( $response instanceof Response ) return $response;
            return new Response($response ? $callback."(".json_encode($response).")" : "FIN");
        }
        return new Response(__METHOD__." : Undefined function {$service}", 500);
    }
    
    protected function getCacheGridList(Request $request, \Doctrine\DBAL\Connection $conn)
    {
        $data = array();
        if (file_exists($this->mapcacheXmlFile)){
            $xml = simplexml_load_file($this->mapcacheXmlFile);
            $grids = $xml->xpath("//mapcache/grid");
            foreach($grids as $grid){
                $data[] = array(
                    "id"     => (string)$grid->attributes()->name,
                    "title"  => (string)$grid->metadata[0]->title[0],
                    "extent" => explode(" ", (string)$grid->extent[0]),
                    "size"   => explode(" ", (string)$grid->size[0]),
                    "srs"    => (string)$grid->srs[0],
                    "units"  => (string)$grid->units[0],
                    "resolutions" => explode(" ", (string)$grid->resolutions[0]),
                );
            }
        }
        return $data;
    }
    
    protected function updateCacheGrid(Request $request, \Doctrine\DBAL\Connection $conn)
    {
        $xml = simplexml_load_file($this->mapcacheXmlFile);
        
        $id = $request->get('id', null);
        $title = $request->get('title', null);
        $extent = array(
            $request->get('minx', null), 
            $request->get('miny', null), 
            $request->get('maxx', null), 
            $request->get('maxy', null), 
        );
        $extent = array_diff($extent, array("", null, 0, "0"));
        $size = array(
            $request->get('width', null), 
            $request->get('height', null), 
        );
        $size = array_diff($size, array("", null, 0, "0"));
        $units = "m";
        $srs = $request->get('projection', $this->container->getParameter("PRODIGE_DEFAULT_EPSG"));
        $resolutions = $request->get('resolutions', array());
        
        if ( count($extent)==4 && count($size)==2 && !empty($resolutions) && $srs ){
            $exists = false;
            if ( $id ){
                $grid = $xml->xpath('//mapcache/grid[@name="'.$id.'"]');
                if ( empty($grid) ){
                    $grid = $xml->addChild("grid");
                    $grid->addAttribute("name", $id);
                } else {
                    $exists = true;
                    $grid = $grid[0];
                }
            } else {
                $ids = array();
                $grids = $xml->xpath("//mapcache/grid");
                foreach($grids as $gridid){
                    $ids[] = (string)$gridid->attributes()->name;
                }
                $grid = $xml->addChild("grid");
                $cpt = (count($grids)+1);
                $id = "grid_".$cpt++;
                while( in_array($id, $ids) ) $id = "grid_".$cpt++;
                $grid->addAttribute("name", $id);
            }
            
            if ( !$exists ){
                $grid->addChild("metadata")->addChild('title', $title);
                $grid->addChild("srs", "EPSG:".$srs);
                $grid->addChild("units", $units);
                $grid->addChild("size", implode(" ", $size));
                $grid->addChild("resolutions", implode(" ", $resolutions));
                $grid->addChild("extent", implode(" ", $extent));
            }
            else {
                  $grid->metadata[0]->title[0] = $title;
                  $grid->size[0] = implode(" ", $size);
                  $grid->extent[0] = implode(" ", $extent);
                  $grid->resolutions[0] = implode(" ", $resolutions);
                  $grid->srs[0] = "EPSG:".$srs;
            }
            $xml->asXML($this->mapcacheXmlFile);
        }
        
        $xml = simplexml_load_file($this->mapcacheXmlFile);
        $data = $request->request->all();
        $success = false;
        if ( $id ){
            $grids = $xml->xpath('//mapcache/grid[@name="'.$id.'"]');
            foreach($grids as $grid){
                $success = true;
                $data = array(
                    "id"     => (string)$grid->attributes()->name,
                    "title"  => (string)$grid->metadata[0]->title[0],
                    "extent" => explode(" ", (string)$grid->extent[0]),
                    "size"   => explode(" ", (string)$grid->size[0]),
                    "srs"    => (string)$grid->srs[0],
                    "units"  => (string)$grid->units[0],
                    "resolutions" => explode(" ", (string)$grid->resolutions[0]),
                );
                $srs = explode(":", $data["srs"]);
                $data["projection"] = end($srs);
                $data = array_merge($data, array_combine(array("minx", "miny", "maxx", "maxy"), $data["extent"]));
                $data = array_merge($data, array_combine(array("height", "width"), $data["size"]));
            }
        }
        
        return new JsonResponse(array("success"=>$success, "record"=>$data));
    }
    
    protected function deleteCacheGrid(Request $request, \Doctrine\DBAL\Connection $conn)
    {
        if (!file_exists($this->mapcacheXmlFile)){
            return new JsonResponse(array("success"=>true, "id"=>null));
        }
        $xml = simplexml_load_file($this->mapcacheXmlFile);
        $id = $request->get('id', null);
        if ( !$id ){
            return new JsonResponse(array("success"=>true, "id"=>null));
        }
            
        $tilesets = $xml->xpath('//tileset/grid[text()="'.$id.'"]');
        if ( !empty($tilesets) ){
            return new JsonResponse(array("success"=>false, "message"=>"Cette grille est utilisée sur des couches tuilées"));
        }
          
        $grids = $xml->xpath('//mapcache/grid');
        foreach($grids as $index=>$grid){
            $aid = ((string)$grid->attributes()->name);
            if ( $id==$aid ){
                unset($xml->grid[$index]);
            }
        }

        $xml->asXML($this->mapcacheXmlFile);
        $grids = $xml->xpath('//mapcache/grid[@name="'.$id.'"]');
        
        return new JsonResponse(array("success"=>empty($grids), "id"=>$id, "message"=>(empty($grids) ? null : "La suppression a échoué")));
    }
    
    protected function getCouchesList(Request $request) {

        $connCatalogue = $this->getConnection('catalogue', 'catalogue');
        $data = array();
        //Récupération des données d'une vue en json;
        $query = "select couchd_nom, couchd_emplacement_stockage, couchd_type_stockage, fmeta_id, metadata.uuid ".
            " from catalogue.couche_donnees ".
            " inner join catalogue.fiche_metadonnees on (fmeta_fk_couche_donnees=pk_couche_donnees) ".
            " inner join public.metadata on (metadata.id::text=fmeta_id) ".
            " where couchd_type_stockage=1 OR couchd_type_stockage=0 ".
            " order by couchd_nom";
        
        $rs_tables = $connCatalogue->fetchAllAssociative($query);
        

        $connCarto = $this->getConnection('prodige', 'public');
        ServicesCatalogue::setConnection($connCarto);
        $dataCarto = ServicesCatalogue::getTableList();
        ServicesCatalogue::setConnection($connCatalogue);

        foreach($rs_tables as $row){
            if ( !$row["couchd_emplacement_stockage"] ) continue;
            if(in_array($row["couchd_emplacement_stockage"], $dataCarto) && $row["couchd_type_stockage"] == 1) {
                $id = $row["couchd_emplacement_stockage"];
                $data[] = array('path'=>$row["couchd_emplacement_stockage"], 'id'=>$id, 'fmeta_id'=>$row["fmeta_id"], 'uuid'=>$row["uuid"], 'name'=>$row["couchd_nom"],'type'=>$row["couchd_type_stockage"]);
            }
            else if($row["couchd_type_stockage"] == 0) {
                $id = explode(".", str_replace('/','_', $row["couchd_emplacement_stockage"]));
                $id = current($id)."_".$row["fmeta_id"];
                $data[] = array('path'=>$row["couchd_emplacement_stockage"], 'id'=>$id, 'fmeta_id'=>$row["fmeta_id"], 'uuid'=>$row["uuid"], 'name'=>$row["couchd_nom"],'type'=>$row["couchd_type_stockage"]);
            }
        }
        
        return $data;
    }
    
    protected function getDefaultParams(Request $request, \Doctrine\DBAL\Connection $conn)
    {
        $data = array();
        $query = " SELECT * from prodige_wmts_settings order by pk_prodige_wmts_settings_id ";
        $rs = $conn->fetchAllAssociative($query);
        $i =0;
        foreach($rs as $row){
            foreach($row as $key => $value){
                if(!is_numeric($key)) $data[$i][$key]= $value;
            }
            $i++;
        }
        return $data;
    }
    
    
    protected function hasMap(Request $request, \Doctrine\DBAL\Connection $conn)
    {
        $directory = rtrim($this->container->getParameter('defines')['public']["PRO_MAPFILE_PATH"], '/')."/";
        
        $layerId = null;
        $id = $request->get('id', null);
        $path = $request->get('path', null);
        $layerType = $request->get('type', null);
        $mapfile = "layers/wmts_".$id;
        
        $mapfile = $directory.$mapfile.".map";
        $exists = file_exists($mapfile) && is_file($mapfile);
        if(!$exists){
            $connCatalogue = $this->getConnection('catalogue', 'catalogue');
            $data = array();
            //Récupération des données d'une vue en json;
            $query = "select couchd_nom, couchd_emplacement_stockage, couchd_type_stockage, fmeta_id, metadata.uuid ".
                " from catalogue.couche_donnees ".
                " inner join catalogue.fiche_metadonnees on (fmeta_fk_couche_donnees=pk_couche_donnees) ".
                " inner join public.metadata on (metadata.id::text=fmeta_id) ".
                " where couchd_emplacement_stockage like :couchd_emplacement_stockage".
                " order by couchd_nom";

            $rs_tables = $connCatalogue->fetchAllAssociative($query, array("couchd_emplacement_stockage" => $id));
            foreach($rs_tables as $key => $value){
                $uuid = $value["uuid"];
            }
            $mapfile = "layers/wmts_".$uuid;
            $mapfile = $directory.$mapfile.".map";
            $exists = file_exists($mapfile) && is_file($mapfile);
        
        }
   
        if ( $exists ){
            $oMap = @ms_newMapObj($mapfile); 
            if($oMap) {
                for($i=0; $i<$oMap->numlayers; $i++) {
                    $oLayer = $oMap->getLayer($i);
                    if ( $oLayer->connectiontype==MS_POSTGIS && $layerType==1 ){
                        if(preg_match("!\b".$path."\b!", $oLayer->data)) {
                            $layerId = $oLayer->name;
                            break;
                        }
                    }
                    else if ( $oLayer->type==MS_LAYER_RASTER && $layerType==0 ){
                        if($oLayer->data == $directory.$path || $oLayer->tileindex == $directory.$path){
                            $layerId = $oLayer->name;
                            break;
                        }
                    }
                }
            }
        }
                    
        $exists = !is_null($layerId) && $exists;
        return array("status"=>($exists ? "done" : "todo"), "wmts"=>$mapfile, "msname"=>$layerId);
    }
    
    protected function openMap(Request $request, \Doctrine\DBAL\Connection $conn)
    {
        $edit_url = null;
        $connection = $this->getProdigeConnection('carmen');
        $url_admincarto = $this->container->getParameter("PRODIGE_URL_ADMINCARTO");
        $url_catalogue = $this->container->getParameter("PRODIGE_URL_CATALOGUE");
    
        $uuid = $request->get('uuid', null);
        $mapfile = $request->get('mapfile', null);
        
        if ( !$mapfile || !$uuid){
            return array("success"=>false);
        }
        $mapfile = "layers/".$mapfile;
        
        // 1. recherche du mapfile de représentation WMTS pour la couche sélectionnée
        $tabMaps = $connection->fetchAllAssociative("select map_wmsmetadata_uuid from map where map_file = :map_file and published_id is null", array (
            "map_file" => $mapfile 
        ));
        if( !empty($tabMaps) && file_exists($this->container->getParameter('defines')['public']["PRO_MAPFILE_PATH"]."/".$mapfile.'.map') ) {
            // 1: Réponse=OUI : le mapfile de représentation WMTS existe déjà => on redirige vers son édition
            foreach($tabMaps as $val){
                $mapUuid = $val ['map_wmsmetadata_uuid'];
                $edit_url = $url_admincarto. "/edit_map/" . $mapUuid;
            }
        } else { 
            // 1: Réponse=NON 
            // 2. recherche du mapfile de représentation par défaut de la métadonnée associé à la couche sélectionnée pour recopie vers le mapfile WMTS de la couche
            $mapUuid = uniqid();
            $tabMaps = $connection->fetchAllAssociative("select map_id from map where map_wmsmetadata_uuid = :uuid and published_id is null", array (
                "uuid" => $uuid 
            ));
            if ( !empty($tabMaps) ) {
                // 2: Réponse=OUI : génère le mapfile de représentation WMTS par copie de la représentation par défaut de la métadonnée
                foreach($tabMaps as $val){
                    $mapId = $val ['map_id'];
                    $carmenEditMapAction = $url_admincarto . "/api/map/edit/" . $mapId."/resume";
                    $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenEditMapAction);
                    $jsonResp = $this->curl($carmenEditMapAction, 'GET', array ('ticket' => $ticket), array());
                    
                    $jsonObj = json_decode($jsonResp);
                    if ( $jsonObj && $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId ) {
                        $map_id = $jsonObj->map->mapId;
                        // 2 change map parameters
                        $prodigeConnection = $this->getProdigeConnection("carmen");
                        $prodigeConnection->executeStatement("update map set published_id =:map_id,  map_wmsmetadata_uuid=:uuid where map_id=:map_id", array (
                            "map_id" => $map_id,
                            "uuid" => $mapUuid 
                        ));
                        
                        // 4 save/publish Map
                        $carmenSaveMapAction = $url_admincarto . "/api/map/publish/" . $map_id . "/" . $map_id;
                        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($carmenSaveMapAction);
                        $jsonResp = $this->curl($carmenSaveMapAction, 'POST', array ('ticket' => $ticket), array (
                            "mapModel" => "false",
                            "mapFile" => $mapfile 
                        ));
                        $jsonObj = json_decode($jsonResp);
                        if ( $jsonObj && $jsonObj->success && $jsonObj->map ) {
                            // redirect to map
                            $edit_url = $url_admincarto . "/edit_map/" . $mapUuid;
                        }
                    }
                    // $urlOpenMap = $request->getSchemeAndHttpHost().$this->generateUrl('catalogue_geosource_layerAddToMap', array("uuid" =>$uuid))."?mapUuid=".$mapUuid."&model=".$mapId."&prefixMapfile=layers/WMS/";
                }
            } else { 
                // 2: Réponse=NON : génère le mapfile de représentation WMTS à partir d'un modèle
                $edit_url = $url_catalogue . "/geosource/layerAddToMap/". $uuid . "?mapUuid=" . $mapUuid . "&prefixMapfile=layers/wmts_";
            }
        }
        
        if ( $edit_url) {
          return array(
          	"success" => true,
          	"edit_url" => $edit_url,
            "data"=>array(
            	"mapfile" => $mapfile,
          	  "mapUuid" => $mapUuid
            )
          );
        } else {
          return array(
          	"success" => false,
          );
        }
    }
    
    protected function getLayersList(Request $request, \Doctrine\DBAL\Connection $conn)
    {
        $wmtsMapDirectory = $this->container->getParameter('defines')['public']["PRO_MAPFILE_PATH"]."/layers";
        
        $data = array();
        if (file_exists($this->mapcacheXmlFile)){
            $xml = simplexml_load_file($this->mapcacheXmlFile);
            $tilesets = $xml->xpath("//mapcache/tileset");
            foreach($tilesets as $tileset){
            	$metadata = $tileset->metadata[0];
            	if ( !$metadata ) continue;
            	$generation = $metadata->generation[0];
            	if ( !$generation ) continue;
            	
            	$wmts = null;
            	$source = $xml->xpath('//mapcache/source[@name="'.((string)$tileset->source[0]).'"]');
            	if ( count($source)>0 ){
            		$source = $source[0];
            		$source = $source->xpath('./getmap/params/MAP');
            		if ( count($source)>0 ){
            			$wmts = basename((string)$source[0]);
            			if ( !(file_exists($wmtsMapDirectory."/".$wmts) && is_file($wmtsMapDirectory."/".$wmts)) ){
            				$wmts = null;
            			} 
            		}
            	}
            	$data[] = array(
            		"id"     => (string)$tileset->attributes()->name,
            		"title"  => (string)$metadata->title[0],
            		"format" => (string)$tileset->format[0],
            		"grid"   => (string)$tileset->grid[0],
            		"status" => (string)$generation->attributes()->status,
            		"mode"   => (string)$generation->attributes()->mode,
            		"time"   => (string)$generation->attributes()->time,
            		"wmts"   => $wmts,
            	);
            	
            }
        }
        return $data;
    }            

    protected function updateLayer(Request $request, \Doctrine\DBAL\Connection $conn)
    {
        $cache = "disk";
        $metatile = "5 5";
        $metabuffer = "10";
        $expires = "3600";
        $layer = $request->get('layer', array());
        $nodes = array(
        	"tileset"=> array(
        		'@id' => '[@name="'.$layer["id"].'"]',
        		'@attributes' => array(
        			"name" => $layer["id"],
        		),
        		"source" => "source_".$layer["id"],
        		"cache"  => $cache,
        		"grid"   => $layer["grid"],
        		"format" => $layer["format"],
		        "metatile" => $metatile,
		        "metabuffer" => $metabuffer,
		        "expires" => $expires,
        		"metadata" => array(
        			"title" => $layer["title"],
        			"generation" => array(
        				"@attributes" => array(
        					"mode" => $layer["mode"],
        					"time" => (isset($layer["time"]) ? $layer["time"] : ""),
        					"status" => "todo"
        				), 
        			),
        		),
        	),
        	"source" => array(
        		'@id' => '[@name="source_'.$layer["id"].'"]',
        		'@attributes' => array(
        			"name" => "source_".$layer["id"],
        			"type" => "wms",
        		),
        		"getmap" => array(
        			"params" => array(
        				"FORMAT" => "image/".strtolower($layer["format"]),
        				"LAYERS" => $layer["msname"],
        				"TRANSPARENT" => "true",
        				"MAP" => realpath($layer["wmts"]),
        			),
        		),
        		"http" => array(
        			"url" => $this->container->getParameter('defines')['public']["CARMEN_URL_SERVER_DATA"].'/cgi-bin/mapserv?'
        		)
        	),
        );
        
        $toXml = function(\DOMDocument $doc, \DOMXPath $xpath, \DOMElement $root, array $nodes)use(&$toXml){
        	foreach ($nodes as $tag=>$contents){
        		$id = null;
        		if ( is_array($contents) ){
        		  $node = $doc->createElement($tag);
        		  
        		  if ( isset($contents["@attributes"]) ){
        		  	foreach ($contents["@attributes"] as $name=>$value){
        		  		$node->setAttribute($name, $value);
        		  	}
        		  	unset($contents["@attributes"]);
        		  }
        		  
        		  if ( isset($contents["@id"]) ){
	        		  $id = $contents["@id"];
	        		  unset($contents["@id"]);
        		  }
        		  
        		  $toXml($doc, $xpath, $node, $contents);
        		} else {
        			$node = $doc->createElement($tag, $contents);
        		}
        		if (isset($id)){
        			
        			$finds = $xpath->query("./".$tag.$id, $root);
        			if ( $finds->length>0 ){
        				$oldNode = $finds->item(0);
        				$root->replaceChild($node, $oldNode);
        			} else { 
        				$root->appendChild($node);
        			}
        		} else {
        			$root->appendChild($node);
        		}
        	}
        };
        
        $doc = new \DOMDocument();
        $doc->load($this->mapcacheXmlFile);
        $root = $doc->documentElement;
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;
        $xpath = new \DOMXPath($doc);

        $toXml($doc, $xpath, $root, $nodes);
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;
        file_put_contents($this->mapcacheXmlFile, $doc->saveXml());
        
        
        return array("success"=>true);
    }
    
    
    protected function deleteLayer(Request $request, \Doctrine\DBAL\Connection $conn)
    {
        $tilesetId = $request->get("tilesetId", -1);

        if(is_dir($this->tilesDirectory."/".$tilesetId."/")){
            $rmdir = $this->tilesDirectory."/".$tilesetId."/"; 
            $process_args = array("rm", "-r", $rmdir); 
            $process = new Process($process_args); 
            $process-> run (); 
            
            //exec("rm -r ".$this->tilesDirectory."/".$tilesetId."/");
        }
        if(file_exists(PRO_MAPFILE_PATH."/Publication/layers/wmts_".$tilesetId.".map")) {
            unlink(PRO_MAPFILE_PATH."/Publication/layers/wmts_".$tilesetId.".map");
        }
        
        $xml = simplexml_load_file($this->mapcacheXmlFile);
        $tilesets = $xml->xpath('//mapcache/tileset');
        $sources = $xml->xpath('//mapcache/source');
        foreach ($tilesets as $t_index=>$tileset){
        	if ( ((string)$tileset->attributes()->name)==$tilesetId ){
        		$sourceId = (string)$tileset->source[0];
        		foreach ($sources as $s_index=>$source){
        			if ( ((string)$source->attributes()->name)==$sourceId ){
        				unset($sources[$s_index]);
        			}
        		}
        		unset($tilesets[$t_index]);
        	}
        }
        $xml->asXML($this->mapcacheXmlFile);
        
        return array("success"=>true);
    }
        
    
    protected function getStatus($value) {
         switch ($value) {
             case 0 : return "A la volée";
             case 1 : return "En cours";
             case 2 : return "Généré";
             default : return "Statut inconnu";
         }
   }
   
}
