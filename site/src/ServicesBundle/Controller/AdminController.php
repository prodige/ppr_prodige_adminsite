<?php

namespace ProdigeAdminSite\ServicesBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use ProdigeAdminSite\ServicesBundle\Common\ServiceFunction;

use Prodige\ProdigeBundle\Controller\BaseController;

use Symfony\Component\HttpFoundation\JsonResponse;


class AdminController extends BaseController {
    const MODE_SERVICES = 0;
    const MODE_WFS      = 1;
    const MODE_WMS      = 2;
    const MODE_WMTS     = 3;
    const MODE_PROJ     = 4;
    const MODE_TOOLS    = 5;
    const MODE_WMSC     = 6;
    const MODE_MODELES  = 7;
    
    const DEFAULT_DIRECTORY = "cartes";
    
    //TODO à remettre dans un fichier de config
    const SYSTEM_PROJECTIONS = "/usr/share/proj/epsg";

    protected $files = array(
        0 => array("concat"=>false, "file"=>"/SYSTEM/services.xml"),
        1 => array("concat"=>true,  "file"=>"/Publication/ServeursWFS_Administration.xml"),
        2 => array("concat"=>true,  "file"=>"/Publication/ServeursWMS_Administration.xml"),
        4 => array("concat"=>true,  "file"=>"/Publication/Projections_Administration.xml"),
        5 => array("concat"=>false, "file"=>"/SYSTEM/Tools_Administration.xml"),
        6 => array("concat"=>true,  "file"=>"/Publication/ServeursWMSC_Administration.xml"),
        7 => array("concat"=>true,  "file"=>"/Publication/Modele_Administration.xml"),
    );
    /**
     * Get Doctrine connection
     * @param string $name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($name, $schema="public") {
        $conn = $this->getDoctrine()->getConnection($name);
        $conn->exec('set search_path to '.$schema);
        // Pour que la connection soit utilisé dans le namespace ServicesFunction
        ServiceFunction::setConnection($conn);
        return $conn;
    }

    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/admin/getdataflow/{iMode}/{serviceUrl}", name="admin_getdataflow", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getDataFlowAction(Request $request, $iMode, $serviceUrl=null) { // testé hismail
        /*
         * Modified on 14 March 2016
         * Version Prodige v4
         * */

        $strDataPath = $this->container->getParameter('CARMEN_URL_PATH_DATA');

        $directory = $request->get("directory", "");
        
        $serviceUrl = urldecode($serviceUrl);

        $fileDef = $this->files[$iMode];
        $file =  $strDataPath.( $fileDef["concat"] ? "/".$directory : "").$fileDef["file"];
        
        if(isset($serviceUrl)){
                if($file) {
                    if(file_exists($file)) {
                        $tailleFichier = filesize($file);
                        return new Response(file_get_contents($file), Response::HTTP_OK, array(
                            "Content-Type"=>"application/xml; charset=ISO-8859-1",
                            "Content-Length"=>$tailleFichier,
                        ));
                    } else {
                        return new Response("File does not exists, check your configuration: ".$file, 500);
                    }
                } else {
                    return new Response("Invalid service mode: ".$iMode, 500);
                }
        }
    } // end action getDataFlow


    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/admin/update_service/{path}/{option}/{serviceUrl}", name="admin_updateService", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function updateServiceAction(Request $request, $path, $option, $serviceUrl=null) {
        //hismail
        /*
         * Modified on 17 march. 2016
         * Version Prodige V4
         * */

        $strDataPath = CARMEN_URL_PATH_DATA;

        if($request->request->get("call_to_add_proj", false)) {
            $file = $strDataPath."/cartes/Publication/Projections_Administration.xml";
            $serviceProj= Routing.generate('carto_get_params');
            $epsg = $request->request->get("epsg_proj");
            $nom = $request->request->get("nom");
            $proj4 = $request->request->get("proj4_proj");
            $srid		= $request->request->get("srid_proj");
            $auth_name	= $request->request->get("auth_name_proj");
            $auth_srid	= $request->request->get("auth_srid_proj");
            $srtext = $request->request->get("srtext_proj");
            if($request->request->get("call_to_add_proj") == 1){
                //Modification
                $idRow = $request->request->get("idRow");
                ServiceFunction:: updateProjectionFromForm($file, $serviceProj, $idRow, $epsg, $nom, $proj4, $srid, $auth_name, $auth_srid, $srtext);
            }else{
                //Ajout
                ServiceFunction:: addProjectionFromForm($file, $serviceProj, $epsg, $nom, $proj4, $srid, $auth_name, $auth_srid, $srtext);
            }
            //On ferme la fenetre
        }
        // Fin de l'extrait

        $action = $request->get("action", null);
        $field = $request->get("field", null);
        $value = $request->get("value", null);
        $option = $request->get("option", null);
        $strDirectory = $request->get("path", null);
        $strDirectory = $strDirectory ? urldecode($strDirectory) : $strDirectory;
        $id = $request->get("id", null);
        $serviceUrl = $request->get("serviceUrl", null);

        $fileDef = $this->files[$option];
        $file =  $strDataPath.( $fileDef["concat"] ? "/".$strDirectory : "").$fileDef["file"];
        
        switch ($action){
            //mode add
            case "add": //testé hismail
                if($option != self::MODE_PROJ)
                    ServiceFunction::addServeur($file, /*$serviceUrl*/null);
                    break;

            //mode update
            case "update": //testé hismail
                ServiceFunction::updateServeur($file, $field, $value, $id, /*$serviceUrl*/null);
                break;

            //mode delete
            case "delete": //testé hismail
                $strNom = $request->get("nom", null);
                if( isset($strNom) ) {
                    if($option == self::MODE_PROJ){
                        if($strProj = $request->get("epsg", null)) {
                            ServiceFunction::delProjection($file, $strNom, $strProj, /*$sericeUrl*/null);
                        }
                    } else {
                        $strUrl = $request->get("url", null);
                        if( isset($strUrl) ) {
                            ServiceFunction::delServeur($file, $strNom, $strUrl, /*$sericeUrl*/null, true);
                        }
                    }
                }
            break;

                //on reecrit dans le bon ordre
            case "rewrite_ordered_xml":
                $nbrows = $request->get("nbrows", null);
                $elems = $request->get("elems", null);
                if( isset($nbrows) && isset($elems) ) {
                    if($option == self::MODE_PROJ){
                        $noms = array();
                        $projs = array();
                        $elems_explode = explode("|", $elems);
                        foreach($elems_explode as $elem){
                            $elem_explode = explode("=", $elem);
                            if(preg_match('/^NOM/', $elem_explode[0]))
                                $noms[] = $elem_explode[1];
                                if(preg_match('/^EPSG/', $elem_explode[0]))
                                    $projs[] = $elem_explode[1];
                        }
                        //reorder_xml_proj_file($file, $nbrows, $noms, $projs, $serviceUrl);
                        ServiceFunction::reorder_xml_proj_file($file, $nbrows, $noms, $projs, $serviceUrl);
                    }else{
                        $noms = array();
                        $urls = array();
                        $elems_explode = explode("|", $elems);
                        foreach($elems_explode as $elem){
                            $elem_explode = explode("=", $elem);
                            if(preg_match('/^NOM/', $elem_explode[0]))
                                $noms[] = $elem_explode[1];
                                if(preg_match('/^URL/', $elem_explode[0]))
                                    $urls[] = $elem_explode[1];
                        }
                        //reorder_xml_server_file($file, $nbrows, $noms, $urls, $serviceUrl);
                        ServiceFunction::reorder_xml_server_file($file, $nbrows, $noms, $urls, $serviceUrl);
                    }
                }
                break;
        }
        return new JsonResponse(array("success"=>true));
    }
    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/admin/get_proxy_spatial_ref_sys", name="admin_get_proxy_spatial_ref_sys", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getProxySpatialRefSys(Request $request) {
        $strDataPath = CARMEN_URL_PATH_DATA;
        $strDirectory = self::DEFAULT_DIRECTORY;
        $fileDef = $this->files[self::MODE_PROJ];
        $fileProj = $strDataPath.( $fileDef["concat"] ? "/".$strDirectory : "").$fileDef["file"];

        $conn = $this->getConnection('prodige');

        $mode = $request->get("mode", "");

        if($mode == "epsg_intersect") {
            $doc = new \DOMDocument();
            $doc->load($fileProj);
            $xpath = new \DOMXPath($doc);
            
            $projections_base = array();
            $projections_merge = array();
            //On recupere les projections presentes en base
            $strSql = "SELECT srid, auth_name, auth_srid, srtext, proj4text  FROM public.spatial_ref_sys";
            $ds = $conn->fetchAllAssociative($strSql);
            foreach($ds as $dr) {
                if ( $xpath->query('//PROJECTIONS/PROJECTION[@EPSG="'.$dr["srid"].'"]')->length>0 ) continue;
                $projections_base[ $dr["srid"] ] = array(
                    "srid"                      => $dr["srid"],
                    "auth_name" => $dr["auth_name"],
                    "auth_srid" => $dr["auth_srid"],
                    "srtext"            => $dr["srtext"],
                    "proj4"     => $dr["proj4text"],
                    "nom" => "",
                );
            }
            $projections_merge = $projections_base;
            //On recupere les projections presentes dans le fichier epsg
            // Marie et/ou Vincent
            //$input_file = file_get_contents("/usr/share/proj/epsg");
            $input_file = file_get_contents(self::SYSTEM_PROJECTIONS);
            $input_file = explode("<>", $input_file);
            //On parcours chaque ligne du ficheir
            foreach($input_file as $projection){
                $srid_proj = array(); $name_proj = array();
                preg_match('/\<\w+\>/', $projection, $srid_proj);
                preg_match('/\#.+/', $projection, $name_proj);
                if(isset($srid_proj[0])){
                    $srid_proj = substr($srid_proj[0], 1, strlen($srid_proj[0])-2);
                    $name_proj = trim($name_proj[0]);
                    $name_proj = substr($name_proj, 2);
                    //On test si la projection actuelle du fichier est presente en base
                    //Si c'est le cas, on l'ajoute au tableau resultats 
                    if ( isset($projections_merge[$srid_proj]) )
                        $projections_merge[$srid_proj]["nom"] = $name_proj;
                }
            }

            $projections_merge = array_values($projections_merge);
            
            //On renvoie l'intersection des deux
            $response = json_encode($projections_merge);
            
        }elseif($mode == "preremplir") {
            // modifications : à l'ouverture du formulaire le mode préremplir est actif : on cherche à interroger spatialreference
            // si la projection existe déjà cela n'a aucun sens : les informations sont en base 
            // 
            //On recupere le code epsg a rechercher
            $epsg_proj = $request->get("epsg_proj", "");   
                 
            // test d'existence en base 
            $strSql = "SELECT srid, auth_name, auth_srid, srtext, proj4text  FROM public.spatial_ref_sys where srid =:srid";
            $ds = $conn->fetchAllAssociative($strSql, array("srid"=>$epsg_proj));
            
            if(isset($ds[0]) && !empty($ds[0])) {
                
                $result_array = array(); 
                    $result_array['srid'] = $ds[0]["srid"] ;
                    $result_array['auth_name']= $ds[0]["auth_name"];
                    $result_array['auth_srid']= $ds[0]["srid"];
                    $result_array['srtext'] = $ds[0]["srtext"]; 
                    $result_array['proj4'] = $ds[0]["proj4text"]; 
             
            } else {
                
                //On recupere le code epsg a rechercher
                $epsg_proj = $request->get("epsg_proj", "");
                $url = 'http://spatialreference.org/ref/epsg/'.$epsg_proj.'/';
                $result_array = array();
                $result_curl = "";

                //On initialise la session cURL
                $curl = curl_init();


                //On recupere le resultat postgis
                $urlpostgis = $url.'postgis/';
                curl_setopt($curl, CURLOPT_URL, $urlpostgis);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $result_curl = curl_exec($curl);

                if(preg_match('/^Not found/', $result_curl)) {
                    $result_array['postgis']	= '';
                    $result_array['srid'] 		= '';
                    $result_array['auth_name']	= '';
                    $result_array['auth_srid']	= '';
                    $result_array['srtext']		= '';
                }else{
                    $result_postgis = array();
                    preg_match('/^insert into.+\((.+)\)\s*values\s*\((.+)\)/is', $result_curl, $result_postgis);
                    if(!empty($result_postgis)) {
                        $fields = trim($result_postgis[1]);
                        $values = trim($result_postgis[2]);

                        $fields = explode(", ", $fields);
                        $values = preg_split('/(?<=[\d\']), (?=[\d\'])/is', $values);
                        $values = array_map(function($v){return trim($v, "'");}, $values);
                        $result_array = array_combine($fields, $values);
                        $result_array["proj4"] = $result_array["proj4text"];                    
                    } else {
                        $result_array["proj4"] = ""; 
                    }
                }

                curl_close($curl);                
            }
            
            $epsg_nom = array();
            $input_file = file_get_contents(self::SYSTEM_PROJECTIONS);
            preg_match("!(<>\s+|^)# ([^#]+?)\s+<".$epsg_proj.">.+?<>!s", $input_file, $epsg_nom);
            $epsg_nom = end($epsg_nom);
            $result_array["nom"] = utf8_encode($epsg_nom);         
            
            $response = json_encode($result_array);
            
        } elseif($mode == "save_epsg") {
        	set_time_limit(0);
            
            $success = true;
            $response = "";
            
            $epsg      = $request->get("epsg", "");
            $nom       = $request->get("nom", "");
            $proj4     = $request->get("proj4", "");
            $srid      = $request->get("srid", "");
            $auth_name = $request->get("auth_name", "");
            $auth_srid = $request->get("auth_srid", "");
            $srtext    = $request->get("srtext", "");
            if($epsg != "" && $nom != "" && $proj4 != "" && $srid != "" && $auth_name != "" && $auth_srid != "" & $srtext != ""){
            	$response = array();
            	try {
            		$success = true;
            		try {
                        /*
                         * Etape 1 : Ajout/update dans public.spatial_ref_sys
                         */
                        $fields = array(
                            "srid"      => $srid,
                          "auth_name" => $auth_name,
                          "auth_srid" => $auth_srid,
                          "srtext"    => $srtext,
                          "proj4text" => $proj4,
                        );
                        $query = $conn->createQueryBuilder();
                        if($conn->executeQuery("SELECT 1 FROM public.spatial_ref_sys WHERE auth_srid=?", array($auth_srid))->rowCount() == 0) {
                            $query->insert('public.spatial_ref_sys');
                            $func = "setValue";
                        } else {
                              $query->update('public.spatial_ref_sys')->where("auth_srid=:auth_srid");
                            $func = "set"; 
                        }
                        foreach(array_keys($fields) as $name){
                            $query->$func($name, ":".$name);
                        }
                        $query->setParameters($fields);
                        $query->execute();
            		    $response[] = "L'enregistrement de la projection dans la base de données a réussi.";
            		} catch (\Exception $exception){
            			throw new \Exception("L'enregistrement de la projection dans la base de données a échoué : ".$exception->getMessage(), 500, $exception);
            		}
            		
            		try {
                        /*
                         * Etape 2 : Ajout/update dans $file[self::MODE_PROJ]
                         */
			            $doc = new \DOMDocument();
			            $doc->load($fileProj);
			            $xpath = new \DOMXPath($doc);
			            $nodes = $xpath->query('//PROJECTIONS/PROJECTION[@EPSG="'.$auth_srid.'"]');
			            if ( $nodes->length>0 ){
			            	foreach($nodes as $node){
			            		$node instanceof \DOMNode;
			            		$node->attributes->getNamedItem('NOM')->nodeValue = $nom;
			            	}
			            } else {
			            	$projection = $doc->createElement("PROJECTION");
			            	$projection->setAttribute("NOM", $nom);
			            	$projection->setAttribute("EPSG", $auth_srid);
			            	$doc->documentElement->appendChild($projection);
			            }
			            $doc->save($fileProj);
                        $response[] = "L'enregistrement de la projection dans le fichier d'administration du site a réussi.";
            		} catch (\Exception $exception){
            			throw new \Exception("L'enregistrement de la projection dans le fichier d'administration du site a échoué : ".$exception->getMessage(), 500, $exception);
            		}
            		
            		try {
                        /*
                         * Etape 3 : Ajout/update dans self::SYSTEM_PROJECTIONS
                         */
                        $contents = file_get_contents(self::SYSTEM_PROJECTIONS);
                        $instruction = "# ".$nom."\n<".$auth_srid."> ".$proj4." <>";
                        if ( strpos($contents, "<".$auth_srid.">")===false ){
                            $contents .= $instruction."\n";
                        } else {
                            $contents = preg_replace("/^.*<".$auth_srid.">.*$/", $instruction, $contents);
                        }
                        file_put_contents(self::SYSTEM_PROJECTIONS, $contents);
                        $response[] = "L'enregistrement de la projection dans le fichier système du serveur a réussi.";
            		} catch (\Exception $exception){
                        $response[] = "L'enregistrement de la projection dans le fichier système du serveur a échoué : ".$exception->getMessage();
            		}
                    
                    try {
                        /*
                         * Etape 4 : Ajout/update dans PRO_FRONTCARTO_PROJ_JSFILES
                         */   
                        
                        $string = "Proj4js.defs[\"EPSG:".$auth_srid."\"] = \"".$proj4."\";";
                        
                        $contents = file_get_contents($this->getProjExtendPath());

                        if ( strpos($contents, "EPSG:".$auth_srid)===false ){
                            $contents .= $string."\n";
                        } else {
                            $contents = preg_replace("/.+EPSG:".$auth_srid.".+/", $string, $contents);
                        }
                        file_put_contents($this->getProjExtendPath(), $contents);
                        
                        $response[] = "L'enregistrement de la projection dans les fichiers js a réussi.";
            		} catch (\Exception $exception){
                        $response[] = "L'enregistrement de la projection dans les fichiers js a échoué : ".$exception->getMessage();
                    }
                    
                    try {
                        /*
                         * Etape 5 : Ajout/update dans lex_dns (admincarto)
                         */
                        $fields = array(
                            "projection_provider" => strtoupper($auth_name),
                            "projection_name"    => $nom,
                            "projection_epsg" => $epsg,
                        );
                        $query = $conn->createQueryBuilder();
                        if($conn->executeQuery("SELECT 1 FROM carmen.lex_projection WHERE projection_epsg=? and projection_provider=?", array($epsg, strtoupper($auth_name)))->rowCount() == 0) {
                            $query->insert('carmen.lex_projection');
                            $func = "setValue";
                        } else {
                              $query->update('carmen.lex_projection')->where("projection_epsg=:projection_epsg");
                            $func = "set"; 
                        }
                        foreach(array_keys($fields) as $name){
                            $query->$func($name, ":".$name);
                        }
                        $query->setParameters($fields);
                        $query->execute();
            		    $response[] = "L'enregistrement de la projection dans la base de données du module admincarto a réussi.";
            		} catch (\Exception $exception){
            			throw new \Exception("L'enregistrement de la projection dans la base de données du module admincarto a échoué : ".$exception->getMessage(), 500, $exception);
            		}
                    
            	} catch (\Exception $exception){
            		$success = false;
            		$response[] = $exception->getMessage();
            	}
            } else {
                // le mode pré-remplir n'a pas permi de renseigner l'ensemble des champs requis : il faut donc les rajouter à la main
                $success = false; 
                $response = "Une erreur à été détectée lors de la récupération des champs [ESPG, Nom, proj4, srid, auth_name, auth_srid, srtext], merci de renseigner manuellement ces différentes valeurs"; 
            }
            return new JsonResponse(array("success"=>$success, "message"=>$response));
        }
        else if($mode == "update_epsg") {
            
            $response = "";
            $file = PRO_MAPFILE_PATH."Projections_Administration.xml";
            $epsg      = $request->get("epsg", "");
            $nom       = $request->get("nom", "");
            $proj4     = $request->get("proj4", "");
            $srid      = $request->get("srid", "");
            $auth_name = $request->get("auth_name", "");
            $auth_srid = $request->get("auth_srid", "");
            $srtext    = $request->get("srtext", "");
            $idRow     = $request->get("idRow", "");
            
            if($file != "" && $idRow != "" && $epsg != "" && $nom != "" && $proj4 != "" && $srid != "" && $auth_name != "" && $auth_srid != "" & $srtext != "") {
                ServiceFunction::updateProjectionFromForm($file, $idRow, $epsg, $nom, $proj4, $srid, $auth_name, $auth_srid, $srtext);
            }
        }
        return new Response($response ?: "FIN");
    }
     /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/admin/get_infos_form", name="admin_get_infos_form", options={"expose"=true}, methods={"GET","POST"} )
     */
    public function getInfosFormAction(Request $request) {
        $idRow = $request->get('idRow', -1);
        if($idRow != -1) {
            $infos_form = ServiceFunction::load_formulaire($idRow);
            return new Response(json_encode($infos_form) ?: "FIN");   
        }
        return new Response("FIN");   
    }
}