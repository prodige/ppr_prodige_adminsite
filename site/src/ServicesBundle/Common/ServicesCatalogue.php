<?php
namespace ProdigeAdminSite\ServicesBundle\Common;



class ServicesCatalogue
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected static $conn;

    /**
     * @param \Doctrine\DBAL\Connection $conn
     */
    public static function setConnection(\Doctrine\DBAL\Connection $conn)
    {
        self::$conn = $conn;
    }

    /** Ne sera pas utilisé pour l'instant
     * Pas besoin de décoder puis d'encoder de nouveau
     * Code pris de l'ancien version du prodige pour les service deleteTable.php et getTableList.php
     * Décode un token
     * @param string $token
     * @return array {success, msg, params}
     */
    /*public static function readToken($token) {
        $tabRes = array(
            "success" => true,
            "msg" => null,
            "params" => null,
        );
        if ($token==null) {
            $tabRes["success"] = false;
            $tabRes["msg"] = "service indisponible";
            return $tabRes;
        }

        //$strParams = getDecodeParam($token);
        $strParams = Util::getDecodeParam($token);

        $tabGet = array();
        $tabParams = explode("&", $strParams);
        foreach($tabParams as $strParam) {
            $tabParam = explode("=", $strParam,2);
            if( count($tabParam)==2 ) {
                $tabGet[$tabParam[0]] = $tabParam[1];
            }
        }

        if (!isset($tabGet["tableName"])) {
            $tabRes["success"] = false;
            $tabRes["msg"] = "service indisponible";
            return $tabRes;
        }

        $tabRes["params"] = $tabGet;
        return $tabRes;
    }*/


    /**
     * Supprime un ensemble de tables ou de vues PG
     * @param array $data
     */
    public static function deleteTable(array $tableNames) {
        $success = true;
        $schemaname = "public";
        self::$conn->beginTransaction();
        //$strSql = "";
        //foreach ($data as $tableName){
        foreach($tableNames as $tableName) {
            try {
              //$query = "select viewname from pg_views where schemaname='public' and  viewname='".$tableName."';";
              $query = self::$conn->createQueryBuilder();
              $query->select("1")
                    ->from("pg_views")
                    ->where("schemaname='".$schemaname."' and  viewname=:viewname")
                    ->setParameter("viewname", $tableName);
              //$rs_tables=$dao->BuildResultSet($query);
              //if ($rs_tables->GetNbRows() >0){
              if($query->execute()->rowCount()) {
                  //$strSql .=" DROP VIEW \"".$tableName."\" CASCADE; delete from geometry_columns where f_table_name = '".$tableName."'; ";
                  $sql = "DROP VIEW  if exists ".$schemaname.".\"".$tableName."\" CASCADE";
              } else {
                  //$strSql .=" DROP TABLE \"".$tableName."\" CASCADE; delete from geometry_columns where f_table_name = '".$tableName."'; ";
                  $sql = "DROP TABLE if exists ".$schemaname.".\"".$tableName."\" CASCADE";
              }
              //$dao->Execute($strSql);
              $query = self::$conn->executeQuery($sql);
              $query = self::$conn->executeQuery("delete from public.geometry_columns where f_table_name = :f_table_name", array("f_table_name" => $tableName));
            } catch(\Exception $exception){
                self::$conn->rollBack();
                $success = false;
                throw $exception;
                break;
            }
        }
        if(self::$conn->isTransactionActive()) {
            self::$conn->commit();
        }
        return $success;
    }

    public static function getTableList($bview=null) {

        //$AdminPath = "../Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        //require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");

        //$dao = new DAO();
        //$callback =(isset($_GET["callback"])?$_GET["callback"]:NULL);
        //$bview = (isset($_GET["bView"]) ? $_GET["bView"] : false);

        $data = array();
        if(!$bview){
            //$tableSkippedClause = " and not (tablename in ('spatial_ref_sys','geometry_columns')) ";
            $tableSkippedClause = " not (tablename in ('spatial_ref_sys','geometry_columns')) ";
        }
        else {
            //$tableSkippedClause = " and not (viewname in ('geography_columns')) ";
            $tableSkippedClause = " not (viewname in ('geography_columns')) ";
        }
        //Récupération des données d'une vue en json;
        /*$query = "select ".($bview ? "viewname" : "tablename")." from ".($bview ? "pg_views" : "pg_tables").
        " where schemaname='public' ".$tableSkippedClause."order by ".($bview ? "viewname" : "tablename").";";*/
        $query = self::$conn->createQueryBuilder();
        $query->select(($bview ? "viewname" : "tablename"))
              ->from(($bview ? "pg_views" : "pg_tables"))
              ->where("schemaname='public'")
              ->andWhere($tableSkippedClause)
              ->orderBy(($bview ? "viewname" : "tablename"));

        //$rs_tables=$dao->BuildResultSet($query);
        $rs_tables = $query->execute()->fetchAll();
        $data = array();
        //$strData ="[";
        //for ($rs_tables->First(); !$rs_tables->EOF(); $rs_tables->Next())
        foreach($rs_tables as $row)
        {
            //$strData .= "{'name':'".$rs_tables->read(0)."'},";
            $data[] = $row[($bview ? "viewname" : "tablename")];
        }
        //$strData = substr($strData, 0, -1). "]";
        //if(isset($callback))
            //echo $callback."(".$strData.")";
         //else 
            //echo $strData;
        return $data;
    }

}