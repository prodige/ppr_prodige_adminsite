<?php
namespace ProdigeAdminSite\ServicesBundle\Common;

class Util
{
    /**
     *  Encode puis retourne une chaine de caractères
     *        représentant un paramètre dans une URL http
     *
     * @param strParam  Chaine de caractères à encoder
     * @return string
     */
    public static function getEncodeParam($strParam, $bCheckSum=true) {
        $strEncode = "";
        for($i=0; $i<strlen($strParam); $i++) {
            $strEncode .= dechex(ord(substr($strParam, $i, 1)));
        }
        return $strEncode;
    }

    /**
     *  Récupère le paramètre http selon la méthode REQUEST
     *        Décode le param encodée par getEncodeParam()
     *        Vérifie le format en fonction de $strDefaultValue et $strFunctionTestType
     *        Puis retourne la valeur du paramètre
     *
     * @param strParamName         Nom du paramètre
     * @param strDefaultValue      Valeur par défaut, chaine vide par défaut
     * @param strFunctionTestType  Nom de la fonction de test, optionnel
     * @return string
     */
    public static function getDecodeParam($strParamValue,  $strDefaultValue="", $strFunctionTestType="") {

        //décodage
        $strDecode = self::decodeValue($strParamValue);

        // vérifie le format du param décodé
        $bTest = true;
        if( $strFunctionTestType != "" ) {
            eval("\$bTest = $strFunctionTestType(\$strDecode);");
        }
        $strDecode = (($bTest && $strDecode!="") ? $strDecode : $strDefaultValue);

        return $strDecode;
    }

    /**
     * Décode la valeur passée en paramètre puis retourne le résultat
     * @param strValue valeur à décoder
     * @return string
     */
    public static function decodeValue($strValue) {
        //décodage
        $strDecode = "";
        for($i=0; $i<strlen($strValue)-1; $i+=2 ) {
            $strDecode .= chr(hexdec(substr($strValue,  $i, 1).substr($strValue, $i+1, 1)));
        }
        return $strDecode;
    }

    // Non utilisable pour l'instant
    public static function buildErrorPage($msg, $urlBack=null) {
        $htmlTemplate =
        '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
            <HTML>
                <HEAD>
                    <!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
                    <META HTTP-EQUIV="X-UA-Compatible" content="IE=9"/>
                    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
                    <TITLE>Erreur</TITLE>
                    <META NAME="GENERATOR" CONTENT="OpenOffice.org 3.2  (Unix)">
                    <META NAME="CREATED" CONTENT="0;0">
                    <META NAME="CHANGED" CONTENT="20101029;15051300">
                    <META NAME="" CONTENT="">
                    <!-- <LINK REL="stylesheet" TYPE="text/css" HREF="../../HTML_PRIVATE/JavaScript/ext-3.0.0/resources/css/ext-all.css" /> -->
                    <link rel="stylesheet" type="text/css" href="../../../../web 3/lib/ext3.0/resources/css/ext-all.css">
                    <script  src="../../../../web 3/lib/ext3.0/adapter/ext/ext-base.js"></script>
                    <script  src="../../../../web 3/lib/ext3.0/ext-3.0.0/ext-all.js"></script>
                    <script  src="../../../../web 3/lib/ext3.0/ext-3.0.0/extjs-overload-crossdomain.js"></script>
                    <SCRIPT>
                        function init() {
                            var msg = "%MSG%";
                            if (msg=="")
                                msg = "Problème avec l\'application d\'administration cartographique";

                                var urlBack = "%URLBACK%";

                                var returnFn = urlBack=="" ? Ext.emptyFn : function() { window.location.href = this.urlBack; };
                                Ext.Msg.alert("Erreur", msg, returnFn, {urlBack : urlBack});
                        }
                        Ext.onReady(init);
                    </SCRIPT>
                </HEAD>
                <BODY LANG="fr-FR" DIR="LTR"></BODY>
                </BODY>
            </HTML>';

        //headers_sent(&$file, &$line);

        $html = $htmlTemplate;
        $msg= isset($msg) && $msg!=null ? $msg : "";
        $html = str_replace("%MSG%", $msg, $html);

        $urlBack = isset($urlBack) && $urlBack!=null ? $urlBack : "";
        $html = str_replace("%URLBACK%", $urlBack, $html);

        //header('Content-Type: text/html');
        //echo $html;
        return $html;
        //var_dump($file, $line);
    }

    // La fonction http_params de récupération de variables postées ne seront pas implémenté comme symfony contient la meme fonctionnalité

    public static function handle_error_ajax_ext($errno, $errmsg) {
        if (error_reporting()>0) {
            /*if (strstr($errmsg, 'CARMEN_ERROR')!=FALSE) {
             $info = split('\|', $errmsg);
             $errno = intval($info[0]);
             $errmsg = $info[1];
             }*/
            @$result = array(
                'success' => false,
                'errmsg' => $errmsg,
                'failureType' => $errno
            );

            @header("Content-type:text/html");
            @header("HTTP/1.1 404 Not Found");
            echo json_encode($result);
            die();
        }
    }
}