<?php

/*
 * Created on 1 juil. 09
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
//hismail
/*
 * Modified on 18 march. 2016
 * Version Prodige V4
 * */

namespace ProdigeAdminSite\ServicesBundle\Common;

class ServiceFunction
{
    
    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected static $conn;

    /**
     * @param \Doctrine\DBAL\Connection $conn
     */
    public static function setConnection(\Doctrine\DBAL\Connection $conn)
    {
        self::$conn = $conn;
    }
    
    public static function fileToObject($fileName, $serviceUrl) {

        // création du nouvel objet document
        $dom = new \DomDocument();

        // chargement à partir du fichier ou d'une URL
        if($serviceUrl){
            $strFileContent =  file_get_contents($serviceUrl."?file=".$fileName);
            $dom->loadXML($strFileContent);
        } else
            $dom->load($fileName);

        // validation à partir de la DTD référencée dans le document.
        // En cas d'erreur, on ne va pas plus loin

        // création de l'objet résultat
        $object = new \stdClass();

        // on référence l'adresse du fichier source
        $object->source = $fileName;

        // on récupère l'élément racine, on le met dans un membre
        // de l'objet nommé "root"

        $root = $dom->documentElement;
        $object->root = new \stdClass();

        // appel d'une fonction récursive qui traduit l'élément XML
        // et passe la main à ses enfants, en parcourant tout l'arbre XML.
        self::getElement($root, $object->root);
        return $object;
    }

    public static function getElement($dom_element, &$object_element) {
        // récupération du nom de l'élément
        $object_element->name = $dom_element->nodeName;

        // récupération de la valeur CDATA,
        // en supprimant les espaces de formatage.
        $object_element->textValue = @trim($dom_element->firstChild->nodeValue);

        // Récupération des attributs
        if($dom_element->hasAttributes()) {
            $object_element->attributes = array();
            foreach($dom_element->attributes as $attName=>$dom_attribute) {
            $object_element->attributes[$attName] = $dom_attribute->value;
            }
        }

        // Récupération des éléments fils, et parcours de l'arbre XML
        // on veut length >1 parce que le premier fils est toujours
        // le noeud texte
        if ($dom_element->childNodes->length > 0) {
            $object_element->children = array();
            foreach($dom_element->childNodes as $dom_child) {
                if($dom_child->nodeType == XML_ELEMENT_NODE) {
                    $child_object = new \stdClass();
                    self::getElement($dom_child, $child_object);
                    array_push($object_element->children, $child_object);
                }
            }
        }
    }

    public static function ObjectToFile($xmlObject, $serviceUrl) {
        // Création d'un nouvel objet document
        $dom = new \DomDocument();

        // Création de l'élément racine
        $root = $dom->createElement($xmlObject->root->name);
        $dom->appendChild($root);

        // appel d'une fonction récursive qui construit l'élément XML
        // à partir de l'objet, en parcourant tout l'arbre de l'objet.
        self::setElement($dom, $xmlObject->root, $root);
        if($serviceUrl){
          $strXml = $dom->saveXML();
          $postdata = http_build_query(
            array(
              "xml"  => $strXml,
              "file" => $xmlObject->source,
              "bDebug" => "1"
            )
          );
          $opts = array('http' =>
            array(
              'method'  => 'POST',
              'header'  => 'Content-type: application/x-www-form-urlencoded',
              'content' => $postdata,
              'timeout' => 5
            )
          );
          $context = stream_context_create($opts);
          //echo file_get_contents($serviceUrl, 0, $context);
          //return file_get_contents($serviceUrl, 0, $context);
          //echo file_get_contents($serviceUrl."?file=".$xmlObject->source."&xml=".urlencode($strXml));
        }else{
          // Mise à jour du fichier source original
          $dom->save($xmlObject->source);
        }
    }


    public static function setElement($dom_document, $object_element, $dom_element) {

        // récupération de la valeur CDATA de l'élément
        if (isset($object_element->textValue)) {
            $cdata = $dom_document->createTextNode($object_element->textValue);
            $dom_element->appendChild($cdata);
        }

        // récupération des attributs
        if(isset($object_element->attributes)) {
            foreach($object_element->attributes as $attName=>$attValue) {
                $dom_element->setAttribute($attName, $attValue);
            }
        }
        // construction des éléments fils, et parcours de l'arbre
        if(isset($object_element->children)) {
            foreach($object_element->children as $childObject) {
                $child = $dom_document->createElement($childObject->name);
                self::setElement($dom_document, $childObject, $child);
                $dom_element->appendChild($child);
            }
        }
    }

    public static function addServeur($file, $serviceUrl) {
        $dom = new \DomDocument;
        //$objServices = fileToObject($file, $serviceUrl);
        $objServices = self::fileToObject($file, $serviceUrl);
        //création d'un nouvel objet
        $object = new \stdClass();
        $object->name = "SERVEUR";
        $object->textValue ="";
        $object->attributes["NOM"] = "";
        $object->attributes["URL"] = "";
        $objServices->root->children[count($objServices->root->children)] = $object;
        //ObjectToFile($objServices, $serviceUrl);
        self::ObjectToFile($objServices, $serviceUrl);
    }

    public static function updateServeur($file, $field, $value, $rowNumber, $serviceUrl) {
        $dom = new \DomDocument;
        //$objServices = fileToObject($file, $serviceUrl);
        $objServices = self::fileToObject($file, $serviceUrl);
        //$dom->load($file);
        if(substr_count($file,"Projections_Administration")>0)
            $objServices->root->children[$rowNumber]->name = "PROJECTION";
        else
            $objServices->root->children[$rowNumber]->name = "SERVEUR";
        $objServices->root->children[$rowNumber]->textValue ="";
        $objServices->root->children[$rowNumber]->attributes[$field] = $value;
        //ObjectToFile($objServices, $serviceUrl);
        self::ObjectToFile($objServices, $serviceUrl);
    }

    public static function delServeur($file, $nom, $url, $serviceUrl, $isUrl) {

        $dom = new \DomDocument;
        //$objServices = fileToObject($file, $serviceUrl);
        $objServices = self::fileToObject($file, $serviceUrl);

        for($i=0;$i<count($objServices->root->children);$i++){
            if($objServices->root->children[$i]->attributes["NOM"] == $nom && $objServices->root->children[$i]->attributes["URL"] == stripslashes($url))
                unset($objServices->root->children[$i]);
        }
        //ObjectToFile($objServices, $serviceUrl);
        self::ObjectToFile($objServices, $serviceUrl);
    }

    public static function reorder_xml_server_file($file, $nbrows, $noms, $urls, $serviceUrl) {
        $dom = new \DomDocument;
        //$objServices = fileToObject($file, $serviceUrl);
        $objServices = self::fileToObject($file, $serviceUrl);

        for($i = 0; $i < $nbrows; ++$i) {
            $object = new \stdClass();
            $object->name = "SERVEUR";
            $object->textValue = "";
            $object->attributes["NOM"] = $noms[$i];
            $object->attributes["URL"] = $urls[$i];
            $objServices->root->children[count($objServices->root->children)] = $object;
        }
        $foo = array_splice($objServices->root->children, 0, $nbrows);
        //ObjectToFile($objServices, $serviceUrl);
        self::ObjectToFile($objServices, $serviceUrl);
    }

    /** Fonctions concernant le parametrage des projections **/
    public static function load_formulaire($id_row) {
        //$_file = CARMEN_URL_PATH_DATA."cartes/Publication/Projections_Administration.xml";
        $_file = PRO_MAPFILE_PATH."Projections_Administration.xml";
        //$_serviceProj= PRODIGE_CARTO_URL."/PRRA/getFile.php";
        //$_serviceProj= self::getParameter("adminsite")["PRODIGE_CARTO_URL"]."/PRRA/getFile.php";

        $dom = new \DomDocument;
        //$objServices = fileToObject($_file, $_serviceProj);
        $objServices = self::fileToObject($_file, null);
        $output = $objServices->root->children[$id_row];
        //ObjectToFile($objServices, $_serviceProj);
        self::ObjectToFile($objServices, null);
        return $output;
    }

    //TODO Non utilisable pour l'instant
    /*public static function addProjectionFromForm($file, $serviceProj, $epsg, $nom, $proj4, $srid, $auth_name, $auth_srid, $srtext) {
        $dom = new \DomDocument;
        //$objServices = fileToObject($file, $serviceProj);
        $objServices = self::fileToObject($file, $serviceProj);
        //création d'un nouvel objet
        $object = new \stdClass();
        $object->name = "PROJECTION";
        $object->textValue = "";
        $object->attributes["NOM"] = $nom;
        $object->attributes["EPSG"] = $epsg;
        if(!(isset($objServices->root->children)))
            $objServices->root->children = "";
        $objServices->root->children[count($objServices->root->children)] = $object;
        //ObjectToFile($objServices, false);
        self::ObjectToFile($objServices, false);

        if($epsg != "") {
            //Ajout en base
            //$dbConn = pg_connect("host=".ALK_POSTGRES_HOST." port=".ALK_POSTGRES_PORT." dbname=".ALK_POSTGRES_BD." user=".ALK_POSTGRES_LOGIN." password=".ALK_POSTGRES_PWD);
            $dbConn = self::getConnection('prodige'); //TODO extend from ?
            //$strSql = "SELECT count(*) as inbase FROM spatial_ref_sys WHERE srid=".$epsg.";";
            $strSql = "SELECT count(*) as inbase FROM spatial_ref_sys WHERE srid=?;";
            //$ds = pg_query($dbConn, $strSql);
            $ds = $strSql->execute()->fetchAll(array($epsg));

            //while($dr = pg_fetch_row($ds)){
            foreach($ds as $row) {
                //$inbase = $dr[0];
                $inbase = $row["inbase"];
                if($inbase == 1)
                    //echo "Projection ajoutée à la liste des projections de la plateforme.";
                    return "Projection ajoutée à la liste des projections de la plateforme.";
                else {
                    //$strSql = "INSERT INTO spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) VALUES (".$epsg.",'".$auth_name."',".$auth_srid.",'".$srtext."','".$proj4."');";
                    $strSql = "INSERT INTO spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) VALUES (?, ?, ?, ?, ?);";
                    //$result_insert = pg_prepare($dbConn, "strsql", $strSql);
                    //$result_insert = pg_execute($dbConn, "strsql", array());
                    $result_insert = $strSql->execute(array($epsg, $auth_name, $auth_srid, $srtext, $proj4));
                    if($result_insert)
                        //echo "Projection ajoutée au système.<br>Projection ajoutée à la liste des projections de la plateforme.";
                        return "Projection ajoutée au système.<br>Projection ajoutée à la liste des projections de la plateforme.";
                    else
                        //echo "La projection n'a pu etre ajoutée.";
                        return "La projection n'a pu etre ajoutée.";
                }
            }
        }
    }*/
    public static function updateProjectionFromForm($file, $idRow, $epsg, $nom, $proj4, $srid, $auth_name, $auth_srid, $srtext) {
        $dom = new \DomDocument;
        //$objServices = fileToObject($file, $serviceProj);
        $objServices = self::fileToObject($file, null);
        $objServices->root->children[$idRow]->name = "PROJECTION";
        $objServices->root->children[$idRow]->textValue = "";
        $objServices->root->children[$idRow]->attributes["NOM"] = $nom;
        $objServices->root->children[$idRow]->attributes["EPSG"] = $epsg;
        $objServices->root->children[$idRow]->attributes["PROJ4"] = $proj4;
        $objServices->root->children[$idRow]->attributes["SRID"] = $srid;
        $objServices->root->children[$idRow]->attributes["AUTH_NAME"] = $auth_name;
        $objServices->root->children[$idRow]->attributes["AUTH_SRID"] = $auth_srid;
        $objServices->root->children[$idRow]->attributes["SRTEXT"] = $srtext;
        //ObjectToFile($objServices, $serviceProj);
        self::ObjectToFile($objServices, null);

        //Modification en base
        //$dbConn = pg_connect("host=".ALK_POSTGRES_HOST." port=".ALK_POSTGRES_PORT." dbname=".ALK_POSTGRES_BD." user=".ALK_POSTGRES_LOGIN." password=".ALK_POSTGRES_PWD);
        //$strSql = "SELECT count(*) as inbase FROM spatial_ref_sys WHERE srid=".$epsg.";";
         $strSql = "SELECT count(*) as inbase FROM spatial_ref_sys WHERE srid=?;";
        //$ds = pg_query($dbConn, $strSql);
        $ds = self::$conn->fetchAll($strSql, array($epsg));
        //while($dr = pg_fetch_row($ds)){
        foreach($ds as $row) {
            //$inbase = $dr[0];
            $inbase = $row["inbase"];
            if($inbase == 0)
                //echo "La modification n'a pu aboutir.";
                return "La modification n'a pu aboutir.";
            else {
                //$strSql = "UPDATE spatial_ref_sys SET srid=".$epsg.", auth_name='".$auth_name."', auth_srid=".$auth_srid.", srtext='".$srtext."', proj4text='".$proj4."' where srid=".$epsg.";";
                $strSql = self::$conn->createQueryBuilder();
                $strSql->update("spatial_ref_sys")
                ->set("srid", ":srid")
                ->set("auth_name", ":auth_name")
                ->set("auth_srid", ":auth_srid")
                ->set("srtext", ":srtext")
                ->set("proj4text", ":proj4text")
                ->where("srid= :srid")
                ->setParameters(array("srid" => $epsg, "auth_name" => $auth_name, "auth_srid" => $auth_srid, "srtext" => $srtext, "proj4text" => $proj4, "srid" => $epsg));

                //$result_insert = pg_prepare($dbConn, "strsql", $strSql);
                //$result_insert = pg_execute($dbConn, "strsql", array());
                $result_insert = $strSql->execute();
                if($result_insert) {
                    //echo "La projection a ete modifiee.";
                    return "La projection a ete modifiee.";
                }
                else {
                    //echo "La projection n'a pu etre modifiee.";
                    return "La projection n'a pu etre modifiee.";
                }
            }
        }
    }

    public static function delProjection($file, $nom, $proj, $serviceProj) {
        $dom = new \DomDocument;
        //$objServices = fileToObject($file, $serviceProj);
        $objServices = self::fileToObject($file, $serviceProj);
        for($i=0;$i<count($objServices->root->children);$i++){
            if($objServices->root->children[$i]->attributes["NOM"] == $nom && $objServices->root->children[$i]->attributes["EPSG"] == stripslashes($proj))
            unset($objServices->root->children[$i]);
        }
        //ObjectToFile($objServices, false);
        self::ObjectToFile($objServices, false);
    }

    public static function reorder_xml_proj_file($file, $nbrows, $noms, $projs, $serviceUrl) {
        $dom = new \DomDocument;
        //$objServices = fileToObject($file, $serviceUrl);
        $objServices = self::fileToObject($file, $serviceUrl);

        for($i = 0; $i < $nbrows; ++$i) {
            $object = new \stdClass();
            $object->name = "PROJECTION";
            $object->textValue = "";
            $object->attributes["NOM"] = $noms[$i];
            $object->attributes["EPSG"] = $projs[$i];
            $objServices->root->children[count($objServices->root->children)] = $object;
        }
        $foo = array_splice($objServices->root->children, 0, $nbrows);
        //ObjectToFile($objServices, $serviceUrl);
        self::ObjectToFile($objServices, $serviceUrl);
    }
}