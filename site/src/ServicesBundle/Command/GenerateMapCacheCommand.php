<?php

namespace ProdigeAdminSite\ServicesBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ProdigeAdminSite\ServicesBundle\Controller\WMTSController;

class GenerateMapCacheCommand extends Command
{
    const ACTION_GENERATE = "generate";
    const ACTION_CHANGE_STATUS = "status";
    protected static $ACTIONS = array(self::ACTION_GENERATE, self::ACTION_CHANGE_STATUS);


    /**
     * Configures the command definition
     */
    protected function configure()
    {
        //TODO create user

        $this
            ->setName('adminsite:generate_mapcache')
            ->setDescription('Launch the mapcache seed generator')
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $conn = $this->getConnection();
        $em   = $this->getManager();

        $controller = new WMTSController();
        $controller->generateWMTSTask();
    }




    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager(EntityManager $manager)
    {
        return $manager;
    }

    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return Connection
     */
    protected function getConnection(Connection $connection)
    {
        return $connection;
    }







}