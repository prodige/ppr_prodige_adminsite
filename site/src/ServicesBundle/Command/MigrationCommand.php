<?php

namespace ProdigeAdminSite\ServicesBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Query;
use ProdigeAdminSite\ServicesBundle\Controller\WMTSController;

class MigrationCommand extends ContainerAwareCommand
{
    
    /**
     * Configures the command definition
     */
    protected function configure()
    {
        //TODO create user
        
        $this
            ->setName('adminsite:migrate_mapcache')
            ->setDescription('update mapcache configuration')
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $dialog = ($input->isInteractive() ? $this->getHelper('dialog') : null);
        $conn = $this->getConnection();
        $em   = $this->getManager();
        
        $controller = new WMTSController();
        $controller->migrateAction();
        
        $output->writeln("done");
    }
    
    
    
    
    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
    
    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection()
    {
        return $this->getContainer()->get('doctrine')->getConnection();
    }
    
    

    
    
    

}