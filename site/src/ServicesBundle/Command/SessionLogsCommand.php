<?php

namespace ProdigeAdminSite\ServicesBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Query;
use ProdigeAdminSite\ServicesBundle\Controller\WMTSController;

class SessionLogsCommand extends ContainerAwareCommand
{
    
    /**
     * Configures the command definition
     */
    protected function configure()
    {
        //TODO create user
        
        $this
            ->setName('adminsite:populate_session_logs')
            ->setDescription('Populate the session logs file')
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $CATALOGUE = $this->getConnection('catalogue');
        $CATALOGUE->executeQuery("set search_path to catalogue");
        $stmt = $CATALOGUE->executeQuery("select * from catalogue.prodige_settings");
        foreach ($stmt as $row){
            $settings[$row["prodige_settings_constant"]] = $row["prodige_settings_value"];
        }

        $sessionNumber = null;
        //if(isset($settings["PRO_CATALOGUE_NB_SESSION_USER"]) && $settings["PRO_CATALOGUE_NB_SESSION_USER"] == "on") {
        
            $this->getContainer()->get('prodige.logger');
            $logger = \Prodige\ProdigeBundle\Services\Logs::getLogger('Sessions');
        
            $query = "delete from catalogue.PRODIGE_SESSION_USER where date_connexion < (now()- interval '30 minute');";
            $CATALOGUE->executeQuery($query);
            
            $query = "select  count(date_connexion) from catalogue.PRODIGE_SESSION_USER where date_connexion >= (now()- interval '30 minute');";
            $sessionNumber = $CATALOGUE->executeQuery($query)->fetchColumn(0);
            
            $logger->info('Nombre de sessions actives : '.$sessionNumber);
            $output->writeln('Nombre de sessions actives : '.$sessionNumber);
       // }
        
    }
    
    
    
    
    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }
    
    /**
     * Returns the default DBAL PDO Connection.
     *
     * @return \Doctrine\DBAL\Driver\PDOConnection
     */
    protected function getConnection($connection_name)
    {
        return $this->getContainer()->get('doctrine')->getConnection($connection_name);
    }
    
    

    
    
    

}