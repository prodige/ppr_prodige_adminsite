<?php

namespace ProdigeAdminSite\WebBundle\EventListener;


use Alk\Common\CasBundle\Event\AuthenticateUserEvent;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Listen to CAS authentication
 */
class AuthenticateUserListener {

    use ContainerAwareTrait;
    
    /**
     * This function is called after a user is authenticated through the cas. 
     * It's up to you to load a custom user entity or throw an exception by using $event->setUser(...)
     * 
     * @param AuthenticateUserEvent $event
     */
    public function onAuthenticateUser(AuthenticateUserEvent $event)
    {
        $user = \Prodige\ProdigeBundle\Controller\User::GetUser();
        // if user is not a Prodige Admin -> raise exception
        if( ! $user->IsProdigeAdmin() ) {
            $event->setException( new \Exception(\Prodige\ProdigeBundle\Common\SecurityExceptions::MSG_DEFAULT_ACCESS_DENIED) );
        }
    }

}
