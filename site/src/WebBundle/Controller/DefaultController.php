<?php

namespace ProdigeAdminSite\WebBundle\Controller;

use Prodige\ProdigeBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/", name="adminsite_web_index", options={"expose"=true})
     */
    public function indexAction() : Response
    {
        $parameters = $this->container->getParameter('adminsite');
        $response = new Response();
        $response->headers->set('Content-Type', 'text/html');
        $response->setContent($this->renderView('webbundle/index.html.twig', array('parameters'=>$parameters)));
        return $response;
    }
}
